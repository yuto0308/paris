<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
$(function(){
	setInterval(function(){
			location.reload();
	},300000);//５分ごとにリロード
});
</script>
<div id="pagebodymain">
<h1>POSレジ業務画面トップページ</h1></p>
<?php 
include("../../etc_html/db_info.php");
$sql_info=' SELECT * FROM info WHERE check_del=0 ORDER BY createdate DESC';
$rs_info=mysqli_query($db_info,$sql_info);
$str_info="";
$i_info=1;
	while($info=mysqli_fetch_assoc($rs_info)){
		if($i_info==1){
			$str_info.=nl2br(htmlspecialchars($info['data']));
		}else{
			$str_info.="</p>";
			$str_info.=nl2br(htmlspecialchars($info['data']));
		}
	$i_info++;
	}
	echo $str_info;
?>


<p>【ご注意】ブラウザのポップアップブロックを解除してください。</br>
<a href="http://ebid-portal.kumamoto-idc.pref.kumamoto.jp/popblock.html" target="_blank">その１</a>
<a href="http://121ware.com/qasearch/1007/app/servlet/relatedqa?QID=014194" target="_blank">その２(Windows8/IE10)</a> </p>

<p>【ご注意】ログイン後、左メニュー「基本情報の編集」からパスワードを変更してください。</br>
変更後、パスワードは忘れないようお願いいたします。</br>
ＰＯＳサポートでパスワードをお教えすることはできません。</p>
<table id="index_table1">
	<tr>
		<th colspan="5" class="b">レジ金の時点集計（５分ごとに更新されます）</th>
	</tr>
	<tr>
		<th class="a">現金</th>
		<th class="a">クレジット</th>
		<th class="a">使用チケット</th>
		<th class="a">割引</th>
		<th class="a">レジ移動金</th>
	</tr>
	<tr>
	<?php
	include("db_connect.php");
	$sql_reg="SELECT SUM(count) as count_sum,SUM(sum) as sum_sum,SUM(cash) as cash_sum,SUM(card) as card_sum,SUM(ticket) as ticket_sum, SUM(discount) as discount_sum FROM shop_log_sub WHERE date >= '".date("Y-m-d")."'"." and date <= '".date("Y-m-d",strtotime("+1 day"))."' and shop_id=$shop_id";
	$rs1 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
	$data = mysqli_fetch_assoc($rs1);
	if($data['cash_sum']==NULL){
		$cash_sum_str="0";
		$card_sum_str="0";
		$ticket_sum_str="0";
		$discount_sum_str="0";
		$count_sum_str="0";
		$sum_sum_str="0";
		$sum_str="0";
	}else{
		$cash_sum_str=$data['cash_sum'];
		$card_sum_str=$data['card_sum'];
		$ticket_sum_str=number_format($data['ticket_sum']);
		$discount_sum_str=number_format($data['discount_sum']);
		$count_sum_str=number_format($data['count_sum']);
		$sum_sum_str=number_format($data['sum_sum']);
		$sum_str=$data['cash_sum']+$data['card_sum'];
		$cash_sum_str=number_format($cash_sum_str);
		$card_sum_str=number_format($card_sum_str);
		$sum_str=number_format($sum_str);
		}
	$sql_reg="SELECT SUM(price) as price_sum FROM pos_move WHERE datetime >= '".date("Y-m-d")."'"." and datetime <= '".date("Y-m-d",strtotime("+1 day"))."' and shop_id=$shop_id";
	$rs2 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
	$data2 = mysqli_fetch_assoc($rs2);
	if($data2['price_sum']==NULL){
		$move_str="0";
	}else{
		$move_str=$data2['price_sum'];
	}
	echo
		'<th style="text-align:right;"><span id="cash">'.$cash_sum_str.'</span>円</th>
		<th style="text-align:right;"><span id="card">'.$card_sum_str.'</span>円</th>
		<th style="text-align:right;"><span id="ticket">'.$ticket_sum_str.'</span>枚</th>
		<th style="text-align:right;"><span id="discount">'.$discount_sum_str.'</span>円</th>
		<th style="text-align:right;"><span id="move">'.$move_str.'</span>円</th>';
	?>
	</tr>
</table>
<p>
	<table id="index_table2">
		<tr>
			<th class="b" colspan="8">売上の集計(5分ごとに更新されます)</th>
		</tr>
		<tr>
			<th class="a"></th>
			<th class="a">販売件数</th>
			<th class="a">販売商品数</th>
			<th class="a">販売金額</th>
			<th class="a">返品商品数</th>
			<th class="a">返品金額</th>
			<th class="a">売上金額</th>
		</tr>
		<tr>
		<?php
		
		$sql_reg="SELECT * FROM shop_log_sub WHERE date >= '".date("Y-m-d")."'"." and date <= '".date("Y-m-d",strtotime("+1 day"))."' and shop_id=$shop_id";
		$rs3=mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$row_count = mysqli_num_rows($rs3);
		
		echo
			'<th class="a" style="text-align:center;">本日</th>
			<th style="text-align:right;"><span id="guest_count">'.$row_count.'</span>件</th>
			<th style="text-align:right;"><span id="sales_count">'.$count_sum_str.'</span>点</th>
			<th style="text-align:right;"><span id="pre_sum">'.$sum_sum_str.'</span>円</th>
			<th style="text-align:right"><span id="back_num">0</span>点</th>
			<th style="text-align:right"><span id="back_price">0</span>円</th>
			<th style="text-align:right;"><span id="sum">'.$sum_str.'</span>円</th>';
			?>
		</tr>
		<?php
		$sql_reg="SELECT * FROM shop_log_sub WHERE date >= '".date("Y-m-d",strtotime("-1 day"))."'"." and date <= '".date("Y-m-d")."' and shop_id=$shop_id";
		$rs3=mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$row_count = mysqli_num_rows($rs3);
		$sql_reg="SELECT SUM(count) as count_sum,SUM(sum) as sum_sum,SUM(cash) as sum_cash,SUM(card) as sum_card FROM shop_log_sub WHERE date >= '".date("Y-m-d",strtotime("-1 day"))."'"." and date <= '".date("Y-m-d")."' and shop_id=$shop_id";
		$rs4 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$data3 = mysqli_fetch_assoc($rs4);
		if($data3['count_sum']==NULL){
			$count_sum_str="0";
			$sum_sum_str="0";
			$sum_str="0";
		}else{
			$count_sum_str=$data3['count_sum'];
			$sum_sum_str=$data3['sum_sum'];
			$sum_str=$data3['sum_cash']+$data3['sum_card'];
			$count_sum_str=number_format($count_sum_str);
			$sum_sum_str=number_format($sum_sum_str);
			$sum_str=number_format($sum_str);
		}
		echo '<tr>
			<th class="a" style="text-align:center;">昨日</th>
			<th style="text-align:right;"><span id="guest_count">'.$row_count.'</span>件</th>
			<th style="text-align:right;"><span id="guest_count">'.$count_sum_str.'</span>点</th>
			<th style="text-align:right;"><span id="pre_sum">'.$sum_sum_str.'</span>円</th>
			<th style="text-align:right"><span id="back_num">0</span>点</th>
			<th style="text-align:right"><span id="back_price">0</span>円</th>
			<th style="text-align:right;"><span id="guest_count">'.$sum_str.'</span>円</th>
		</tr>';
		
		$sql_reg="SELECT * FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01")."'"." and date <= '".date("Y-m-1 00:00:00",strtotime("+1 month"))."' and shop_id=$shop_id";
		$rs3=mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$row_count = mysqli_num_rows($rs3);
		$sql_reg="SELECT SUM(count) as count_sum,SUM(sum) as sum_sum, SUM(cash) as sum_cash,SUM(card) as sum_card FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01")."'"." and date <= '".date("Y-m-1 00:00:00",strtotime("+1 month"))."' and shop_id=$shop_id";
		$rs4 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$data3 = mysqli_fetch_assoc($rs4);
		if($data3['count_sum']==NULL){
			$count_sum_str="0";
			$sum_sum_str="0";
			$sum_str="0";
		}else{
			$count_sum_str=$data3['count_sum'];
			$sum_sum_str=$data3['sum_sum'];
			$sum_str=$data3['sum_cash']+$data3['sum_card'];
			$count_sum_str=number_format($count_sum_str);
			$sum_str=number_format($sum_str);
			$sum_sum_str=number_format($sum_sum_str);
		}

		echo '
		<tr>
			<th class="a" style="text-align:center;">今月</th>
			<th style="text-align:right;"><span id="guest_count">'.$row_count.'</span>件</th>
			<th style="text-align:right;"><span id="guest_count">'.$count_sum_str.'</span>点</th>
			<th style="text-align:right;"><span id="pre_sum">'.$sum_sum_str.'</span>円</th>
			<th style="text-align:right"><span id="back_num">0</span>点</th>
			<th style="text-align:right"><span id="back_price">0</span>円</th>
			<th style="text-align:right;"><span id="guest_count">'.$sum_str.'</span>円</th>
		</tr>
		';
		$sql_reg="SELECT * FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01",strtotime("-1 month"))."'"." and date <= '".date("Y-m-1 00:00:00")."' and shop_id=$shop_id";
		$rs3=mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$row_count = mysqli_num_rows($rs3);
		$sql_reg="SELECT SUM(count) as count_sum,SUM(sum) as sum_sum,SUM(cash) as sum_cash,SUM(card) as sum_card FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01",strtotime("-1 month"))."'"." and date <= '".date("Y-m-1 00:00:00")."' and shop_id=$shop_id";
		$rs4 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$data3 = mysqli_fetch_assoc($rs4);
		if($data3['count_sum']==NULL){
			$count_sum_str="0";
			$sum_sum_str="0";
			$sum_str="0";
		}else{
			$count_sum_str=$data3['count_sum'];
			$sum_sum_str=$data3['sum_sum'];
			$sum_str=$data3['sum_cash']+$data3['sum_card'];
			$count_sum_str=number_format($count_sum_str);
			$sum_sum_str=number_format($sum_sum_str);
			$sum_str=number_format($sum_str);
		}

		echo '
		<tr>
			<th class="a" style="text-align:center;">先月</th>
			<th style="text-align:right;"><span id="guest_count">'.$row_count.'</span>件</th>
			<th style="text-align:right;"><span id="guest_count">'.$count_sum_str.'</span>点</th>
			<th style="text-align:right;"><span id="pre_sum">'.$sum_sum_str.'</span>円</th>
			<th style="text-align:right"><span id="back_num">0</span>点</th>
			<th style="text-align:right"><span id="back_price">0</span>円</th>
			<th style="text-align:right;"><span id="guest_count">'.$sum_str.'</span>円</th>


		</tr>
		';
		$sql_reg="SELECT * FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01",strtotime("-2 month"))."'"." and date <= '".date("Y-m-1 00:00:00",strtotime("-1 month"))."' and shop_id=$shop_id";
		$rs3=mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$row_count = mysqli_num_rows($rs3);
		$sql_reg="SELECT SUM(count) as count_sum,SUM(sum) as sum_sum,SUM(cash) as sum_cash,SUM(card) as sum_card FROM shop_log_sub WHERE date >= '".date("Y-m-1 00:00:01",strtotime("-2 month"))."'"." and date <= '".date("Y-m-1 00:00:00",strtotime("-1 month"))."' and shop_id=$shop_id";
		$rs4 = mysqli_query($db,$sql_reg) or exit("クエリ失敗");
		$data3 = mysqli_fetch_assoc($rs4);
		if($data3['count_sum']==NULL){
			$count_sum_str="0";
			$sum_sum_str="0";
			$sum_str="0";
		}else{
			$count_sum_str=$data3['count_sum'];
			$sum_sum_str=$data3['sum_sum'];
			$sum_str=$data3['sum_cash']+$data3['sum_card'];
			$count_sum_str=number_format($count_sum_str);
			$sum_sum_str=number_format($sum_sum_str);
			$sum_str=number_format($sum_str);
		}

		echo '
		<tr>
			<th class="a" style="text-align:center;">先々月</th>
			<th style="text-align:right;"><span id="guest_count">'.$row_count.'</span>件</th>
			<th style="text-align:right;"><span id="guest_count">'.$count_sum_str.'</span>点</th>
			<th style="text-align:right;"><span id="pre_sum">'.$sum_sum_str.'</span>円</th>
			<th style="text-align:right"><span id="back_num">0</span>点</th>
			<th style="text-align:right"><span id="back_price">0</span>円</th>
			<th style="text-align:right;"><span id="guest_count">'.$sum_str.'</span>円</th>

		</tr>
		';
		?>
	</table>
	<p>
</div>
<?php include("footer.php"); ?>
