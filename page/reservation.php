<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php"); ?>
<script>
function timetable(){
		var year = $('#year_e').val();
		var month = $('#month_e').val();
		var day = $('#day_e').val();
		var shops=$('#shops').val();
		window.location.href = "./timetable.php?year="+year+"&month="+month+"&day="+day+"&shops="+shops;			
}
</script>
<script>
$(document).ready(function(){
	    var year = $('#year_e').val();
		var month = $('#month_e').val();
		var day = $('#day_e').val();
		var shops =$('#shops').val();
		$("tr[name='add']").remove();
		$("tr[name='add2']").remove();
	$.ajax({type:"POST",
 			url:"search_res.php",
 				data:{	year:year,
 						month:month,
 						day:day,
 						shops:shops
 						},
 						dataType:"json",
				success:function(data){
						// $('#table').append(data);
						$('#table').append(data[0]);
						$('#table').append(data[1]);
						$('#num_rsv').text(data[2]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown);
						}
						});
});
</script>
<script>
	function search(){
		var year = $('#year_e').val();
		var month = $('#month_e').val();
		var day = $('#day_e').val();
		var shops =$('#shops').val();
	
$("tr[name='add']").remove();
		$("tr[name='add2']").remove();
	$.ajax({type:"POST",
 			url:"search_res.php",
 				data:{	year:year,
 						month:month,
 						day:day,
 						shops:shops
 						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#table').append(data[1]);
						$('#num_rsv').text(data[2]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						$('#table').append('error : ' + errorThrown);
						}
						});
}
	</script>

</script>
<div id="pagebodymain">
	<h1>
	<div style="text-align:left; float:left;">予約の一覧</div>
	</h1>

	<p>
	<table>
		<tr>
			<th class="b" colspan="2">検索条件</th>
		</tr>
		<tr>
			<th class="a">検索日</th>
			<th style="text-align:center">
			<select id="year_e">
				<?php
				$year_e = date("Y");
					for($i=2000;$i<=$year_e;$i++){
					if($i!=$year_e){
						echo '<option name="year_e" value="'.$i.'">'.$i.'年</option>';	
						}else{
						echo '<option name="year_e" value="'.$i.'" selected>'.$i.'年</option>';	
						}
					}
				?>
			</select>
			<select id="month_e">
				<?php
				$month_e = date("m");
					for($i=1;$i<=12;$i++){
					if($i!=$month_e){
						echo '<option name="month_e" value="'.$i.'">'.$i.'月</option>';	
						}else{
						echo '<option name="month_e" value="'.$i.'" selected>'.$i.'月</option>';	
						}
					}
				?>
			</select>
			<select id="day_e">
				<?php
				$day_e = date("j");
					for($i=1;$i<=31;$i++){
					if($i!=$day_e){
						echo '<option name="day_e" value="'.$i.'">'.$i.'日</option>';	
						}else{
						echo '<option name="day_e" value="'.$i.'" selected>'.$i.'日</option>';	
						}
					}
					$day_e = $day_e + 1;
				?>
			</select>
			</th>
		</tr>
		<tr>
			<th class="a">検索店舗</th>
			<th style="text-align:center;">
				<select id="shops">
				<?php
					$sql=' SELECT * FROM members WHERE shop_id!='.$shop_id.' ORDER BY shop_id ASC';
					$query=mysqli_query($db,$sql);
					while($data=mysqli_fetch_assoc($query)){
						echo '<option value="'.$data['shop_id'].'">'.$data['name'].'</option>';
					}
				?>
				</select>
			</th>
		</tr>
		<tr>
			<th colspan="2" style="text-align:center">
				<input type="image" src="../css/image/contents/search_reset.gif" onclick="location.reload();" alt="条件をリセット">　 
				<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索" onclick="search();">
		</tr>
	</table>


	<p>
		<div style="text-align:right;">
		<a href="#">
			<img src="../css/image/contents/btn_yoyaku2.gif" alt="タイムテーブルを表示" onclick="timetable();">
		</a>
		</div>
	</p>

	<p style="text-align:right;">
		予約件数: <span id="num_rsv"></span>件
	</p>

	<p>
	<table id="table">
	</table>
	<div id="span" align="center">
	</div>
	<p>
	<div style="height:75px;"></div>
</div>
<?php include("footer.php"); ?>