<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
	<?php
	include("db_connect.php");
	$end = $_GET['end'];
	$sql = 'SELECT *,members.name as shop_name,DATE_FORMAT(pos_finish.start,"%Y年 %m月 %d日 %H:%i:%s") as starttime, pos_finish.name as staffname,DATE_FORMAT(datetime,"%Y年%m月%日 %H:%i:%s") as finishtime,DATE_FORMAT(datetime,"%Y-%m-%d") as times FROM pos_finish,members WHERE datetime="'.$end.'" AND pos_finish.shop_id=members.shop_id';
	$recordset = mysqli_query($db,$sql);
	$data = mysqli_fetch_assoc($recordset);
	?>
	<h1><div style="text-align:left; float:left;">レジ締め詳細</div>
		<div style="text-align:right; padding-right:15px;"><a href="posfinish.php">レジ締めの確認へ</a></div></h1>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">基本情報</th>
		<tr> 
			<th class="a">店舗</th>
			<th colspan="3"><?php echo $data['shop_name'];?></th>
		<tr>
			<th class="a">対象日時</th>
			<th><?php echo $data['starttime']; ?></th>
			<th class="a">処理日時</th>
			<th><?php echo$data['finishtime']; ?></th>
		<tr>
			<th class="a">スタッフ番号</th>
			<th><?php echo $data['id']; ?></th>
			<th class="a">担当者名</th>
			<th><?php echo $data['staffname']; ?></th>
	</table>
<p>

	<table>
		<tr>
			<th colspan="5" class="b">レジ処理の集計</th>
		<tr>
			<th class="a">件数</th>
			<th class="a">点数</th>
			<th class="a">現金</th>
			<th class="a">クレジット</th>
			<th class="a">使用チケット</th>
		<tr><!--現在のレジ状況?-->
			<th style="text-align:right;"><?php echo $data['nums']; ?></th>
			<th style="text-align:right;"><?php echo $data['counts']; ?></th>
			<th style="text-align:right;"><?php echo $data['cash']; ?></th>
			<th style="text-align:right;"><?php echo $data['card']; ?></th>
			<th style="text-align:right;"><?php echo $data['ticket']; ?></th>
	</table>
</p>



<p>
	<table>
		<tr>
			<th colspan="4" class="b">レジ金の移動</th>
		</tr>
		<tr>
			<th class="a">日時</th>
			<th class="a">金額</th>
			<th class="a">担当者</th>
			<th class="a">備考</th>
		</tr>
		<?php
		$sqlmove = 'SELECT *,DATE_FORMAT(datetime,"%Y年%m月%d日 %H:%i:%s") as datetime FROM pos_move WHERE shop_id='.$data['shop_id'] .' AND DATE_FORMAT(datetime,"%Y-%m-%d")="'.$data['times'].'" ORDER BY datetime';
		$recordSetmove = mysqli_query($db, $sqlmove);
		$summoveset = mysqli_query($db, 'SELECT SUM(price) as sump, DATE_FORMAT(datetime,"%Y-%m-%d") as datef FROM pos_move WHERE shop_id='.$data['shop_id'] .' AND DATE_FORMAT(datetime,"%Y-%m-%d")="'.$data['times'].'" GROUP BY datef DESC');
		$summove = mysqli_fetch_array($summoveset);
		while ($table = mysqli_fetch_assoc($recordSetmove)){
		?>
		<tr><!--現在のレジ状況?-->
			<th>
			<?php 
			print(htmlspecialchars($table['datetime'])); ?>
			</th>
			<th>
			<?php
			print(htmlspecialchars($table['price'])); ?>
			</th>
			<th>
			<?php
			print(htmlspecialchars($table['name'])); ?>
			</th>
			<th>
			<?php
			print(htmlspecialchars($table['remarks'])); ?>
			</th>
		</tr>
	<?php
	}
	?>
		<tr>
			<th colspan="4" style="text-align:right;">
				移動合計金額:
				<big>
				<?php
				if($summove['datef'] == date('Y-m-d')){
				echo $summove['sump'];
				}else{echo "0";}
				?> 円</big>
			</th>
		</tr>
	</table>

</p>
<p>
	<table>
		<tr>
			<th colspan="6" class="b">レジ金の集計</th>
		<tr>
			<th class="a">開始レジ金</th>
			<th style="text-align:right;"><?php echo $data['start_reg']; ?></th>
			<th class="a">理論レジ金</th>
			<th style="text-align:right;">
			<?php echo $data['theory_reg']; ?></th>
			<th class="a">実レジ金</th>
			<th style="text-align:right;"><?php echo $data['actually']; ?>円</th>
		<tr>
			<th class="a">預かり</th>
			<th style="text-align:right;"><?php echo $data['cash_reg']; ?></th>
			<th class="a">お釣り</th>
			<th style="text-align:right;"><?php echo $data['changes']; ?></th>
			<th class="a">レジ金の差額</th>
			<th style="text-align:right;"><?php echo $data['difference'] ;?>円</th>
	</table>
</p>

</div>
<?php include("footer.php"); ?>
