<?php

	session_start();
	include("db_connect.php");

	$str="";

	$dom=mysqli_real_escape_string($db,$_GET['dom']);

	if($_GET['useshop'] == 0){
	$useshop = 1;
	}else{
	$useshop = ' shop_log_sub.shop_id= '.mysqli_real_escape_string($db,$_GET['useshop']);
	$useshop.= ' AND staff.shop_id= '.mysqli_real_escape_string($db,$_GET['useshop']);
	}

	if($_GET['staffid']==""){
	$staffid = 1;
	}else{
	$staffid = ' staff.id= "'.mysqli_real_escape_string($db,$_GET['staffid']).'"';
	}

	$year_s		= mysqli_real_escape_string($db,$_GET['year_s']);
	$month_s	= mysqli_real_escape_string($db,$_GET['month_s']);
	$day_s		= mysqli_real_escape_string($db,$_GET['day_s']);
	$year_e		= mysqli_real_escape_string($db,$_GET['year_e']);
	$month_e	= mysqli_real_escape_string($db,$_GET['month_e']);
	$day_e		= mysqli_real_escape_string($db,$_GET['day_e']);
	$day_e=$day_e + 1;

	if($dom==1){
	$select_dom = ' DATE_FORMAT(date, "%Y年%m月%d日") as datetime, DATE_FORMAT(date, "%a") as week, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-'.$day_s.'" AND date <= "'.$year_e.'-'.$month_e.'-'.$day_e.'"';
	}else{
	$select_dom = ' DATE_FORMAT(date, "%Y年%m月") as datetime, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-'.$day_s.'" AND date <= "'.$year_e.'-'.$month_e.'-'.$day_e.'"';
	}

	$sql = ' SELECT staff.name, '.$select_dom;
	$sql.= ' SUM(count) as sum_item, COUNT(count) as count_item, staff.shop_id as ssid, SUM(cash) as s_cash, SUM(card) as s_card, SUM(discount) as s_discount,SUM(kinken) as sum_kinken, ';
	$sql.= ' SUM(sum) as sum, SUM(ticket) as ticket, staff.id, ';
	$sql.=' SUM(ticket * CASE WHEN date <= "2014-03-31 23:59:59" THEN 1050
								  WHEN date >= "2014-04-01 00:00:00" THEN 1080
								  END
					) as ticketsum ';
	$sql.= ' FROM staff,shop_log_sub ';
	$sql.= ' WHERE shop_log_sub.staff_id=staff.id ';
	$sql.= ' AND shop_log_sub.shop_id = staff.shop_id ';
	$sql.= ' AND '. $useshop. ' AND '. $staffid. ' AND '.$date ;
	$sql.= ' GROUP BY datetime,shop_log_sub.shop_id, shop_log_sub.staff_id ';
	$sql.= ' ORDER BY datetime DESC ';
	$recordset = mysqli_query($db, $sql);
	if(mysqli_num_rows($recordset) == 0){
		echo "対象データがありません";
		return;
	}
		if($dom==1){
		$filename = "staff_daily.csv";
		$str.= "日付,曜日,スタッフ番号,スタッフ名,店舗番号,店舗名,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,販売金額,売上金額\n";
		while ($table = mysqli_fetch_assoc($recordset)){
			//店舗番号から店舗名を持ってくる
			$s_id = $table['ssid'];
			$sql_shop_name = " SELECT name FROM members WHERE shop_id = $s_id ";
			$rs_name = mysqli_query($db,$sql_shop_name);
			$s_name = mysqli_fetch_assoc($rs_name);
			$shop_name = $s_name['name'];
			
			$datetime=$table['datetime'];
			$week=$table['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
				}
			$staffname=$table['name'];
			$staffid=$table['id'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$sum=$table['sum'];
			$ticket_num = $table['ticket'];
			$non_ticket=$table['sum']-$table['ticketsum'];
			$sum_kinken = $table['sum_kinken'];
			$s_card = $table['s_card'];
			$s_cash = $table['s_cash'];
			$s_discount = $table['s_discount'];
			
			$str.= "$datetime,$week_str,$staffid,$staffname,$s_id,$shop_name,$count_item,$sum_item,$s_cash,$s_card,$ticket_num,$sum_kinken,$s_discount,$sum,$non_ticket\n";
			}
					}else{
					$filename = "staff_monthly.csv";
			$str.= "日付,スタッフ番号,スタッフ名,店舗番号,店舗名,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,販売金額,売上金額\n";
			while($table = mysqli_fetch_assoc($recordset)){
			//店舗番号から店舗名を持ってくる
			$s_id = $table['ssid'];
			$sql_shop_name = " SELECT name FROM members WHERE shop_id = $s_id ";
			$rs_name = mysqli_query($db,$sql_shop_name);
			$s_name = mysqli_fetch_assoc($rs_name);
			$shop_name = $s_name['name'];
			
			$datetime =$table['datetime'];
			$staffname=$table['name'];
			$staffid=$table['id'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$sum=$table['sum'];
			$ticket_num = $table['ticket'];
			$non_ticket=$table['sum']-$table['ticketsum'];
			$sum_kinken=$table['sum_kinken'];
			$s_card = $table['s_card'];
			$s_cash = $table['s_cash'];
			$s_discount = $table['s_discount'];
			
			$str.= "$datetime,$staffid,$staffname,$s_id,$shop_name,$count_item,$sum_item,$s_cash,$s_card,$ticket_num,$sum_kinken,$s_discount,$sum,$non_ticket\n";

			}
			
		}

		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=$filename");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));
		return;
	?>