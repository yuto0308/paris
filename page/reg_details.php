<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php");?>
<div id="pagebodymain">
<?php
$reg1=$_GET['reg_id1'];
$reg2=$_GET['reg_id2'];
$reg2 = sprintf("%05d",$reg2);
$shop_id = $_GET['shop'];

	$sql ="SELECT *,shop_log_sub.kinken as kinken, DATE_FORMAT(date,('%Y 年%m 月%d 日 %H:%i:%s')) as datetime, members.name as shop_name FROM shop_log_sub,guest,shop_log,members WHERE shop_log_sub.reg_id1='$reg1' AND  shop_log_sub.reg_id2='$reg2' AND shop_log_sub.guest_id=guest.guestseq AND shop_log.guest_id=guest.guestseq AND shop_log_sub.reg_id1=shop_log.reg_id1 AND shop_log_sub.reg_id2=shop_log.reg_id2 and shop_log_sub.shop_id=$shop_id AND guest.shop_id=$shop_id ";
	$sql.=" AND shop_log_sub.shop_id=shop_log.shop_id 
			AND shop_log_sub.shop_id=guest.shop_id 
			AND shop_log_sub.shop_id=members.shop_id 
			AND shop_log.shop_id=guest.shop_id 
			AND shop_log.shop_id=members.shop_id 
			AND guest.shop_id=members.shop_id";
$recordSet = mysqli_query($db, $sql);
if(mysqli_num_rows($recordSet)==0){
	$sql ="SELECT *, DATE_FORMAT(date,('%Y 年%m 月%d 日 %H:%i:%s')) as datetime, members.name as shop_name FROM shop_log_sub,shop_log,members WHERE shop_log_sub.reg_id1='$reg1' AND  shop_log_sub.reg_id2='$reg2' AND shop_log_sub.reg_id1=shop_log.reg_id1 AND shop_log_sub.reg_id2=shop_log.reg_id2 and shop_log_sub.shop_id=$shop_id ";
	$sql.=" AND shop_log_sub.shop_id=shop_log.shop_id 
			AND shop_log_sub.shop_id=members.shop_id 
			AND shop_log.shop_id=members.shop_id ";
	$recordSet = mysqli_query($db, $sql);
}
$data = mysqli_fetch_assoc($recordSet);
$sql_log ="SELECT SUM(price) as price_total FROM shop_log WHERE reg_id1='$reg1' AND reg_id2='$reg2' and shop_id=$shop_id";
$recordSet_log =mysqli_query($db, $sql_log);
$data_log = mysqli_fetch_assoc($recordSet_log);

?>
<script>
	$(document).ready(function(){
		var id = $('#staffid').val();
		var shop = <?php echo $shop_id; ?>;
		$('#show_id').load('owner_getdata.php',{id:id,shop:shop});}
	);
</script>
<script>
function SearchId(){
		var id = $('#staffid').val();
		var shop = <?php echo $shop_id; ?>;
		$('#show_id').load('owner_getdata.php',{id:id,shop:shop});
	}

function submit(){
	var flag = 0;
	if(document.detail.staffid.value == "0"){
		flag = 1;
}
	if(flag>0){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}
    if(confirm("この内容で登録しますか") == true){
	var staff_id = $('#staffid').val();
	var method = $("input:radio[name='method']:checked").val();
	var bikou = $('#bikou').val();
	$.post("reg_details_do.php?reg_id1=<?php echo $reg1 ;?>&reg_id2=<?php echo $reg2 ;?>&shop=<?php echo $shop_id; ?>",
		{staff_id:staff_id,
		method:method,
		bikou:bikou},
		function(data){
		if(data == 0){
			alert("エラー")
		}else{
			alert(data);
			document.location = "posreglist.php";}});
}else{
	alert("キャンセルされました。");
}
}

</script>
<h1>
<div style="text-align:left; float:left;">レジ売上の詳細</div>
<div style="text-align:right"><a href="javascript:history.back();" style="margin-right:15px;">レジ売上の一覧</a></div>
</h1>
<form name="detail" action="#" method="post">
<table>
	<tr>
		<th colspan="4" class="b">レジ売上の基本情報</th>
	</tr>
	<tr>
		<th class="a">レジ番号</th>
		<th><?php echo $reg1.'-'.$reg2; ?></th>
		<th class="a">売上日時</th>
		<th><?php echo $data['datetime']; ?></th>
	</tr>
	<tr>
		<th class="a">スタッフ番号<font color="red">(*)</font></th>
		<th>
			<select id="staffid" onchange="SearchId();">
			<option value="0" name="staff">IDを選択してください</option>
			<?php 
			$sql_name = "SELECT id FROM staff WHERE shop_id=$shop_id ORDER BY id";
			$recordSet_name = mysqli_query($db, $sql_name);
			while($arr_item = mysqli_fetch_assoc($recordSet_name))
				{
					foreach($arr_item as $value){
						if($value  == $data['staff_id']){
							echo "<option value= $value selected> $value </option><br>";
						}else{
							echo "<option value= $value> $value </option><br>"; 
						}
					}
				}
			?></th>
		<th class="a">担当者名</th>
		<th id="show_id"></th>
	</tr>
	<tr>
		<th class="a">購入方法</th>
		<th colspan="3"><input type="radio" name="method" id="shop" value="0" 
			<?php 
			if($data['method'] == 0){
			echo 'checked="checked"';
			}
			?>/><label for="shop">店頭</label>
						<input type="radio" name="method" id="other" value="1" 
			<?php 
			if($data['method'] == 1){
			echo 'checked="checked"';
			}
			?>/><label for="other">その他</label>
		</th>
	</tr>
	<tr>
		<th class="a">購入店舗</th>
		<th colspan="3"><?php echo $data['shop_name']; ?></th>
	</tr>
</table>
<p>
	<table>
		<tr>
			<th class="b" colspan="4">顧客の情報</th>
		</tr>
		<tr>
			<th class="a">会員状況</th>
			<th>
			<?php
			if(!isset($data['id'])){
				echo "非会員";
			}else if($data['member_check'] == 0){
			echo "会員";
			}else if($data['member_check'] == 1){
					echo "退会中";
			}
			?>
			</th>
			<th class="a">会員番号</th>
			<th><?php 
			if(!isset($data['id'])){
				echo "";
			}else{
				if($data['sub_id']==""){
					echo $data['id']; 
				}else{
					echo $data['sub_id']." - ".$data['id'];
				}
				}
				?></th>
		</tr>
		<tr>
			<th class="a">お名前</th>
			<th><a href="customer_update.php?id=<?php print($data['guestseq']);?>&shop_id=<?php print($data['shop_id']); ?>">
				<?php
				if(!isset($data['id'])){
				echo "";
			}else{
				echo $data['fullname_kanji'];
			}
				?></a></th>
			<th class="a">フリガナ</th>
			<th>
			<?php 
			if(!isset($data['id'])){
				echo "";
			}else{
			echo $data['fullname_kana'];
			}
			?></th>
		</tr>
		<tr>
			<th class="a">住所</th>
			<th colspan="3">
			<?php
			if(!isset($data['id'])){
				echo "";
			}else{
				?>
			<?php echo $data['postal_num1']?>-<?php echo$data['postal_num2'];?> <?php echo $data['prefecture'];?> <?php echo $data['city'];?> <?php echo $data['address'];?> <?php echo $data['building']; ?>
			<?php } ?>
			</th>
		</tr>
		<tr>
			<th class="a">電話番号</th>
			<th>
				<?php
				if(!isset($data['id'])){
				echo "";
			}else{
				?>
				<?php echo $data['tel1'];?><?php echo ",".$data['tel2']; ?>
				<?php } ?></th>
			<th class="a">メールアドレス</th>
			<th><?php 
			if(!isset($data['id'])){
				echo "";
			}else{
			echo $data['mailaddress']; 
		}
		?></th>
		</tr>
</table>
<p>
<table>
	<tr>
		<th class="b" colspan="6">商品明細</th>
	</tr>
	<tr>
		<th class="a" style="text-align:center;">商品コード</th>
		<th class="a" style="text-align:center;">商品名</th>
		<th class="a" style="text-align:center; width:60px;">割引</th>
		<th class="a" style="text-align:center; width:60px;">単価</th>
		<th class="a" style="text-align:center; width:30px;">数量</th>
		<th class="a" style="text-align:center; width:40px;">金額</th>
	</tr>
	
	<?php
	$sql ="SELECT *, DATE_FORMAT(date,('%Y 年%m 月%d 日 %H:%i:%s')) as datetime FROM shop_log_sub,shop_log WHERE shop_log_sub.reg_id1='$reg1' AND shop_log_sub.reg_id2='$reg2' AND shop_log_sub.reg_id1=shop_log.reg_id1 AND shop_log_sub.reg_id2=shop_log.reg_id2 and shop_log_sub.shop_id=$shop_id ";
	$sql.=" AND shop_log_sub.shop_id=shop_log.shop_id ";
$recordSet = mysqli_query($db, $sql);
while($table = mysqli_fetch_assoc($recordSet)){
	?>
		<tr>
			<th style="text-align:center;">
			<?php 
			print(htmlspecialchars($table['goods_id'])); ?>
			</th>
			<th>
			<?php
			print(htmlspecialchars($table['goods_name'])); ?>
			</th>
			<th style="text-align:right;">
			<?php
			print(htmlspecialchars($table['discount'])); ?>
			</th>
			<th style="text-align:right;">
			<?php
			$tanka = ($table['price'] + $table['discount'])/$table['goods_num'];
			echo  $tanka;
			?>
			</th>
			<th style='text-align:right;'>
			<?php
			print(htmlspecialchars($table['goods_num'])); ?>
			</th>
			<th style='text-align:right;'>
			<?php
			print(htmlspecialchars($table['price'])); ?>
			</th>
		</tr>
	<?php
	}
	?>
	<tr>
		<th colspan="3" class="a"></th>
		<th class="a" style="text-align:center;">合計</th>
		<th class="a" style="text-align:right;"><?php echo $data['count']; ?></th>
		<th class="a" style="text-align:right;"><?php echo $data['sum'] - $data['tax']; ?></th>
	</tr>
</table>
<p>
<table id="details_low">
	<tr>
		<th class="b" colspan="4">お支払の情報</th>
	</tr>
	<tr>
		<th class="a">消費税</th>
		<th><?php echo $data['tax']; ?>円</th>
		<th class="a">合計金額</th>
		<th><?php echo $data['sum']; ?>円</th>
	</tr>
	<tr>
		<th class="a">使用チケット</th>
		<th><?php echo $data['ticket']; ?>枚</th>
		<th class="a">現金預かり</th>
		<th><?php echo $data['cashpluschange']; ?>円</th>
	</tr>
	<tr>
		<th class="a">商品券</th>
		<th><?php echo $data['kinken']; ?></th>
		<th class="a">クレジット預かり</th>
		<th><?php echo $data['card']; ?>円</th>
	</tr>
	<tr>
		<th class="a">お支払方法</th>
		<th><?php if($data['cashorcard'] == 1){
			echo "現金";
		}else{
			if($data['cashorcard'] == 2){
				echo "カード";
			}else{
				if($data['cashorcard'] == 3){
					echo "現金+カード";
				}
			}
		}

			 ?></th>
		<th class="a">お釣り</th>
		<th><?php echo $data['changes']; ?>円</th>
	</tr>
</table>
<p>
<table>
	<tr>
		<th class="b" colspan="6">その他</th>
	</tr>
	<tr>
		<th class="a" style="text-align:center;">備考</th>
		<th  style="text-align:center;"><textarea rows = "10" cols = "60" id="bikou" ><?php echo $data['bikou']; ?></textarea></th>
	</tr>
</table>
<p>
</form>
<p style="text-align:center;">

<input type="image" src="../css/image/contents/view.gif" onclick="submit();"  alt="入力内容の確認"/>
</div>
<?php include("footer.php"); ?>
