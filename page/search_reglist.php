<?php 
	/*
		作成者	：油谷
		昨日	：shop_log_subを参照し、レジの売り上げ一覧に
				　結果を表示
				　JSONで結果を返し,リストと会員数を同時に更新	
	*/
/*
$ary=array();
$ary[]="1";
$ary[]="2";
$ary[]="3";

echo json_encode($ary);
*/	
	session_start();
	include("db_connect.php");
	$maxrow = 10;//一度に表示する行数
	$str="";
	$str_button="";
	//result_ary : １つ目に行表示HTML
	//			 : ２つめに該当会員数
	//			 : ３つめにインデックスボタン

	$result_ary=array();
 	$num=$_POST['num'];
 	$show = ($num - 1) * $maxrow;
 	
 	if($_POST['shop'] == 0){
	$shop_name = "1";
	}else{
	$shop_name = 'members.shop_id='.$_POST['shop'];
	}


 	//cash_or_card
	//0 指定無し
	//1 現金
	//2 カード
	//3 現金＋カード
	$cash_or_card = $_POST['cash_or_card'];	
	if($cash_or_card != 0){
	$cash_or_card_wrd = "cashorcard = ".$cash_or_card;
	}else{
		$cash_or_card_wrd = "1";
	}
	//guest_name
	//顧客の名前、もしくはフリガナが入る
	//名字と名前の間には全角スペースを入れてもらう
	$guest_name = $_POST['guest_name'];
	
	//reg_id1
	//regid1に対応する
	$reg_id1 = $_POST['reg_id1'];
	if($reg_id1 == ""){
		$reg_id1_str = "1";
	}else{
		$reg_id1_str = "reg_id1 = ".$reg_id1;
	}
	//reg_id2
	//reg_id2に対応する
	$reg_id2 = $_POST['reg_id2'];
	if($reg_id2 == ""){
		$reg_id2_str = "1";
	}else{
		$reg_id2_str = "reg_id2 = ".$reg_id2;
	}
		
	//staff_num
	//staff_idに対応する
	$staff_id = $_POST['staff_id'];
	if($staff_id == ""){
		$staff_id_str = "1";
	}else{
		$staff_id_str = "shop_log_sub.staff_id = '".$staff_id."' ";
	}
	
	$goods_name = $_POST['goods_name'];
	if($goods_name == ""){
		$goods_name_str = "1";
	}else{
		$goods_name_str = 'goods_name like '."'%".$goods_name."%'";
	}
	
	$year_s=$_POST['year_s'];
	$month_s=$_POST['month_s'];
	$day_s=$_POST['day_s'];
	$year_e=$_POST['year_e'];
	$month_e=$_POST['month_e'];
	$day_e=$_POST['day_e'];
	
	
	if($guest_name != ""){
	$guest_name_str = 'guest_name like "%'.$guest_name.'%"';
	}else{
		$guest_name_str = "1";
	}

	
	$sql = 'SELECT * , DATE_FORMAT(date,"%Y-%m-%d<br>%k:%i") as formateddate, shop_log_sub.shop_id as post_shop_id FROM shop_log_sub,members WHERE shop_log_sub.shop_id=members.shop_id AND '. $shop_name .' AND '. $goods_name_str.' and '.$guest_name_str.' and '.$cash_or_card_wrd.' and '.$reg_id1_str.' and '.$reg_id2_str.'  and '.$staff_id_str.' and  date >= "'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00" and date <= "'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59" order by date DESC LIMIT '.$show.','.$maxrow;


	$rs = mysqli_query($db,$sql) or exit("クエリ失敗");
	$sql_count = 'SELECT * FROM shop_log_sub,members WHERE shop_log_sub.shop_id=members.shop_id AND ' .$shop_name .' AND '. $goods_name_str.' and '.$guest_name_str.' and '.$cash_or_card_wrd.' and '.$reg_id1_str.' and '.$reg_id2_str.' and '.$staff_id_str.' and date >= "'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00" and date <= "'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59"';


	$rs_count = mysqli_query($db,$sql_count) or exit("クエリ失敗");
	
	$num_list = mysqli_num_rows($rs_count);
	while($arr_item = mysqli_fetch_assoc($rs)){
			
			
		//顧客idから顧客名を引っ張ってくる
		$name=$arr_item['guest_name'];
		// $guest_id=$arr_item['guest_id'];
		// $shop_number=$arr_item['post_shop_id'];
		// $sql_guest = 'SELECT * FROM guest WHERE shop_id='.$shop_number.' AND  id = '.$guest_id;
		// $guest_name_search = mysqli_query($db,$sql_guest) or exit("クエリに失敗しました");
		// $guest_name = mysqli_fetch_assoc($guest_name_search);
		// $name = $guest_name['GuestName_kanji'] .'　'. $guest_name['guestname_kanji_first'];
		
		//スタッフIDからスタッフ名もってくる
		$staff_id = $arr_item['staff_id'];
	    $sql_staff_id = 'SELECT * FROM staff WHERE id = "'.$staff_id.'" AND shop_id='.$arr_item['shop_id'];
        $staff_name_search = mysqli_query($db,$sql_staff_id) or exit("クエリに失敗しました");
        $staff_name = mysqli_fetch_assoc($staff_name_search);
        $name_s = $staff_name['abb_name'];
			
		//現金orカードの場合分け			
		if($arr_item['cashorcard'] == "1"){
				$cash_or_card_str = "現金";
			}else if($arr_item['cashorcard'] == "2"){
				$cash_or_card_str = "カード";
			}else if($arr_item['cashorcard'] == "3"){
				$cash_or_card_str = "現金＋カード";
			}

		if($arr_item['ticket'] == "" || $arr_item['ticket']== 0){
			$ticket_str="";
		}else{
			$ticket_str='<font color="red">('.$arr_item['ticket'].')</font>';
		}

		if($arr_item['kinken'] == "" || $arr_item['kinken']== 0){
			$kinken_str="";
		}else{
			$kinken_str='<font color="blue">('.$arr_item['kinken'].')</font>';
		}

		if($arr_item['points'] == "" || $arr_item['points'] == 0){
			$points_str="";
		}else{
			$points_str='<font color="green">('.$arr_item['points'].')</font>';
		}

		$post_shop_id=$arr_item['post_shop_id'];
			
		//$cashcard=$arr_item['cash']+$arr_item['card'];
		if($arr_item['date'] <= "2014-03-31 23:59:59"){
			$times_temp = 1050;
		}else{
			$times_temp = 1080;
		}
		$cashcard = $arr_item['sum'] - ($arr_item['ticket'] * $times_temp);
		$cashcard=number_format($cashcard);
		$str .= '<tr name="add">
				<th style="text-align:center;">
				<a href="delete_log.php?reg_id1='.$arr_item['reg_id1'].'&amp;reg_id2='.$arr_item['reg_id2'].'&amp;shop='.$post_shop_id.'" onclick="return confirm('."'削除してよろしいですか？'".');">
				<input type="button" name="sakuzyo" value="削除" />
				</a>
				</th>
				<th>
				<a href="reg_details.php?reg_id1='.$arr_item['reg_id1'].'&amp;reg_id2='.$arr_item['reg_id2'].'&amp;shop='.$post_shop_id.'">'.$arr_item['reg_id1'].'-<br>'.str_pad($arr_item['reg_id2'],5,"0",STR_PAD_LEFT).'</a>
				</th>
				<th>'.$name.'</th>
				<th>'.$cash_or_card_str.$ticket_str.$kinken_str.$points_str.'</th>
				<th>'.$arr_item['name'].'</th>
				<th>'.$name_s.'</th>
				<th>'.$arr_item['formateddate'].'</th>
				<th style="text-align:right;">'.$cashcard.'</th>
			</tr>';
			}
		if($str==""){
				$str .= '<tr name="add"><th colspan="8" style="text-align:center;">対象データがありません</th></tr>';
			}
//ボタン-------------------------------------------------
		$button_num = floor(($num_list - 1 )/ $maxrow) + 1;

		if($button_num <= 12){
			for($i = 1;$i<=$button_num;$i++){
				if($i == $num){
					$str_button .= '<input type="button" style="color:blue;font-size:large;"';
					$str_button .= ' name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
				}else{
					$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
				}
			}
		}else{
			if($num - 1 <= 5){
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($num - 1) + 5) && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else if($button_num - $num <= 5){
					$front_reader = 0;
					$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
					}else{
					if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($button_num - $num) + 5) && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else{
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if(abs($num - $i) <= 5){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reglist('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}	
		}

		$result_ary[] = $str;
		$result_ary[] = $num_list;
		$result_ary[] = $str_button;
		$result_ary[] = $num;
		echo json_encode($result_ary);

?>