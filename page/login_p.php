<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ユーザ認証</title>
</head>
<body>
<?php

session_start();
  
htmlspecialchars($_POST['id'], ENT_QUOTES, 'utf-8');
htmlspecialchars($_POST['pass'], ENT_QUOTES, 'utf-8');

include("../../../etc_html/login_check_owner.php");

mysqli_real_escape_string($conn, $_POST['id']);
mysqli_real_escape_string($conn, sha1($_POST['pass']));

  if(mysqli_num_rows($result)!=0){
    while($arr_item = mysqli_fetch_assoc($result)){
      $series=$arr_item['series'];
      $id=$arr_item['id'];
      $pass=$arr_item['pass'];
      $db_name=$arr_item['db_name'];
      $db_id=$arr_item['db_id'];
      $db_pass=$arr_item['db_pass'];
    }


    $db = mysqli_connect($db_name,$db_id,$db_pass,$series) or die("エラー");
    $sql_login="SELECT * FROM members WHERE login_id='$id' AND password='$pass'";
    $login_check=mysqli_query($db,$sql_login);
    $login_data=mysqli_fetch_assoc($login_check);

    $_SESSION['db_name'] = $db_name;
    $_SESSION['db_id'] = $db_id;
    $_SESSION['db_pass'] = $db_pass;
    $_SESSION['series'] = $series;
    $_SESSION['name'] = $login_data['name'];
    $_SESSION['shop_id'] = $login_data['shop_id'];
    $_SESSION['id'] = $_POST['id'];
    $_SESSION['pass'] = $_POST['pass'];
    $_SESSION['ownercheck'] = $login_data['ownercheck'];
    //ログイン情報を記録する
    if($_POST['save'] == 'on'){
      setcookie('db_name', $db_name, time()+60*60*24*14);
      setcookie('db_id', $db_id, time()+60*60*24*14);
      setcookie('db_pass', $db_pass, time()+60*60*24*14);
      setcookie('series', $series, time()+60*60*24*14);
      setcookie('id', $_POST['id'], time()+60*60*24*14);
      setcookie('shop_id', $login_data['shop_id'], time()+60*60*24*14);
      setcookie('pass', $_POST['pass'], time()+60*60*24*14);
      setcookie('ownercheck', $login_data['ownercheck'], time()+60*60*24*14);
      setcookie('name', $login_data['name'], time()+60*60*24*14);
    }


    header('location: index.php');
    exit();
  }
  else{
    echo "ユーザ認証に失敗しました。もう一度入力しなおしてください。";
    echo '<form action="login.php" method="POST">
<input type="submit" name="sub" value="戻る">
</form>';
}
  
  mysqli_close($conn);

?>


</body>
</html>