<?php 
	/*
		作成者	：油谷
		昨日	：ゲストテーブルを参照し、カスタマーリストに
				　結果を表示
				　JSONで結果を返し,リストと会員数を同時に更新	
	*/
/*
$ary=array();
$ary[]="1";
$ary[]="2";
$ary[]="3";

echo json_encode($ary);

*/
	session_start();
	include("db_connect.php"); 
	$maxrow = 10;//一度に表示する行数
	$str="";
	$str_button="";
	//result_ary : １つ目に行表示HTML
	//			 : ２つめに該当会員数
	//			 : ３つめにインデックスボタン
	$result_ary=array();

 	$num=$_POST['num'];
 	$show = ($num - 1) * $maxrow;
 	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順

 	$sort_info=$_POST['sort_info'];
 	if($sort_info=="0"){
	 	$sort_str="created DESC";
 	}else if($sort_info=="1"){
	 	$sort_str="use_times DESC";
 	}else if($sort_info=="2"){
 		$sort_str="quantity DESC";
 	}else if($sort_info=="3"){
	 	$sort_str="price DESC";
 	}

 	//key_ctgr : 0 選択無し
	//		   : 1 会員名
	//         : 2 会員名（ふりがな）
	//         : 3 会員住所//今のところ保留
	//         : 4 会員電話番号
	//         : 5 メールアドレス
	//         : 6 会員番号
 	$key_ctgr=$_POST['key_ctgr'];
 	if($key_ctgr == "0"){
 		$ctgr = "1";
 	}else if($key_ctgr == "1"){
	 	$ctgr = "fullname_kanji like";
 	}else if($key_ctgr == "2"){
	 	$ctgr = "fullname_kana like";
 	}else if($key_ctgr == "3"){
	 	$ctgr = "1";
 	}else if($key_ctgr == "4"){
	 	$ctgr = "tel1 like ";
 	}else if($key_ctgr == "5"){
 		$ctgr = "mailaddress like ";
 	}else if($key_ctgr == "6"){
 		$ctgr = " concat(sub_id,'-',id) like BINARY";
 	}
 	$key_ctgr_word='"%';
 	$key_ctgr_word.=$_POST['key_ctgr_word'];
 	$key_ctgr_word.='%"';
 	if($key_ctgr == "0" || $key_ctgr == "3"){
	 	$ctgr_result = $ctgr;
 	}else{
 		$ctgr_result = $ctgr . $key_ctgr_word;	
 	}
 	//key_member_check : 0 指定無し
	//				   : 1 会員
	//                 : 2 退会
 	$key_member_check=$_POST['key_member_check'];
 	if($key_member_check == "0"){
	 	$key_member_check_str="1";
 	}else if($key_member_check == "1"){
	 	$key_member_check_str='member_check = "0"';
 	}else if($key_member_check == "2"){
	 	$key_member_check_str='member_check = "1"';
 	}
 	//key_dm	: 0 指定無し
	//			: 1 配信する
	//			: 2 配信しない
 	$key_dm=$_POST['key_dm'];
 	if($key_dm =="0"){
 		$key_dm_str="1";
 	}else if($key_dm =="1"){
	 	$key_dm_str='dm = "1"';
 	}else if($key_dm =="2"){
	 	$key_dm_str='dm = "0"';
 	}

 	if($_POST['key_shop_name'] == 0){
 		$use_shop = 1;
 	}else if($_POST['key_useshop']==0){
 		$use_shop ="shop_id=".$_POST['key_shop_name'];
 	}else if($_POST['key_useshop']==1){
 		$use_shop ="last_shop=".$_POST['key_shop_name'];
 	}

 	if($_POST['use_day'] == 0){
 		$use_day = "created";
 	}else{
 		$use_day ="last";
 	}

 	$key_year_s=$_POST['key_year_s'];
 	$key_month_s=$_POST['key_month_s'];
 	$key_day_s=$_POST['key_day_s'];
 	$key_year_e=$_POST['key_year_e'];
 	$key_month_e=$_POST['key_month_e'];
 	$key_day_e=$_POST['key_day_e'];
 	$key_day_e=$key_day_e + 1;
 	$key_year_use_s=$_POST['key_year_use_s'];
 	$key_month_use_s=$_POST['key_month_use_s'];
 	$key_day_use_s=$_POST['key_day_use_s'];
 	$key_year_use_e=$_POST['key_year_use_e'];

 	if($key_year_use_e == 0){
	 	$key_year_use_e = date("Y");
 	}

 	$key_month_use_e=$_POST['key_month_use_e'];
 	if($key_month_use_e == 0){
	 	$key_month_use_e = date("m");
 	}

 	$key_day_use_e=$_POST['key_day_use_e'];
 	if($key_day_use_e == 0){
	 	$key_day_use_e = date("d");
 	}

 	$key_day_use_e = $key_day_use_e  + 1;
	
	
	$sql = "SELECT * ";
	$sql .= "FROM guest ";
	$sql .= 'WHERE '.$use_shop .' and ' .$ctgr_result.' and '.$key_member_check_str.' and '.$key_dm_str ;
	$sql .= ' and updating >= "'.$key_year_s.'-'.$key_month_s.'-'.$key_day_s.'" and updating <= "'.$key_year_e.'-'.$key_month_e.'-'.$key_day_e.'" and '.$use_day.' >=  "'.$key_year_use_s.'-'.$key_month_use_s.'-'.$key_day_use_s.'" and '.$use_day.' <= "'.$key_year_use_e.'-'.$key_month_use_e.'-'.$key_day_use_e.'"';
	$sql .= " order by $sort_str ";
	$sql .= " LIMIT $show,$maxrow ";
	$rs = mysqli_query($db,$sql) or exit($sql);

	$sql_count = 'SELECT * FROM guest WHERE '.$use_shop.' and '.$ctgr_result.' and '.$key_member_check_str.' and '.$key_dm_str.' and updating >= "'.$key_year_s.'-'.$key_month_s.'-'.$key_day_s.'" and updating <= "'.$key_year_e.'-'.$key_month_e.'-'.$key_day_e.'" and '.$use_day.' >=  "'.$key_year_use_s.'-'.$key_month_use_s.'-'.$key_day_use_s.'" and '.$use_day.' <= "'.$key_year_use_e.'-'.$key_month_use_e.'-'.$key_day_use_e.'"';
	$rs_count = mysqli_query($db,$sql_count) or exit($sql_count);
	if(!$rs){
		die('クエリ失敗 連絡をおねがいします');
			}
			
	$num_guest = mysqli_num_rows($rs_count);
	while($arr_item = mysqli_fetch_assoc($rs)){
			$guestseq=$arr_item['guestseq'];
			$tenp_id=$arr_item['shop_id'];
			$id=$arr_item['id'];
			$name1 = $arr_item['GuestName_kanji'];
			$name2 = $arr_item['guestname_kanji_first'];
			$name = $name1 . $name2; 
			$name_katakana = $arr_item['GuestName_Katakana'] . $arr_item['guestname_katakana_first'];
			$pref = $arr_item['prefecture'];
			$dm = $arr_item['dm'];
			if($dm==0){
				$dm_check = "";
			}else{
				$dm_check = "●";
				}
			$ticket = $arr_item['Tickets'];
			$use_times = $arr_item['use_times'];
			$quantity = $arr_item['quantity'];
			$price = $arr_item['price'];
			$member_check = $arr_item['member_check'];
			if($member_check == 0){
				$member_check_str="";
			}else{
				$member_check_str="●";
			}
			
		$str .= '<tr name="add">
		<th style="text-align:center;"><a href="delete.php?id='.$guestseq.'&data=guest&shop_id='.$tenp_id.'" onclick="return confirm('.'\''.'削除してよろしいですか？'.'\''.');"><input type="button" name="sakuzyo" value="削除" /></a></th>
		<th><a href="customer_update.php?id='.$guestseq.'">'.$name.'</a></th>
		<th>'.$pref.'</th>
		<th style="text-align:center;">'.$dm_check.'</th>
		<th >'.$ticket.'</th>
		<th >'.$use_times.'</th>
		<th >'.$quantity.'</th>
		<th >'.number_format($price).'</th>
		<th style="text-align:center;">'.$member_check_str.'</th>
		</tr>';
			}
		if($str==""){
				$str .= '<tr name="add"><th colspan="9" style="text-align:center;">対象データがありません</th></tr>';
			}
		
		$button_num = floor(($num_guest -1 )/ $maxrow) + 1;
		if($button_num <= 12){
			for($i = 1;$i<=$button_num;$i++){
				if($i == $num){
					$str_button .= '<input type="button" style="color:blue;font-size:large;"';
					$str_button .= ' name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
				}else{
					$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
				}
			}
		}else{
			if($num - 1 <= 5){
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($num - 1) + 5) && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else if($button_num - $num <= 5){
					$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
					}else{
					if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($button_num - $num) + 5) && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else{
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if(abs($num - $i) <= 5){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}	
		}
		
		
		$result_ary[] = $str;
		$result_ary[] = $num_guest;
		$result_ary[] = $str_button;
		$result_ary[] = $num;
		echo json_encode($result_ary);

?>