<?php
session_start();
include("db_connect.php");


$result_ary = array();
$str1 = '';
$str2 = '';

if($_POST['month'] < 10){
	$month = '0'.$_POST['month'];
}else{
	$month = $_POST['month'];
}
$date = $_POST['year'].'-'.$month.'-01';
$sdate = $_POST['year'].'-'.$month;
$date = strtotime($date);

$month1 = date("Y")*12 + date('m');
$month2 = date("Y",$date)*12 + date('m',$date);

$result = $month1 -$month2;
if($result == 0){
	$adj = '';
}else{
	$adj = ' - INTERVAL '.$result.' MONTH ';
}

$str1 .= '<tr name="add">';
for($i=2;$i<8;$i++){
	$sql2 = '';
	$count = 0;
	$s = $i * 10;
	$e = $s + 10;

	$sql2 = ' SELECT COUNT(guestseq) FROM guest WHERE ';
	$sql2.= ' CONCAT(birth_y,"-",birth_m,"-",birth_d) BETWEEN ';
	$sql2.= ' DATE_FORMAT(CURDATE() + INTERVAL 1 DAY - INTERVAL '.$e.' YEAR '.$adj.',"%Y-%m-%d") ';
	$sql2.= ' AND DATE_FORMAT(CURDATE() - INTERVAL '.$s.' YEAR '.$adj.',"%Y-%m-%d") AND shop_id = '.$shop_id;
	$que= mysqli_query($db,$sql2) or exit($sql2);
	$rs = mysqli_fetch_assoc($que);
	$count = $rs['COUNT(guestseq)'];
	$str1.= '<th style="text-align:center;">'.$count.'人</th>';
}
$str1 .= '</tr>';

for($i2=2;$i2<8;$i2++){
	$rank_ary = array();
	$sql3 = '';
	$row_count = 0;
//	$test = 0;
	$str_rank = "";
	$s2 = $i2 * 10;
	$e2 = $s2 + 10;

	$sql3 = ' SELECT COUNT(guestseq),SUM(sum),SUM(tax) FROM guest,shop_log_sub WHERE ';
	$sql3.= ' CONCAT(birth_y,"-",birth_m,"-",birth_d) BETWEEN ';
	$sql3.= ' DATE_FORMAT(CURDATE() + INTERVAL 1 DAY - INTERVAL '.$e2.' YEAR '.$adj.',"%Y-%m-%d") ';
	$sql3.= ' AND DATE_FORMAT(CURDATE() - INTERVAL '.$s2.' YEAR '.$adj.',"%Y-%m-%d") ';
	$sql3.= ' AND guest.guestseq = shop_log_sub.guest_id ';
	$sql3.= ' AND DATE_FORMAT(shop_log_sub.date,"%Y-%m") = "'.$sdate.'" AND shop_log_sub.shop_id = '.$shop_id;
	$que3= mysqli_query($db,$sql3) or exit($sql3);
	$rs3 = mysqli_fetch_assoc($que3);
	if($rs3['COUNT(guestseq)'] == NULL){
		$rs3['COUNT(guestseq)'] = 0;
	}
	if($rs3['SUM(sum)'] == NULL ){
		$rs3['SUM(sum)'] = 0;
	}
	$count2 = $rs3['COUNT(guestseq)'];
	$sum2 = $rs3['SUM(sum)'] - $rs3['SUM(tax)'];

	$sql4 = ' SELECT category_big_str as gname,category_big, SUM(s.price) as cprice ';
	$sql4.= ' FROM shop_log s, guest c, goods g WHERE';
	$sql4.= ' CONCAT(birth_y,"-",birth_m,"-",birth_d) BETWEEN ';
	$sql4.= ' DATE_FORMAT(CURDATE() + INTERVAL 1 DAY - INTERVAL '.$e2.' YEAR '.$adj.',"%Y-%m-%d") ';
	$sql4.= ' AND DATE_FORMAT(CURDATE() - INTERVAL '.$s2.' YEAR '.$adj.',"%Y-%m-%d") ';
	$sql4.= ' AND c.guestseq = s.guest_id ';
	$sql4.= ' AND DATE_FORMAT(s.day,"%Y-%m") = "'.$sdate.'" ';
	$sql4.= ' AND s.goods_id = g.id ';
	$sql4.= ' AND s.shop_id = '.$shop_id;
	$sql4.= ' GROUP BY category_big ';
	$que4 = mysqli_query($db,$sql4) or exit($sql4);

	while($rs4 = mysqli_fetch_assoc($que4) ){
		if( !isset( $rank_ary[$rs4['gname']] ) ){
			$rank_ary[$rs4['gname']] = $rs4['cprice'];
		}else{
			$rank_ary[$rs4['gname']] = $rank_ary[$rs4['gname']] + $rs4['cprice'];
		}
	}
//		++$test;

	$sum2 = number_format($sum2);
	$count2 = number_format($count2);
	if($row_count == 0){
		foreach( $rank_ary as $key => $val ){
			$str_rank .= '<th name="add" style="text-align:center;">'.$key.'</th><th name="add" style="text-align:center;">'.number_format($val).'円</th></tr>';
			++$row_count;
		}
	}else{
		foreach( $rank_ary as $key => $val ){
			$str_rank .= '<tr name="add"><th style="text-align:center;">'.$key.'</th><th style="text-align:center;">'.number_format($val).'円</th></tr>';
			++$row_count;
		}
	}
	if($str_rank == ''){
		$str_rank = '<th></th><th style="text-align:center;">0円</th></tr>';
		$row_count = 0;
	}
	$str2.= '<tr name="add">
			<th style="text-align:center;" rowspan="'.$row_count.'">'.$s2.'代</th>
			<th style="text-align:center;" rowspan="'.$row_count.'">'.$count2.'人</th>
			<th style="text-align:center;" rowspan="'.$row_count.'">'.$sum2.'</th>
			'.$str_rank;
}

$result_ary[] = $str1;
$result_ary[] = $str2;
echo json_encode($result_ary);
return;
?>