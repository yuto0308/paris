<?php
session_start();
include('db_info.php');

set_time_limit(0);
$zip = new ZipArchive();
$zipFileName = '添付ファイル.zip';
$zipTmpDir = '../../img/zip';
$result = $zip->open($zipTmpDir.$zipFileName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);

if( $result !== true ){
	echo 'error';
	exit();
}

$img_seq = $_GET['img_seq'];
$img_seq = explode(",",$img_seq);
$img_count = count($img_seq) - 1;
for($i = 0;$i < $img_count;$i++){
	$sql = ' SELECT `path` FROM img WHERE seq = '.$img_seq[$i];
	$que = mysqli_query($db_info,$sql) or exit($sql);
	$rs = mysqli_fetch_assoc($que);

	$path = $rs['path'];
	$fname = explode("/",$path);
	$fname = $fname[3];

	$zip->addFile($path,$fname);

}

$zip->close();

header('Content-Type: application/zip; name="' . $zipFileName . '"');
header('Content-Disposition: attachment; filename="' . $zipFileName . '"');
header('Content-Length: '.filesize($zipTmpDir.$zipFileName));
echo file_get_contents($zipTmpDir.$zipFileName);
 
unlink($zipTmpDir.$zipFileName);
exit();
?>