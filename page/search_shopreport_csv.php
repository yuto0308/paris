<?php

	session_start();
	include("db_connect.php");

	$str="";

	if($_GET['useshop'] == 0){
	$shop_name = "1";
	}else{
	$shop_name = 'members.shop_id='.mysqli_real_escape_string($db,$_GET['useshop']);
	}

	$staff_id = mysqli_real_escape_string($db,$_GET['staff_id']);
	if($staff_id == ""){
		$staff_id=1;
	}else{
		$staff_id=' shop_log_sub.staff_id= '.mysqli_real_escape_string($db,$_GET['staff_id']);
	}

	$year_st = mysqli_real_escape_string($db,$_GET['year_st']);
	$month_st = mysqli_real_escape_string($db,$_GET['month_st']);

	if($month_st < 10){//一桁の時にエラーが出るため、先頭に0を付加する
		$month_st = "0".$month_st;
	}
	$day_st = mysqli_real_escape_string($db,$_GET['day_st']);
	if($day_st < 10){
		$day_st = "0".$day_st;
	}
	$year_en = mysqli_real_escape_string($db,$_GET['year_en']);
	$month_en = mysqli_real_escape_string($db,$_GET['month_en']);
	if($month_en < 10){
		$month_en = "0".$month_en;
	}
	$day_en = mysqli_real_escape_string($db,$_GET['day_en']) + 1;//正常にインデックスするようにするため
	if($day_en < 10){
		$day_en = "0".$day_en;
	}

	$dom = mysqli_real_escape_string($db,$_GET['day_or_month']);
	//日別のときは0,月別のときは1
	if($dom == 0){
		$d_o_m=' DATE_FORMAT(date,"%Y-%m-%d") as time, DATE_FORMAT(date,"%w") AS week , DATE_FORMAT(date,"%Y年%m月%d日")  ';
		$str .= "店舗番号,店舗名,日付,曜日,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,返品点数,返品金額,販売金額,売上金額\n";
		$filename = "shop_daily.csv";
	}else{
		$d_o_m=' DATE_FORMAT(date,"%Y-%m") as time, DATE_FORMAT(date,"%Y年%m月")';
		$str .= "店舗番号,店舗名,日付,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,返品点数,返品金額,販売金額,売上金額\n";
		$filename = "shop_monthly.csv";

	}

	$sort = mysqli_real_escape_string($db,$_GET['sort']);
	$sort_str = "";
	//日付順のときは0,売り上げ順の時は1
	if($sort == 1){
		$sort_str = "SUM(sum)";
	}else if($sort == 0){
		$sort_str = "shop_log_sub.date";
	}


	$sql = ' SELECT date,SUM(sum) as sum, SUM(count) as count, COUNT(count) as kensu,'. $d_o_m .' as formattime , SUM(cash) as cash , SUM(card) as card,SUM(discount) as discount,SUM(ticket) as ticket, SUM(payback) as payback,SUM(kinken) as kinken, SUM(case when payback != 0 then 1 else 0 end) as p_count ,members.shop_id AS ms_id, members.name AS mname ';
	$sql .= ' FROM shop_log_sub , members ';
	$sql .= ' WHERE date >= "'.$year_st.'-'.$month_st.'-'.$day_st.'" and date <= "'.$year_en.'-'.$month_en.'-'.$day_en.'"';
	$sql .= ' AND members.shop_id=shop_log_sub.shop_id AND '. $shop_name. ' AND '.$staff_id;
	$sql .= ' group by time  ORDER BY '.$sort_str.' DESC ';

	$recordset=mysqli_query($db, $sql);
		if(!$recordset){
		echo "対象データがありません";
		return;
	}
	if(mysqli_num_rows($recordset) == 0){
		echo "対象データがありません";
		return;
	}

		while($arr_item=mysqli_fetch_assoc($recordset)){
		if($dom == 0){//日別
		if($arr_item['week']==0){
			$week = "日";
		}else if($arr_item['week']==1){
			$week = "月";
		}else if($arr_item['week']==2){
			$week = "火";
		}else if($arr_item['week']==3){
			$week = "水";
		}else if($arr_item['week']==4){
			$week = "木";
		}else if($arr_item['week']==5){
			$week = "金";
		}else if($arr_item['week']==6){
			$week = "土";
		}
		$id = $arr_item['ms_id'];
		$name = $arr_item['mname'];
		$time = $arr_item['formattime'];
		$kensu = $arr_item['kensu'];
		$count_item = $arr_item['count'];
		$cash = $arr_item['cash'];
		$card = $arr_item['card'];
		$kinken = $arr_item['kinken'];
		$discount = $arr_item['discount'];
		$ticket = $arr_item['ticket'];
		$payback = $arr_item['payback'];
		$p_count = $arr_item['p_count'];
		if($arr_item['date'] <= "2014-03-31 23:59:59"){
			$times_temp = 1050;
		}else{
			$times_temp = 1080;
		}
		$sum = $arr_item['sum'] - ($arr_item['ticket'] * $times_temp);
		$general  = $arr_item['sum'];
		//$sum = $arr_item['cash']+$arr_item['card'];
		//$general = $sum + ($ticket * 1080);
		if($_GET['useshop'] == 0){
		$str.=	"全店舗,全店舗,$time,$week,$kensu,$count_item,$cash,$card,$ticket,$kinken,$discount,$p_count,$payback,$general,$sum\n";
			}else{
				$str.=	"$id,$name,$time,$week,$kensu,$count_item,$cash,$card,$ticket,$kinken,$discount,$p_count,$payback,$general,$sum\n";
			}
		}else{//月別
		$id = $arr_item['ms_id'];
		$name = $arr_item['mname'];
		$time = $arr_item['formattime'];
		$kensu = $arr_item['kensu'];
		$count_item = $arr_item['count'];
		$cash = $arr_item['cash'];
		$card = $arr_item['card'];
		$kinken =$arr_item['kinken'];
		$discount = $arr_item['discount'];
		$ticket = $arr_item['ticket'];
		$payback = $arr_item['payback'];
		$p_count = $arr_item['p_count'];
		if($arr_item['date'] <= "2014-03-31 23:59:59"){
			$times_temp = 1050;
		}else{
			$times_temp = 1080;
		}
		$sum = $arr_item['sum'] - ($arr_item['ticket'] * $times_temp);
		$general  = $arr_item['sum'];
		//$sum = $arr_item['cash']+$arr_item['card'];
		//$general = $sum + ($ticket * 1080);
		if($_GET['useshop'] == 0){
		$str.=	"全店舗,全店舗,$time,$kensu,$count_item,$cash,$card,$ticket,$kinken,$discount,$p_count,$payback,$general,$sum\n";
		}else{
			$str.=	"$id,$name,$time,$kensu,$count_item,$cash,$card,$ticket,$kinken,$discount,$p_count,$payback,$general,$sum\n";

		}
				}
}

		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=$filename");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));
		return;

?>