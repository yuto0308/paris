<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php
	include("db_connect.php");
	$post_id = $_GET['id'];
	$sql = "SELECT * FROM guest WHERE guestseq=$post_id ";
	$recordSet = mysqli_query($db, $sql);
	$data = mysqli_fetch_assoc($recordSet);
	$cus_id=$data['id'];
	$sub_id=$data['sub_id'];
	$update = "SELECT DATE_FORMAT(updating,'%Y年%m月%d日 %H:%i:%s') as updating, DATE_FORMAT(created,'%Y年%m月%d日 %H:%i:%s') as created, DATE_FORMAT(last, '%Y年%m月%d日 %H:%i:%s') as last, DATE_FORMAT(member_check_time,'%Y年%m月%d日 %H:%i:%s') as member_check_time FROM guest WHERE guestseq=$post_id";
	$upd = mysqli_query($db, $update);
	$data2 = mysqli_fetch_assoc($upd);
	$last = "SELECT DATE_FORMAT(day,'%Y年%m月%d日 %H:%i:%s') as last FROM shop_log WHERE guest_id=$post_id order by day DESC LIMIT 1";
	$last_query = mysqli_query($db, $last);
	$data3 = mysqli_fetch_assoc($last_query);
?>
<script type="text/javascript">


function kana_check(){
	var kana_check1 = document.submit.f_name_kana.value;
	var kana_check2 = document.submit.g_name_kana.value;
   if( kana_check1.match( /[^ァ-ン　\s]+/ ) ) {
      alert("フリガナは、全角カタカナのみで入力して下さい。");
      return 1;
   }
   if( kana_check1.match( /[^ァ-ン　\s]+/ ) ) {
   	  alert("フリガナは、全角カタカナのみで入力して下さい。");
   	   return 1;
   	}
   	return 0;;
   }

function cus_input(){
	var check = 0
	check += kana_check();
	if(check>0){
		return false;
	}
	var flag = 0;
	if(document.submit.f_name.value == ""){ // スタッフ番号の入力をチェック以下同
		flag = 1;
	}else if(document.submit.g_name.value == ""){
		flag = 1;
	}else if(document.submit.f_name_kana.value == ""){
		flag = 1;
	}else if(document.submit.g_name_kana.value == ""){
		flag = 1;
	}else if(document.submit.tel1.value == ""){
		flag = 1;
	}else if(document.submit.sex.value == "0"){
		flag = 1
	}else if(document.submit.birth_y.value == ""){
        flag = 1;
    }else if(document.submit.birth_m.value == ""){
        flag = 1;
}else if(document.submit.birth_d.value == ""){
	flag = 1;
}

	if(flag>0){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}

    var hankaku = 0
    if(document.submit.cus_id.value.match( /[^0-9]+/)){
    	alert("会員番号は半角で入力して下さい。");
    	return false;
    }
    if(document.submit.tel1.value.match( /[^0-9]+/)){
    	alert("電話番号は半角数字のみで入力して下さい。");
    	return false;
   	}
    if(document.submit.tel2.value.match( /[^0-9]+/)){
    	alert("電話番号は半角数字のみで入力して下さい。");
    	return false;
   	}
   	if(document.submit.fax.value.match( /[^0-9]+/)){
    	alert("ファック番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.zip31.value.match( /[^0-9]+/ )){
    	alert("郵便番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.zip32.value.match( /[^0-9]+/ )){
    	alert("郵便番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.ticket.value.match( /[^0-9^+-]+/ )){
    	alert("保有チケット枚数は半角数字のみで入力して下さい。");
    	return false;
    }

    if(confirm("この内容で登録しますか。") == true){

	var id = $('#cus_id').val();
	var GuestName_kanji =$('#f_name').val();
	var guestname_kanji_first =$('#g_name').val();
	var GuestName_Katakana =$('#f_name_kana').val();
	var guestname_katakana_first =$('#g_name_kana').val();
	var postal_num1 =$('#zip31').val();
	var postal_num2 =$('#zip32').val();
	var prefecture =$('#pref31').val();
	var city =$('#addr31').val();
	var address =$('#add').val();
	var building =$('#build').val();
	var tel1 =$('#tel1').val();
	var tel2 =$('#tel2').val();
	var fax =$('#fax').val();
	var mailaddress =$('#mail').val();
	var sex =$("input:radio[name='sex']:checked").val();
	var birth_y =$('#birth_y').val();
	var birth_m =$('#birth_m').val();
	var birth_d =$('#birth_d').val();
	var mail_mag =$("input:radio[name='mail_mag']:checked").val();
	var dm =$("input:radio[name='dm']:checked").val();
	var motive =$("input:radio[name='motive']:checked").val();
	var tickets =$('#ticket').val();
	var level =$("input:radio[name='level']:checked").val();
	var remarks =$('#memo').val();
	var member_check =$("input:radio[name='member_check']:checked").val();

	$.post('customer_update_do.php?id=<?php print(htmlspecialchars($data["id"]));?>',
		{id:id,
		 GuestName_kanji:GuestName_kanji,
		 guestname_kanji_first:guestname_kanji_first,
		 GuestName_Katakana:GuestName_Katakana,
		 guestname_katakana_first:guestname_katakana_first,
		 postal_num1:postal_num1,
		 postal_num2:postal_num2,
		 prefecture:prefecture,
		 city:city,
		 address:address,
		 building:building,
		 tel1:tel1,
		 tel2:tel2,
		 fax:fax,
		 mailaddress:mailaddress,
		 sex:sex,
		 birth_y:birth_y,
		 birth_m:birth_m,
		 birth_d:birth_d,
		 mail_mag:mail_mag,
		 dm:dm,
		 motive:motive,
		 tickets:tickets,
		 level:level,
		 remarks:remarks,
		 member_check:member_check},
		function(data){
			if(data == 0){
				alert("会員番号の重複を確認して下さい。");
				return;
			}else{
				alert(data);
				document.location = "customerlist.php";}});
}else{
	alert("キャンセルされました。");
}
}

</script>

<div id="pagebodymain">
<h1>
	<div style="text-align:left; float:left;">
		会員の編集</div>
	<div style="text-align:right;"><a href="javascript:history.back();" style="margin-right:10px;">会員データの一覧へ</a></div>
</h1>

<form action="#" name="submit" method="post">
<table id="cus_new">
	<tr>
		<th class="b" colspan="2">
			会員の情報</th>
	</tr>
	<tr>
		<th class="a">会員番号</th>
		<?php
		if($data['sub_id']==""){
			echo 	'<th>
				<span id="input_box">
					<input type="hidden" id="cus_id2" />
					<input name="cus_id" id="cus_id" type="text" size="20" maxlength="20" value=
					"'.$cus_id.'" disabled="disabled" /></br>
				</span>
					</th>';
		}else{
			echo 	'<th>
				<span id="input_box">
					<input name="cus_id" id="cus_id2" type="text" size="10" maxlength="10" value=
					"'.$sub_id.'" disabled="disabled"/> - 
					<input name="cus_id" id="cus_id" type="text" size="20" maxlength="20" value=
					"'.$cus_id.'" disabled="disabled"/></br>
				</span>
					</th>';
		}
		?></th>
	</tr>
	<tr>
		<th class="a">会員名<font color="red">(*)</font></th>
		<th>姓 &nbsp;<input type="text" name="f_name" id="f_name" size="20" value=
			"<?php print(htmlspecialchars($data['GuestName_kanji'], ENT_QUOTES)); ?>"/>
			名 &nbsp;<input type="text" name="g_name" id="g_name" size="20" value=
			"<?php print(htmlspecialchars($data['guestname_kanji_first'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th class="a">フリガナ<font color="red">(*)</font></th>
		<th>セイ<input type="text" name="f_name_kana" id="f_name_kana" size="20" value=
		     "<?php print(htmlspecialchars($data['GuestName_Katakana'], ENT_QUOTES)); ?>" onblur="kana_check();"/>
			メイ<input type="text" name="g_name_kaba" id="g_name_kana" size="20" value=
			 "<?php print(htmlspecialchars($data['guestname_katakana_first'], ENT_QUOTES)); ?>" onblur="kana_check();"/></th>
	</tr>
	<tr>
		<th class="a" rowspan="5">住所</th>
		<th>郵便番号 <input type="text" id="zip31" name="zip31" size="4" maxlength="3" value=
			"<?php print(htmlspecialchars($data['postal_num1'], ENT_QUOTES)); ?>"/> －<input type="text" id="zip32" name="zip32" size="5" maxlength="4" value=
			"<?php print(htmlspecialchars($data['postal_num2'], ENT_QUOTES)); ?>" onKeyUp="AjaxZip3.zip2addr('zip31','zip32','pref31','addr31','addr31');" /> (半角数字で入力)</th>
	</tr>
	<tr>
		<th>都道府県 <input type="text" name="pref31" id="pref31" size="20" value=
			"<?php print(htmlspecialchars($data['prefecture'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th>市区町村 <input type="text" name="addr31" id="addr31" size="40" value=
			"<?php print(htmlspecialchars($data['city'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th>番地等 &nbsp;&nbsp;&nbsp;<input type="text" name="add" id="add" size="40" value=
			"<?php print(htmlspecialchars($data['address'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th>建物名等&nbsp;<input type="text" name="build" id="build" size="40" value=
			"<?php print(htmlspecialchars($data['building'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th class="a">電話番号1<font color="red">(*)</font></th>
		<th><input type="text" name="tel1" id="tel1" size="40" value=
			"<?php print(htmlspecialchars($data['tel1'], ENT_QUOTES)); ?>"/> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a">電話番号2</th>
		<th><input type="text" name="tel2" id="tel2" size="40" value=
			"<?php print(htmlspecialchars($data['tel2'], ENT_QUOTES)); ?>"/> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a">ファックス番号</th>
		<th><input type="text" name="fax" id="fax" size="40" value=
			"<?php print(htmlspecialchars($data['fax'], ENT_QUOTES)); ?>"/> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a">メールアドレス</th>
		<th><input type="text" name="mail" id="mail" size="40" value=
			"<?php print(htmlspecialchars($data['mailaddress'], ENT_QUOTES)); ?>"/></th>
	</tr>
	<tr>
		<th class="a">性別<font color="red">(*)</font></th>
		<th>
			<input type="radio" name="sex" id="sex_non" value="0" <?php if($data['sex'] == 0){print('checked="checked"');} ?>/><label for="sex_non">未入力</label>
			<input type="radio" name="sex" id="sex_male" value="1" <?php if($data['sex'] == 1){print('checked="checked"');} ?>/><label for="sex_male">男性</label>
			<input type="radio" name="sex" id="sex_female" value="2" <?php if($data['sex'] == 2){print('checked="checked"');} ?>/><label for="sex_female">女性</label>
		</th>
	</tr>
	<tr>
		<th class="a">生年月日<font color="red">(*)</font></th>
		<th>
			<select id="birth_y" name="birth_y">
				<?php
				if($data['birth_y'] != ""){
				$y = $data['birth_y'];
				}
				$now = date("Y");
				for($i = 1900; $i <= $now; $i++):?>
				<option value="<?php echo $i;?>" <?php if($i == $y){echo 'selected="selected"';} ?>><?php echo $i;?></option>
			<?php endfor;?>
		</select>年

		<select id="birth_m" name="birth_m">
				<?php
				if($data['birth_m'] != ""){
				$m = $data['birth_m'];
				}
				for($i = 1; $i <= 12; $i++):?>
				<option value="<?php echo $i;?>" <?php if($i == $m){echo 'selected="selected"';} ?>><?php echo $i;?></option>
		<?php endfor;?>
	</select>月

		<select id="birth_d" name="birth_d">
				<?php
				if($data['birth_d'] != ""){
				$y = $data['birth_d'];
				}
				for($i = 1; $i <= 31; $i++):?>
				<option value="<?php echo $i;?>" <?php if($i == $y){echo 'selected="selected"';} ?>><?php echo $i;?></option>
		<?php endfor;?>
	</select>日

		</th>
	</tr>
	<tr>
		<th class="a">メルマガ</th>
		<th><input type="radio" name="mail_mag" id="mail_mag_off" value="0" <?php if($data['mail_mag'] == 0){print('checked="checked"');} ?>/><label for="mail_mag_off">未登録</label>
			<input type="radio" name="mail_mag" id="mail_mag_on" value="1" <?php if($data['mail_mag'] == 1){print('checked="checked"');} ?>/><label for="mail_mag_on">登録済</label></th>
	</tr>
	<tr>
		<th class="a">DM</th>
		<th><input type="radio" name="dm" id="dm_off" value="0" <?php if($data['dm'] == 0){print('checked="checked"');} ?>/><label for="dm_off">配信しない</label>
			<input type="radio" name="dm" id="dm_on" value="1" <?php if($data['dm'] == 1){print('checked="checked"');} ?>/>
			<label for="dm_on">配信する</label></th>
	</tr>
	<tr>
		<th class="a">来店動機</th>
		<th><input type="radio" name="motive" id="mot1" value="0" <?php if($data['motive'] == 0){print('checked="checked"');} ?> /><label for="mot1">不明</label>
			<input type="radio" name="motive" id="mot2" value="1" <?php if($data['motive'] == 1){print('checked="checked"');} ?> /><label for="mot2">立寄</label>
			<input type="radio" name="motive" id="mot3" value="2" <?php if($data['motive'] == 2){print('checked="checked"');} ?> /><label for="mot3">紹介</label>
			<input type="radio" name="motive" id="mot4" value="3" <?php if($data['motive'] == 3){print('checked="checked"');} ?> /><label for="mot4">DM</label>
			<input type="radio" name="motive" id="mot5" value="4" <?php if($data['motive'] == 4){print('checked="checked"');} ?> /><label for="mot5">HP/blog</label>
			<input type="radio" name="motive" id="mot6" value="5" <?php if($data['motive'] == 5){print('checked="checked"');} ?> /><label for="mot6">広告</label>
			<input type="radio" name="motive" id="mot7" value="6" <?php if($data['motive'] == 6){print('checked="checked"');} ?> /><label for="mot7">Fﾍﾟｰﾊﾟｰ</label>
			<input type="radio" name="motive" id="mot8" value="7" <?php if($data['motive'] == 7){print('checked="checked"');} ?> /><label for="mot8">チラシ</label>
			<input type="radio" name="motive" id="mot9" value="8" <?php if($data['motive'] == 8){print('checked="checked"');} ?> /><label for="mot9">ｷｬｯﾁ</label>
			<input type="radio" name="motive" id="mot10" value="9" <?php if($data['motive'] == 9){print('checked="checked"');} ?> /><label for="mot10">その他</label>
		</th>
	</tr>
	<tr>
		<th class="a">保有チケット</th>
		<th><input type="text" name="ticket" id="ticket" value="<?php print($data['Tickets']); ?>"/>枚</th>
	</tr>
	<tr>
		<th class="a">会員レベル</th>
		<th>
		<input type="radio" name="level" id="lev1" value="0" <?php if($data['level'] == 0){print('checked="checked"');} ?> /><label for="lev1">レベル1</label>
		<input type="radio" name="level" id="lev2" value="1" <?php if($data['level'] == 1){print('checked="checked"');} ?>/><label for="lev2">レベル2</label>
		<input type="radio" name="level" id="lev3" value="2" <?php if($data['level'] == 2){print('checked="checked"');} ?>/><label for="lev3">レベル3</label>
		<input type="radio" name="level" id="lev4" value="3" <?php if($data['level'] == 3){print('checked="checked"');} ?>/><label for="lev4">レベル4</label>
		<input type="radio" name="level" id="lev5" value="4" <?php if($data['level'] == 4){print('checked="checked"');} ?>/><label for="lev5">レベル5</label></th>
	</tr>
	<tr>
		<th class="a">運営者用のメモ</th>
		<th><textarea name="memo" id="memo" rows="5" cols="70" ><?php print($data['remarks']); ?></textarea></th>
	</tr>
	<tr>
		<th class="a">退会処理</th>
		<th><input type="radio" name="member_check" id="member" value="0" <?php if($data['member_check'] == 0){print('checked="checked"');} ?> /><label for="member">会員登録中</label>
			<input type="radio" name="member_check" id="non_member" value="1" <?php if($data['member_check'] == 1){print('checked="checked"');} ?> /><label for="non_member">退会中</label></th>
	</tr>
</table>
</form>
<p>
<table>
	<tr>
		<th class="b" colspan="4">利用状況</th>
	</tr>
	<tr>
		<th class="a">登録日時</th>
		<th><?php echo $data2['created']; ?></th>
		<th class="a">更新日時</th>
		<th>
		<?php echo $data2['updating']; ?>
        </th>
	</tr>
	<tr>
		<th class="a">最終購入日時</th>
		<th><?php echo $data3['last']; ?></th>
		<th class="a">退会日時</th>
		<th><?php if($data['member_check'] == 1){print($data2['member_check_time']);} ?></th>
	</tr>
	<tr>
		<th class="a">利用回数</th>
		<th><?php echo $data['use_times']; ?>回</th>
		<th class="a">購入数量</th>
		<th><?php echo $data['quantity']; ?>点</th>
	</tr>
	<tr>
		<th class="a">購入金額</th>
		<th><?php echo number_format($data['price']); ?>円</th>
		<th class="a">使用チケット</th>
		<th><?php echo $data['use_ticket']; ?>枚</th>
	</tr>
</table>
<p>
<table>
	<tr>
		<th class="b" colspan="5">
			利用状況</th>
	</tr>
	<tr>
		<th class="a" style="text-align:center;">レジ番号/日時</th>
		<th class="a" style="text-align:center;">購入点数</th>
		<th class="a" style="text-align:center;">決済方法</th>
		<th class="a" style="text-align:center;">金額</th>
		<th class="a" style="text-align:center;">状況</th>
	</tr>
	<?php
	$guest_id = htmlspecialchars($data['guestseq'], ENT_QUOTES);
	$sql_customer_log = 'SELECT *,DATE_FORMAT(date,"%Y-%m-%d<br>%k:%i:%s") as log FROM shop_log_sub WHERE guest_id = '.$guest_id.' ORDER BY date DESC';
	$rs_customer_log = mysqli_query($db,$sql_customer_log);	
		while($data_customer_log = mysqli_fetch_assoc($rs_customer_log)){
		$cash_or_card = "未選択";
			if($data_customer_log['cashorcard'] == 1){
				$cash_or_card = "現金";
			}else if($data_customer_log['cashorcard'] == 2){
				$cash_or_card = "カード";
			}else if($data_customer_log['cashorcard'] == 3){
				$cash_or_card = "現金＋カード";
			}
		if($data_customer_log['date']<="2014-03-31 23:59:59"){
			$ticket_temp = 1050;
		}else{
			$ticket_temp = 1080;
		}
		$nonticket=$data_customer_log['sum']-($data_customer_log['ticket'] * $ticket_temp);
		$nonticket=number_format($nonticket);
			echo '<tr rowspan="2">
		<th style="text-align:left;" rowspan="2"><a href="reg_details.php?reg_id1='.$data_customer_log['reg_id1'].'&amp;reg_id2='.$data_customer_log['reg_id2'].'&amp;shop='.$data_customer_log['shop_id'].'">'.$data_customer_log['reg_id1'].'-'.str_pad($data_customer_log['reg_id2'],5,"0",STR_PAD_LEFT).'</a>'.'<br>'.$data_customer_log['date'].'</th>
		<th style="text-align:center;">'.$data_customer_log['count'].'点</th>
		<th style="text-align:center;">'.$cash_or_card.'</th>
		<th style="text-align:center;">'.$nonticket.'円</th>
		<th style="text-align:center;">注文確定</th>
	</tr>
	<tr>
		<th class="a" style="text-align:center;">備考</th><th colspan = "3" style="text-align:center;"><textarea rows="4" cols = "60" readonly>'.$data_customer_log['bikou'].'</textarea></th>
		</tr>

	';
		}
	?>
</table>
<p>
<font color="red">(*)は必須項目です。</font>
<p>
<p style="text-align:center">
<input type="image" src="../css/image/contents/view.gif" onclick="return cus_input();"/>



</div>
<?php include("footer.php"); ?>
