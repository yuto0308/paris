<?php

	session_start();
	include("db_connect.php");

	$str="";
	$str_button="";
	$result_ary=array();

	$maxrow = 10;
	$num=$_POST['num'];
	$show = ($num - 1) * $maxrow;

	$dom=$_POST['dom'];

	if($_POST['useshop'] == 0){
	$useshop = 1;
	}else{
	$useshop = ' shop_log_sub.shop_id= '.$_POST['useshop'] ;
	}

	if($_POST['staffid']==""){
	$staffid = 1;
	}else{
	$staffid = ' staff.id= "'.$_POST['staffid'].'"';
	}

	$year_s		= $_POST['year_s'];
	if($_POST['month_s']<10){
		$month_s ='0'.$_POST['month_s'];
	}else{
		$month_s = $_POST['month_s'];
	}

	if($_POST['day_s']<10){
		$day_s		='0'.$_POST['day_s'];
	}else{
		$day_s = $_POST['day_s'];
	}

	$year_e		= $_POST['year_e'];

	if($_POST['month_e']<10){
		$month_e ='0'.$_POST['month_e'];
	}else{
		$month_e	= $_POST['month_e'];
	}
	if($_POST['day_e']<10){
		$day_e		='0'.$_POST['day_e'];
	}else{
		$day_e		=$_POST['day_e'];
	}
	$day_e=$day_e + 1;

	if($dom==1){
	$select_dom = ' DATE_FORMAT(date, "%Y年%m月%d日") as datetime, DATE_FORMAT(date, "%a") as week, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-'.$day_s.'" AND date <= "'.$year_e.'-'.$month_e.'-'.$day_e. '"';
	}else{
	$select_dom = ' DATE_FORMAT(date, "%Y年%m月") as datetime, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-'.$day_s.'" AND date <= "'.$year_e.'-'.$month_e.'-'.$day_e.'" ';
	}

	$sql = ' SELECT staff.name, '.$select_dom;
	$sql.= ' SUM(count) as sum_item, COUNT(count) as count_item, ';
	$sql.= ' SUM(sum) as sum, SUM(ticket) as ticket, staff.id, ';
	$sql.=' SUM(ticket * CASE WHEN date <= "2014-03-31 23:59:59" THEN 1050
								  WHEN date >= "2014-04-01 00:00:00" THEN 1080
								  END
					) as ticketsum ';
	$sql.= ' FROM staff,shop_log_sub ';
	$sql.= ' WHERE shop_log_sub.staff_id=staff.id AND shop_log_sub.shop_id=staff.shop_id ';
	//$sql.= ' AND '
	$sql.= ' AND '. $useshop. ' AND '. $staffid. ' AND '.$date ;
	$sql.= ' GROUP BY datetime, shop_log_sub.shop_id,shop_log_sub.staff_id ';

	$sql_count = $sql;
	$rs_count=mysqli_query($db, $sql_count);
	$count = mysqli_num_rows($rs_count);

	$sql.= ' ORDER BY datetime DESC ';
	$sql.= ' LIMIT '.$show.' , '.$maxrow ;
	$recordset = mysqli_query($db, $sql);

		if($dom==1){
		$str.=	'
				<tr name="add">
				<th class="a" style="text-align:center;">年月日</th>
				<th class="a" style="text-align:center;">曜日</th>
				<th class="a" style="text-align:center;">スタッフ番号</th>
				<th class="a" style="text-align:center;">スタッフ名</th>
				<th class="a" style="text-align:center;">件数</th>
				<th class="a" style="text-align:center;">点数</th>
				<th class="a" style="text-align:center;">販売金額</th>
				<th class="a" style="text-align:center;">売上金額</th>
				</tr>
				';
		while ($table = mysqli_fetch_assoc($recordset)){
			$datetime=$table['datetime'];
			$week=$table['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
				}
			$staffname=$table['name'];
			$staffid=$table['id'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$sum=$table['sum'];
			$non_ticket=$table['sum']-$table['ticketsum'];
			$sum=number_format($sum);
			$non_ticket=number_format($non_ticket);
			$str.='<tr name="add">
					<th style="text-align:center;">'.$datetime.'</th>
					<th style="text-align:center;">'.$week_str.'</th>
					<th style="text-align:center;">'.$staffid.'</th>
					<th style="text-align:center;">'.$staffname.'</th>
					<th style="text-align:center;">'.$count_item.'</th>
					<th style="text-align:center;">'.$sum_item.'</th>
					<th style="text-align:center;">'.$sum.'</th>
					<th style="text-align:center;">'.$non_ticket.'</th>
					</tr>';
			}
			if($count==""){
			$str.='<tr name="add">
					<th colspan="8" style="text-align:center;">対象データがありません</th>
					</tr>';
			}
		}else{
			$str.=	'
					<tr name="add">
					<th class="a" style="text-align:center;">年月</th>
					<th class="a" style="text-align:center;">スタッフ番号</th>
					<th class="a" style="text-align:center;">スタッフ名</th>
					<th class="a" style="text-align:center;">件数</th>
					<th class="a" style="text-align:center;">点数</th>
					<th class="a" style="text-align:center;">販売金額</th>
					<th class="a" style="text-align:center;">売上金額</th>
					</tr>
					';
			while($table = mysqli_fetch_assoc($recordset)){
			$datetime =$table['datetime'];
			$staffname=$table['name'];
			$staffid=$table['id'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$sum=$table['sum'];
			$non_ticket=$table['sum']-$table['ticketsum'];
			$sum=number_format($sum);
			$non_ticket=number_format($non_ticket);
			$str.='<tr name="add">
					<th style="text-align:center;">'.$datetime.'</th>
					<th style="text-align:center;">'.$staffid.'</th>
					<th style="text-align:center;">'.$staffname.'</th>
					<th style="text-align:center;">'.$count_item.'</th>
					<th style="text-align:center;">'.$sum_item.'</th>
					<th style="text-align:center;">'.$sum.'</th>
					<th style="text-align:center;">'.$non_ticket.'</th>
					</tr>';
			}
			if($count==""){
			$str.='<tr name="add">
					<th colspan="7" style="text-align:center;">対象データがありません</th>
					</tr>';
			}
		}

//-----------------ボタン
		$button_num = floor(($count - 1 )/ $maxrow) + 1;

		if($button_num <= 12){
			for($i = 1;$i<=$button_num;$i++){
				if($i == $num){
					$str_button .= '<input type="button" style="color:blue;font-size:large;"';
					$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}else{
					$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}
			}
		}else{
			if($num - 1 <= 5){
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($num - 1) + 5) && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else if($button_num - $num <= 5){
					$front_reader = 0;
					$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
					if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($button_num - $num) + 5) && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else{
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}	
		}
//ここまで

		$result_ary[] = $str;
		$result_ary[] = $str_button;
		echo json_encode($result_ary);


	?>