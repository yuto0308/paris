<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
	<link rel="stylesheet" href="../css/print_css.css">
</head>
<body>
<div id="cont">
<?php
session_start();
include("db_connect.php");
$result=$_GET['result'];
$result_pieces=explode(",", $result);
$str="";
$cnt=$_GET['tkd'];
$shpnm=$_GET['shopnum'];
if($shpnm==0){
	$shpnm_str=' 1 ';
}else{
	$shpnm_str=' shop_id='.$shpnm;
}

for($i=0;$i<$cnt;$i++){
//POS_FINISH文SQL
$sql_fin =' SELECT DATE_FORMAT(start,"%Y年%m月%d日") as start FROM pos_finish WHERE DATE_FORMAT(datetime,"%Y年%m月%d日") = '.$result_pieces[$i].' AND '.$shpnm_str;
$sql_fin.=' ORDER BY DATE_FORMAT(datetime,"%Y年%m月%d日") DESC ';
$rs_fin=mysqli_query($db,$sql_fin);
$fin=mysqli_fetch_assoc($rs_fin);
	$start=$fin['start'];



//SHOP_LOG_SUB分SQL文
$sql =' SELECT DATE_FORMAT(date,"%Y-%m-%d") as formatdate, SUM(card) as card, ';
$sql.=' DATE_FORMAT(date,"%Y年%m月%d日") as day, ';
$sql.=' SUM(sum) as sum,SUM(cash) as cash, COUNT(count) as count_count, SUM(count) as sum_count, ';
$sql.=' SUM(ticket) as ticket, SUM(tax) as tax, ';
$sql.=' SUM(kinken) as kinken, ';
$sql.=' COUNT(cashorcard = 2 or null) as count_card, COUNT(cashorcard = 1 or null) as count_cash, ';
$sql.=' COUNT(cashorcard = 3 or null) as count_both, ';
$sql.=' COUNT(kinken_type = 1 or null) count_kinken1,COUNT(kinken_type = 2 or null) as count_kinken2, ';
$sql.=' SUM(count_payback) as sum_countback, SUM(payback) as sum_back, ';
$sql.=' SUM(CASE WHEN discount>0 THEN 1 ELSE 0 END) as count_discount,SUM(discount) as sum_discount ';
$sql.=' FROM shop_log_sub WHERE DATE_FORMAT(date,"%Y年%m月%d日") = '.$result_pieces[$i];
$sql.=' AND '.$shpnm_str;
$sql.=' GROUP BY formatdate ORDER BY formatdate DESC';
$rs=mysqli_query($db,$sql);
$data=mysqli_fetch_assoc($rs);
	$card=$data['card'];
	$cash=$data['cash'];
	$day=$data['day'];
	$count=$data['count_count'];
	$sum_discount=$data['sum_discount'];
	$count_discount=$data['count_discount'];
	$sum_tax=$data['tax'];
	$kinken=$data['kinken'];
	$ticket_tax=$data['ticket']*80;
	$sum_tax=$sum_tax-$ticket_tax;
	$sum_count=$data['sum_count'];
	$sum_ticket=$data['ticket'];
	$count_card=$data['count_card'];
	$count_cash=$data['count_cash'];
	$count_both=$data['count_both'];
	$count_kinken=$data['count_kinken1']+$data['count_kinken2'];
	$count_card=$count_card+$count_both;
	$count_cash=$count_cash+$count_both;
	$sum_countback=$data['sum_countback'];
	$sum_back=$data['sum_back'];
	$sum=$data['sum']-($data['ticket']*1080);
	$sum=number_format($sum);
	$card=number_format($card);
	$cash=number_format($cash);
	$sum_tax=number_format($sum_tax);
	$sum_discount=number_format($sum_discount);
	$sum_back=number_format($sum_back);

//SHOP_LOG_SUB月分SQL
$sql_mon =' SELECT DATE_FORMAT(date,"%Y-%m") as month,DATE_FORMAT(date,"%Y年%m月") as show_month, ';
$sql_mon.=' SUM(card) as mon_card, SUM(cash) as mon_cash,SUM(kinken) as mon_kinken ';
$sql_mon.=' FROM shop_log_sub ';
$sql_mon.=' WHERE DATE_FORMAT(date,"%Y-%m") IN ';
$sql_mon.=' ( SELECT DISTINCT DATE_FORMAT(date,"%Y-%m") as month ';
$sql_mon.=' FROM shop_log_sub ';
$sql_mon.=' WHERE DATE_FORMAT(date,"%Y年%m月%d日") = '.$result_pieces[$i].' ) ';
$sql_mon.=' AND '.$shpnm_str;
$sql_mon.=' GROUP BY month DESC';

$rs_mon=mysqli_query($db,$sql_mon);
$show_month=mysqli_fetch_assoc($rs_mon);
	$show_mon=$show_month['show_month'];
	$mon_card=$show_month['mon_card'];
	$mon_cash=$show_month['mon_cash'];
	$mon_kinken=$show_month['mon_kinken'];
	$mon_sum=$mon_card+$mon_cash+$mon_kinken;
	$mon_sum=number_format($mon_sum);



	$str .='<table id="table"><tbody><tr><td style="text-align:center;" colspan="2">
			<font size="4">売上日報レシート</font>
			</td></tr></tbody></table>
		<p>Paris de skin '.$_SESSION['name'].'様</p>
		'.date('Y/m/d H:i:s').'</br>
		───────────────────</br>
		<table id="table">
		<tbody>
		<tr><td>
		<font size="3">【集計期間】</font>
		</td></tr>';
	$str.='<tr><td>'.$start.' </tr></td>
		</tbody></table></br>
		───────────────────</br>';
	$str.='<table id="table"><tbody>
		<tr><td>
		<font size="4"><strong>【売上金額】</strong></font>
		</td>';
	$str.='<td style="text-align:right;""><font size="4"><strong>￥'.$sum.'
		</strong></font></td>
		</tr></tbody></table>';
	$str.='───────────────────</br>';
	$str.='<table id="table"><tbody><tr>
		<td><font size=3>【売上明細】</font></td>';
	$str.='<tr><td>販売件数</td>';
	$str.='<td style="text-align:right;">'.$count.'件</td></tr>';
	$str.='<tr><td>販売商品数</td>';
	$str.='<td style="text-align:right;">'.$sum_count.'件</td></tr>';
	$str.='<tr><td>販売金額</td>';
	$str.='<td style="text-align:right;">￥'.$sum.'</td></tr>';
	$str.='</tbody></table>';
	$str.='-----------------------------------</br>';
	$str.='<table id="table"><tbody>';
	$str.='<tr><td>現金</td>';
	$str.='<td style="text-align:right;">￥'.$cash.'</td></tr>';
	$str.='<tr><td></td><td style="text-align:right;">';
	$str.=$count_cash.'件</td><tr>';
	$str.='<tr><td>';
	$str.='クレジット</td>';
	$str.='<td style="text-align:right;">￥'.$card.'</td></tr>';
	$str.='<tr><td></td><td style="text-align:right;">';
	$str.=$count_card.'件</td></tr>';
	$str.='<tr><td>';
	$str.='商品券</td>';
	$str.='<td style="text-align:right;">￥'.$kinken.'</td></tr>';
	$str.='<tr><td></td><td style="text-align:right;">';
	$str.=$count_kinken.'件</td></tr>';
	$str.='</tbody></table>';
	$str.='-----------------------------------</br>';
	$str.='<table id="table"><tbody><tr><td>';
	$str.='使用チケット</td>';
	$str.='<td style="text-align:right;">'.$sum_ticket.'枚</td></tr></tbody></table>';
	$str.='───────────────────</br>';
	$str.='<table id="table"><tbody><tr><td>';
	$str.='<font size="3">【消費税】</font></td></tr>';
	$str.='<tr><td></td>';
	$str.='<td style="text-align:right;">￥'.$sum_tax.'</td></tr>';
	$str.='</tbody></table>';
	$str.='───────────────────</br>';
	$str.='<table id="table"><tbody><tr><td>';
	$str.='<font size="3">【割引】</font></td></tr>';
	$str.='<tr><td>割引件数</td>';
	$str.='<td style="text-align:right;">'.$count_discount.'件</td></tr>';
	$str.='<tr><td>割引金額</td>';
	$str.='<td style="text-align:right;">▲'.$sum_discount.'円</td></tr>';
	$str.='</tbody></table>';
	$str.='───────────────────</br>';
	$str.='<table id="table"><tbody><tr><td>';
	$str.='<font size="3">【返品】</font></td></tr>';
	$str.='<tr><td>返品点数</td>';
	$str.='<td style="text-align:right;">'.$sum_countback.'点</td></tr>';
	$str.='<tr><td>返品金額</td>';
	$str.='<td style="text-align:right;">▲'.$sum_back.'</td></tr>';
	$str.='</tbody></table>';
	$str.='───────────────────</br>';
	$str.='<table id="table"><tbody><tr><td>';
	$str.='<font size="3">【'.$show_mon.'の累計売上】</font></td><tr>';
	$str.='<tr><td style="text-align:right;">';
	$str.='<font size="3"><strong>￥'.$mon_sum.'</strong></font></td></tr>';
	$str.='</tbody></talbe>';
	$str.='<p>';
}
echo $str;
?>
</div>
</body>
</html>