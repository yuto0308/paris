<?php 
/*
$ary=array();
$ary[]="1";
$ary[]="2";
$ary[]="3";

echo json_encode($ary);

*/
	session_start();
	include("db_connect.php"); 
	$maxrow = 10;//一度に表示する行数
	$str="";
	$str_button="";
	//result_ary : １つ目に行表示HTML
	//			 : ２つめに該当会員数
	//			 : ３つめにインデックスボタン
	$result_ary=array();
	if(!$_POST['num']){
		$num = 1;
	}else{
		$num=$_POST['num'];
	}
	$show = ($num - 1) * $maxrow;
	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順
 	$sort_info=$_POST['sort_info'];
 	if($sort_info=="0"){
	 	$sort_str="guest.created DESC";
 	}else if($sort_info=="1"){
	 	$sort_str="guest.use_times DESC";
 	}else if($sort_info=="2"){
 		$sort_str="guest.quantity DESC";
 	}else if($sort_info=="3"){
	 	$sort_str="guest.price DESC";
 	}

 	$key_ctgr=$_POST['key_ctgr'];
 	if($key_ctgr == "0"){
 		$ctgr = "1";
 	}else if($key_ctgr == "1"){
	 	$ctgr = "goods.category_big = 1";
 	}else if($key_ctgr == "2"){
	 	$ctgr = "goods.category_big = 7";
 	}else if($key_ctgr == "3"){
	 	$ctgr = "goods.category_big = 6";
 	}else if($key_ctgr == "4"){
	 	$ctgr = "goods.category_big = 10";
 	}else if($key_ctgr == "5"){
	 	$ctgr = "1";
 	}

 	$key_visit_times = $_POST['key_visit_times'];
 	if($key_visit_times == "0"){
 		$times = "1";
 	}else if($key_visit_times == "1"){
 		$times = "guest.use_times = 1";
 	}else if($key_visit_times == "2"){
 		$times = "guest.use_times = 2";
 	}else if($key_visit_times == "3"){
 		$times = "guest.use_times = 3";
 	}else if($key_visit_times == "4"){
 		$times = "guest.use_times = 4";
 	}else if($key_visit_times == "5"){
 		$times = "guest.use_times >= 5";
 	}

 	$key_year_s=$_POST['key_year_s'];
 	if($key_year_s == 0){
 		$key_year_s = '0000';
 	}
 	$key_month_s=$_POST['key_month_s'];
 	$key_day_s=$_POST['key_day_s'];
 	$key_year_e=$_POST['key_year_e'];
 	if($key_year_e == 0){
 		$key_year_e = '0000';
 	}
 	$key_month_e=$_POST['key_month_e'];
 	$key_day_e=$_POST['key_day_e'];

 	if($key_month_s < 10){
 		$key_month_s= '0'.$key_month_s;
 	}
 	if($key_day_s < 10){
 		$key_day_s ='0'.$key_day_s;
 	}

 	if($key_month_e < 10){
 		$key_month_e = '0'.$key_month_e;
 	}

	if($key_day_e < 10){
		$key_day_e = '0'.$key_day_e;
	}

	$key_datetime1 = $key_year_s.'-'.$key_month_s.'-'.$key_day_s.' 00:00:00';
	$key_datetime2 = $key_year_e.'-'.$key_month_e.'-'.$key_day_e.' 23:59:59';
	
	
	$sql  = " SELECT guest.guestseq,guest.id,fullname_kanji,guest.use_times,guest.quantity,guest.price ";
	$sql .= " FROM guest,shop_log_sub,shop_log,goods ";
	$sql .= ' WHERE '.$ctgr.' AND '.$times.' AND shop_log_sub.date >= "'.$key_datetime1.'" and shop_log_sub.date <= "'.$key_datetime2.'" ';
	$sql .= ' AND guest.shop_id='.$shop_id.' AND shop_log_sub.shop_id = '.$shop_id.' AND shop_log.shop_id = '.$shop_id;
	$sql .= ' AND goods.id = shop_log.goods_id ';
	$sql .= ' AND guest.guestseq = shop_log.guest_id AND guest.guestseq = shop_log_sub.guest_id AND shop_log.guest_id = shop_log_sub.guest_id ';
	$sql .= ' AND shop_log.reg_id1 = shop_log_sub.reg_id1 AND shop_log.reg_id2 = shop_log_sub.reg_id2 ';
	$sql .= ' GROUP BY guestseq ';
	$sql .= " order by $sort_str ";
	$sql .= " LIMIT $show,$maxrow";
	$rs = mysqli_query($db,$sql) or exit($sql);
	$sql_count  = " SELECT guest.guestseq,guest.id,fullname_kanji,guest.use_times,guest.quantity,guest.price ";
	$sql_count .= " FROM guest,shop_log_sub,shop_log,goods ";
	$sql_count .= ' WHERE '.$ctgr.' and '.$times.' and guest.last >= "'.$key_datetime1.'" and guest.last <= "'.$key_datetime2.'" ';
	$sql_count .= ' AND guest.shop_id='.$shop_id.' AND shop_log_sub.shop_id = '.$shop_id.' AND shop_log.shop_id = '.$shop_id;
	$sql_count .= ' AND goods.id = shop_log.goods_id ';
	$sql_count .= ' AND guest.guestseq = shop_log.guest_id AND guest.guestseq = shop_log_sub.guest_id AND shop_log.guest_id = shop_log_sub.guest_id ';
	$sql_count .= ' AND shop_log.reg_id1 = shop_log_sub.reg_id1 AND shop_log.reg_id2 = shop_log_sub.reg_id2 ';
	$rs_count = mysqli_query($db,$sql_count) or exit($sql_count);
	if(!$rs){
		die('クエリ失敗 連絡をおねがいします');
			}
	$num_guest = mysqli_num_rows($rs_count);
	while($arr_item = mysqli_fetch_assoc($rs)){
			$guestseq=$arr_item['guestseq'];
			$id=$arr_item['id'];
			$name = $arr_item['fullname_kanji']; 
			$use_times = $arr_item['use_times'];
			$use_times = number_format($use_times);
			$quantity = $arr_item['quantity'];
			$quantity = number_format($quantity);
			$price = $arr_item['price'];
			$price = number_format($price);
		$str .= '<tr name="add">
		<th style="text-align:center;"><a href="customer_update.php?id='.$id.'&seq='.$guestseq.'">'.$name.'</a></th>
		<th style="text-align:center;">'.$use_times.'</th>
		<th style="text-align:center;">'.$quantity.'</th>
		<th style="text-align:center;">'.$price.'</th>
		</tr>';
			}
		if($str==""){
				$str .= '<tr name="add"><th colspan="9" style="text-align:center;">対象データがありません</th></tr>';
			}
		
		
		/*ここからボタン追加処理*/
		$button_num = floor(($num_guest -1 )/ $maxrow) + 1;
		if($button_num <= 12){
			for($i = 1;$i<=$button_num;$i++){
				if($i == $num){
					$str_button .= '<input type="button" style="color:blue;font-size:large;"';
					$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}else{
					$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}
			}
		}else{
			if($num - 1 <= 5){
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($num - 1) + 5) && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else if($button_num - $num <= 5){
					$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
					if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($button_num - $num) + 5) && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else{
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}	
		}

		/*ここまでボタン追加処理*/
		
		$result_ary[] = $str;
		$result_ary[] = $num_guest;
		$result_ary[] = $str_button;
		$result_ary[] = $num;
		echo json_encode($result_ary);

?>