<!DOCTYPE html>
<html lang="ja">
<head>
<?php session_start(); ?>
 	<?php
	if($_SESSION['id'] == ""){
	header('location: logout.php');
	}
	?>
<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<link rel="stylesheet" href="../css/style-posfront.css">
<title><?php echo $_SESSION['name'];?></title>
</head>
<body>
<?php
	include("db_connect.php");
?>
<script src="jquery-2.1.1.js"></script>
<script>
function search_guest(series,num){
	var guest_id = $("#number").val();
	var guest_id_sub = $('#sub_number').val();
	var name_sei = $("#name_first").val();
	var name_mei = $("#name_last").val();
	var kana_sei = $("#name_first_kana").val();
	var kana_mei = $("#name_last_kana").val();
	var address = $("#mailaddress").val();
	//選択無し　０
	//男　　　　１
	//女　　　　２
	var sex = $("input[name='sex']:checked").val();
	var tel = $("#tel").val();
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({
		type:"POST",
		url:"posfront_search_guest_rsv.php",
		data:{
		series:series,
		num:num,
		guest_id:guest_id,
		guest_id_sub:guest_id_sub,
		name_sei:name_sei,
		name_mei:name_mei,
		kana_sei:kana_sei,
		kana_mei:kana_mei,
		address:address,
		sex:sex,
		tel:tel	
		},
		dataType:"json",
		async:false,
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         },
         success:function(data){
						$('#table_staff').append(data[0]);
						$('#num_guest').text(data[1]);
						$('#span').append(data[2]);
						}
	});
	

}
</script>
 <script>
		function sub_id(){
			if($('#id_sub').prop('checked') == 1){
				$('#sub_number').val("");
				$('#number').val("");
				$('#sub_number').removeAttr("disabled");
			}else{
				$('#sub_number').val("");
				$('#number').val("");
				$('#sub_number').attr("disabled","disabled");
			}
	}
</script>

<div id="wrapper">
<div id="header">
<div id="header_up">
<div id="header_shop">
<?php echo($_SESSION['name']) ?>
</div>
<div id="logout">
<?php
if(isset($_GET['RSV'])){
	echo '| <a  style="text-align:right;" href="reservation_new.php?RSV='.$_GET['RSV'].'">戻る</a> |';
}else{
	echo '| <a  style="text-align:right;" href="reservation_new.php">戻る</a> |';
}
?>
| <a  style="text-align:right;" href="login.php">ログアウト</a> |
</div>
</div>

<div id="header_low">
<div id="time">
<?php 
include("year.php");
?>
<div id="time_div">
</div>
</div>
</div>
</div>
<div id="contents">
<p>
	<form name="search_customer" action="#" method="post">
<table id="search">
	<tr>
		<th class="c">会員番号</th>
		<th>	
			<span id="input_box">
				<input id="sub_number" size="2" maxlength="10" disabled/>-
				<input name="number" id="number" type="text" size="8" maxlength="20" ></br>
			</span>
			<input type="checkbox" name="id_sub" id="id_sub" onclick="sub_id();">
			<label for="id_sub">ハイフン等を使う</label>
		</th>
		<th class="c">会員名</th>
		<th><input type="text" name="name_first" id="name_first" size="9"></br><input type="text" name="name_last" id="name_last" size="9"></th>
		<th class="c" style="text-align:center;">メール</th>
		<th style="text-align:left;"><input type="text" name="mailaddress" id="mailaddress" size="25"></th>
		<th rowspan="2" style="width:220px;">
			<div id="search_left">
			<img src="../css/pos_f/searchbtn.gif" alt="会員を検索"
			onmouseover="this.src='../css/pos_f/searchbtn_on.gif';" 
			onmouseout="this.src='../css/pos_f/searchbtn.gif';"
			onclick = "search_guest('<?php echo $series;?>',1);"/>
		</div>
			<div id="search_right">
			<img src="../css/pos_f/resetbtn.gif" alt="条件をリセット"
			onmouseover="this.src='../css/pos_f/resetbtn_on.gif';" 
			onmouseout="this.src='../css/pos_f/resetbtn.gif';" 
			onclick="location.reload();"/>
		</div>
		</th>
	</tr>
	<tr>
		<th class="c">性別</th>
		<th><div style="text-align:left;">
			<input type="radio" name="sex" id="non" checked="checked" value="0"><label for="non">選択なし</label>
			<input type="radio" name="sex" id="male" value="1"><label for="male">男性</label>
			<input type="radio" name="sex" id="female" value="2"><label for="female">女性</label>
		</div>
		</th>
		<th class="c">フリガナ</th>
		<th><input type="text" name="name_first_kana" id="name_first_kana" size="9">
			 <input type="text" name="name_last_kana" id="name_last_kana" size="9">
		</th>
		<th class="c">TEL</th>
		<th style="text-align:left;"><input type="text" name="tel" id="tel" size="20"></th>
</table>
	</form>
	<p style="text-align:right;">
		<?php
		$countsql = "SELECT * FROM guest WHERE shop_id=$shop_id";
		$countrs = mysqli_query($db, $countsql);
		$count = mysqli_num_rows($countrs);
		?>
		該当数:
		<span id="num_guest">
		<?php
		echo $count;
		?>
		</span>
		人
<table id="table_staff">
	<tr>
		<th class="d">会員番号</th>
		<th class="d">会員名</th>
		<th class="d">フリガナ</th>
		<th class="d">性別</th>
		<th class="d">TEL</th>
		<th class="d">メールアドレス</th>
		<th class="d">チケット</th>
		<th class="d">詳細</th>
		<th class="d">選択</th>
	</tr>
	<?php
	$sql="SELECT * FROM guest WHERE shop_id=$shop_id ORDER BY fullname_kana LIMIT 0,10";
	$recordset = mysqli_query($db, $sql);
	while ($data = mysqli_fetch_assoc($recordset)){
	?>
	<tr name="add">
		<th>
		<?php 
		if($data['sub_id']==""){
			print(htmlspecialchars($data['id']));
		}else{
			print(htmlspecialchars($data['sub_id']."-".$data['id']));
		} ?>
		</th>
		<th>
		<?php 
		print(htmlspecialchars($data['fullname_kanji'])); ?>
		</th>
		<th>
		<?php 
		print(htmlspecialchars($data['fullname_kana'])); ?>
		</th>
		<th>
		<?php 
		if($data['sex'] == 1){
		echo "男性";
		}else{
		if($data['sex'] == 2){
		echo "女性";
		}
		}
		?>
		</th>
		<th>
		<?php 
		print(htmlspecialchars($data['tel1'])); ?>
		</th>
		<th>
		<?php 
		print(htmlspecialchars($data['mailaddress'])); ?>
		</th>
		<th>
		<?php 
		print(htmlspecialchars($data['Tickets'])); ?>
		</th>
		<th>
		<a href="customer_update.php?id=<?php echo $data['id']; ?>&seq=<?php echo $data['guestseq'];?>"/>
		<img src="../css/pos_f/detail.gif"
		onmouseover="this.src='../css/pos_f/detail_on.gif';" 
		onmouseout="this.src='../css/pos_f/detail.gif';" />
		</a>
		</th>
		<th>
		<?php
		if(isset($_GET['RSV'])){
			echo '<a href="reservation_new.php?RSV='.$_GET['RSV'].'&amp;id='.$data['id'].'&amp;sub_id='.$data['sub_id'].'&amp;seq='.$data['guestseq'].'"/>';
		}else{
			echo '<a href="reservation_new.php?id='.$data['id'].'&amp;sub_id='.$data['sub_id'].'&amp;seq='.$data['guestseq'].'"/>';
		}
		?>

		<img src="../css/pos_f/select.gif"
		onmouseover="this.src='../css/pos_f/select_on.gif';"
		onmouseout="this.src='../css/pos_f/select.gif';">
		</a>
		</th>
	</tr>
	<?php
	}
	?>
</table>
<div id="span" style="text-align:center;">
<?php
$str_button="";
$sql="SELECT * FROM guest WHERE shop_id=".$shop_id;
$recordset = mysqli_query($db, $sql);
$num_record = mysqli_num_rows($recordset);
$button_num = floor(($num_record -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_guest('.'\''.$series.'\''.','.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.'\''.$series.'\''.','.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_guest('.'\''.$series.'\''.','.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.'\''.$series.'\''.','.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_guest('.'\''.$series.'\''.','.$i.')">';
			}
			}
			echo $str_button;
}
?>
</div>
</div><!--contents-->
</div><!--wrapper-->
</html>