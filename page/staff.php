<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>

<script>
function search_staff(num){
	
 	var search_id=$('#search_id').val();
 	var s_name=$('#search_name').val();
 	var search_remarks=$('#search_remarks').val();
 	var search_sex=$("input:radio[name='search_sex']:checked").val();
 	//指定無しは２、男は１、女は０
 	var search_display=$("input:radio[name='display']:checked").val();
 	//ID順が０、５０音順は１
 	$("tr[name='add']").remove();
 	$("input[name='add_button']").remove();
 	$.post('staff_search.php',{search_num:num,
 							   search_id:search_id,
 							   s_name:s_name,
 							   search_remarks:search_remarks,
 							   search_sex:search_sex,
 							   search_display:search_display},
 							   function(data){
 							   	$('#table').append(data);
 							   });
 	$.ajax({type:"POST",
 			url:"staff_span.php",
 				data:{	search_num:num,
 						search_id:search_id,
 					    search_name:s_name,
 						search_remarks:search_remarks,
 						search_sex:search_sex,
 						search_display:search_display},
 						dataType:"json",
				success:function(data){
						$('#span').append(data[0]);
						$('#staff_num').text(data[1]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown);
						}
						});	
						}
</script>

<?php
include("db_connect.php");
?>
<div id="pagebodymain">
	<h1>スタッフの一覧</h1>
	<table id="staff">
	<tr>
		<th class="b" colspan="4">検索条件</th>
	</tr>
	<tr>
		<th class="a">スタッフ番号</th>
		<th><input type="text" name="search_id" id="search_id" value=""></th>
		<th class="a">名前/フリガナ</th>
		<th><input type="text" name="search_name" id="search_name" value=""></th>
	<tr>
		<th class="a">備考欄</th>
		<th colspan="3"><input type="text" name="search_remarks" id="search_remarks" size="80" value=""/></th>
	<tr>
		<th class="a">性別</th>
		<th><input type="radio" name="search_sex" id="non" value="2" checked="checked"/><label for="non">指定なし</label>
		    <input type="radio" name="search_sex" id="male" value="1"/><label for="male">男性</label>
		    <input type="radio" name="search_sex" id="female" value="0"/><label for="female">女性</label></th>
		<th class="a">一覧の表示順</th>
		<th><input type="radio" name="display" id="id" value="0" checked="checked"/><label for="id">ID順</label>
		    <input type="radio" name="display" id="jpn" value="1" /><label for="jpn">五十音順</label></th>
	<tr>
		<th colspan="4" style="text-align:center;">
			<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
			<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick=<?php
				echo '"search_staff(1);"';
				?>>
</table>

	


<?php
$recordSet = mysqli_query($db, 'SELECT * FROM staff');
$count = mysqli_num_rows($recordSet);
?>

<p style="text-align:right;">会員数: <span id="staff_num"><?php print("$count"); ?></span>人
	<form name="staff">
<table id="table">
	<tbody>
	<tr>
		<th class="a" style="text-align:center;">削除</th>
		<th class="a" style="text-align:center;">スタッフ番号</th>
		<th class="a" style="text-align:center;">スタッフ名</th>
		<th class="a" style="text-align:center;">フリガナ</th>
		<th class="a" style="text-align:center;">略称</th>
		<th class="a" style="text-align:center;">性別</th>
		<th class="a" style="text-align:center;">停止</th>
	</tr>
	<?php
	$recordSet = mysqli_query($db, 'SELECT * FROM staff ORDER BY id LIMIT 0,10');
	while ($table = mysqli_fetch_assoc($recordSet)){
	?>

	<tr name="add">
		<th style="text-align:center;">
			<a href="delete.php?id=<?php print(htmlspecialchars($table['STFSEQ']));?>&data=staff&shop_id=<?php print(htmlspecialchars($table['shop_id'])); ?>" onclick="return confirm('削除してよろしいですか？');"><input type="button" name="sakuzyo" value="削除" /></a></th>
		<th style="text-align:center;"><a href="staff_update.php?id=<?php print($table['STFSEQ']);?>&shop_id=<?php print($table['shop_id']);?>"><?php print(htmlspecialchars($table['id']) ); ?></a></th>
		<th style="text-align:center;"><a href="staff_update.php?id=<?php print($table['STFSEQ']);?>"><?php print(htmlspecialchars($table['name'])); ?></a></th>
		<th><?php print(htmlspecialchars($table['name_katakana'])); ?></th>
		<th><?php print(htmlspecialchars($table['abb_name'])); ?></th>
		<th><?php if($table['sex'] == 0){
			print(htmlspecialchars("女性"));
		}else{print(htmlspecialchars("男性"));} ?></th>
		<th style="text-align:center;"><?php if($table['stop'] == 1){
			print(htmlspecialchars("●"));} ?></th>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>

<div id="span" align="center">
<?php
$str_button="";
		$sql = 'SELECT * FROM staff ';
		$rs = mysqli_query($db,$sql);
		$row = mysqli_num_rows($rs);//グループ化されたデータの件数を取得
$button_num = floor(($row -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_staff('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_staff('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_staff('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_staff('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_staff('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>

<p><p>※「スタッフ番号」「スタッフ名」をクリックするとスタッフ情報を編集できます。
</div>
</form>
<?php include("footer.php"); ?>