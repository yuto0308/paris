<div id="pagebodysub">

<p><a href="index.php">POSレジ管理画面トップ</a></p>

<p>
<?php 
// include("./db_info.php");
// $sql_preinfo=' SELECT * FROM info WHERE (`add` = 0 OR `add` = 1) AND check_del=0 ORDER BY createdate DESC';
// $rs_preinfo=mysqli_query($db_info,$sql_preinfo);
// $midoku_count = 0;
// while($pre_info = mysqli_fetch_assoc($rs_preinfo)){
//   $info_seq = $pre_info['SEQ'];

//   $sql_info = ' SELECT * FROM reply WHERE seq = '.$info_seq.' AND id = "'.$_SESSION['id'].'"';
//   $rs_info = mysqli_query($db_info,$sql_info);
//   $rows = mysqli_num_rows($rs_info);
//   if($rows == 0){
//     $midoku_count = $midoku_count + 1;
//   }
// }

// $str_info="";

// if($midoku_count != 0){
//   $str_info .= "<a href ='message_list.php' target='_blank'>".$midoku_count."件の新着メッセージがあります。</a>";
// }else{
//   $str_info .= "<a href ='message_list.php' target='_blank'>新着メッセージはありません。</a>";
// }

//   echo $str_info;
?>
</p>


<div id="acc">
<h3>POSレジの管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posstart.php">開始レジ金の確認</a></li>
<li><a href="posmovelist.php">レジ金移動の確認</a></li>
<li><a href="posfinish.php">レジの締め処理の確認</a></li>
</ul>
</div>

<h3>売り上げデータの集計</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posreglist.php">レジの売り上げ一覧</a></li>
<li><a href="shopreport.php">月日別の集計</a></li>
<li><a href="shopsreport.php">店舗別の集計</a></li>
<li><a href="staffreport.php">スタッフ別の集計</a></li>
<li><a href="itemreport.php">商品別の集計</a></li>
<li><a href="categoryreport.php">商品カテゴリ別の集計</a></li>
<!-- <li><a href="itemranking.php">箇所別ランキング</a></li> -->
</ul>
</div>

<h3>マスターの管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="customerlist.php">会員の一覧</a></li>
</ul>
</div>

<h3>予約の管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="reservation.php">予約の一覧</a></li>
<?php
	$year=date("Y");
	$month=date("n");
	$day=date("j");

echo '<li><a href="timetable.php?year='.$year.'&month='.$month.'&day='.$day.'">タイムスケジュール</a></li>'
?>
</ul>
</div>

<h3>店舗の管理</h3>
<div class="pulldown_menu">
<ul class="side_list">
<li><a href="posbasic.php">基本情報の編集</a></li>
<li><a href="posshoplist.php">店舗の一覧</a></li>
<li><a href="staff.php">スタッフの一覧</a></li>
<?php
// if($_SESSION['id'] == 'upfield'){
//   echo '<li><a href="secret.php">test</a></li>';
// }
?>
</ul>
</div>
</div>


</div>