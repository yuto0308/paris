<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php
	include("db_connect.php");
?>
<script>
function goods_search(num){
	//店舗
	var shops = $('#useshop').val();
	//cat_*
	//カテゴリ
	var cat_big = $("#cat_big").val();
	//ソート
	var sort = $('input[name="sort"]:checked').val();
	//月日	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({
		type:"POST",
		url:"search_itemranking.php",
		data:{
			num:num,
			shops:shops,
			sort:sort,
			cat_big:cat_big,
			year_s:year_s,
			month_s:month_s,
			year_e:year_e,
			month_e:month_e
		},
		dataType:"json",
		success:function(data){
						$('#table').append(data[0]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}
function set(){
	goods_search(1);
}
$(document).ready(set);
</script>
<script>
function search_itemreport_csv(num){
	//店舗
	var shops = $('#useshop').val();
	//cat_*
	//カテゴリ
	var cat_big = $("#cat_big").val();
	//ソート
	var sort = $('input[name="sort"]:checked').val();
	//月日	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	
	window.open("./search_itemranking_csv.php?sort="+sort+"&shops="+shops+"&cat_big="+cat_big+"&year_s="+year_s+"&month_s="+month_s+"&year_e="+year_e+"&month_e="+month_e);	

}
</script>

<div id="pagebodymain">
<h1>箇所別ランキング</h1>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">検索条件</th>
		<tr>
			<th class="a">店舗</th>
			<th>
				<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
							include("db_connect.php");
							$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
							$recordSet = mysqli_query($db, $sql);
							while($arr_item = mysqli_fetch_assoc($recordSet)){
								echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
							}
						?>
				</select>
			</th>
			<th class="a">表示順</th>
			<th style="text-align:center;">
				<input type="radio" value="0" name="sort" checked id="uriage" /><label for="uriage">売上</label>
				<input type="radio" value="1" name="sort" id="tensu" /><label for="tensu">点数</label>
			</th>
		</tr>
		<tr>
			<th class="a">カテゴリー</th>
			<th colspan="3">大カテゴリー
			<select id="cat_big" onchange="change_select_big();">
					<option value="0">大カテゴリを選択してください</option>
				<?php
					$sql_make_big_cat = " SELECT category_big_str , category_big FROM goods ";
					$sql_make_big_cat .= " WHERE (shop_id=0 OR shop_id = $shop_id) ";
					$sql_make_big_cat .= " GROUP BY category_big_str ORDER BY category_big ";
					$rs_make_big_cat = mysqli_query($db,$sql_make_big_cat) or exit("クエリ失敗");
					while($arr_make_big_cat = mysqli_fetch_assoc($rs_make_big_cat)){
						echo "<option value='".$arr_make_big_cat['category_big']."'>".$arr_make_big_cat['category_big_str']."</option>";
					}
				?>
			</select>
			</th>
		<tr>
			<th class="a">集計期間</th>
			<th colspan="3" style="text-align:center;">
			<select id="year_s">
			<?php
				$year_s=date("Y");
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_s">
			<?php
				$month_s=date("m");
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月 ～
			<select id="year_e">
			<?php
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_e">
			<?php
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月
			</th>
		<tr>
			<th colspan="4" style="text-align:center;">
				<input type="image" src="../css/image/contents/search_reset.gif" onclick="location.reload();" alt="条件をリセット">
				<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索" onclick=<?php
				echo '"goods_search(1);"';
				?>>
			</th>
	</table>
	
<!-- <p style="text-align: right;">検索条件を閉じる</p> -->
<p>
<div style="text-align:left;float:left">
	<a href="#" onclick="search_itemreport_csv(1);">
		<img src="../css/image/contents/csv_btn.gif" alt="csvを出力">
	</a>
	</br><small>集計結果の一部を表示します。全ての集計結果を確認する場合は「CSVデータ」をダウンロードして下さい。</small>
</a>
</div>
<!-- <div style="text-align: right;"><a href="#" onclick="search_item_graph(1);"><img src="../css/contents_img/graph(3).gif"></a></div> -->
</p>

<table id="table">

</table>

<?php
		$sql_search = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date ";
		$sql_search .= " FROM shop_log WHERE shop_id=$shop_id ";
		$sql_search .= " AND day >= '".date("Y")."-".date("m")."-01' ";
		$sql_search .= " AND day <= '".date("Y")."-".date("m")."-31' "; 
		$sql_search .= " GROUP BY date ";
		$query_search = mysqli_query($db,$sql_search) or exit("クエリ失敗");
		$num_rows = mysqli_num_rows($query_search);
		if($num_rows == 0){
			$num_rows = 1;
		}
?>
<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($num_rows -1 )/ 5) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>
<div style="height:100px;"></div>
</div>
<?php include("footer.php"); ?>
