<?php
	session_start();
	include("db_connect.php");

	$result_ary=array();
	$count = 0;
	$str="";
	$str2 = "";
	$year = $_POST['year'];
	$month = $_POST['month'];
	$goodsid_arr="";
	if($month < 10){
		$month = "0".$month;
	}
	$day = $_POST['day'];
	if($day < 10){
		$day = "0".$day;
	}
	$shop_id = $_POST['shops'];
	
	$sql = ' SELECT DATE_FORMAT(CHA_startdate,"%Y/%m/%d") as date, DATE_FORMAT(CHA_startdate,"%H:%i") as s_time, DATE_FORMAT(CHA_enddate,"%H:%i") as e_time,reservation.price as price,discount, SUM((reservation.price-discount)*goods_count) as sum, COUNT(reservation.shop_id) as rowcount, ';
	$sql.= ' guest_name, CHA_staffname, RSVno,sub_id,method,';
	$sql.= ' reservation.id as id,guestseq,goods.category_big_str as category_big_str ';
	$sql.= ' FROM reservation,goods ';
	$sql.= ' WHERE CHA_startdate >= "'.$year.'-'.$month.'-'.$day.' 00:00:00" AND ';
	$sql.= ' CHA_startdate <= "'.$year.'-'.$month.'-'.$day.' 23:59:59" AND ';
	$sql.= ' reservation.shop_id='.$shop_id.' AND ';
	$sql.= ' goods.id=reservation.goods_id ';
	$sql.= ' GROUP BY RSVno ORDER BY CHA_startdate DESC  ';

	$sql_count=$sql;
	$rs_count=mysqli_query($db,$sql_count);
	$count=mysqli_num_rows($rs_count);

	$recordset=mysqli_query($db, $sql);
		if(!$recordset){
		$str='<tr class="a" name="add2">
			<th style="text-align:center; width:9%;">予約日</th>
			<th style="text-align:center; width:12%;">時間</th>
			<th style="text-align:center; width:9%;">No</th>
			<th style="text-align:center; width:22%;">会員名</th>
			<th style="text-align:center; width:15%;">担当</th>
			<th style="text-align:center; width:14%;">種別</th>
			<th style="text-align:center; width:8%;">方法</th>
			<th style="text-align:center; width:11%;">金額</th>
			</tr>';
		$str2 = '<tr name="add"><th colspan="9" style="text-align:center;">対象データがありません</th></tr>';
		$result_ary[] = $str;
		$result_ary[] = $str2;
		$result_ary[] = "0";
		echo json_encode($result_ary);
		return;
	}else if(mysqli_num_rows($recordset)==0){
		$str='<tr class="a" name="add2">
			<th style="text-align:center; width:9%;">予約日</th>
			<th style="text-align:center; width:12%;">時間</th>
			<th style="text-align:center; width:9%;">No</th>
			<th style="text-align:center; width:22%;">会員名</th>
			<th style="text-align:center; width:15%;">担当</th>
			<th style="text-align:center; width:14%;">種別</th>
			<th style="text-align:center; width:8%;">方法</th>
			<th style="text-align:center; width:11%;">金額</th>
			</tr>';
		$str2 = '<tr name="add"><th colspan="9" style="text-align:center;">対象データがありません</th></tr>';
		$result_ary[] = $str;
		$result_ary[] = $str2;
		$result_ary[] = "0";
		echo json_encode($result_ary);
		return;
	}

		$str='<tr class="a" name="add2">
			<th style="text-align:center; width:9%;">予約日</th>
			<th style="text-align:center; width:12%;">時間</th>
			<th style="text-align:center; width:9%;">No</th>
			<th style="text-align:center; width:22%;">会員名</th>
			<th style="text-align:center; width:15%;">担当</th>
			<th style="text-align:center; width:14%;">種別</th>
			<th style="text-align:center; width:8%;">方法</th>
			<th style="text-align:center; width:11%;">金額</th>
			</tr>';


		while($arr_item=mysqli_fetch_assoc($recordset)){
		/*
$sql = ' SELECT CHA_startdate,CHA_enddate,price,discount, ';
	$sql.= ' guest_name, CHA_staffname, RSVno ';
	$sql.= ' FROM reservation ';
	$sql.= ' WHERE date >= "'.$year.'-'.$month.'-'.$day.'" AND ';
	$sql.= ' date <= "'.$year.'-'.$month.'-'.$day.'" AND ';
	$sql.= ' shop_id='.$shop_id;
	$sql.= ' ORDER BY CHA_startdate DESC ';	
*/
		// $sql_num=' SELECT * FROM reservation WHERE RSVno='.$arr_item['RSVno'];
		// $query_num=mysqli_query($db,$sql_num);
		// $item_num=mysqli_num_rows($query_num);
		$guest_name = $arr_item['guest_name'];
		$date = $arr_item['date'];
		$res_start = $arr_item['s_time'];
		if($arr_item['sub_id']=="" && $arr_item['guestseq'] != 0){
			$id=$arr_item['id'];
		}else if($arr_item['sub_id']!="" && $arr_item['guestseq'] != 0){
			$id=$arr_item['sub_id']."-".$arr_item['id'];
		}else if($arr_item['guestseq']==0){
			$id="新規";
		}

		if($arr_item['method']==1){
			$method="TEL";
		}else if($arr_item['method']==2){
			$method="店頭";
		}else if($arr_item['method']==3){
			$method="ネット";
		}else{
			$method="不明";
		}

		$res_end = $arr_item['e_time'];
		$staff = $arr_item['CHA_staffname'];
		$summ = $arr_item['sum'];
		$no = $arr_item['RSVno'];
		$row = $arr_item['rowcount'];
		$category_big_str=$arr_item['category_big_str'];
		if($category_big_str=="物販(化粧品)"){
			$category_big_str="物販";
		}
		$str2 .= '<tr name="add"><th style="text-align:center;">'.$date.'</th>
			<th>'.$res_start.'~'.$res_end.'</th>
			<th style="text-align:center;">'.$id.'</th>
			<th style="text-align:center;">'.$guest_name.'</th>
			<th style="text-align:center;">'.$staff.'</th>
			<th style="text-align:center;">'.$category_big_str.'</th>
			<th style="text-align:center;">'.$method.'</th>
			<th style="text-align:center;">'.number_format($summ).'円</th>
			<th hidden>'.$no.'</th>
			</tr>';

	
		}

		$result_ary[] = $str;
		$result_ary[] = $str2;
		$result_ary[] = $count;
		echo json_encode($result_ary);
		return;
?>
