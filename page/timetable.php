<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php"); ?>
<div id="pagebodymain">
	<h1>
	<div style="text-align:left; float:left;">
	<?php
	if(isset($_GET['year'])){
	$year = $_GET['year'];
	$month = $_GET['month'];
	if($month < 10){
		$month = "0".$month;
	}
	$day = $_GET['day'];
	if($day < 10){
		$day = "0".$day;
	}
	}else if(isset($_POST['shops'])){
		$year = date("Y");
		$month= date("m");
		$day = date("d");
		$shop_id=$_POST['shop'];
	}
	echo $year."年".$month."月".$day."日の";
	?>
	タイムテーブル</div>
	<div style="text-align:right">
	<a href="reservation.php" style="margin-right:15px;">
	予約の一覧</a>
	</div>
	</h1>
	<form>
		<table>
			<tr>
				<th class="b" colspan="2">
					検索条件
				</th>
			</tr>
			<tr>
				<th class="a" style="text-align:center;">検索店舗</th>
				<th style="text-align:center;">
					<select name="shops">
						<?php
							$sql=' SELECT * FROM members WHERE shop_id != '.$shop_id.' ORDER BY shop_id ASC ';
							$query=mysqli_query($db,$sql);
							while($data=mysqli_fetch_assoc($query)){
								if($_GET['shops']==$data['shop_id']){
									echo '<option value="'.$data['shop_id'].'" selected>'.$data['name'].'</option>';
								}else{
									echo '<option value="'.$data['shop_id'].'">'.$data['name'].'</option>';
								}
								
							}
						?>
					</select>
				</th>
			</tr>
			<tr>
				<th colspan="2" style="text-align:center;">
					<input type="submit" value="検索" action="#">
		</table>
	</form>
	<p>
    <?php

	if(isset($_GET['shops']) && !isset($_POST['shops'])){
		$shop_id=$_GET['shops'];
	}else if(isset($_POST['shops']) && !isset($_POST['shops'])){
		$shop_id=$_POST['shops'];
	}else if(!isset($_POST['shops']) && !isset($_POST['shops'])){
		$sql=' SELECT MIN(shop_id) FROM members WHERE shop_id!='.$shop_id;
		$query=mysqli_query($db,$sql);
		$data=mysqli_fetch_assoc($query);
		$shop_id=$data['MIN(shop_id)'];
	}
	$button_ary=array();
	$button_ary1=array();
	$button_ary2=array();
		//まずはスタッフごとにグループ分けする
	$sql = " SELECT name,STFSEQ,id FROM staff WHERE shop_id=".$shop_id;
	$sql .= " ORDER BY STFSEQ ";
	$rs = mysqli_query($db,$sql);
	if(!$rs){
		echo "スタッフが登録されていません";
		return;
	}else if(mysqli_num_rows($rs)==0){
	    echo "スタッフが登録されていません";
	    return;
	}
	
	while($arr=mysqli_fetch_assoc($rs)){
		$staff_name = $arr['name'];
		$staff_id =$arr['id'];
		$stfseq = $arr['STFSEQ'];
		$sql = ' SELECT DATE_FORMAT(CHA_startdate,"%k.%i") as s_time, DATE_FORMAT(CHA_enddate,"%k.%i") as e_time,price,discount, SUM(price) as sum, SUM(discount) as dis_sum, COUNT(shop_id) as rowcount, GROUP_CONCAT(goods_name) as goods_name, ';
	$sql.= ' guest_name, CHA_staffname, CHA_staffid, RSVno, UNIX_TIMESTAMP(CHA_enddate) - UNIX_TIMESTAMP(CHA_startdate) as dif_time ';
	$sql.= ' FROM reservation ';
	$sql.= ' WHERE CHA_startdate >= "'.$year.'-'.$month.'-'.$day.' 00:00:00" AND ';
	$sql.= ' CHA_startdate <= "'.$year.'-'.$month.'-'.$day.' 23:59:59" AND ';
	$sql.= ' shop_id='.$shop_id.' AND CHA_staffid = "'.$staff_id.'"';
	$sql.= ' GROUP BY RSVno ORDER BY CHA_startdate ASC ';

	$rs1 = mysqli_query($db,$sql);
	$button_ary[] = 'table'.$stfseq;
	$button_ary1[] = 'table'.$stfseq;
	$button_ary2[] = 'table'.$stfseq;
	if(!$rs1){
		//空のタイムテーブル
		echo '<div><a href="./reservation_new.php?rsvchastfid='.$arr['id'].'">'.$staff_name.'</a></div>';
		echo '<div id="table'.$stfseq.'"></div>';
		echo '<script type="text/javascript">';
		echo 'var table'.$stfseq.' = new Timetable("table'.$stfseq.'");';
		echo '</script>';
	}else if(mysqli_num_rows($rs1)==0){
	    //空のタイムテーブル
	    echo '<div><a href="./reservation_new.php?rsvchastfid='.$arr['id'].'">'.$staff_name.'</a></div>';
		echo '<div id="table'.$stfseq.'"></div>';
		echo '<script type="text/javascript">';
		echo 'var table'.$stfseq.' = new Timetable("table'.$stfseq.'");';
		echo '</script>';
	}else{
		
		echo '<div><a href="./reservation_new.php?rsvchastfid='.$arr['id'].'">'.$staff_name.'</a></div>';
		echo '<div id="table'.$stfseq.'"></div>';
		echo '<script type="text/javascript">';
		echo 'var table'.$stfseq.' = new Timetable("table'.$stfseq.'");';
		while($arr1 = mysqli_fetch_assoc($rs1)){
		$arrexplode = explode(",",$arr1['goods_name']);
		if(count($arrexplode) == 1){
			$goods_explode = $arrexplode[0];
		}else if(count($arrexplode) == 2){
			$goods_explode = $arrexplode[0].",".$arrexplode[1];
		}else if(count($arrexplode) >= 3){
			$goods_explode = $arrexplode[0].",".$arrexplode[1]."等";
		}
			echo 'table'.$stfseq.'.addEvent("'.$arr1['guest_name'].'<br>'.$arr1['s_time'].'~'.$arr1['e_time'].'<br>'.$goods_explode.'", '.$arr1['s_time'].', '.gmdate( "G.i" ,$arr1['dif_time']).');';
		}
		echo '</script>';
	}

	}
?>
<p>
<div style = "text-align:center">
<?php
echo '<input type="button" value="午前９時を表示" onclick="';
while(($pop = array_pop($button_ary)) != NULL){
	echo $pop.'.goTo(9.00);';
}
echo '">';
?>
  <?php
echo '<input type="button" value="現在時刻を表示" onclick="';
$now = date("H.00");
while(($pop = array_pop($button_ary1)) != NULL){
	echo $pop.'.goTo('.$now.');';
}
echo '">';
?>
  <?php
echo '<input type="button" value="午後10時を表示" onclick="';
while(($pop = array_pop($button_ary2)) != NULL){
	echo $pop.'.goTo(22.00);';
}
echo '">';
?>
</div>
<div style = "height:30px;">
</div>
<p>
<!--
<div id="mondayTable"></div>
<div id="tuesdayTable"></div>
<script type="text/javascript">
var Monday = new Timetable("mondayTable");
Monday.addEvent("French", 8, 1.3);
Monday.addEvent("Sport", 9.3, 1.3);
Monday.addEvent("Spanish", 11, 1);
Monday.addEvent("Maths", 14, 2);
Monday.addEvent("History", 16, 1);
</script>
<script type="text/javascript">
var Tuesday = new Timetable("tuesdayTable");
Tuesday.addEvent("French", 8, 1.3);
Tuesday.addEvent("Sport", 9.3, 1.3);
Tuesday.addEvent("Spanish", 11, 1);
Tuesday.addEvent("Maths", 14, 2);
Tuesday.addEvent("History", 16, 1);
</script>
<input type="button" value="All timetables : Go to 9:30" onclick="Monday.goTo(9.3);Tuesday.goTo(9.3);">
-->
</div>
<?php include("footer.php"); ?>