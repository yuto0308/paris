<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<title>CSV入力</title> 
<link rel="stylesheet" href="../css/style.css">
<script src="jquery-2.1.1.js"></script>
<script src="./ajaxzip3.js" charset="UTF-8"></script>
<link type="text/css" rel="stylesheet"
  href="http://code.jquery.com/ui/1.10.3/themes/cupertino/jquery-ui.min.css" />
<script type="text/javascript"
  src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript">
$(function() {
  $('#acc').accordion({
    collapsible: true ,
    active: 100 ,
    heightStyle: 'content'
  });
});

</script>
</head>
<body>
<?php session_start(); ?>
  <?php
  if($_SESSION['id'] == ""){
    header('location: login.php');
  }else{
    if($_SESSION['ownercheck'] != 0){
      header('location: login.php');
    }
  }
  ?><?php
if(!isset($_FILES['my_file'])){
    echo 'ページ遷移が不正です。';
}else{
    if($_FILES['my_file']['error'] !== UPLOAD_ERR_OK){
         
        //エラーが発生している
        if($_FILES['my_file']['error'] == UPLOAD_ERR_FORM_SIZE){
            echo 'ファイルサイズがHTMLで指定した MAX_FILE_SIZE を超えています。';
        }elseif($_FILES['my_file']['error'] == UPLOAD_ERR_NO_FILE){
            echo 'ファイルが選択されていません。';
        }else{
            echo 'その他のエラーが発生しています。';
        }
         
    }else{
         
        //ここから通常の処理
 
        //ユーザーが指定したファイル名
        $myfile_name = $_FILES['my_file']['name'];
        //ファイルのMIME型
        $myfile_type = $_FILES['my_file']['type'];
        //ファイルサイズ
        $myfile_size = $_FILES['my_file']['size'];
        //アップロードしたファイルが保存されている一時保存場所
        $myfile_tmp_path = $_FILES['my_file']['tmp_name'];
         
        //SQLインジェクション対策用
        $safesql_myfile_name = mysql_real_escape_string($myfile_name);
        $safesql_myfile_type = mysql_real_escape_string($myfile_type);
     
        //HTML表示用
        $safehtml_myfile_name = htmlspecialchars($myfile_name);
        $safehtml_myfile_type = htmlspecialchars($myfile_type);
         
         
        //拡張子の取得
        $tmp_ary = explode('.',$myfile_name);
        if(count($tmp_ary)>1){
            $extension = $tmp_ary[count($tmp_ary)-1];
             if($extension != "csv"){
             		echo "アップロードされたファイルが不正です<br>";
	             return;
             }
            //拡張子が半角英数字以外なら拡張子がないものとする。
            if( !preg_match("/^[0-9a-zA-Z]+$/",$extension) ) $extension='';
        }else{
            //拡張子がない場合はそのまま。Macなど。
            $extension='';
        }
         
        //SQLインジェクション対策用
        $safesql_extension = mysql_real_escape_string($extension);
        //HTML表示用
        $safehtml_extension = htmlspecialchars($extension);
             
         
        //新しいファイル名を作成する
        $new_file_name = date("Ymd-His").'-'.mt_rand().'.'.$safehtml_extension;
         
        //php側でもファイルサイズのチェックを行う。
        if($myfile_size>10485760 or $myfile_size==0)die('エラー ファイルサイズが不正です。');        
 
        //ファイルの保存場所
        $myfile_new_path = './uploads';
        if(!move_uploaded_file($myfile_tmp_path,"$myfile_new_path/$new_file_name")){
            die('エラー ファイルを保存できませんでした。');
        }
        $handle_read = fopen("./uploads/".$new_file_name, "r");
		$row = 1;
		/*
while (($data = fgetcsv($handle_read, 100000, ",")) !== FALSE) {	
			$num = count($data);
			$row++;
				for ($c=0; $c < $num; $c++) {
					$test=$data[$c] ;
					$test=mb_convert_encoding($test, "utf-8","sjis-win");
					echo $test." ";
				} 
				echo "<br>";
		}
*/
        $unknown_id_count = 1;
        $data = fgetcsv($handle_read, 100000, ",");	
		$num = count($data);
		for ($c=0; $c < $num; $c++) {
			$test=$data[$c];
			$test=mb_convert_encoding($test, "utf-8","sjis-win");
			//echo $test." ";
			if($c == 2 && $test == "日付"){
				//echo "これは売り上げデータです";
				$file_check = 1;
			}else if($c == 2 && $test == "姓"){
				//echo "これは顧客データです";
				$file_check = 2;
			}else if($c == 2){
				echo "アップロードされたファイルが不正です";
				return;
			}
			} 
		echo "<br>";

       
        if($file_check == 2){
	         //顧客CSVの時
	         echo "これは顧客データです<br>";
	         while (($data = fgetcsv($handle_read, 1000, ",")) !== FALSE) {	
			$num = count($data);
			$arr = array();
				for ($c=0; $c < $num; $c++) {
					$temp=$data[$c];
					$arr[]=mb_convert_encoding($temp, "utf-8","sjis-win");
					echo $c." ".$arr[$c]."<br>";
				} 
			
				//arr[0] : SEQ ?謎
				//無視
				//arr[1] : コード
				//id無しの人は9999-[]で入れる????????
				//sub_id絡みもちょっと保留
				if($arr[1] == ""){
					$guest_id="9999"
				}
				$guest_id=$arr[1];
				
				
				//arr[2] : 姓
				//空の場合は身元不明と入れる
				if($arr[2] == ""){
					$myouzi_kanji = "身元不明";
				}else{
				$myouzi_kanji = $arr[2];
				}
				
				//arr[3] : 名
				//空の場合は身元不明と入れる
				if($arr[3] == ""){
					$namae_kanji = "身元不明";
				}else{
					$namae_kanji = $arr[3];
				}
				
				//arr[4] : セイ
				//空の場合はミモトフメイと入れる
				if($arr[4] == ""){
					$myouzi_kana = "ミモトフメイ";
				}else{
					$myouzi_kana = $arr[4];
				}
				
				//arr[5] : メイ
				//空の場合はミモトフメイと入れる
				if($arr[5] == ""){
					$namae_kana = "ミモトフメイ";
				}else{
					$namae_kana = $arr[5];
				}
				
				//arr[6] : 郵便番号前
				//空の場合は000と入れる
				if($arr[6] == ""){
					$post_first = "000";
				}else{
					$post_first = $arr[6];
				}
				
				//arr[7] : 郵便番号後
				//空の場合は0000と入れる
				if($arr[7] == ""){
					$post_second = "0000";
				}else{
					$post_second = $arr[7];
				}
				
				//arr[8] : 都道府県
				//空の場合は空
				$pref = $arr[8];
				
				//arr[9] : 市町村
				//空の場合は空
				$city = $arr[9];
				
				
				//arr[10] : 番地等
				//空の場合は空
				$address = $arr[10];
				
				//arr[11] : 建物名等
				//空の場合は空
				$building = $arr[11];
				
				//arr[12] : 電話番号1
				//空の場合は空
				$tel_first = $arr[12];
				
				//arr[13] : 電話番号2				
				//空の場合は空
				$tel_second = $arr[13];
				
				//arr[14] : メールアドレス
				//空の場合は空
				$mail = $arr[14];
				
				//arr[15] : 誕生年
				//空の場合は1900
				if($arr[15] == ""){
					$year = "1900";
				}else{
					$year = $arr[15];
				}			
				
				//arr[16] : 誕生月
				//空の場合は1
				if($arr[16] == ""){
					$month = "1";
				}else{
				$month = $arr[16];
				}
				
				//arr[17] : 誕生日
				//空の場合は1
				if($arr[17] == ""){
					$day = "1";
				}else{
					$day = $arr[17];
				}
				
				//arr[18] : メール
				//空の場合は0
				if($arr[18] == ""){
					$mail_mag = "0";
				}else{
					$mail_mag = $arr[18];
				}
				
				//arr[19] : DM
				//空の場合は0
				if($arr[19] == ""){
					$dm = "0";
				}else{
					$dm = $arr[19];
				}
			
				echo "<br>";
		}
        }else if($file_check == 1){
       		 //売り上げデータの時
	   		 echo "これは売り上げデータです<br>";
	   		 while (($data = fgetcsv($handle_read, 1000, ",")) !== FALSE) {	
			$num = count($data);
			$arr=array();
				for ($c=0; $c < $num; $c++) {
					$temp=$data[$c];
					$arr[]=mb_convert_encoding($temp, "utf-8","sjis-win");
					echo $c." ".$arr[$c]."<br>";
				} 
			/*
				arr[0] : 店舗番号
				arr[1] : 店舗名
				arr[2] : 日付
				arr[3] : 曜日
				arr[4] : レジ番号
				arr[5] : 決済方法
				arr[6] : 担当者
				arr[7] : 顧客番号
				arr[8] : 顧客名
				arr[9] : 商品コード
				arr[10] : 商品名
				arr[11] : 通常価格
				arr[12] : 割引
				arr[13] : 販売価格
				arr[14] : 数量
				arr[15] : 消費税
				arr[16] : 合計金額
			*/
				echo "<br>";
		}
        } 
        echo 'アップロードは成功しました。<br /><br />';
         
        echo 'ファイル名 : '.$safehtml_myfile_name.'<br />';
        echo 'MIME型 : '.$safehtml_myfile_type.'<br />';
        echo 'ファイルサイズ : '.number_format($myfile_size).' bytes<br />';
        echo '新しいファイル名 : '.$new_file_name.'<br />';
 
    }
}
?>  
</body>
</html>