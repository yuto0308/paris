<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
	function search(num){
		$("tr[name='add']").remove();
		$("input[name='add_button']").remove();
		var dom = $('*[name="dom"]:checked').val();
		var year_s = $('#year_s').val();
		var month_s = $('#month_s').val();
		var day_s = $('#day_s').val();
		var year_e = $('#year_e').val();
		var month_e = $('#month_e').val();
		var day_e = $('#day_e').val();
			$.ajax({type:"POST",
					url:"search_shopsreport.php",
					data:{	num:num,
							dom:dom,
							year_s:year_s,
							month_s:month_s,
							day_s:day_s,
							year_e:year_e,
							month_e:month_e,
							day_e:day_e
						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#span').append(data[1]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown);
						}
						});
}
</script>
<script>
function search_shopsreport_csv(num){
		var dom = $('*[name="dom"]:checked').val();
		var year_s = $('#year_s').val();
		var month_s = $('#month_s').val();
		var day_s = $('#day_s').val();
		var year_e = $('#year_e').val();
		var month_e = $('#month_e').val();
		var day_e = $('#day_e').val();
	    window.open("./search_shopsreport_csv.php?num="+num+"&dom="+dom+"&year_s="+year_s+"&month_s="+month_s+"&day_s="+day_s+"&year_e="+year_e+"&month_e="+month_e+"&day_e="+day_e);						

}

</script>
<?php include("db_connect.php"); ?>
<div id="pagebodymain">
<h1>店舗別の集計</h1>
<p>
<table>
	<tr>
		<th class="b" colspan="2">検索条件</th>
	</tr>
	<tr>
		<th class="a">集計対象</th>
		<th><input type="radio" id="month" name="dom" value="0" ><label for="month">月別</label>
			<input type="radio" id="day" name="dom" value="1" checked="checked"><label for="day">日別</label></th>
	</tr>
	<tr>
		<th class="a">集計期間</th>
			<th colspan="3" style="text-align:center">
			<select id="year_s">
				<?php
				$year_s = date("Y");
					for($i=2000;$i<=$year_s;$i++){
					if($i!=date("Y")){
						echo '<option name="year_s" value="'.$i.'">'.$i.'年</option>';	
						}else{
						echo '<option name="year_s" value="'.$i.'" selected>'.$i.'年</option>';	
						}
					}
				?>
			</select>
			<select id="month_s">
				<?php
				$month_s = date("m");
					for($i=1;$i<=12;$i++){
					if($i!=$month_s){
						echo '<option name="month_s" value="'.$i.'">'.$i.'月</option>';	
						}else{
						echo '<option name="month_s" value="'.$i.'" selected>'.$i.'月</option>';	
						}
					}
				?>
			</select>
			<select id="day_s">
				<?php
					for($i=1;$i<=31;$i++){
					if($i!=1){
						echo '<option name="day_s" value="'.$i.'">'.$i.'日</option>';	
						}else{
						echo '<option name="day_s" value="'.$i.'" selected>'.$i.'日</option>';	
						}
					}
				?>
			</select>
			〜
			<select id="year_e">
				<?php
				$year_e = date("Y");
					for($i=2000;$i<=$year_e;$i++){
					if($i!=$year_e){
						echo '<option name="year_e" value="'.$i.'">'.$i.'年</option>';	
						}else{
						echo '<option name="year_e" value="'.$i.'" selected>'.$i.'年</option>';	
						}
					}
				?>
			</select>
			<select id="month_e">
				<?php
				$month_e = date("m");
					for($i=1;$i<=12;$i++){
					if($i!=$month_e){
						echo '<option name="month_e" value="'.$i.'">'.$i.'月</option>';	
						}else{
						echo '<option name="month_e" value="'.$i.'" selected>'.$i.'月</option>';	
						}
					}
				?>
			</select>
			<select id="day_e">
				<?php
				$day_e = date("j");
					for($i=1;$i<=31;$i++){
					if($i!=$day_e){
						echo '<option name="day_e" value="'.$i.'">'.$i.'日</option>';	
						}else{
						echo '<option name="day_e" value="'.$i.'" selected>'.$i.'日</option>';	
						}
					}
					$day_e = $day_e + 1;
				?>
			</select>
			</th>
	</tr>
	<tr>
		<th colspan="2" style="text-align:center;">
		<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
		<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search(1);"></th>
	</tr>
	</table>

<p>
	<a href="#" onclick="search_shopsreport_csv(1);"><img src="../css/image/contents/csv_btn.gif"></a>
</p>
	<table id="table">
		<tr name="add">
			<th class="a" style="text-align:center;">年月日</th>
			<th class="a" style="text-align:center;">曜日</th>
			<th class="a" style="text-align:center;">店舗名</th>
			<th class="a" style="text-align:center;">件数</th>
			<th class="a" style="text-align:center;">点数</th>
			<th class="a" style="text-align:center;">販売金額</th>
			<th class="a" style="text-align:center;">売上金額</th>
		</tr>
		<?php
		$sql = ' SELECT members.name, DATE_FORMAT(date, "%Y年%m月%d日") as datetime, ';
		$sql.= ' DATE_FORMAT(date, "%a") as week, SUM(count) as sum_item, COUNT(count) as count_item, ';
		$sql.= ' SUM(sum) as sum, SUM(ticket) as ticket ';
		$sql.= ' FROM members, shop_log_sub ';
		$sql.= ' WHERE shop_log_sub.shop_id=members.shop_id AND ';
		$sql.= ' date >= "'.$year_s.'-'.$month_s.'-01" and date <= "'.$year_e.'-'.$month_e.'-'.$day_e.'" ';
		$sql.= ' GROUP BY datetime, shop_log_sub.shop_id ';
		$sql_count = $sql;
		$rs_count=mysqli_query($db, $sql_count);
		$count = mysqli_num_rows($rs_count);

		$sql.= ' ORDER BY datetime DESC ';
		$sql.= ' LIMIT 0,10 ';
		$recordset = mysqli_query($db, $sql);
		$str="";
		while ($table = mysqli_fetch_assoc($recordset)){
			$datetime=$table['datetime'];
			$week=$table['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
				}
			$shopname=$table['name'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$sum=$table['sum'];
			$ticket=$table['ticket'] * (1000*1.08);
			$non_ticket=$sum-$ticket;
			$sum=number_format($sum);
			$non_ticket=number_format($non_ticket);
			$str.='<tr name="add">
					<th style="text-align:center;">'.$datetime.'</th>
					<th style="text-align:center;">'.$week_str.'</th>
					<th style="text-align:center;">'.$shopname.'</th>
					<th style="text-align:center;">'.$count_item.'</th>
					<th style="text-align:center;">'.$sum_item.'</th>
					<th style="text-align:center;">'.$sum.'</th>
					<th style="text-align:center;">'.$non_ticket.'</th>
					</tr>';
			}
			if($count==""){
			$str.='<tr name="add">
					<th colspan="7" style="text-align:center;">対象データがありません</th>
					</tr>';
			}
			echo $str;
		?>
	</table>
<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($count -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>
<br>
<small>※「売上金額」は「販売金額」-「チケットでの売り上げ」です。</small>
</p>
</div>
<?php include("footer.php"); ?>
