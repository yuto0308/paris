<!DOCTYPE html>
<html lang="ja">
<meta name="robots" content="noindex">
<head>
<?php session_start(); ?>
  <?php
  if($_SESSION['id'] == ""){
    header('location: logout.php');
  }
  ?>
<meta http-equiv="Content-Type" content="text"; charset="UTF-8">

<script src="jquery-2.1.1.js"></script>

        <script type="text/javascript">
        var insert = [];
        var insert_goods_name = [];
        var insert_goods_id = [];
        var ticket_num = 0;
        var discount = 0;
        var row_count = 0;
        function SearchId(){
            var id = $('#staffid').val();
            $('#show_id').load('getdata.php',{id:id});//id="show_id" にphpからの値入れる
        }
        
        function SerchGoods(){
        //同じ月かどうかのチェック
        //違う月ならカウントを１に
        $.post('reg_post_count.php',{id:goods_id});

            var goods_id = $('#goodsid').val();
          //  $('#goods_name').load('goods_name.php',{id:goods_id});//id="goods_name" にphpからの値入れる
          $.post('goods_name.php',{id:goods_id},function(data){$('#goods_name1').val(data);});
          $.post('price.php',{id:goods_id},function(data){$('#price').val(data);});

//            $('#price').load('price.php',{id:goods_id});
        }
        
        function delete_table_each(rowcount){
        var row = $('#rowcount'+rowcount).closest('tr').index();//消去したいインデックス＋２の値が出る
        var gds_name = $('#goods_name_temp_'+rowcount).text();
        
        if(gds_name == 401001){
	       ticket_num -= 11;
       }else if(gds_name == 401002){
	       ticket_num -= 23;
       }else if(gds_name == 401003){
	       ticket_num -= 34;
       }else if(gds_name == 401004){
	       ticket_num -= 58;
       }

        Number(row);
        insert.splice(row-2,1);
	        insert_goods_name.splice(row-2,1);
	        insert_goods_id.splice(row-2,1);
	        $('#rowcount'+rowcount).remove();
	        $('#ticket_use').val("");
	         var i;
       var sum_total_price = 0;
       var sum_total_count = 0;
       var sum_total_sum = 0;
       var sum_total_price_off = 0;
	   //var user = document.getElementsByName("user");
	   var data_total_price = document.getElementsByName("total_price");
	   var data_total_count = document.getElementsByName("total_count");
	   var data_total_sum = document.getElementsByName("total_sum");
	   var data_total_price_off = document.getElementsByName("total_price_off");
	   
	   for(i = 0; i < data_total_price.length; i++){
			sum_total_price += parseFloat(data_total_price[i].innerHTML);
			sum_total_count += parseFloat(data_total_count[i].innerHTML);
			sum_total_sum += parseFloat(data_total_sum[i].innerHTML);
			sum_total_price_off += parseFloat(data_total_price_off[i].innerHTML);

		}
	  
       target_count = document.getElementById("sum_of_count");
       target_count.innerHTML = sum_total_count;
       
       target_sum = document.getElementById("sum_of_sum");
       target_sum.innerHTML = Math.round(sum_total_sum * 1.08);
       
       target_price = document.getElementById("sum_of_price");
       target_price.innerHTML = sum_total_sum;
       
       target_tax = document.getElementById("sum_of_tax");
       target_tax.innerHTML = Math.round(sum_total_sum * 0.08);
       
       discount = Math.round(sum_total_price_off);
       
       
       //入力箇所に自動的に金額を入力
       //現金＋カードのときはどちらも空とする
       if($('#ticket_use').val() == ""){
			      var ticket_use = 0;
		      }else{
			      var ticket_use = $('#ticket_use').val();
		      }
       if($('#payment').val() == 1){//現金払いの時
       	   $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
	       $('#cash').val(Math.round(sum_total_sum * 1.08) - ticket_use * 1080);
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0); 
	   }else if($('#payment').val() == 2){//カード払いの時
	       $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
	       $('#card').val(Math.round(sum_total_sum * 1.08) - ticket_use * 1080);
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0);
       }else if($('#payment').val() == 3){//現金＋カードの時
	       $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
       	   $('#change').text("");
       }else if($('#payment').val() == 4){
           $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
	       $('#emoney').val(Math.round(sum_total_sum * 1.08) - ticket_use * 1080);
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0);
       }


        }
        
        
        function delete_table(){
        	$("tr[name='add']").remove();
	        //$('#data_in').remove();
	        //$('#over_wrap').append('<table id="data_in" bgcolor="#E0FFFF"></table>');
	        $('#sum_of_count').text("0");
	        $('#sum_of_price').text("0");
	        $('#sum_of_tax').text("0");
	        $('#sum_of_sum').text("0");
	        $('#cash').val("");
       	    $('#card').val("");
       	    $('#emoney').val("");
       	    $('#change').text("");
	        
	        insert = [];
	        insert_goods_name = [];
	        insert_goods_id = [];
	        ticket_num = 0;
	        discount = 0;
	        //alert(insert);
        }
			
        function submit(){       
        var goods_num = $('#goodsid').val();
        var goods_name = "";
        var goods_name_sub = "";
        var price_off = 0;
        var goods_price = $('#price').val();
        
        if(goods_price == ""){
	        goods_price = 0;
        }

        if(isNaN(goods_price)){
        	if(goods_price.charAt(goods_price.length - 1) == "#" && !isNaN(goods_price.substr(0,goods_price.length-1))){
	        	goods_price = goods_price.substr(0,goods_price.length-1)/ 1.08;	
        	}else{
	       		 alert("単価が全角文字であるか、数字ではありません");
		   		 return;
	        }
        }
        
        
        var guest_seq = $('#guest_seq').val();
        goods_name = $('#goods_name1').val();
        goods_name_sub =$('#goods_name2').val();
        var goods_coment = goods_name + " " +goods_name_sub;
        var goods_price_off = $('#price_off').val();
        if(isNaN(goods_price_off)){
        	if(goods_price_off.charAt(goods_price_off.length - 1) == "#" && !isNaN(goods_price_off.substr(0,goods_price_off.length-1))){
        		var price_off_temp = goods_price_off.substr(0,goods_price_off.length-1);
        		Number(price_off_temp);
	        	goods_price_off = price_off_temp/ 1.08;////////////	
        	}else{
	       		 alert("割引額が全角文字であるか、数字ではありません");
		   		 return;
	        }
        }
        if(goods_price_off == ""){
	        goods_price_off = 0;
        }
        var goods_price_off_per = $('#price_off_per').val();
        if(isNaN(goods_price_off_per)){
	        alert("割引額が全角文字であるか、数字ではありません");
	        return;
        }
        if(goods_price_off_per == ""){
	        goods_price_off_per = 0;
        }
        Number(goods_price_off_per);
        Number(goods_price);
        goods_price_off_per = eval(goods_price_off_per) / 100;
        goods_price_off_per = eval(goods_price) * eval(goods_price_off_per);
        /*ここでパーセントから円への変換処理を行う*/
        
        
        
        var goods_count = $('#count').val();
        //合計計算
        var sum = (eval(goods_price) * eval(goods_count) - ((eval(goods_price_off) +eval(goods_price_off_per)) * eval(goods_count))); 
        if(goods_price_off == 0 && goods_price_off_per == 0){
	     	price_off = 0;   
        }else if(goods_price_off == 0 && goods_price_off_per != 0){
	        price_off = goods_price_off_per;
        }else if(goods_price_off != 0 && goods_price_off_per == 0){
	        price_off = goods_price_off;
        }else{
	        alert("割引入力は一カ所だけにしてください");
	        return;
        }
        var tax = (eval(sum) / eval(goods_count)) * 0.08;
        if(goods_name != ""){
      // $('#kounyu').append('<tr class="kounyu" name="add" id="rowcount'+row_count+'"><th><input type="button" value="削除" onclick="delete_table_each('+row_count+');"></th><th id="goods_name_temp_'+row_count+'">'+goods_num+'</th><th>'+goods_coment+'</th><th name="total_price_off">'+price_off+'</th><th name="total_price">'+goods_price+'</th><th name="total_count">'+goods_count+'</th><th name="total_sum">'+sum+'</th><th></th></tr>');
      $('#kounyu').append('<tr class="kounyu" name="add" id="rowcount'+row_count+'"><th><input type="button" value="削除" onclick="delete_table_each('+row_count+');"></th><th id="goods_name_temp_'+row_count+'">'+goods_num+'</th><th>'+goods_coment+'</th><th hidden name="total_price_off">'+price_off+'</th><th>'+Math.round(price_off)+'</th><th hidden name="total_price">'+goods_price+'</th><th>'+Math.round(goods_price)+'</th><th name="total_count">'+goods_count+'</th><th hidden name="total_sum">'+sum+'</th><th>'+Math.round(sum)+'</th><th hidden name="total_tax">'+(Math.round(tax))*eval(goods_count)+'</th><th></th></tr>');
      row_count++;
       insert.push(goods_num+'\',\''+goods_count+'\',\''+sum+'\',\''+goods_coment+'\',\''+price_off);
       insert_goods_name.push(goods_coment+'.');
       insert_goods_id.push(goods_num);
              
       
       if(goods_num == 401001){
	       ticket_num += 11;
       }else if(goods_num == 401002){
	       ticket_num += 23;
       }else if(goods_num == 401003){
	       ticket_num += 34;
       }else if(goods_num == 401004){
	       ticket_num += 58;
       }
       Number(goods_price_off);
       Number(discount);
       discount += eval(price_off);
       var i;
       var sum_total_price = 0;
       var sum_total_count = 0;
       var sum_total_sum = 0;
       var sum_total_tax = 0;
	   //var user = document.getElementsByName("user");
	   var data_total_price = document.getElementsByName("total_price");
	   var data_total_count = document.getElementsByName("total_count");
	   var data_total_sum = document.getElementsByName("total_sum");
	   var data_total_tax = document.getElementsByName("total_tax");
	   for(i = 0; i < data_total_price.length; i++){
			sum_total_price += parseFloat(data_total_price[i].innerHTML);
			sum_total_count += parseFloat(data_total_count[i].innerHTML);
			sum_total_sum += parseFloat(data_total_sum[i].innerHTML);
			sum_total_tax += parseFloat(data_total_tax[i].innerHTML);
		}
	  
       target_count = document.getElementById("sum_of_count");
       target_count.innerHTML = Math.round(sum_total_count);
       
       target_sum = document.getElementById("sum_of_sum");
       target_sum.innerHTML = Math.round(sum_total_sum + sum_total_tax);//////
       
       target_price = document.getElementById("sum_of_price");
       target_price.innerHTML = Math.round(sum_total_sum);
       
       target_tax = document.getElementById("sum_of_tax");
       target_tax.innerHTML = Math.round(sum_total_tax);/////////
       
       //入力箇所に自動的に金額を入力
       //現金＋カードのときはどちらも空とする
       if($('#ticket_use').val() == ""){
			      var ticket_use = 0;
		      }else{
			      var ticket_use = $('#ticket_use').val();
		      }
       if($('#payment').val() == 1){//現金払いの時
       		$('#kinken').val("");
       		$('#points').val("");
       	   $('#cash').val("");
       	   $('#card').val("");
	       $('#cash').val(Math.round(sum_total_sum + sum_total_tax) - ticket_use * 1080);
	       $('#emoney').val("");
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0); 
	   }else if($('#payment').val() == 2){//カード払いの時
	   	$('#kinken').val("");
	   	   $('#points').val("");
	       $('#cash').val("");
       	   $('#card').val("");
	       $('#card').val(Math.round(sum_total_sum + sum_total_tax) - ticket_use * 1080);
	       $('#emoney').val("");
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0);
       }else if($('#payment').val() == 3){//現金＋カードの時
       		$('#ticket_use').val("");
       		$('#kinken').val("");
       		$('#points').val("");
	       $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
       	   $('#change').text("");
       }else if($('#payment').val() == 4){
       		$('#kinken').val("");
	   	   $('#points').val("");
	       $('#cash').val("");
       	   $('#card').val("");
       	   $('#emoney').val("");
	       $('#emoney').val(Math.round(sum_total_sum + sum_total_tax) - ticket_use * 1080);
	       $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		   $('#change').text(0);
       }
       
       $('#goodsid').val(0);
       $('#goods_name1').val("");
       $('#goods_name2').val("");
       $('#price_off').val(0);
       $('#price').val("");
       $('#count').val(1);
       $('#price_off_per').val(0);
		}else{
	        alert("商品が選択されていません");
        }
        }
        
	function query_in(){
		var str_check = $("#username").val();
		var str_sub_check = $('#sub_username').val();

		if($('#guest_seq').val() == "" || $('#guest_seq').val() == 0){
			$.ajax({type:"POST",
						url:"get_guest_seq.php",
						data:{id:str_check,sub_id:str_sub_check},
						async:false,
						success:function(data){$('#guest_seq').val(data);},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
 							$("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
							$("#textStatus").html("textStatus : " + textStatus);
							$("#errorThrown").html("errorThrown : " + errorThrown.message);
						}
			});
		}

        //insert into 
        //日付にはCURRENT_TIMEを使用する
            var guest_seq_temp = "";
            var guest_name_temp = "";
	        var staff_id = $('#staffid').val();//スタッフの番号
	        var guest_seq = $('#guest_seq').val();
	        if((guest_seq == "" || guest_seq == 0) && $("#username").val() != ""){
		        alert("通信エラーが発生しています。\nもう一度登録ボタンを押してください。");
		        return;
	        }
	        
	        var guest_name=$("#guest_name").text(); 
	        var guest_id = $("#username").val();//ゲストの番号
	        var cash = $('#payment').val();//キャッシュは１カードは２両方は3
	        var kinken_type = $('#kinken_type').val();
	        var kinken = $('#kinken').val();
	        var points = $('#points').val();
	        if(kinken == ""){
		        kinken = 0;
	        }
	        if(points == ""){
	        	points = 0;
	        }
	        var x = 0;
	        var pay_cash=0;
	        var pay_card=0;
	        var pay_emoney=0;
	        var reg_id="";
	        if(cash == 3){
		     pay_card=$('#card').val();
		     if(pay_card==""){
			     pay_card=0;
		     }
		     pay_cash=eval($('#sum_of_sum').text())-pay_card;
	        }else{
	        pay_cash = $('#cash').val();
	        pay_card = $('#card').val();
	        pay_emoney = $('#emoney').val();
	        }
	        var remarks = $('#textarea').val();
	        var remarks_uriage = $('#textarea_uriage').val();
	        if(isNaN(pay_cash) || isNaN(pay_card) || isNaN(pay_emoney) ){
		        alert("お預かり金額は半角で入力してください");
		        return;
	        }
	        
	        if(staff_id != 0 && guest_id != "" && insert.length > 0 && parseFloat($('#change').text()) >= 0){
	        if(cash == 2 && eval($('#change').text()) > 0){
		     alert("カードのお預かり金額を確認してください");
		     return;   
	        }
	        /*
if((eval(($('#ticket_h').text())) + eval(ticket_num) - eval($('#ticket_use').val())) < 0 ){
		        alert("チケットの使用枚数を確認してください");
		        return;
	        }
*/
	    
	        if(confirm('会計内容は正しいですか？')){
			$.ajax({type:"POST",
						url:"get_guest_seq_name.php",
						data:{name:guest_name},
						async:false,
						dataType:"json",
						success:function(data){
							guest_seq_temp = data[0];
							guest_name_temp = data[1];	
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }        
         });	
	       if((guest_seq_temp != guest_seq) && (guest_name != guest_name_temp)){
		       guest_seq = guest_seq_temp;
	       }
	        
	        
	        //顧客情報にチケット情報を送信
	        $.ajax({type:"POST",
	        url:"ticket_post.php",
	        data:{
	        	ticket:ticket_num,
	        	guest_id:guest_id,
	        	guest_seq:guest_seq
	        	},
	        async:false,
	        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }
         	        });
			//顧客情報に備考情報を送信
	        $.ajax({type:"POST",
	        url:"remarks_post.php",
	        data:{remarks:remarks,guest_id:guest_id,guest_seq:guest_seq},
	        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }
	        }); 
			//顧客情報に使用チケット情報と商品券情報を送信
	        $.ajax({type:"POST",
	        url:"ticket_use.php",
	        data:{ticket:$('#ticket_use').val(),guest_id:guest_id,guest_seq:guest_seq,kinken:kinken,points:points},
	        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }
	        });
	        $.ajax({
	        type:"POST",
		    url:"update_last.php",
		    data:{guest_id:guest_id,guest_seq:guest_seq},
		    error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
			}
	     });
	        /*ここからreg_post_sub.phpに渡す引数の計算*/
	        var goods_count_sub = $('#sum_of_count').text();//お買い上げ点数
	        var card_sub = $('#card').val();
	        if(card_sub == ""){
		        card_sub = 0;
	        }
	        var cashpluschange = $('#cash').val();
	        if(cashpluschange==""){
		        cashpluschange = 0;
	        }
	        var emoney_sub = $('#emoney').val();
	        if(emoney_sub == ""){
	        	emoney_sub = 0;
	        }
	        var ticket_sub = $('#ticket_use').val();//チケット使用数
	        if(ticket_sub ==""){
		        ticket_sub = 0;
	        }
	        var totalpayment=Math.round(eval($('#sum_of_sum').text()));//購入金額
	        var totaltax_sub = eval($('#sum_of_tax').text());
	        var cash_or_card_sub = $('#payment').val();//現金は１、カードは２、両方は３,e-money 4
	        var total_change = eval($('#change').text());
	        var cash_sub = eval(cashpluschange) - eval(total_change);
	        var temp_insert_string = "";
	        for (x = 0, len = insert_goods_name.length; x < len; x++) {
				temp_insert_string += insert_goods_name[x];
				}

				// if(kinken_type == 2 && kinken > totalpayment && total_change == 0){
				// 	totalpayment = kinken;
				// }
	        /*ここまで*/
	        	 
	        //月別表示のための情報をshop_log_subに送信
	        //guestテーブルの顧客情報も合わせて更新
	        /*guestテーブルに送信する物は 
	        利用回数(インクリメントするだけ),
	        購入数量goods_count_sub(count),
	        購入金額totalpayment(payment),
	        使用チケットticket_sub(ticket)*/
	        $.ajax({
	        type:"POST",
	        url:"reg_post_sub.php",
	        data:{staff_id:staff_id,
	        kinken_type:kinken_type,
	        kinken:kinken,
	        points:points,
	        guest_id:guest_id,
	        guest_seq:guest_seq,
	        count:goods_count_sub,
	        cash:cash_sub,
	        card:card_sub,
	        emoney:emoney_sub,
	        cashpluschange:cashpluschange,
	        discount:discount,
	        ticket:ticket_sub,
	        payment:totalpayment,
	        tax:totaltax_sub,
	        cardorcash:cash_or_card_sub,
	        total_change:total_change,
	        insert_goods_name:temp_insert_string,
	        ary_id:insert_goods_id,
	        guest_name:guest_name,
	        remarks_uriage:remarks_uriage
	        },
	        async:false,
	        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }
	        });
	        
			for (x = 0, len = insert.length; x < len; x++) {
				var temp_string = '\''+staff_id+'\',\''+guest_seq+'\',\''+insert[x]+'\',\''+cash+'\'';
				$.ajax({
						type:"POST",
						url:"reg_post.php",
						data:{input1:temp_string
							},
						async:false,
						error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }				});
				}
				$.ajax({type:"POST",
						url:"reg_post_count_incr.php",
						async:false,
						error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }        });
				alert("会計が登録されました");
				//レジIDをインクリメント
				document.location = "posfront.php";
				}
        }else{
	        alert("エラー　入力内容を確認してください");
        }
        }
    
        function cash_or_card(){
	        var kinken_type = $('#kinken_type').val();
	        if(kinken_type == 0){
	        var pay = $('#payment').val();
	        if(pay==1){
	          $('#cash').removeAttr("disabled");
		      $('#card').attr("disabled", "disabled");
		      $('#card').val("");
		      $('#emoney').attr("disabled", "disabled");
		      $('#emoney').val("");
		      $('#points').val("");
		      if($('#ticket_use').val() == ""){
			      var ticket_use = 0;
		      }else{
			      var ticket_use = $('#ticket_use').val();
		      }
		      $('#cash').val(eval($('#sum_of_sum').text()) - (1080 * ticket_use));
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text(0);
	        }else if(pay==2){
		      $('#card').removeAttr("disabled");
		      $('#cash').attr("disabled", "disabled");  
		      $('#cash').val("");
		      $('#emoney').attr("disabled", "disabled");  
		      $('#emoney').val("");
		      $('#points').val("");
		      if($('#ticket_use').val() == ""){
			      var ticket_use = 0;
		      }else{
			      var ticket_use = $('#ticket_use').val();
		      }
		      $('#card').val(eval($('#sum_of_sum').text()) - (1080 * ticket_use));
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text(0);  
	        }else if(pay==3){
	          $('#cash').removeAttr("disabled");
	          $('#card').removeAttr("disabled");
	          $('#emoney').attr("disabled", "disabled");
		      $('#emoney').val("");
	          $('#cash').val("");
	          $('#card').val("");
	          $('#points').val("");
	          $('#change').text("");
	        }else if(pay==4){
	          $('#emoney').removeAttr("disabled");
	          $('#cash').attr("disabled", "disabled");
		      $('#cash').val("");
		      $('#card').attr("disabled", "disabled");
		      $('#card').val("");
		      $('#points').val("");
		      if($('#ticket_use').val() == ""){
			      var ticket_use = 0;
		      }else{
			      var ticket_use = $('#ticket_use').val();
		      }
		      $('#emoney').val(eval($('#sum_of_sum').text()) - (1080 * ticket_use));
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text(0);  
	        }
	        }else{
		    var pay = $('#payment').val();
	        if(pay==1){
	          $('#cash').removeAttr("disabled");
		      $('#card').attr("disabled", "disabled");
		      $('#card').val("");
		      $('#emoney').attr("disabled", "disabled");
		      $('#emoney').val("");
		      $('#ticket_use').val("");
		      $('#kinken').val("");
		      $('#points').val("");
		      $('#cash').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");
	        }else if(pay==2){
		      $('#card').removeAttr("disabled");
		      $('#cash').attr("disabled", "disabled");  
		      $('#cash').val("");
		      $('#emoney').attr("disabled", "disabled");
		      $('#emoney').val("");
		      $('#ticket_use').val("");
			  $('#kinken').val("");
			  $('#points').val("");
		      $('#card').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");  
	        }else if(pay==3){
	          $('#cash').removeAttr("disabled");
	          $('#card').removeAttr("disabled");
	          $('#cash').val("");
	          $('#card').val("");
	          $('#emoney').attr("disabled", "disabled");
		      $('#emoney').val("");
	          $('#ticket_use').val("");
	          $('#kinken').val("");
	          $('#points').val("");
	          $('#change').text("");
	        }else if(pay==4){
	        	$('#emoney').removeAttr("disabled");
		      $('#cash').attr("disabled", "disabled");  
		      $('#cash').val("");
		      $('#card').attr("disabled", "disabled");
		      $('#card').val("");
		      $('#ticket_use').val("");
			  $('#kinken').val("");
			  $('#points').val("");
		      $('#card').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");  
	        }
 
	        }
        }
 function kinken(){
	        var kinken_type = $('#kinken_type').val();
	        if(kinken_type==0){
	          $('#card').val("");
		      $('#cash').val("");
		      $('#emoney').val("");
		      $('#ticket_use').val("");
		      $('#kinken').val("");
		      $('#points').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");
	        }else if(kinken_type==1){
	        
		      $('#card').val("");
		      $('#cash').val("");
		      $('#emoney').val("");
		      $('#ticket_use').val("");
		      $('#kinken').val("");
		      $('#points').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");  
		      
	        }else if(kinken_type==2){
	        
	          $('#card').val("");
		      $('#cash').val("");
		      $('#emoney').val("");
		      $('#ticket_use').val("");
		      $('#kinken').val("");
		      $('#points').val("");
		      $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
		      $('#change').text("");  
	        }
        }

    </script>
    <script>
		function sub_id(){
			if($('#id_sub').prop('checked') == 1){
				$('#sub_username').val("");
				$('#username').val("");
				$('#guest_name').text("");//id="guest_name" にphpからの値入れる
				$('#guest_ticket').text("");
				$('#ticket_h').text("");
				$('#guest_kana').text("");
				$('#textarea').text("");
				$('#guest_seq').val("");

				$('#sub_username').removeAttr("disabled");
			}else{
				$('#sub_username').val("");
				$('#username').val("");
				$('#guest_name').text("");//id="guest_name" にphpからの値入れる
				$('#guest_ticket').text("");
				$('#ticket_h').text("");
				$('#guest_kana').text("");
				$('#textarea').text("");
				$('#guest_seq').val("");
				$('#sub_username').attr("disabled","disabled");
			}
	}
</script>
    <script>
     $(document).ready(function(){//guest_seq の値を元に動作するように書き直し
	    var str_seq = $('#guest_seq').val();      
      	//strの中身をphpに飛ばす
      	if(str_seq != ""){
      	$('#guest_name').load('get_guest_name_seq.php',{seq:str_seq});//id="guest_name" にphpからの値入れる
      	$('#guest_ticket').load('get_guest_yomi_seq.php',{seq:str_seq});
      	$('#ticket_h').load('get_guest_ticket_seq.php',{seq:str_seq});
      	$('#guest_kana').load('get_guest_ticket_seq.php',{seq:str_seq});
      	$('#textarea').load('get_remarks_seq.php',{seq:str_seq});
      	}else{
	      	$('#guest_name').text("");
	      	$('#guest_ticket').text("");
	      	$('#ticket_h').text("");
	      	$('#guest_kana').text("");
	      	$('#textarea').text("");
      	}
     });
    </script>
        <script>
  $(document).ready(function(){   
    $("#username").keyup(function(e) {
        var str = $("#username").val();
        var str_sub = $('#sub_username').val();
      	//strの中身をphpに飛ばす
      	if(str != ""){
      	$('#guest_name').load('get_guest_name.php',{id:str,sub_id:str_sub});//id="guest_name" にphpからの値入れる
      	$('#guest_ticket').load('get_guest_yomi.php',{id:str,sub_id:str_sub});
      	$('#ticket_h').load('get_guest_ticket.php',{id:str,sub_id:str_sub});
      	$('#guest_kana').load('get_guest_ticket.php',{id:str,sub_id:str_sub});
      	$('#textarea').load('get_remarks.php',{id:str,sub_id:str_sub});	
      	
      	$.ajax({type:"POST",
						url:"get_guest_seq.php",
						data:{id:str,sub_id:str_sub},
						async:false,
						success:function(data){$('#guest_seq').val(data);},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }        });
      	 //$.post('get_guest_seq.php',{id:str,sub_id:str_sub},function(data){$('#guest_seq').val(data);});
      //	$('#guest_seq').load('get_guest_seq.php',{id:str,sub_id:str_sub});
      	}else{
	      	$('#guest_name').text("");
	      	$('#guest_ticket').text("");
	      	$('#ticket_h').text("");
	      	$('#guest_kana').text("");
	      	$('#textarea').text("");
	      	$('#guest_seq').val("");
	      	
      	}
    });
    $("#sub_username").keyup(function(e) {
        var str = $("#username").val();
        var str_sub = $('#sub_username').val();
      	//strの中身をphpに飛ばす
      	if(str != ""){
      	$('#guest_name').load('get_guest_name.php',{id:str,sub_id:str_sub});//id="guest_name" にphpからの値入れる
      	$('#guest_ticket').load('get_guest_yomi.php',{id:str,sub_id:str_sub});
      	$('#ticket_h').load('get_guest_ticket.php',{id:str,sub_id:str_sub});
      	$('#guest_kana').load('get_guest_ticket.php',{id:str,sub_id:str_sub});
      	$('#textarea').load('get_remarks.php',{id:str,sub_id:str_sub});      	
      	$.ajax({type:"POST",
						url:"get_guest_seq.php",
						data:{id:str,sub_id:str_sub},
						async:false,
						success:function(data){$('#guest_seq').val(data);},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("#XMLHttpRequest").html("XMLHttpRequest : " + XMLHttpRequest.status);
            $("#textStatus").html("textStatus : " + textStatus);
            $("#errorThrown").html("errorThrown : " + errorThrown.message);
         }        });
      	}else{
	      	$('#guest_name').text("");
	      	$('#guest_ticket').text("");
	      	$('#ticket_h').text("");
	      	$('#guest_kana').text("");
	      	$('#textarea').text("");
	      	$('#guest_seq').val("");

      	}
	  	 });
	  	
	$('#cash').keyup(function (e) {
		
		var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var kinken_type = $('#kinken_type').val();
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_ticket = $('#ticket_use').val();
        var v_emoney = $('#emoney').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
        if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        if(kinken_type == 1 || kinken_type==0){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	    var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
         if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) - eval(points) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney')
        $('#kinken').val("");
        $('#points').val("");
        $('#ticket_use').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }

        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
	        alert("預かり金額が多すぎます");
	        $('#cash').val("");
	        return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }

		}        });
    $('#card').keyup(function (e) {
       		var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var kinken_type = $('#kinken_type').val();
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_emoney = $('#emoney').val();
        var v_ticket = $('#ticket_use').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
        if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        if(kinken_type == 1 || kinken_type==0){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	   var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
         if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) -eval(points) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney').val("");
        $('#ticket_use').val("");
        $('#kinken').val("");
        $('#points').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }

        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
	        alert("預かり金額が多すぎます");
	        $('#cash').val("");
	        return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }

		}         });

	$('#emoney').keyup(function (e) {
		
		var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var kinken_type = $('#kinken_type').val();
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_ticket = $('#ticket_use').val();
        var v_emoney = $('#emoney').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
        if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        if(kinken_type == 1 || kinken_type==0){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	    var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
         if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) - eval(points) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney')
        $('#kinken').val("");
        $('#points').val("");
        $('#ticket_use').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }

        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
	        alert("預かり金額が多すぎます");
	        $('#cash').val("");
	        return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }

		}        });

	$('#ticket_use').keyup(function (e) {
		var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var kinken_type = $('#kinken_type').val();
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_emoney = $('#emoney').val();
        var v_ticket = $('#ticket_use').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
        if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        if(kinken_type == 1 || kinken_type==0){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	    var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
         if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) - eval(points) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney').val("");
        $('#ticket_use').val("");
        $('#kinken').val("");
        $('#points').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }

        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
	        alert("預かり金額が多すぎます");
	        $('#cash').val("");
	        return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) +eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		} 
        });
        $('#kinken').keyup(function (e) {
		var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var kinken_type = $('#kinken_type').val();
		if(kinken_type == 0){
			alert("金券の種類を選択してください");
			$('#kinken').val("");
			return;
		}
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_emoney = $('#emoney').val();
        var v_ticket = $('#ticket_use').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
        if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        if(kinken_type == 1){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	    var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
        $('#card').val("");
        $('#cash').val("");
        $('#emoney').val("");
        $('#ticket_use').val("");
        $('#points').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(0);	        
	    return;
        }
               if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney').val("");
        $('#ticket_use').val("");
        $('#kinken').val("");
        $('#points').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
        }

		}
        });

        $('#points').keyup(function (e) {
       	var kinken = $('#kinken').val();
		if(kinken == ""){
			kinken = 0;
		}
		var kinken_type = $('#kinken_type').val();
		var points = $('#points').val();
		if(points == ""){
			points = 0;
		}
		var v_cash = $('#cash').val();
        var v_card = $('#card').val();
        var v_emoney = $('#emoney').val();
        var v_ticket = $('#ticket_use').val();
        var vcoc = $('#payment').val();
		if(v_cash == ""){
	        v_cash=0;
        }
		if(v_card == ""){
	        v_card=0;
        }
        if(v_ticket == ""){
	        v_ticket=0;
        }
		if(v_emoney == ""){
        	v_emoney = 0;
        }
        if($.isNumeric(v_card) == 0 || $.isNumeric(v_cash) == 0 || $.isNumeric(v_ticket)==0 || $.isNumeric(v_emoney)==0 || $.isNumeric(kinken)==0 || $.isNumeric(points)==0 ){
	        $('#change').css({color:'#ff0000',fontSize:'x-small'});
	        $('#change').html("半角で入力してください");
	        return;    
        }
        $('#cash').val('');
        $('#card').val('');
        $('#emoney').val('');
        if(kinken_type == 1 || kinken_type==0){//お釣りありの時
        var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
        var vsub = eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) - eval(points);
        if(vsub < 0){
        	sub = vsub;
        	vsub = 0;
        }
        if(vcoc == 1){
        	v_cash = vsub;
        }else if(vcoc == 2){
        	v_card = vsub;
        }else if(vcoc == 4){
        	v_emoney = vsub;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(kinken) + eval(points) - eval(sum_of_sum);
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));
            if(vcoc == 1){
            	$('#cash').val(Math.round(vsub));
	        }else if(vcoc == 2){
	        	$('#card').val(Math.round(vsub));
	        }else if(vcoc == 4){
	        	$('#emoney').val(Math.round(vsub));
	        }
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
	    	if(vcoc == 1){
            	$('#cash').val(Math.round(vsub));
	        }else if(vcoc == 2){
	        	$('#card').val(Math.round(vsub));
	        }else if(vcoc == 4){
	        	$('#emoney').val(Math.round(vsub));
	        }
        }
		}else if(kinken_type == 2){//お釣り無しの時
		
	   var sum_of_price = $('#sum_of_price').text();
        var sum_of_sum = Math.round(eval(sum_of_price) * 1.08);
         if(eval(sum_of_sum) - eval($('#ticket_use').val() * 1080) - eval(kinken) -eval(points) < 0){
	        alert("金額を確認してください");
	         $('#card').val("");
        $('#cash').val("");
        $('#emoney').val("");
        $('#ticket_use').val("");
        $('#kinken').val("");
        $('#points').val("");
	    $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text("");
	    return;
        }

        sum_of_sum = sum_of_sum - eval(kinken);
        if(sum_of_sum <= 0){
	        alert("預かり金額が多すぎます");
	        $('#cash').val("");
	        return;
        }
        var sub = eval(v_cash) + eval(v_card) + eval(v_emoney) + eval($('#ticket_use').val() * 1080) + eval(points) - eval(sum_of_sum);
        var vsub = eval(sub) * -1;
        sub = sub - vsub;
        if(sub < 0){
        $('#change').css({color:'#ff0000',fontWeight:'bold',fontSize:'large'});
        $('#change').text(Math.round(sub));     
        	if(vcoc == 1){
            	$('#cash').text(Math.round(vsub));
	        }else if(vcoc == 2){
	        	$('#card').text(Math.round(vsub));
	        }else if(vcoc == 4){
	        	$('#emoney').text(Math.round(vsub));
	        }
        }else{
        $('#change').css({color:'#000000',fontWeight:'bold',fontSize:'large'});
	    $('#change').text(Math.round(sub));  
	    	if(vcoc == 1){
            	$('#cash').text(Math.round(vsub));
	        }else if(vcoc == 2){
	        	$('#card').text(Math.round(vsub));
	        }else if(vcoc == 4){
	        	$('#emoney').text(Math.round(vsub));
	        }
        }
		}
	});
  });
  </script>

<link rel="stylesheet" href="../css/style-posfront.css">
<title><?php echo $_SESSION['name'];?></title>

<body>
<div id="wrapper">
	<?php
		include("db_connect.php");
		$sqls = "SELECT DATE_FORMAT(start_time,'%Y-%m-%d') as stime FROM pos_start WHERE shop_id=$shop_id ORDER BY stime DESC";
		$recordsets =mysqli_query($db, $sqls);
		$stime = mysqli_fetch_assoc($recordsets);
		$ndate2=date("Y-m-d");
		?>
		<script>
		$(document).ready (function submit(){
		var ndate2 = <?php echo "'".$ndate2."'"; ?>;
		var stime = <?php echo "'".$stime['stime']."'"; ?>;
		if(ndate2 != stime){
		alert("「レジ開始金の設定」を行ってください。");
		location.href = 'posstart.php';
		}
		});
		</script>

</script>
<div id="header">
<div id="header_up">
<div id="header_shop">
<?php echo($_SESSION['name']); ?>

</div>
<div id="logout">
| <a  style="text-align:right;" href="login.php">ログアウト</a> |
</div>
</div>

<div id="header_low">
<div id="time">
<?php 
include("year.php");
?>
<div id="time_div">
</div>
</div>
<div id="menu">
<ul class="menu">
	<li><a href="posfront.php"><img src="../css/pos_f/h_pos_on.gif" height="40" width="145" align=center ></a></li>
	<li><a href="posstart.php">
		<img src="../css/pos_f/h_register.gif"
		onmouseover="this.src='../css/pos_f/h_register_on.gif';" 
		onmouseout="this.src='../css/pos_f/h_register.gif';" height="40" width="145" align=center></a></li>
	<li><a href="index.php">
		<img src="../css/pos_f/h_menu.gif" 
		onmouseover="this.src='../css/pos_f/h_menu_on.gif';" 
		onmouseout="this.src='../css/pos_f/h_menu.gif';"height="40" width="145" align=center></a></li>
</ul>
</div>
</div>
</div>

<div id="contents">
<div id="top_table">
<table id="t_left">
	<tr>
		<th class="a">スタッフ番号</th>
		<th>
		<select id="staffid"onchange="SearchId();">
		<option value="0">IDを選択してください</option>
		<?php 
		$sql = "SELECT id FROM staff WHERE shop_id=$shop_id ORDER BY id";
		$rs = mysqli_query($db, $sql);
		if(!$rs){
			die('クエリ失敗');
			}
		while(($arr_item = mysqli_fetch_assoc($rs)))
			{
				foreach($arr_item as $value){
					echo "<option value= $value > $value </option><br>";
					}
			}
	?>
</th>
	<tr>
		<th class="a" height="20">担当者名</th>
		<th id="show_id">
		</th>
	</tr>
</table>
<table id="t_right">
	<tr>
		<th class="a" rowspan="2">会員状況</th>
		<th class="member_check" rowspan="2">
				<a href="#"><img src="../css/pos_f/memberbtn_on.gif" height="50" width="60"></a> 
				<a href="./customernew.php">
					<img src="../css/pos_f/nonmemberbtn.gif" 
					onmouseover="this.src='../css/pos_f/nonmemberbtn_on.gif';" 
					onmouseout="this.src='../css/pos_f/nonmemberbtn.gif';"
					height="50" width="60"></a>
			</th>
		<th class="a">
		会員番号
		</th>
		<th id="sub_or_not">
			<span id="input_box">
				<input id="sub_username" 
				<?php
					if(isset($_GET['sub_id'])&&$_GET['sub_id'] != ""){
						echo "value=".$_GET['sub_id'];
					}else{
						echo 'disabled';
					}
				?> size="4" maxlength="10"/>-
				<input name="main_id" id="username" type="text" size="10" maxlength="20" 
				<?php
					if(isset($_GET['id'])){
						echo "value=".$_GET['id'];
					}
				?>></br>
			</span>
			<input type="checkbox" name="id_sub" id="id_sub" onclick="sub_id();" 
			<?php
					if(isset($_GET['sub_id'])&&$_GET['sub_id'] != ""){
						echo "checked";
					}
				?>>
			<label for="id_sub">ハイフン等を使う</label>
		</th>
		<th class="a">会員名</th>
		<th id="guest_name" width="200"><!--会員名表示-->
		</th>
	</tr>
	<tr>
		<th class="a">保有チケット</th>
		<th id="guest_kana">			
		</th>
		<th class="a">フリガナ</th>
		<th id="guest_ticket" width="200">
		<!--フリガナ表示--></th>
	</tr>
</table>
</div>
<input hidden type="text" id = "guest_seq" value=<?php if(isset($_GET['seq'])){echo "'".$_GET['seq']."'";}?> >
<p style="text-align:right; margin-top:30px;">
<a href="./posfront_customer.php">
	<img src="../css/pos_f/search_btn.gif"
    onmouseover="this.src='../css/pos_f/search_btn_on.gif';"
    onmouseout="this.src='../css/pos_f/search_btn.gif';"></a>
</p>

<table id="kounyu">
	<tr class="b" id="base">
		<th width="80">削除</th>
		<th width="150">商品名</th>
		<th width="300">商品名/備考</th>
		<th width="120">割引</th>
		<th width="150">単価(税別)</th>
		<th width="30">数量</th>
		<th width="180">金額</th>
		<th width="80">登録</th>
	</tr>
	<tr><!--入力する段-->
		<th><input type="button" value="全削除" id="delete" onclick="delete_table();"></th>
		<th><!--商品番号プルダウン-->
		<select id="goodsid"onchange="SerchGoods();">
		<option value="0">商品を選択してください</option>
		<?php 
		$sql = "SELECT id,name FROM goods WHERE (shop_id=$shop_id or shop_id=0) ORDER BY category_big ASC,id";
		$rs = mysqli_query($db,$sql);
		if(!$rs){
			die('クエリ失敗');
			}
		while(($arr_item = mysqli_fetch_assoc($rs)))
			{
				$id = $arr_item['id'];
				$name= $arr_item['name'];
				if($id != 104100 && $id != 401100 && $id != 401300 && $id != 401500 && $id !=104101 && $id != 1300021 && $id != 1300022){
					echo "<option value= $id > $name </option><br>";
					}
			}
			
	?>
		</th>
		<th ><!--商品名--><input id="goods_name1" type="text" value=""><br><input id="goods_name2" type="text" value=""></th>
		<th><input type="text" id="price_off"value="0" size="5"> 円
		<input type="text" id="price_off_per"value="0" size = "5"> ％
		</th>
		<th><input id="price" type="text" value="" size="12"></th>
		<th><input type="text" id="count" value = "1" style="width:20px;"><!--数量--></th>
		<th id="sum"><!--金額(税込)--></th>
		<th><input type="image" src="../css/pos_f/price.gif" onmouseover="this.src='../css/pos_f/price_on.gif';" onmouseout="this.src='../css/pos_f/price.gif';" id="submit" onclick="submit();">
		</th>
	</tr>
	
</table>

<p></p>
<div id="low_table">
	<div id="l_t_left_div">
	<table id="l_t_left">
	<tr>
		<th class="a" style="font-size:90%">備考<br>(顧客情報)<br>編集可能</th>
		<th><!--入力フォーム-->
		<!--<input type="text"  style="width:300px;height:200px;">-->
		<textarea cols="30" rows="8" id="textarea"></textarea>
		</th>
		</tr>
		<tr>
		<th class="a" style="font-size:90%">備考<br>(売上情報)<br>編集可能</th>
		<th><!--入力フォーム-->
		<!--<input type="text"  style="width:300px;height:200px;">-->
		<textarea cols="30" rows="8" id="textarea_uriage"></textarea>
		</th>
	</tr>
	</table>
	</div>

<table id="l_t_right">
	<tr style="height:40px;">
		<th class="a" style="width:130px">商品金額</th>
		<th id="sum_of_count">0</th>
		<th style="width:50px;">点</th>
		<th id="sum_of_price">0</th>
		<th>円</th>
		<th class="a" style="width:100px;">消費税</th>
		<th id="sum_of_tax">0</th>
		<th>円</th>
		<th></th>
	<tr>
		<th class="a">保有</br>チケット</th>
		<th colspan="3" id="ticket_h"></th>
		<th>枚</th>
		<th class="a">合計金額</th>
		<th id="sum_of_sum" class="sum_of_sum">0</th>
		<th>円</th>
		<th></th>
	<tr style="height:40px;">
		<th class="a">使用</br>チケット</th>
		<th colspan="3"><input type="text" id="ticket_use"></th>
		<th>枚</th>
		<th class="a">現金</br>預かり</th>
		<th><input type="text" id="cash" size="18"></th>
		<th>円</th>
		<th></th>
	<tr style="height:40px;">
		<th class="a">商品券</th>
		<th colspan="2"><select onchange="kinken();" id="kinken_type"><option value="0">金券の種類</option><option value="1">お釣りあり</option><option value="2">お釣りなし</option></select></th>
		<th colspan="1"><input type="text" id="kinken" size="8"></th><th colspan="1">円</th>
		<th class="a">ｸﾚｼﾞｯﾄ<br>預かり</th>
		<th><input type="text" disabled="disabled" id="card" size="18"></th>
		<th>円</th>
				<th rowspan="3">
			<input type="image" 
			src="../css/pos_f/check.gif"
			onmouseover="this.src='../css/pos_f/check_on.gif';"
			onmouseout="this.src='../css/pos_f/check.gif';"
			onclick="query_in();" alt="入力内容を確認"
			width="90" height="85"></th>
	<tr style="height40px:">
		<th class="a">WAON POINT</th>
		<th colspan="3"><input type="text" id="points" size="20"></th>
		<th>円</th>
		<th class="a">電子ﾏﾈｰ</th>
		<th><input type="text" disabled="disabled" id="emoney" size="18"></th>
		<th>円</th>
	</tr>
	<tr style="height:40px;">
		<th class="a">お支払<br>方法</th>
		<th colspan="4"><select onchange="cash_or_card();" id="payment">
			<option value="1">現金</option>
			<option value="2">カード</option>
			<option value="3">現金＋カード</option>
			<option value="4">電子マネー</option>
		</select></th>
		<th class="a">お釣り</th>
		<th id="change" class="change">0</th>
		<th>円</th>
</table>
</div><!--low_table-->
</div><!--contents-->
</div>
<div id="hidden"></div>
</body>
</html>