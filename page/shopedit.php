<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">

<?php
    $log = $_GET['id'];
    include("db_connect.php");
	$sql = sprintf("SELECT * FROM members WHERE shop_id=%s",mysqli_real_escape_string($db, $log));
	$recordSet = mysqli_query($db, $sql);
	$data = mysqli_fetch_assoc($recordSet);
?>

<script>
function NumberCheck() {
   var str = document.shop_input.pass.value;
   if( str.match(/[^a-zA-Z0-9]+$/) ) {
      alert("パスワードは、半角英数字のみで入力して下さい。");
      return 1;}
      return 0;}

function shop(){
	   var check = 0;
	   check += NumberCheck();
	   if( check > 0 ) {
       return false;
   }
    var flag = 0;
	if(document.shop_input.name.value == ""){
		flag = 1;
	}
	else if(document.shop_input.abb_name.value == ""){
		flag = 1;
	}
	if(flag){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}
       if(confirm("この内容で登録しますか") == true){
	var name = $('#name').val();
	var abb_name = $('#abb_name').val();
	var login_id = $('#login_id').val();
	var password = $('#pass').val();
	var mailaddress = $('#mailaddress').val();
	var start = $('#start').val();
	var end =$("#end").val();
	var shop_id =$("#shop_id").val();
	$.post('shopedit_do.php',{
		name:name,abb_name:abb_name,login_id:login_id,password:password,mailaddress:mailaddress,start:start,end:end,shop_id:shop_id
	},function(data){
		if(data == 0){
			alert("更新失敗");
		}else{
			alert(data);
			document.location = "shopedit.php?id=<?php echo $data['shop_id']; ?>";}});
}else{
	alert("キャンセルされました。");
}
}

</script>

<h1>基本情報の編集</h1>
			<form name="shop_input" action="#" method="post">
<p>
	<table>
		<tr>
			<th class="a">店舗番号</th>
			<th><?php echo $data['shop_id']; ?> (店舗番号は変更できません。)</th>
		</tr>
		<tr>
			<th class="a">店舗名<font color="red">(*)</font></th>
			<th><input type="text" name="name" id="name" value="<?php print(htmlspecialchars($data['name'], ENT_QUOTES)); ?>" ></th>
		<tr>
			<th class="a">略称<font color="red">(*)</font></th>
			<th><input type="text" name="abb_name" id="abb_name" value="<?php print(htmlspecialchars($data['abb_name'], ENT_QUOTES)); ?>"/></th>
		<tr>
			<th class="a">店舗ID</th>
			<th><?php print(htmlspecialchars($data['login_id'], ENT_QUOTES)); ?> (店舗IDは変更できません)
			     <input type="hidden" id="login_id" value="<?php print(htmlspecialchars($data['login_id'], ENT_QUOTES)); ?>"></input></th>
		<tr>
			<th class="a">パスワード</th>
			<th><input type="text" name="pass" id="pass" value="<?php print $data['password']; ?>" onblur="NumberCheck();"/>
				（必ず半角英数字で入力してください。４文字以上を推奨します。）</th>
		<tr>
			<th class="a">店舗管理者メールアドレス</th>
			<th>
				<input type="text" name="mailaddress" id="mailaddress" value="<?php print(htmlspecialchars($data['mailaddress'], ENT_QUOTES)); ?>"/></th>
		<tr>
			<th class="a">開始レジ金のメッセージ</th>
			<th>
			<select name="start" id="start">
			<?php
			if($data['start'] != ""){
			$sta = $data['start'];
			}
			for($i = 0; $i<=23; $i++):?>
			<option value="<?php echo $i;?>" <?php if($i == $sta){echo 'selected="selected"';}?>><?php echo $i;?></option>
		    <?php endfor;?>
		    </select>時（開始レジ金のエラーメッセージを表示する時間）</th>
		<tr>
			<th class="a">締め処理のメッセージ</th>
			<th>
			<select name="end" id="end">
			<?php
			if($data['end'] != ""){
			$end = $data['end'];
			}
			for($i = 0; $i<=23; $i++):?>
			<option value="<?php echo $i ;?>" <?php if($i == $end){echo 'selected="selected"';}?>><?php echo $i;?></option>
		    <?php endfor;?>
		    </select>時（開始レジ金のエラーメッセージを表示する時間）</th>
		    <input type="hidden" id="shop_id" value="<?php print(htmlspecialchars($data['shop_id'], ENT_QUOTES)); ?>"></input>
	</table>
</form>
<p>
<font color="#ff0000">(*)</font>は必須項目です。
<p style="text-align:center;">
<input type="image" src="../css/image/contents/view.gif" onclick="return shop();" alt="上記の内容で登録"/>
</p>
</div>
<?php include("footer.php"); ?>
