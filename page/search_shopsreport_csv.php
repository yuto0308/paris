<?php

	session_start();
	include("db_connect.php");

	$str="";

	$year_s		= mysqli_real_escape_string($db,$_GET['year_s']);
	$month_s	= mysqli_real_escape_string($db,$_GET['month_s']);
	$day_s		= mysqli_real_escape_string($db,$_GET['day_s']);
	$year_e		= mysqli_real_escape_string($db,$_GET['year_e']);
	$month_e	= mysqli_real_escape_string($db,$_GET['month_e']);
	$day_e		= mysqli_real_escape_string($db,$_GET['day_e']);
	$day_e=$day_e + 1;

	$dom=mysqli_real_escape_string($db,$_GET['dom']);
	if($dom==1){
	$select_dom = ' DATE_FORMAT(date, "%Y年%m月%d日") as datetime, DATE_FORMAT(date, "%a") as week, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-'.$day_s.'" AND date <= "'.$year_e.'-'.$month_e.'-'.$day_e. '"';
	}else{
	$select_dom = ' DATE_FORMAT(date, "%Y年  %m月") as datetime, ';
	$date = ' date >= "'.$year_s.'-'.$month_s.'-1" AND date <= "'.$year_e.'-'.$month_e.'-31" ';
	}

		$sql = ' SELECT members.name, '.$select_dom;
		$sql.= ' SUM(count) as sum_item, COUNT(count) as count_item, members.shop_id as m_sid, ';
		$sql.= ' SUM(sum) as sum, SUM(ticket) as ticket, SUM(card) as s_card, SUM(cash) as s_cash, SUM(payback) as s_pay, SUM(count_payback) as s_p_count, SUM(discount) as discount,SUM(kinken) as kinken,date ';
		$sql.= ' FROM members, shop_log_sub ';
		$sql.= ' WHERE shop_log_sub.shop_id=members.shop_id AND '.$date ;
		$sql.= ' GROUP BY datetime, shop_log_sub.shop_id ';
		$sql.= ' ORDER BY datetime DESC ';
		$recordset = mysqli_query($db, $sql);
		if(mysqli_num_rows($recordset) == 0){
			echo "対象データがありません";
			return;
		}

		if($dom==1){
		$filename = "shop_daily.csv";
		$str.=	"日付,曜日,店舗番号,店舗名,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,返品点数,返品金額,販売金額,売上金額\n";
		while ($table = mysqli_fetch_assoc($recordset)){
			$datetime=$table['datetime'];
			$week=$table['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
				}
			$s_id = $table['m_sid'];
			$discount = $table['discount'];
			$pb_count = $table['s_p_count'];
			$pb_sum = $table['s_pay'];
			$shopname=$table['name'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$cash = $table['s_cash'];
			$card= $table['s_card'];
			$sum=$table['sum'];
			$kinken=$table['kinken'];
			//$sum = $cash + $card;
			if($table['date'] <= "2014-03-31 23:59:59"){
				$times_temp = 1050;
			}else{
				$times_temp = 1080;
			}
			$ticket_num = $table['ticket'];
			$ticket=$table['ticket'] * $times_temp;
			$g_sum = $sum;
			$sum= $sum - $ticket; 
			//$g_sum = $sum + $ticket;
			//$non_ticket=$sum-$ticket;
			$str.="$datetime,$week_str,$s_id,$shopname,$count_item,$sum_item,$cash,$card,$ticket_num,$kinken,$discount,$pb_count,$pb_sum,$g_sum,$sum\n";
			}
			}else{
				$filename = "shop_monthly.csv";
			$str.=	"日付,店舗番号,店舗名,販売件数,販売点数,現金,クレジット,使用チケット,商品券,割引,返品点数,返品金額,販売金額,売上金額\n";
			while ($table = mysqli_fetch_assoc($recordset)){
			$datetime=$table['datetime'];
			$s_id = $table['m_sid'];
			$discount = $table['discount'];
			$pb_count = $table['s_p_count'];
			$pb_sum = $table['s_pay'];
			$shopname=$table['name'];
			$sum_item=$table['sum_item'];
			$count_item=$table['count_item'];
			$cash = $table['s_cash'];
			$card= $table['s_card'];
			$sum=$table['sum'];
			$kinken=$table['kinken'];
			//$sum = $cash + $card;
			if($table['date'] <= "2014-03-31 23:59:59"){
				$times_temp = 1050;
			}else{
				$times_temp = 1080;
			}
			$ticket_num = $table['ticket'];
			$ticket=$table['ticket'] * $times_temp;
			$g_sum = $sum;
			$non_ticket=$sum-$ticket;
			$str.="$datetime,$s_id,$shopname,$count_item,$sum_item,$cash,$card,$ticket_num,$kinken,$discount,$pb_count,$pb_sum,$sum,$non_ticket\n";
			}
		}



		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=$filename");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));
		return;
?>