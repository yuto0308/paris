<?php
		session_start();
		include("db_connect.php");
		$maxrow = 5;//一度に表示する行数
		$str="";
		$str_button="";
		
		$result_ary=array();//一つ目に表、２つ目にボタン格納
		
		$num=$_POST['num'];
		$show = ($num - 1) * $maxrow;//表示する範囲を計算
		
		$str .= "<tr class='a' name='add'>
				<th style='text-align:center;'>商品コード</th>
				<th style='text-align:center;'>商品名</th>
				<th style='text-align:center;'>点数</th>
				<th style='text-align:center;'>売上金額</th>
				</tr>";


		$shops = $_POST['shops'];
		if($shops == 0){
			$shops = '1';
		}else{
			$shops = 'shop_log.shop_id = '.$shops;
		}
		$cat_big=$_POST['cat_big'];//大カテゴリ
		if($cat_big == 0){
			$cat_big_str = "1";
		}else{
			$cat_big_str = "category_big = $cat_big ";
		}

		$sort = $_POST['sort'];
		if($sort == 0){
			$sort = ' ORDER BY price DESC ';
		}elseif($sort == 1){
			$sort = ' ORDER BY num DESC ';
		}

		$year_s=$_POST['year_s'];
		$month_s=$_POST['month_s'];
		if($month_s < 10){
			$month_s = "0".$month_s;
		}
		
		$day_s_str = $year_s . "-" . $month_s . "-01 00:00:00";
		
		$year_e=$_POST['year_e'];
		$month_e=$_POST['month_e'];
		if($month_e < 10){
			$month_e = "0".$month_e;
		}
	
		$day_e_str = $year_e . "-" . $month_e . "-31 23:59:59";
		
		$sql_show = " SELECT ";
		$sql_show .= " goods_id, goods.name as goods_name, SUM(goods_num) AS num, ";
		$sql_show .= " SUM(shop_log.price) AS price ";
		$sql_show .= " FROM shop_log,goods ";
		$sql_show .= " WHERE day >= '$day_s_str' AND ";
		$sql_show .= " day <= '$day_e_str' AND $shops ";
		$sql_show .= " AND shop_log.goods_id = goods.id ";
		$sql_show .= " AND  $cat_big_str ";
		$sql_show .= " GROUP BY goods_id $sort ";

		$query_show = mysqli_query($db,$sql_show);
		if(!$query_show){
			$str .=  "
			<tr name='add'>
			<th colspan = '5' style='text-align:center;'>対象データがありません</th>
			</tr>";
			$str_button = '<input type="button" style="color:blue;font-size:large;" name="add_button" value="1" onclick="goods_search(1)">';	
			$result_ary[] = $str;
			$result_ary[] = $str_button;
			echo json_encode($result_ary);
			return;
		}
		$row = mysqli_num_rows($query_show);
		if($row == 0){
			$str .= "
					<tr name='add'>
					<th colspan = '5' style='text-align:center;'>対象データがありません</th>
					</tr>";
		}else{
			while($arr_show = mysqli_fetch_assoc($query_show)){	
				$str .= "<tr name='add' class='table-even'>
					<th style='text-align:center;'>".$arr_show['goods_id']."</th>
					<th style='text-align:center;'>".$arr_show['goods_name']."</th>
					<th style='text-align:center;'>".number_format($arr_show['num'])."</th>
					<th style='text-align:center;'>".number_format($arr_show['price'])."</th>
				  </tr>";
			}
		}

		$result_ary[] = $str;
		echo json_encode($result_ary);


	?>
