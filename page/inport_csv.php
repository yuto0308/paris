<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<title>CSV入力</title> 
<link rel="stylesheet" href="../css/style.css">
<script src="jquery-2.1.1.js"></script>
<script src="./ajaxzip3.js" charset="UTF-8"></script>
<link type="text/css" rel="stylesheet"
  href="http://code.jquery.com/ui/1.10.3/themes/cupertino/jquery-ui.min.css" />
<script type="text/javascript"
  src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript">
$(function() {
  $('#acc').accordion({
    collapsible: true ,
    active: 100 ,
    heightStyle: 'content'
  });
});

</script>
</head>
<body>
<?php session_start(); ?>
  <?php
  if($_SESSION['id'] == ""){
    header('location: login.php');
  }else{
    if($_SESSION['ownercheck'] != 0){
      header('location: login.php');
    }
  }
  ?>

  <form action="./upload.php" method="POST"
enctype="multipart/form-data" accept-charset="utf-8">
<input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
<!--1024*1024*10 = 10MB-->
<input type="file" name="my_file" />
<input type="submit" value="ファイルの転送開始する" />
</form>
  
</body>
</html>