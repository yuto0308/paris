<?php
session_start();
include('db_info.php');

$seq = $_POST['seq'];
$id = $_SESSION['id'];
$name = $_SESSION['name'];
$contents = $_POST['contents'];
$img_seq = $_POST['img_seq'];
$num = $_POST['num'];
$date = date('Y-m-d H:i:s');

if($num == 0){
	$sql = sprintf('INSERT INTO reply SET
		seq=%d,
		id="%s",
		name = "%s",
		contents="%s",
		img_seq="%s",
		`date`="%s"',
		mysqli_real_escape_string($db_info, $seq),
		mysqli_real_escape_string($db_info, $id),
		mysqli_real_escape_string($db_info, $name),
		mysqli_real_escape_string($db_info, $contents),
		mysqli_real_escape_string($db_info, $img_seq),
		mysqli_real_escape_string($db_info, $date)
	);
	$check = mysqli_query($db_info, $sql);
	if($check == FALSE){
		echo 0;
		return;
	}else{
		echo '返信しました。';
	}
}else if($num == 1){
	$sql = sprintf('UPDATE reply SET
		seq=%d,
		id="%s",
		name="%s",
		contents="%s",
		img_seq="%s",
		`date`="%s"
		WHERE 
		seq=%d AND id="%s"',
		mysqli_real_escape_string($db_info, $seq),
		mysqli_real_escape_string($db_info, $id),
		mysqli_real_escape_string($db_info, $name),
		mysqli_real_escape_string($db_info, $contents),
		mysqli_real_escape_string($db_info, $img_seq),
		mysqli_real_escape_string($db_info, $date),
		mysqli_real_escape_string($db_info, $seq),
		mysqli_real_escape_string($db_info, $id)
	);
	$check = mysqli_query($db_info, $sql);
	if($check == FALSE){
		echo 0;
		return;
	}else{
		echo '返信しました。';
	}
}

?>