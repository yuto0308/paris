<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script type="text/javascript">
/* ふりがなチェック */
function FuriganaCheck() {
   var str = document.staff_input.name_katakana.value;
   if( str.match( /[^ァ-ン　\s]+/ ) ) {
      alert("ふりがなは、全角カタカナのみで入力して下さい。");
      return 1;
   }
   return 0;
}

/* 半角数字チェック */
function NumberCheck() {
   var str = document.staff_input.id.value;
   if( str.match( /[^0-9-]+/ ) ) {
      alert("スタッフ番号は、半角数字、ハイフンのみで入力して下さい。");
      return 1;
   }
   var len = document.staff_input.id.value.length;
   if(4>len){
   	alert("スタッフ番号は4文字以上で入力してください。");
   	return 1;
   }

   return 0;
}

function staff_submit() {
	   var check = 0;
	   check += NumberCheck();
	   check += FuriganaCheck();
	   if( check > 0 ) {
      return false;
   }
      var flag = 0;
	if(document.staff_input.id.value == ""){ // スタッフ番号の入力をチェック以下同
		flag = 1;
	}
	else if(document.staff_input.name.value == ""){
		flag = 1;
	}
	else if(document.staff_input.name_katakana.value == ""){
		flag = 1;
	}
	else if(document.staff_input.abb_name.value == ""){
		flag = 1;
	}
	if(flag){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}

    if(confirm("この内容で登録しますか") == true){
	var staff_id = $('#id').val();
	var staff_name = $('#name').val();
	var staff_name_kana = $('#name_katakana').val();
	var abb = $('#abb_name').val();
	var sex = $("input:radio[name='sex']:checked").val();
	var remarks = $('#remarks').val();
	$.post('staffnew_do.php',{id:staff_id,name:staff_name,name_katakana:staff_name_kana,abb_name:abb,sex:sex,remarks:remarks},function(data){
		if(data == 0){
			alert("IDの重複がないか確認してください。\n他店舗で登録済である事も考えられます。\nオーナーに使用可能IDをお訪ねください。");
		}else{
			alert(data);
			document.location = "staffnew.php";}});
	//document.location = "staffnew.php";
}else{
	alert("キャンセルされました。");
}

}
       </script>

<div id="pagebodymain">
	<h1>
	<div style="text-align:left; float:left;">新しくスタッフを登録</div>
	<div style="text-align:right;"><a href="staff.php" style="margin-right:10px;">スタッフの一覧へ</a></div>
</h1>
<!--コメント追加-->
<p>
	<form name="staff_input" action="#" method="post">
	<table>
		<tr>
			<th class="b" colspan="2">スタッフの情報</th>
		</tr>
		<tr>
			<th class="a">スタッフ番号<font color="red">(*)</font></th>
			<th><input name="id" type="text" id="id" size="20" maxlength="20" onblur="NumberCheck();"/>(半角4文字以上20文字まで)</th>
		<tr>
			<th class="a">スタッフ名<font color="red">(*)</font></th>
			<th><input type="text" name="name" id="name" size="20" maxlength="20"></th>
		</tr>
		<tr>
			<th class="a">フリガナ<font color="red">(*)</font></th>
			<th><input type="text" name="name_katakana" id="name_katakana" size="20" maxlength="20" onblur="FuriganaCheck();"></th>
		</tr>
		<tr>
			<th class="a">略称<font color="red">(*)</font></th>
			<th><input type="text" name="abb_name" id="abb_name" size="10" maxlength="10"></th>
		</tr>
		<tr>
			<th class="a">性別<font color="red">(*)</font></th>
			<th>
				<input type="radio" name="sex" id="sex_male" value="1" /><label for="sex_male">男性</label>
				<input type="radio" name="sex" id="sex_female" value="0" checked="checked"/><label for="sex_female">女性</label>
			</th>
		</tr>
	</table>
	<P>
		<table>
			<tr>
				<th class="b" colspan="2">その他</th>
			</tr>
			<tr>
				<th class="a" width="135">備考欄</th>
				<th><textarea name="remarks" id="remarks" rows="5" cols="60" ></textarea></th>
			</tr>
			</table>
		</form>
	<p>
			<font color="red">(*)</font>は必須項目です。


<p style="text-align:center">
<input type="image" src="../css/contents_img/record.gif" onclick="return staff_submit();" />
</div>
<?php include("footer.php"); ?>