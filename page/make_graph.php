<!DOCTYPE html>
<html lang="ja">
<meta name="robots" content="noindex">
<head>
<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<title>売上グラフ</title> 
<script language="javascript" type="text/javascript" src="jquery-2.1.1.js"></script>
<!--[if lt IE 9]>
    <script language="javascript" type="text/javascript" src="excanvas.min.js"></script>
<![endif]-->
<script language="javascript" type="text/javascript" src="jquery.jqplot.min.js"></script>
<script language="javascript" type="text/javascript" src="plugins/jqplot.barRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="plugins/jqplot.pointLabels.min.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.jqplot.min.css" />

<script>
jQuery( function() {
    jQuery . jqplot(
        'jqPlot-sample',
        [
         [ <?php
         $nothing = 0;
         $str2 = "";
			 $ite_count = 0;
	session_start();
	include("db_connect.php");
	
	if(isset($_GET['num'])){
		$num=$_GET['num'];
	}else{
		$num = 1;
	}
	

	if($_GET['useshop'] == 0){
	$shop_name = "1";
	}else{
	$shop_name = 'members.shop_id='.$_GET['useshop'];
	}

	$staff_id = $_GET['staff_id'];
	$staff_id_temp = $staff_id;
	if($staff_id == ""){
		$staff_id=1;
	}else{
		$staff_id=' shop_log_sub.staff_id= "'.$_GET['staff_id'].'" ';
	}

	$year_st = $_GET['year_st'];
	$month_st = $_GET['month_st'];

	if($month_st < 10){//一桁の時にエラーが出るため、先頭に0を付加する
		$month_st = "0".$month_st;
	}
	$day_st = $_GET['day_st'];
	if($day_st < 10){
		$day_st = "0".$day_st;
	}
	$year_en = $_GET['year_en'];
	$month_en = $_GET['month_en'];
	if($month_en < 10){
		$month_en = "0".$month_en;
	}
	$day_en = $_GET['day_en'] + 1;//正常にインデックスするようにするため
	if($day_en < 10){
		$day_en = "0".$day_en;
	}

	$dom = $_GET['day_or_month'];
	$dom_temp = $dom;
	//日別のときは0,月別のときは1
	if($dom == 0){
		$dom=' DATE_FORMAT(date,"%Y-%m-%d") as time, DATE_FORMAT(date,"%Y年%m月%d日")';
		$dom_check=0;
		$maxrow = 10;
	}else{
		$dom=' DATE_FORMAT(date,"%Y-%m") as time, DATE_FORMAT(date,"%Y年%m月")';
		$dom_check=1;
		$maxrow=10;
	}

	$show = ($num - 1) * $maxrow;
	
	$sort = $_GET['sort'];
	$sort_str = "";
	//日付順のときは0,売り上げ順の時は1
	if($sort == 1){
		$sort_str = 'SUM(sum-ticket * CASE 
								  WHEN date <= "2014-03-31 23:59:59" THEN 1050
								  WHEN date >= "2014-04-01 00:00:00" THEN 1080
								  END
					)';
	}else if($sort == 0){
		$sort_str = "shop_log_sub.date";
	}
	$sum=0;
	$day="";


	$sql = ' SELECT shop_log_sub.shop_id as s_id,SUM(sum) as sum, SUM(count) as count, '. $dom .' as formattime , SUM(cash) as cash , SUM(card) as card,SUM(discount) as discount,SUM(ticket) as ticket, SUM(payback) as payback,date';
	$sql .= ' FROM shop_log_sub , members ';
	$sql .= ' WHERE date >= "'.$year_st.'-'.$month_st.'-'.$day_st.'" and date <= "'.$year_en.'-'.$month_en.'-'.$day_en.'"';
	$sql .= ' AND members.shop_id=shop_log_sub.shop_id AND '. $shop_name. ' AND '.$staff_id;
	$sql .= ' group by time  ORDER BY '.$sort_str.' ASC ';

	$sql_count=$sql;

	$rs_count=mysqli_query($db,$sql_count);
	$count=mysqli_num_rows($rs_count);
	if($count == 0){
		$nothing = 1;
		goto nothing;
	}

	$sql .= ' LIMIT '.$show.' , '.$maxrow ;

	$recordset=mysqli_query($db, $sql);
		

		while($arr_item=mysqli_fetch_assoc($recordset)){
		$time = $arr_item['formattime'];
		if($arr_item['date'] <= "2014-0-31 23:59:59"){
			$times_temp = 1050;
		}else{
			$times_temp = 1080;
		}
		$sum = $arr_item['sum'] - ($arr_item['ticket'] * $times_temp);
		//$sum =number_format($sum);
		if($ite_count==0){
		$str2.=	"['".$time."',".$sum."]";
				$ite_count++;
				}else{
					$str2.=	",['".$time."',".$sum."]";
					$ite_count++;
				}

}

		echo $str2;
		nothing:
?>]


        ],
        {
            seriesDefaults: {
                renderer: jQuery . jqplot . BarRenderer,
                rendererOptions: {
                    barPadding: 8,
                    barMargin: 10,
                    barWidth: 50,
                    shadowOffset: 2,
                    shadowDepth: 5,
                    shadowAlpha: 0.08,
                    fillToZero : true
                },
                 pointLabels: {
                    show: true,
                    location: 'n',
                    ypadding: -5,
                    escapeHTML: false,
                    edgeTolerance: -20,
                    formatString: "<b style='color: blue;'>%'d</b>"
                }
            },
            axes: {
                xaxis: {
                    renderer: jQuery . jqplot . CategoryAxisRenderer,
                },
                yaxis:{
                tickOptions: { formatString: "%'d" }
            }
            }
        }
    );
} );

</script>
</head>
<body>
<div style="text-align:center;">
<?php
	
if($nothing == 1){
		echo "対象データがありません";
		return;
	}	
?>
</div>

 <div style="text-align:center;">
 <?php //店舗
 echo "<table border='1' align='center'>";
 echo "<tr>";
echo "<th>集計店舗</th>";
if($_GET['useshop'] == 0){
	echo "<th>全店舗</th>";
}else{
	$sql = " SELECT name FROM members WHERE shop_id = ".$_GET['useshop'];
	$res = mysqli_query($db,$sql);
	$data_arr = mysqli_fetch_assoc($res);
	echo "<th>".$data_arr['name'].'</th>';
}	

	echo "</tr>"; 
	 
 ?>

 <?php //集計対象
 	echo "<tr>";
	 echo "<th>集計対象</th>";
	 if($_GET['day_or_month'] == 0){
		 echo "<th>日別</th>";
	 }else{
		 echo "<th>月別</th>";
	 }
	 echo "</tr>";
 ?>

 <?php //スタッフ番号
 echo "<tr>";
	 echo "<th>スタッフ名</th>";
	 if($staff_id_temp==""){
			echo "<th>指定無し</th>";
		}else{
			$sql = " SELECT  abb_name FROM staff WHERE id = $staff_id_temp ";
			$res = mysqli_query($db,$sql);
	$data_arr = mysqli_fetch_assoc($res);
	echo "<th>".$data_arr['abb_name']."</th>";

		}
		echo "</tr>";
	 
 ?>

 <?php //商品名
 	echo "<tr>";
	 echo "<th>表示順</th>";
	 if($sort == 0){
		 echo "<th>日付順</th>";
	 }else{
		 echo "<th>売上順</th>";
	 }
	 echo "</tr>";
	 echo "</table>";
 ?>
 </div>
 
<div id="jqPlot-sample" style="height: 100%; width: 100%;"></div>
<div style="text-align:center;">
<?php
	
	$str_button = "";
	$button_num = floor(($count - 1 )/ $maxrow) + 1;

		if($button_num <= 12){
			for($i = 1;$i<=$button_num;$i++){
				if($i == $num){
					$str_button .= '<input type="button" style="color:blue;font-size:large;"';
					$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}else{
					$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
				}
			}
		}else{
			if($num - 1 <= 5){
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($num - 1) + 5) && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else if($button_num - $num <= 5){
					$front_reader = 0;
					$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
					if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= (5 - ($button_num - $num) + 5) && $num > $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5 && $num < $i){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}else{
				$front_reader = 0;
				$rear_reader = 0;
				for($i = 1;$i<=$button_num;$i++){
					if($i == $num){
						$str_button .= '<input type="button" style="color:blue;font-size:large;"';
						$str_button .= ' name="add_button" value="'.$i.'" onclick="search('.$i.')">';
					}else{
						if($i == 1){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($i == $button_num){
							$str_button .= 	'<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if(abs($num - $i) <= 5){
							$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
						}else if($num > $i && $front_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$front_reader = 1;
						}else if($num < $i && $rear_reader == 0){
							$str_button .= '<input type="button" name="add_button" value="…" disabled="disabled">';
							$rear_reader = 1;
						}
					}
				}
			}	
		}
echo $str_button;
?>
</div>
</body>
<script>
function search(num){
		var year_st = <?php echo $year_st; ?>;
		var month_st = <?php echo $month_st; ?>;
		var day_st = <?php echo $day_st; ?>;
		var year_en = <?php echo $year_en; ?>;
		var month_en = <?php echo $month_en; ?>;
		var day_en = <?php echo $day_en; ?>;
		var day_or_month = <?php echo $dom_temp; ?>;
		//day_or_month : 日別表示のときは0,月別表示のときは1
		var sort = <?php echo $sort; ?>;
		//day_or_sales : 日付順の時は0,売上順のときは1
		window.location = "./make_graph.php?num="+num+"&year_st="+year_st+"&month_st="+month_st+"&day_st="+day_st+"&year_en="+year_en+"&month_en="+month_en+"&day_en="+day_en+"&day_or_month="+day_or_month+"&sort="+sort+"&staff_id="+"<?php echo $_GET['staff_id']; ?>"+"&useshop="+"<?php echo $_GET['useshop']; ?>";


}
</script>
</html>