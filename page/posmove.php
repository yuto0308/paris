<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
	function SearchId(){
		var id = $('#staffid').val();
		$('#show_id').load('getdata.php',{id:id});}
</script>
<script>
	function NumberCheck() {
   var str = document.move.price.value;
   if( str.match( /[^0-9]+/ ) ) {
      alert("金額は、半角数字のみで入力して下さい。");
      return 1;
   }
   return 0;
}
	function submit(){
	var check = 0;
	check += NumberCheck();
	if( check > 0 ) {
	return false;
	}
	if($("#show_id").text()==""){
		alert("担当者が未選択です。");
		return;
	}
	if($("#mark").val()=="0"){
		alert("種類が未選択です。");
		return;
	}
	if(confirm("この内容で登録しますか。") == true){
				var id = $('#staffid').val();
				var name =$('#show_id').text();
				var mark = $('#mark').val();
				var price = $('#price').val();
				var remarks = $('#remarks').val();
				$.post('posmove_do.php',
				{id:id,
				name:name,
				mark:mark,
				price:price,
				remarks:remarks},
			function(data){
			if(data == 0){
				alert("エラー")
				return;
			}else{
				alert(data);
				document.location = "posmove.php";}});
}else{
	alert("キャンセルされました。");
}
}
</script>
<div id="pagebodymain">
<h1><div style="text-align:left; float:left;">新しくレジ金を移動</div><div style="text-align:right; margin-right:10px;"><a href="posmovelist.php">レジ金移動の一覧へ</a></div>
</h1>
<form name="move" action="#" method="post">
	<table>
		<tr>
			<th colspan="4" class="b">基本情報</th>
		<tr>
			<th class="a">店舗</th>
			<th><?php echo $_SESSION['name']; ?></th>
			<th class="a">移動日時</th>
			<th><?php echo date("Y年m月d日 H:i:s"); ?></th>
		<tr>
			<th class="a">スタッフ番号<font color="#ff0000">(*)</font></th>
			<th>
			<select id="staffid" onchange="SearchId();">
			<option value="0" name="staff">IDを選択してください</option>
			<?php 
			include("db_connect.php");
			$sql = sprintf("SELECT id FROM staff WHERE shop_id=".$shop_id." ORDER BY id");
			$recordSet = mysqli_query($db, $sql);
			while($arr_item = mysqli_fetch_assoc($recordSet))
				{
					foreach($arr_item as $value){
					echo "<option value= $value > $value </option><br>";
				}
		}
	?></th>
			<th class="a">担当者名</th>
			<th id="show_id"></th>
	</table>
</p>
<p>
	<table>
		<tr>
			<th colspan="2" class="b" style="text-align:center;">移動の設定</th>
		<tr>
			<th class="a">種類<font color="red">(*)</font></th>
			<th><select name="mark" id="mark">
				<option value="0">選択してください。</option>
				<option value="1">マイナス</option>
				<option value="2">プラス</option>
			</select>
			</th>
		<tr>
			<th class="a">金額<font color="red">(*)</font></th>
			<th><input type="text" name="price" id="price" onblur="NumberCheck();">円</th>
		<tr>
			<th class="a">備考欄</th>
			<th><textarea name="remarks" id="remarks" rows="5" cols="60" ></textarea></th>
	</table>
	</form>
</p>
<p><font color="red">(*)</font>は必須項目です。
<p style="text-align:center;">
<input type="image" src="../css/contents_img/view.gif" onclick="submit();"  alt="入力内容の確認"/>

</div>
<?php include("footer.php"); ?>
