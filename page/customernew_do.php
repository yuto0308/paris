<?php
session_start();
include("db_connect.php");

if($_POST['id']==""){
$sql_id = ' SELECT MAX(id) as id FROM guest WHERE shop_id='.$_POST['useshop'];
$rs_id = mysqli_query($db, $sql_id);
$date= mysqli_fetch_assoc($rs_id);
$new_id=$date['id'];
$id=$new_id+1;
}else{
$id=$_POST['id'];
}

$sub_id=$_POST['id2'];
$sql_check = ' SELECT id, shop_id FROM guest WHERE id='.$id.' and sub_id="'.$sub_id.'"';
$sql_check.= ' AND shop_id='.$_POST['useshop'];
$rs_check = mysqli_query($db, $sql_check);
$num_check = mysqli_num_rows($rs_check);
if($num_check>0){
echo 0;
return;
}


$sql = sprintf('INSERT INTO guest SET 
	guestname_kanji="%s",
	guestname_kanji_first ="%s",
	guestname_katakana = "%s",
	guestname_katakana_first = "%s",
	id = %d,
	sub_id = "%s",
	postal_num1 = %d,
	postal_num2 = %d,
	prefecture = "%s",
	city = "%s",
	address = "%s",
	building = "%s",
	tel1 = "%s",
	tel2 = "%s",
	fax = "%s",
	mailaddress = "%s",
	sex = %d,
	birth_y = %d,
	birth_m = %02d,
	birth_d = %02d,
	mail_mag = %d,
	dm = %d,
	motive = %d,
	tickets = %d,
	level = %d,
	remarks = "%s",
	member_check = %d,
	fullname_kanji = "%s",
	fullname_kana = "%s",
	shop_id=%d,
	created = NOW(),
	updating = NOW();
	',
	mysqli_real_escape_string($db, $_POST['guestname_kanji']),
	mysqli_real_escape_string($db, $_POST['guestname_kanji_first']),
	mysqli_real_escape_string($db, $_POST['guestname_katakana']),
	mysqli_real_escape_string($db, $_POST['guestname_katakana_first']),
	mysqli_real_escape_string($db, $id),
	mysqli_real_escape_string($db, $_POST['id2']),
	mysqli_real_escape_string($db, $_POST['postal_num1']),
	mysqli_real_escape_string($db, $_POST['postal_num2']),
	mysqli_real_escape_string($db, $_POST['prefecture']),
	mysqli_real_escape_string($db, $_POST['city']),
	mysqli_real_escape_string($db, $_POST['address']),
	mysqli_real_escape_string($db, $_POST['building']),
	mysqli_real_escape_string($db, $_POST['tel1']),
	mysqli_real_escape_string($db, $_POST['tel2']),
	mysqli_real_escape_string($db, $_POST['fax']),
	mysqli_real_escape_string($db, $_POST['mailaddress']),
	mysqli_real_escape_string($db, $_POST['sex']),
	mysqli_real_escape_string($db, $_POST['birth_y']),
	mysqli_real_escape_string($db, $_POST['birth_m']),
	mysqli_real_escape_string($db, $_POST['birth_d']),
	mysqli_real_escape_string($db, $_POST['mail_mag']),
	mysqli_real_escape_string($db, $_POST['dm']),
	mysqli_real_escape_string($db, $_POST['motive']),
	mysqli_real_escape_string($db, $_POST['tickets']),
	mysqli_real_escape_string($db, $_POST['level']),
	mysqli_real_escape_string($db, $_POST['remarks']),
	mysqli_real_escape_string($db, $_POST['member_check']),
	mysqli_real_escape_string($db, $_POST['guestname_kanji']) ."　".mysqli_real_escape_string($db, $_POST['guestname_kanji_first']),
	mysqli_real_escape_string($db, $_POST['guestname_katakana'])."　".mysqli_real_escape_string($db, $_POST['guestname_katakana_first']),
	mysqli_real_escape_string($db,$_POST['useshop'])
	);
$check = mysqli_query($db, $sql);
if($check == FALSE){
echo 0;
}else{
echo "登録しました。";
}
?>