<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
$(document).ready(function(){
	$("#staff_num").keyup(function (e) {
		var num = $("#staff_num").val();
		$('#staff_name').load('get_staff_name_reg.php',{num:num});//"staff_name" にphpからの値入れる
	});
});
</script>
<script>
function search_reglist(num){
	var shop = $('#useshop').val();
	//cash_or_card
	//0 指定無し
	//1 現金
	//2 カード
	//3 現金＋カード
	var cash_or_card = $('#cash_or_card').val();
	//guest_name
	//顧客の名前、もしくはフリガナが入る
	//名字と名前の間には全角スペースを入れてもらう
	var guest_name = $('#customer_name').val();
	
	//reg_id1
	//regid1に対応する
	var reg_id1 = $('#reg_id1').val();
	
	//reg_id2
	//reg_id2に対応する
	var reg_id2 = $("#reg_id2").val();
	
	
	//staff_num
	//staff_idに対応する
	var staff_id = $("#staff_num").val();
	
	//goods_name
	//goods_nameに対応する
	var goods_name = $('#goods_name').val();
	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var day_s = $('#day_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	var day_e = $('#day_e').val();
	
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({
		type:"POST",
		url:"search_reglist.php",
		data:{
			num:num,
			shop:shop,
			cash_or_card:cash_or_card,
			guest_name:guest_name,
			reg_id1:reg_id1,
			reg_id2:reg_id2,
			staff_id:staff_id,
			goods_name:goods_name,
			year_s:year_s,
			month_s:month_s,
			day_s:day_s,
			year_e:year_e,
			month_e:month_e,
			day_e:day_e			
		},
		dataType:"json",
		success:function(data){
						$('#table').append(data[0]);
						$('#count').text(data[1]);
						$('#span').append(data[2]);
						$('#page_number').val(data[3]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}
</script>

<script>
$(document).ready(function(){
	var shop = $('#useshop').val();
	//cash_or_card
	//0 指定無し
	//1 現金
	//2 カード
	//3 現金＋カード
	var cash_or_card = $('#cash_or_card').val();
	//guest_name
	//顧客の名前、もしくはフリガナが入る
	//名字と名前の間には全角スペースを入れてもらう
	var guest_name = $('#customer_name').val();
	
	//reg_id1
	//regid1に対応する
	var reg_id1 = $('#reg_id1').val();
	
	//reg_id2
	//reg_id2に対応する
	var reg_id2 = $("#reg_id2").val();
	
	
	//staff_num
	//staff_idに対応する
	var staff_id = $("#staff_num").val();
	
	//goods_name
	//goods_nameに対応する
	var goods_name = $('#goods_name').val();
	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var day_s = $('#day_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	var day_e = $('#day_e').val();
	
	var page_number = $('#page_number').val();
	
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({
		type:"POST",
		url:"search_reglist.php",
		data:{
			num:page_number,
			shop:shop,
			cash_or_card:cash_or_card,
			guest_name:guest_name,
			reg_id1:reg_id1,
			reg_id2:reg_id2,
			staff_id:staff_id,
			goods_name:goods_name,
			year_s:year_s,
			month_s:month_s,
			day_s:day_s,
			year_e:year_e,
			month_e:month_e,
			day_e:day_e			
		},
		dataType:"json",
		success:function(data){
						$('#table').append(data[0]);
						$('#count').text(data[1]);
						$('#span').append(data[2]);
						$('#page_number').val(data[3]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
});
</script>


<script>
function search_reglist_csv(num){

	var shop = $('#useshop').val();
	//cash_or_card
	//0 指定無し
	//1 現金
	//2 カード
	//3 現金＋カード
	var cash_or_card = $('#cash_or_card').val();
	//guest_name
	//顧客の名前、もしくはフリガナが入る
	//名字と名前の間には全角スペースを入れてもらう
	var guest_name = $('#customer_name').val();
	
	//reg_id1
	//regid1に対応する
	var reg_id1 = $('#reg_id1').val();
	
	//reg_id2
	//reg_id2に対応する
	var reg_id2 = $("#reg_id2").val();
	
	
	//staff_num
	//staff_idに対応する
	var staff_id = $("#staff_num").val();
	
	//goods_name
	//goods_nameに対応する
	var goods_name = $('#goods_name').val();
	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var day_s = $('#day_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	var day_e = $('#day_e').val();
	
	window.open("./search_reglist_csv.php?num="+num+"&cash_or_card="+cash_or_card+"&guest_name="+guest_name+"&reg_id1="+reg_id1+"&reg_id2="+reg_id2+"&staff_id="+staff_id+"&goods_name="+goods_name+"&year_s="+year_s+"&month_s="+month_s+"&day_s="+day_s+"&year_e="+year_e+"&month_e="+month_e+"&day_e="+day_e+"&shop="+shop);		
			
}
</script>
<?php 
	include("db_connect.php");
?>
<?php if(isset($_GET['page_number'])){
		$page_number = $_GET['page_number'];
	}else{
		$page_number = 1;
	}
	echo '<input hidden id = "page_number" value="'.$page_number.'">';
?>

<div id="pagebodymain">
<h1>レジの売上一覧</h1>
<?php
	$name = $_SESSION['name'];
?>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">検索条件</th>
		</tr>
		<tr>
			<th class="a">店舗</th>
			<th style="width:25%;">
					<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?></th>
			<th class="a">決済方法</th>
			<th>
			<select id="cash_or_card">
			<option name="cash_or_card" value="0">決済方法を選択</option>
			<option name="cash_or_card" value="1">現金</option>
			<option name="cash_or_card" value="2">カード</option>
			<option name="cash_or_card" value="3">現金＋カード</option>	
			</select></th>
		</tr>
			<tr>
			<th class="a">顧客名/フリガナ</th>
			<th colspan = "3"><input type="text" id="customer_name" size ="30"><br><span style="font-size:10px">※フルネームを入力する場合は名字と名前の間に全角スペースを入れてください</span></th>
		</tr>	
		<tr>
			<th class="a">レジ番号</th>
			<th ><input type="text" id="reg_id1" size ="7">-<input type="text" id="reg_id2" size="7"></th>
			<th class="a">
			商品名
			</th>
			<th>
			<input type="text" id="goods_name">
			</th>
				</tr>
		<tr>
			<th class="a">スタッフ番号</th>
			<th><input type="text" id="staff_num" size = "30"></th>
			<th class="a">担当者名</th>
			<th id="staff_name"></th>
		</tr>
		<tr>
			<th class="a">売上日</th>
			<th colspan="3" style="text-align:center">
			<select id="year_s">
				<?php
				$year_s = date("Y");
					for($i=2000;$i<=$year_s;$i++){
					if($i!=date("Y")){
						echo '<option name="year_s" value="'.$i.'">'.$i.'年</option>';	
						}else{
						echo '<option name="year_s" value="'.$i.'" selected>'.$i.'年</option>';	
						}
					}
				?>
			</select>
			<select id="month_s">
				<?php
				$month_s = date("m");
					for($i=1;$i<=12;$i++){
					if($i!=$month_s){
						echo '<option name="month_s" value="'.$i.'">'.$i.'月</option>';	
						}else{
						echo '<option name="month_s" value="'.$i.'" selected>'.$i.'月</option>';	
						}
					}
				?>
			</select>
			<select id="day_s">
				<?php
					for($i=1;$i<=31;$i++){
					if($i!=1){
						echo '<option name="day_s" value="'.$i.'">'.$i.'日</option>';	
						}else{
						echo '<option name="day_s" value="'.$i.'" selected>'.$i.'日</option>';	
						}
					}
				?>
			</select>
			〜
			<select id="year_e">
				<?php
				$year_e = date("Y");
					for($i=2000;$i<=$year_e;$i++){
					if($i!=$year_e){
						echo '<option name="year_e" value="'.$i.'">'.$i.'年</option>';	
						}else{
						echo '<option name="year_e" value="'.$i.'" selected>'.$i.'年</option>';	
						}
					}
				?>
			</select>
			<select id="month_e">
				<?php
				$month_e = date("m");
					for($i=1;$i<=12;$i++){
					if($i!=$month_e){
						echo '<option name="month_e" value="'.$i.'">'.$i.'月</option>';	
						}else{
						echo '<option name="month_e" value="'.$i.'" selected>'.$i.'月</option>';	
						}
					}
				?>
			</select>
			<select id="day_e">
				<?php
				$day_e = date("j");
					for($i=1;$i<=31;$i++){
					if($i!=$day_e){
						echo '<option name="day_e" value="'.$i.'">'.$i.'日</option>';	
						}else{
						echo '<option name="day_e" value="'.$i.'" selected>'.$i.'日</option>';	
						}
					}
					$day_e = $day_e + 1;
				?>
			</select>
			</th>
		</tr>
		<tr>
			<th colspan="4" style="text-align:center"><input type="image" src="../css/image/contents/search_reset.gif" onclick="location.reload();" alt="条件をリセット">　 <input type="image" src="../css/image/contents/search.gif" alt="この条件で検索" onclick=<?php
				echo '"search_reglist(1);"';
				?>>
	</table>
	
<p><a href="#" onclick="search_reglist_csv(1);"><img src="../css/image/contents/csv_btn.gif"></a></p>
<p style="text-align: right;">該当件数:<span id="count"></span>件</p>
<p>
<table id="table">
	<tr class="a">
		<th style="text-align:center;">削除</th>
		<th style="text-align:center;">レジ番号</th>
		<th style="text-align:center;">顧客名</th>
		<th style="text-align:center;">決済方法</th>
		<th style="text-align:center;">店舗</th>
		<th style="text-align:center;">担当者</th>
		<th style="text-align:center;">売上日時</th>
		<th style="text-align:center;">金額</th>
	</tr>
</table>
<div id="span" align="center">
</div>

<P>*「レジ番号」をクリックすると注文情報を閲覧できます。
<p style="text-align: right;">
</div>
<?php include("footer.php"); ?>
