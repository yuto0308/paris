<?php
		session_start();
		include("db_connect.php");
		$str="";
		if($_GET['useshop'] == 0){
		$useshop = "1";
		}else{
		$useshop = 'members.shop_id='.mysqli_real_escape_string($db,$_GET['useshop']);
		}


							
		$goods_id=mysqli_real_escape_string($db,$_GET['goods_id']);
		if($goods_id==""){
			$goods_id_str="1";
		}else{
			$goods_id_str=" goods_id = $goods_id ";
		}
		$goods_name=mysqli_real_escape_string($db,$_GET['goods_name']);
		if($goods_name==""){
			$goods_name_str="1";
		}else{
			$goods_name_str=" goods_name LIKE '%$goods_name%' ";
		}
		
		$cat_big=mysqli_real_escape_string($db,$_GET['cat_big']);//大カテゴリ
		if($cat_big == 0){
			$cat_big_str = "1";
			$group_by_str = "category_big_str";
			$order_by_str = "category_big";
		}else{
			$cat_big_str = "category_big = $cat_big ";
			$group_by_str = "category_mid_str";
			$order_by_str = "category_mid";
		}
		$cat_mid=mysqli_real_escape_string($db,$_GET['cat_mid']);//中カテゴリ
		if($cat_mid == 0){
			$cat_mid_str = "1";
		}else{
			$cat_mid_str = "category_mid = $cat_mid ";
		}
		$cat_sma=mysqli_real_escape_string($db,$_GET['cat_sma']);//小カテゴリ
		if($cat_sma == 0){
			$cat_sma_str = "1";
		}else{
			$cat_sma_str = "category_sma = $cat_sma ";
		}
		
		$day_or_mon=mysqli_real_escape_string($db,$_GET['day_or_mon']);//日別は０、月別は１

		
		if($day_or_mon == 1){//月別
		
		$str .= "店舗番号,店舗名,日付,大カテゴリー名,中カテゴリー名,小カテゴリー名,点数,売上金額\n";		
		$year_s=mysqli_real_escape_string($db,$_GET['year_s']);
		$month_s=mysqli_real_escape_string($db,$_GET['month_s']);
		if($month_s < 10){
			$month_s = "0".$month_s;
		}
		
		$day_s_str = $year_s . "-" . $month_s . "-01 00:00:00";
		
		$year_e=mysqli_real_escape_string($db,$_GET['year_e']);
		$month_e=mysqli_real_escape_string($db,$_GET['month_e']);
		if($month_e < 10){
			$month_e = "0".$month_e;
		}
		$day_e_str = $year_e . "-" . $month_e . "-31 23:59:59";
		
		$sql_search = " SELECT DATE_FORMAT(day,'%Y-%m') AS date, ";
		$sql_search .= " DATE_FORMAT(day,'%Y年%m月') AS date_j ";
		$sql_search .= " FROM shop_log,goods,members WHERE shop_log.shop_id=members.shop_id ";
		$sql_search .= " AND shop_log.goods_id = goods.id ";
		$sql_search .= " AND $useshop ";
		$sql_search .= " AND $goods_id_str AND $goods_name_str ";
		$sql_search .= " AND day >= '$day_s_str' AND day <= '$day_e_str' ";
		$sql_search .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
		$sql_search .= " GROUP BY date ";
		$sql_search .= " ORDER BY date DESC  ";
		
				
		
		$query_search = mysqli_query($db,$sql_search);
		if(!$query_search){
			echo "対象データがありません";
			return;
			}
		if(mysqli_num_rows($query_search) == 0){
			echo "対象データがありません";
			return;			 
		}
		
		while($arr_search = mysqli_fetch_assoc($query_search)){
			$date = $arr_search['date'];
			$date_j = $arr_search['date_j'];
			
			$sql_show = " SELECT DATE_FORMAT(day,'%Y-%m') AS date, ";
			$sql_show .= " category_big_str,category_mid_str,category_sma_str , SUM(goods_num) AS num, ";
			$sql_show .= " SUM(shop_log.price) AS price,members.shop_id as msid,members.name as mname ";
			$sql_show .= " FROM shop_log,goods,members ";
			$sql_show .= " WHERE day >= '$date"."-01 00:00:00' AND ";
			$sql_show .= " day <= '$date"."-31 23:59:59' AND shop_log.shop_id=members.shop_id ";
			$sql_show .= " AND shop_log.goods_id = goods.id ";
			$sql_show .= " AND $useshop ";
			$sql_show .= " AND $goods_id_str AND $goods_name_str ";
			$sql_show .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
			$sql_show .= " GROUP BY category_big_str,category_mid_str,category_sma_str ORDER BY $order_by_str ASC ";

			
			$query_show = mysqli_query($db,$sql_show);
			if(!$query_show){
			echo "対象データがありません";
			return;
			}
			$row = mysqli_num_rows($query_show);
			if($row == 0){
			echo "対象データがありません";
			return;
		    }
			
			while($arr_show = mysqli_fetch_assoc($query_show)){	
			
			if($_GET['useshop'] == 0){
				$useshop_id = "全店舗";
				$useshop_name = "全店舗";
				}else{
				$useshop_id = $arr_show['msid'];
				$useshop_name = $arr_show['mname'];
								}

				$str .= "$useshop_id,$useshop_name,$date_j,".$arr_show['category_big_str'].",".$arr_show['category_mid_str'].",".$arr_show['category_sma_str'];
				$str .= ",".$arr_show['num'].",".$arr_show['price']."\n";
			  
		}	
		}
		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=category_monthly.csv");
		print(mb_convert_encoding($str,"SJIS","UTF-8"));
		return;
		}else{//日別
		$str .= "店舗番号,店舗名,日付,曜日,大カテゴリー名,中カテゴリー名,小カテゴリー名,点数,売上金額\n";		
		$year_s=mysqli_real_escape_string($db,$_GET['year_s']);
		$month_s=mysqli_real_escape_string($db,$_GET['month_s']);
		if($month_s < 10){
			$month_s = "0".$month_s;
		}
		$day_s=mysqli_real_escape_string($db,$_GET['day_s']);
		if($day_s < 10){
			$day_s = "0".$day_s;
		}
		
		$day_s_str = $year_s . "-" . $month_s . "-" . $day_s;
		
		$year_e=mysqli_real_escape_string($db,$_GET['year_e']);
		$month_e=mysqli_real_escape_string($db,$_GET['month_e']);
		if($month_e < 10){
			$month_e = "0".$month_e;
		}
		$day_e=mysqli_real_escape_string($db,$_GET['day_e']);
		$day_e=$day_e + 1;
		if($day_e < 10){
			$day_e = "0".$day_e;
		}

		$day_e_str = $year_e . "-" . $month_e . "-" . $day_e;
		
		$sql_search = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date, ";
		$sql_search .= " DATE_FORMAT(day,'%Y年%m月%d日') AS date_j, ";
		$sql_search .= " DATE_FORMAT(day,'%a') AS week ";
		$sql_search .= " FROM shop_log,goods,members WHERE shop_log.shop_id = members.shop_id ";
		$sql_search .= " AND $goods_id_str AND $goods_name_str ";
		$sql_search .= " AND $useshop ";
		$sql_search .= " AND shop_log.goods_id = goods.id ";
		$sql_search .= " AND day >= '$day_s_str' AND day <= '$day_e_str' ";
		$sql_search .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
		$sql_search .= " GROUP BY date ";
		$sql_search .= " ORDER BY date DESC ";
		
		$query_search = mysqli_query($db,$sql_search);
		if(!$query_search){		
			echo "対象データがありません";
			return;
			}
		if(mysqli_num_rows($query_search) == 0){
			echo "対象データがありません";
			return;	 
		}
		
		while($arr_search = mysqli_fetch_assoc($query_search)){
			$date = $arr_search['date'];
			$date_j = $arr_search['date_j'];
			$week = $arr_search['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
			}
			
			
			
			$sql_show = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date, ";
			$sql_show .= " category_big_str,category_mid_str,category_sma_str , SUM(goods_num) AS num, ";
			$sql_show .= " SUM(shop_log.price) AS price,members.shop_id as msid,members.name as mname  ";
			$sql_show .= " FROM shop_log ,goods,members ";
			$sql_show .= " WHERE day >= '$date 00:00:00' AND ";
			$sql_show .= " day <= '$date 23:59:59' AND shop_log.shop_id=members.shop_id ";
			$sql_show .= " AND shop_log.goods_id = goods.id ";
			$sql_show .= " AND $useshop ";
			$sql_show .= " AND $goods_id_str AND $goods_name_str ";
			$sql_show .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
			$sql_show .= " GROUP BY category_big_str,category_mid_str,category_sma_str ORDER BY $order_by_str ASC ";
			
			
			$query_show = mysqli_query($db,$sql_show);
			if(!$query_show){
			echo "対象データがありません";
			return;
			}
			$row = mysqli_num_rows($query_show);
			if($row == 0){
			echo "対象データがありません";
			return;
		    }
			
			while($arr_show = mysqli_fetch_assoc($query_show)){	
			
			if($_GET['useshop'] == 0){
				$useshop_id = "全店舗";
				$useshop_name = "全店舗";
				}else{
				$useshop_id = $arr_show['msid'];
				$useshop_name = $arr_show['mname'];
								}

				$str .= "$useshop_id,$useshop_name,$date_j,$week_str,".$arr_show['category_big_str'].",".$arr_show['category_mid_str'].",".$arr_show['category_sma_str'];
				$str .= ",".$arr_show['num'].",".$arr_show['price']."\n";

			  
		}
		
		}
		




		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=category_daily.csv");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));
		return;
	
	
		}
	?>
