<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php
	$str = "";
	include("db_info.php");
	$sql = ' SELECT SEQ,type,title,data,DATE_FORMAT(createdate,"%Y年%m月%d日") as date FROM info WHERE (`add` = 0 OR `add` = 2) AND check_del = 0 ';
	$rs = mysqli_query($db_info,$sql) or exit($sql);
	$rows = mysqli_num_rows($rs);
	if($rows == 0){
		$str .= '<tr><th style="text-align:center;" colspan="4">メッセージはありません。</th></tr>';
	}

	$height = 470 - ( $rows * 30 );

	while($array = mysqli_fetch_assoc($rs)){

		if($array['type'] == 0){
			$type = '要確認';
		}else if($array['type'] == 1){
			$type = '要返信';
		}

		$content = $array['title'];
		if(mb_strlen($content) > 30){
			$content = substr($content,0,30);
		}

		$reply_sql = ' SELECT * FROM reply WHERE seq ='.$array['SEQ'].' AND id="'.$_SESSION['id'].'"';
		$reply_que = mysqli_query($db_info,$reply_sql) or exit($reply_sql);
		$reply_rows = mysqli_num_rows($reply_que);
		if($reply_rows == 0 && $type == '要確認'){
			$condition = '未読';
		}else if($reply_rows !== 0 && $type == '要確認'){
			$condition = '既読';
		}else if($reply_rows == 0 && $type == '要返信'){
			$condition = '未返信';
		}else if($reply_rows !== 0 && $type == '要返信' ){
			$condition = '返信済';
		}

		$str .= '<tr style="height:30px;">';
		$str .= '<th style="text-align:center;"><a href = "message.php?seq='.$array['SEQ'].'&type='.$array['type'].'">'.$array['date'].'</a></th>';
		$str .= '<th style="text-align:center;"><a href = "message.php?seq='.$array['SEQ'].'&type='.$array['type'].'">'.$type.'</a></th>';
		$str .= '<th style="text-align:center;"><a href = "message.php?seq='.$array['SEQ'].'&type='.$array['type'].'">'.$content.'</a></th>';
		$str .= '<th style="text-align:center;">'.$condition.'</th>';
		$str .= '</tr>';
	}
?>

<div id="pagebodymain">
	<h1>メッセージ一覧</h1>
	<p>
	<table>
		<tr style="height:30px;">
			<th class="b" colspan="4">メッセージ一覧</th>
		</tr>
		<tr class="a" style="height:30px;">
			<th style="text-align:center; width:20%;">日付</th>
			<th style="text-align:center; width:10%;">属性</th>
			<th style="text-align:center; width:60%;">件名</th>
			<th style="text-align:center; width:10%;">状態</th>
		</tr>
		<?php echo $str; ?>
	</table>
	<?php print '<div style="height:'.$height.'px;"></div>'; ?>
</div>
<?php include("footer.php"); ?>