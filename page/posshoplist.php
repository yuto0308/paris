<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
<?php
include("db_connect.php");
$sql = "SELECT * FROM members WHERE shop_id > 0";
$recordset = mysqli_query($db, $sql);
$count = mysqli_num_rows($recordset);
?>
<script>
function search_shop(){
		var shop_id = $("#number").val();
		var shop_name = $("#name").val();
		$("tr[name='add']").remove();
			$.ajax({type:"POST",
 			url:"search_shop.php",
 				data:{	shop_id:shop_id,
 						shop_name:shop_name
 					},
 					dataType:"json",
 					success:function(data){
						$('#table').append(data[0]);
						$('#num_shop').text(data[1]);
						},
					error:function(XMLHttpRequest, textStatus, errorthrown){
						alert('error : ' + errorthrown + XMLHttpRequest.status + textStatus);
						}
 				})
		}
</script>
<h1>店舗の一覧</h1>
<p>
<table>
	<tr>
		<th class="b" colspan="4">検索条件</th>
	</tr>
	<tr>
		<th class="a">店舗番号</th>
		<th>
			<input type="text" name="number" id="number" size="30"></th>
		<th class="a">店舗名</th>
		<th>
			<input type="text" name="name" id="name" size="30"></th>
	</tr>
	<tr>
		<th colspan="4" style="text-align:center;">
			<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
			<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search_shop();"></th>
	</tr>
</table>
<p style="text-align:right;">店舗数: <span id="num_shop"><?php echo $count ;?></span>件
<p>
<table id="table">
	<tr>
	<th class="a" style="text-align:center; width:15%;">店舗番号</th>
	<th class="a" style="text-align:center;">店舗名</th>
	<th class="a" style="text-align:center; width:15%;">略称</th>
	<th class="a" style="text-align:center; width:15%;">ID</th>
	<th class="a" style="text-align:center; width:10%;">PW</th>
</tr>
<?php
while($data = mysqli_fetch_assoc($recordset)){
?>
	<tr name="add">
	<th style="text-align:center;"><a href="shopedit.php?id=<?php echo $data['shop_id']; ?>"><?php print $data['shop_id']; ?></a></th>
	<th style="text-align:center;"><a href="shopedit.php?id=<?php echo $data['shop_id']; ?>"><?php print $data['name']; ?></a></th>
	<th style="text-align:center;"><?php print $data['abb_name']; ?></th>
	<th style="text-align:center;"><?php print $data['login_id']; ?></th>
	<th style="text-align:center;"><?php print $data['password']; ?></th>
</tr>
<?php
}
?>
</table>
<p>
	※「店舗番号」、「店舗名」をクリックすると店舗情報を編集できます。
</div>
<?php include("footer.php"); ?>
