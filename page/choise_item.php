<?php
session_start();
include("db_connect.php");

if(isset($_POST['rsvid'])){
//編集分
$str="";
$result_arr=array();
$goodsid=$_POST['goodsid'];
$sum=$_POST['sum'];
$sum=intval($sum);

$num=$_POST['numItem'];
$id_arr=$_POST['id_arr'];
$id_arr_pieces=explode(",",$id_arr);
for($i=0;$i<$num;$i++){
	if($goodsid==$id_arr_pieces[$i]){
		$sum_str='<span id="sum_str" style="font-size:24px;">'.$sum.'</span>';
		$result_arr[]=$str;
		$result_arr[]=$sum_str;
		$result_arr[]=$sum;
		echo json_encode($result_arr);
		return;
	}
}

$sql=' SELECT * FROM reservation WHERE RSVno='.$_POST['rsvid'].' AND goods_id='.$goodsid.' AND shop_id='.$shop_id;
$query=mysqli_query($db,$sql);
$data=mysqli_fetch_assoc($query);
if((strpos($data['goods_name'],"()"))==FALSE){
	$goodsname=$data['goods_name'];
	$goodsname_sub="";
}else{
$pro_goodsname=explode("(",$data['goods_name']);
$goodsname=$pro_goodsname[0];
$pro_goodsname_sub=explode(")",$pro_goodsname[1]);
$goodsname_sub=$pro_goodsname_sub[0];
}
$discount=$data['discount'];
$count=$data['goods_count'];
$newprice=($data['price']-$data['discount'])*$data['goods_count'];

$str.='<tr id="num_'.$data['goods_id'].'">
		<th style="text-align:center;">
			<input type="text" name="goodsname'.$data['goods_id'].'" value="'.$goodsname.'"></br>
			<input type="text" name="goodsname_sub'.$data['goods_id'].'">
			<input type="hidden" name="goodsid'.$data['goods_id'].'" value="'.$goodsname_sub.'">
		</th>
		<th style="text-align:center;">
			<input type="text" value="'.$data['price'].'" id="goodsprice" name="goodsprice'.$data['goods_id'].'" onkeyup="change_price();">
		</th>
		<th style="text-align:center;">
			<input type="text" value="'.$discount.'" size="7" name="goodsdiscount_yen'.$data['goods_id'].'"  onkeyup="change_price();">円</br>
			<input type="text" value="0" size="7" name="goodsdiscount_per'.$data['goods_id'].'" onkeyup="change_price();">％
		</th>
		<th style="text-align:center;">
			<input type="text" size="1" name="goodscount'.$data['goods_id'].'" value="'.$count.'" onkeyup="change_price();">
		</th>
		<th style="text-align:center;">
			<input type="button" value="削除" onclick="num_delete('.$data['goods_id'].');change_price();">
		</th>';
$sum=$sum+$newprice;
$sum_str='<span id="sum_str" style="font-size:24px;">'.$sum.'</span>';
$result_arr[]=$str;
$result_arr[]=$sum_str;
$result_arr[]=$sum;
echo json_encode($result_arr);
return;

}else{
//新規登録分
$str="";
$result_arr=array();
$goodsid=$_POST['goodsid'];
$sum=$_POST['sum'];
$sum=intval($sum);

$num=$_POST['numItem'];
$id_arr=$_POST['id_arr'];
$id_arr_pieces=explode(",",$id_arr);
for($i=0;$i<$num;$i++){
	if($goodsid==$id_arr_pieces[$i]){
		$sum_str='<span id="sum_str" style="font-size:24px;">'.$sum.'</span>';
		$result_arr[]=$str;
		$result_arr[]=$sum_str;
		$result_arr[]=$sum;
		echo json_encode($result_arr);
		return;
	}
}

$sql=' SELECT * FROM goods WHERE id='.$goodsid;
$query=mysqli_query($db,$sql);
$data=mysqli_fetch_assoc($query);
$str.='<tr id="num_'.$data['id'].'">
		<th style="text-align:center;">
			<input type="text" name="goodsname'.$data['id'].'" value="'.$data['name'].'"></br>
			<input type="text" name="goodsname_sub'.$data['id'].'">
			<input type="hidden" name="goodsid'.$data['id'].'" value="'.$data['id'].'">
		</th>
		<th style="text-align:center;">
			<input type="text" value="'.$data['price'].'" id="goodsprice" name="goodsprice'.$data['id'].'" onkeyup="change_price();">
		</th>
		<th style="text-align:center;">
			<input type="text" value="0" size="7" name="goodsdiscount_yen'.$data['id'].'" onkeyup="change_price();">円</br>
			<input type="text" value="0" size="7" name="goodsdiscount_per'.$data['id'].'" onkeyup="change_price();">％
		</th>
		<th style="text-align:center;">
			<input type="text" value="1" size="1" name="goodscount'.$data['id'].'" onkeyup="change_price();">
		</th>
		<th style="text-align:center;">
			<input type="button" value="削除" onclick="num_delete('.$data['id'].');change_price();">
		</th>';
$sum=$sum+$data['price'];
$sum_str='<span id="sum_str" style="font-size:24px;">'.$sum.'</span>';
$result_arr[]=$str;
$result_arr[]=$sum_str;
$result_arr[]=$sum;
echo json_encode($result_arr);
return;
}
?>