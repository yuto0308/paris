<?php 
	/*
		作成者	：油谷
		昨日	：shop_log_subを参照し、CSVを出力
	*/

	session_start();
	include("db_connect.php");
	$str="店舗番号,店舗名,日付,曜日,レジ番号,決済方法,担当者,顧客番号,顧客名,商品コード,商品名,通常価格,割引,販売価格,数量,消費税,合計金額\n";
	//result_ary : １つ目に行表示HTML
	//			 : ２つめに該当会員数
	//			 : ３つめにインデックスボタン

 	if($_GET['shop'] == 0){
	$shop_name = "1";
	}else{
	$shop_name = 'members.shop_id='.mysqli_real_escape_string($db,$_GET['shop']);
	}


 	//cash_or_card
	//0 指定無し
	//1 現金
	//2 カード
	//3 現金＋カード
	$cash_or_card = mysqli_real_escape_string($db,$_GET['cash_or_card']);	
	if($cash_or_card != 0){
	$cash_or_card_wrd = "cash_or_card = ".$cash_or_card;
	}else{
		$cash_or_card_wrd = "1";
	}
	//guest_name
	//顧客の名前、もしくはフリガナが入る
	//名字と名前の間には全角スペースを入れてもらう
	$guest_name = mysqli_real_escape_string($db,$_GET['guest_name']);
	
	//reg_id1
	//regid1に対応する
	$reg_id1 = mysqli_real_escape_string($db,$_GET['reg_id1']);
	if($reg_id1 == ""){
		$reg_id1_str = "1";
	}else{
		$reg_id1_str = "reg_id1 = ".$reg_id1;
	}
	//reg_id2
	//reg_id2に対応する
	$reg_id2 = mysqli_real_escape_string($db,$_GET['reg_id2']);
	if($reg_id2 == ""){
		$reg_id2_str = "1";
	}else{
		$reg_id2_str = "reg_id2 = ".$reg_id2;
	}
		
	//staff_num
	//staff_idに対応する
	$staff_id = mysqli_real_escape_string($db,$_GET['staff_id']);
	if($staff_id == ""){
		$staff_id_str = "1";
	}else{
		$staff_id_str = "shop_log.staff_id = '".$staff_id."'";
	}
	
	$goods_name = mysqli_real_escape_string($db,$_GET['goods_name']);
	if($goods_name == ""){
		$goods_name_str = "1";
	}else{
		$goods_name_str = 'goods_name like '."'%".$goods_name."%'";
	}
	
	$year_s=mysqli_real_escape_string($db,$_GET['year_s']);
	$month_s=mysqli_real_escape_string($db,$_GET['month_s']);
	$day_s=mysqli_real_escape_string($db,$_GET['day_s']);
	$year_e=mysqli_real_escape_string($db,$_GET['year_e']);
	$month_e=mysqli_real_escape_string($db,$_GET['month_e']);
	$day_e=mysqli_real_escape_string($db,$_GET['day_e']);
	
	
if($guest_name != ""){
	$guest_name_str1 = 'fullname_kanji like "%'.$guest_name.'%"';
	$guest_name_str2 = 'fullname_kana like "%'.$guest_name.'%"';
	//顧客名から顧客idを引っ張ってくる
	$sql_guest = 'SELECT * FROM guest WHERE '.$guest_name_str1.' or  '.$guest_name_str2;
		$guest_id_search = mysqli_query($db,$sql_guest) or exit("クエリに失敗しました");
		if(mysqli_num_rows($guest_id_search) == 0){
			echo "対象データがありません";
			return;
		}
		$loop_count = 0;
		$id_str = "(";
		while($guest_id = mysqli_fetch_assoc($guest_id_search)){
		$id = $guest_id['guestseq'];
		if($loop_count == 0){
		$id_str .= ' guest_id = '.$id;
		}else{
			$id_str .= ' or guest_id = '.$id;

		}
$loop_count++;		
}
$id_str .=")" ;
	}else{
		$id_str = "1";
	}


	$sql = 'SELECT * , DATE_FORMAT(day,"%Y-%m-%d %k:%i") as formateddate, DATE_FORMAT(day,"%w") as week,shop_log.shop_id as post_shop_id,members.name as account_name, shop_log.goods_name AS sl_gname FROM shop_log,members WHERE shop_log.shop_id=members.shop_id AND '. $shop_name .' AND '. $goods_name_str.' and '.$id_str.' and '.$cash_or_card_wrd.' and '.$reg_id1_str.' and '.$reg_id2_str.'  and '.$staff_id_str.' and  day >= "'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00" and day <= "'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59" order by day DESC ';


	$rs = mysqli_query($db,$sql) or exit($sql);
	if(mysqli_num_rows($rs) == 0){
		echo "対象データがありません";
		return;
	}
	
	while($arr_item = mysqli_fetch_assoc($rs)){
			
			
		//顧客idから顧客名を引っ張ってくる
		$guest_id=$arr_item['guest_id'];
		$sql_guest = 'SELECT * FROM guest WHERE guestseq = '.$guest_id;
		$guest_name_search = mysqli_query($db,$sql_guest) or exit("クエリに失敗しました");
		$guest_name = mysqli_fetch_assoc($guest_name_search);
		$name = $guest_name['GuestName_kanji'] .'　'. $guest_name['guestname_kanji_first'];
		$guest_id = $guest_name['id'];
		//スタッフIDからスタッフ名もってくる
		$staff_id = $arr_item['staff_id'];
	    $sql_staff_id = 'SELECT * FROM staff WHERE id = "'.$staff_id.'"';
        $staff_name_search = mysqli_query($db,$sql_staff_id) or exit("クエリに失敗しました");
        $staff_name = mysqli_fetch_assoc($staff_name_search);
        $name_s = $staff_name['abb_name'];
        
        //goods_idから商品の通常価格持ってくる
        $goods_id = $arr_item['goods_id'];
        $sql_goods_price = " SELECT * FROM goods WHERE id = $goods_id ";
        $goods_price_rs = mysqli_query($db,$sql_goods_price) or exit("クエリに失敗しました");
        $goods_price = mysqli_fetch_assoc($goods_price_rs);
        $price_str = $goods_price['price'];
			
		//現金orカードの場合分け			
		if($arr_item['cash_or_card'] == "1"){
				$cash_or_card_str = "現金";
			}else if($arr_item['cash_or_card'] == "2"){
				$cash_or_card_str = "カード";
			}else if($arr_item['cash_or_card'] == "3"){
				$cash_or_card_str = "現金＋カード";
			}
		
		if($arr_item['week']==0){
			$week = "日";
		}else if($arr_item['week']==1){
			$week = "月";
		}else if($arr_item['week']==2){
			$week = "火";
		}else if($arr_item['week']==3){
			$week = "水";
		}else if($arr_item['week']==4){
			$week = "木";
		}else if($arr_item['week']==5){
			$week = "金";
		}else if($arr_item['week']==6){
			$week = "土";
		}
		if($arr_item['day'] <= "2014-03-31 23:59:59"){
			$times_temp1 = 0.05;
			$times_temp2 = 1.05;
		}else{
			$times_temp1 = 0.08;
			$times_temp2 = 1.08;
		}
		$post_shop_id=$arr_item['post_shop_id'];
			
		$str .= $arr_item['post_shop_id'].','.$arr_item['account_name'].','.$arr_item['formateddate'].','.$week.','.$arr_item['reg_id1'].'-'.$arr_item['reg_id2'].',';
		$str .= $cash_or_card_str.','.$name_s.','.$guest_id.','.$name.','.$arr_item['goods_id'].','.$arr_item['sl_gname'].','.$price_str.',';
		$str .= $arr_item['discount'].','.$arr_item['price'] / $arr_item['goods_num'].','.$arr_item['goods_num'].','.round($arr_item['price'] * $times_temp1).','.round($arr_item['price'] * $times_temp2)."\n";
			}
			

header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=saleslist.csv");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));


?>