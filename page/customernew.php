<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php"); ?>
<script type="text/javascript">


function kana_check(){
	var kana_check1 = document.submit.f_name_kana.value;
	var kana_check2 = document.submit.g_name_kana.value;
   if( kana_check1.match( /[^ァ-ン　\s]+/ ) ) {
      alert("フリガナは、全角カタカナのみで入力して下さい。");
      return 1;
   }
   if( kana_check1.match( /[^ァ-ン　\s]+/ ) ) {
   	  alert("フリガナは、全角カタカナのみで入力して下さい。");
   	   return 1;
   	}
   	return 0;;
   }

function cus_input(){
	var check = 0
	check += kana_check();
	if(check>0){
		return false;
	}
	var flag = 0;
	if(document.submit.f_name.value == ""){ // スタッフ番号の入力をチェック以下同
		flag = 1;
	}else if(document.submit.g_name.value == ""){
		flag = 1;
	}else if(document.submit.f_name_kana.value == ""){
		flag = 1;
	}else if(document.submit.g_name_kana.value == ""){
		flag = 1;
	}else if(document.submit.tel1.value == ""){
		flag = 1;
	}else if(document.submit.sex.value == "0"){
		flag = 1
	}else if(document.submit.birth_y.value == ""){
        flag = 1;
    }else if(document.submit.birth_m.value == ""){
        flag = 1;
}else if(document.submit.birth_d.value == ""){
	flag = 1;
}

	if(flag>0){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}

    var hankaku = 0
    if(document.submit.cus_id.value.match( /[^0-9]+/)){
    	alert("会員番号は半角で入力して下さい。\n半角以外を入力する場合は、「ハイフン等を入力する」\nにチェックをいれ、ハイフンの前に入力して下さい。");
    	return false;
    }
    if(document.submit.tel1.value.match( /[^0-9]+/)){
    	alert("電話番号は半角数字のみで入力して下さい。");
    	return false;
   	}
    if(document.submit.tel2.value.match( /[^0-9]+/)){
    	alert("電話番号は半角数字のみで入力して下さい。");
    	return false;
   	}
   	if(document.submit.fax.value.match( /[^0-9]+/)){
    	alert("ファック番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.zip31.value.match( /[^0-9]+/ )){
    	alert("郵便番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.zip32.value.match( /[^0-9]+/ )){
    	alert("郵便番号は半角数字のみで入力して下さい。");
    	return false;
    }
    if(document.submit.ticket.value.match( /[^0-9^+-]+/ )){
    	alert("保有チケット枚数は半角数字のみで入力して下さい。");
    	return false;
    }

    if(confirm("この内容で登録しますか。") == true){



	var id = $('#cus_id').val();
	var id2 = $('#cus_id2').val();
	var useshop = $('#useshop').val();
	var guestname_kanji =$('#f_name').val();
	var guestname_kanji_first =$('#g_name').val();
	var guestname_katakana =$('#f_name_kana').val();
	var guestname_katakana_first =$('#g_name_kana').val();
	var postal_num1 =$('#zip31').val();
	var postal_num2 =$('#zip32').val();
	var prefecture =$('#pref31').val();
	var city =$('#addr31').val();
	var address =$('#add').val();
	var building =$('#build').val();
	var tel1 =$('#tel1').val();
	var tel2 =$('#tel2').val();
	var fax =$('#fax').val();
	var mailaddress =$('#mail').val();
	var sex =$("input:radio[name='sex']:checked").val();
	var birth_y =$('#birth_y').val();
	var birth_m =$('#birth_m').val();
	var birth_d =$('#birth_d').val();
	var mail_mag =$("input:radio[name='mail_mag']:checked").val();
	var dm =$("input:radio[name='dm']:checked").val();
	var motive =$("input:radio[name='motive']:checked").val();
	var tickets =$('#ticket').val();
	var level =$("input:radio[name='level']:checked").val();
	var remarks =$('#memo').val();
	var member_check =$("input:radio[name='member_check']:checked").val();

	$.post('customernew_do.php',
		{id:id,
		 id2:id2,
		 useshop:useshop,
		 guestname_kanji:guestname_kanji,
		 guestname_kanji_first:guestname_kanji_first,
		 guestname_katakana:guestname_katakana,
		 guestname_katakana_first:guestname_katakana_first,
		 postal_num1:postal_num1,
		 postal_num2:postal_num2,
		 prefecture:prefecture,
		 city:city,
		 address:address,
		 building:building,
		 tel1:tel1,
		 tel2:tel2,
		 fax:fax,
		 mailaddress:mailaddress,
		 sex:sex,
		 birth_y:birth_y,
		 birth_m:birth_m,
		 birth_d:birth_d,
		 mail_mag:mail_mag,
		 dm:dm,
		 motive:motive,
		 tickets:tickets,
		 level:level,
		 remarks:remarks,
		 member_check:member_check},
		function(data){
			if(data == 0){
				alert("会員番号の重複を確認して下さい。")
			}else{
				alert(data);
				document.location = "customernew.php";}});
}else{
	alert("キャンセルされました。");
}
}

</script>
<script>
function sub_id(){
	$('#cus_id').remove();
		if($('#id_sub').prop('checked') == 1){
			$('#input_box').html("<input name='cus_id2' id='cus_id2' type='text' size='10' maxlength='10' />"+"-"+"<input name='cus_id' id='cus_id' type='text'  size='20' maxlength='20' /></br>");
		}else{
			$('#input_box').html("<input name='cus_id' id='cus_id' type='text' size='20' maxlength='20' /><input type='hidden' id='cus_id2' /></br>");
		}
		
	}
</script>
<div id="pagebodymain">
<h1>
	<div style="text-aligin;left; float:left;">
		新しく会員を登録</div>
	<div style="text-align:right;"><a href="customerlist.php" style="margin-right:10px;">会員データの一覧へ</a></div>
</h1>
<form action="#" name="submit" method="post">
<table>
	<tr>
		<th colspan="2" class="b">登録店舗の選択</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">登録店舗</th>
		<th>
			<select id="useshop">
			<?php 
				$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
				$recordSet = mysqli_query($db, $sql);
				while($arr_item = mysqli_fetch_assoc($recordSet))
				{
				echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
				}
			?>
		</th>
	</tr>
</table>
<div style="height:25px;"></div>
<table id="cus_new">
	<tr>
		<th class="b" colspan="2">
			会員の情報</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">会員番号</th>
		<th>
		<span id="input_box">
			<input type="hidden" id="cus_id2" />
			<input name="cus_id" id="cus_id" type="text" size="20" maxlength="20" /></br>
		</span>
		<input type="checkbox" name="id_sub" id="id_sub" onclick="sub_id();" />
		<label for="id_sub">ハイフン等を使う</label>
		</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">会員名<font color="red">(*)</font></th>
		<th>姓 &nbsp;<input type="text" name="f_name" id="f_name" size="20" />
			名 &nbsp;<input type="text" name="g_name" id="g_name" size="20" /></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">フリガナ<font color="red">(*)</font></th>
		<th>セイ<input type="text" name="f_name_kana" id="f_name_kana" size="20" onblur="kana_check();"/>
			メイ<input type="text" name="g_name_kaba" id="g_name_kana" size="20" onblur="kana_check();"/></th>
	</tr>
	<tr>
		<th class="a" rowspan="5" style="width:16%;">住所</th>
		<th >郵便番号 <input type="text" id="zip31" name="zip31" size="4" maxlength="3" /> －<input type="text" id="zip32" name="zip32" size="5" maxlength="4"onKeyUp="AjaxZip3.zip2addr('zip31','zip32','pref31','addr31','addr31');" /> (半角数字で入力)</th>
	</tr>
	<tr>
		<th>都道府県 <input type="text" name="pref31" id="pref31" size="20" /></th>
	</tr>
	<tr>
		<th>市区町村 <input type="text" name="addr31" id="addr31" size="40" /></th>
	</tr>
	<tr>
		<th>番地等 &nbsp;&nbsp;&nbsp;<input type="text" name="add" id="add" size="40" /></th>
	</tr>
	<tr>
		<th>建物名等&nbsp;<input type="text" name="build" id="build" size="40" /></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">電話番号1<font color="red">(*)</font></th>
		<th><input type="text" name="tel1" id="tel1" size="40" /> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">電話番号2</th>
		<th><input type="text" name="tel2" id="tel2" size="40" /> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">ファックス番号</th>
		<th><input type="text" name="fax" id="fax" size="40" /> </br>（ハイフン等の記号は入れずに半角数字のみで記入してください。）</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">メールアドレス</th>
		<th><input type="text" name="mail" id="mail" size="40" /></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">性別<font color="red">(*)</font></th>
		<th>
			<input type="radio" name="sex" id="sex_non" value="0" checked="cecked"/><label for="sex_non">未入力</label>
			<input type="radio" name="sex" id="sex_male" value="1" /><label for="sex_male">男性</label>
			<input type="radio" name="sex" id="sex_female" value="2" /><label for="sex_female">女性</label>
		</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">生年月日<font color="red">(*)</font></th>
		<th>
			<select id="birth_y" name="birth_y">
				<option value="">----</option>
			<?php
			for ($y=1900;$y<date(Y)+1;$y++){
				$sel="";
				if($_SESSION["birth_y"] == $y) { $sel = "selected='selected'"; }
				 print '<option value="'.$y.'"'.$sel.'>' . $y . "
				 </option>\n";
				 }
			?>
		</select>年

		<select id="birth_m" name="birth_m">
				<option value="">--</option>
			<?php
			for ($m=01;$m<=12;$m++){
				$sel="";
				if($_SESSION["birth_m"] == $m) { $sel = "selected='selected'"; }
				 print '<option value="'.sprintf("%02d",$m).'"'.$sel.'>' . sprintf("%02d",$m) . "</option>\n";
				 }
			?>
		</select>月

		<select id="birth_d" name="birth_d">
				<option value="">--</option>
			<?php
			for ($d=01;$d<=31;$d++){
				$sel="";
				if($_SESSION["birth_d"] == $d) { $sel = "selected='selected'"; }
				 print '<option value="'.sprintf("%02d",$d).'"'.$sel.'>' . sprintf("%02d",$d) . "</option>\n";
				 }
			?>
		</select>日

		</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">メルマガ</th>
		<th><input type="radio" name="mail_mag" id="mail_mag_off" value="0" checked="checked"/><label for="mail_mag_off">未登録</label>
			<input type="radio" name="mail_mag" id="mail_mag_on" value="1" /><label for="mail_mag_on">登録済</label></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">DM</th>
		<th><input type="radio" name="dm" id="dm_off" value="0" checked="checked"/><label for="dm_off">配信しない</label>
			<input type="radio" name="dm" id="dm_on" value="1" />
			<label for="dm_on">配信する</label></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">来店動機</th>
		<th><input type="radio" name="motive" id="mot1" value="0" checked="checked" /><label for="mot1">不明</label>
			<input type="radio" name="motive" id="mot2" value="1"/><label for="mot2">立寄</label>
			<input type="radio" name="motive" id="mot3" value="2"/><label for="mot3">紹介</label>
			<input type="radio" name="motive" id="mot4" value="3"/><label for="mot4">DM</label>
			<input type="radio" name="motive" id="mot5" value="4"/><label for="mot5">HP/blog</label>
			<input type="radio" name="motive" id="mot6" value="5"/><label for="mot6">広告</label>
			<input type="radio" name="motive" id="mot7" value="6"/><label for="mot7">Fﾍﾟｰﾊﾟｰ</label>
			<input type="radio" name="motive" id="mot8" value="7"/><label for="mot8">チラシ</label>
			<input type="radio" name="motive" id="mot9" value="8"/><label for="mot9">ｷｬｯﾁ</label>
			<input type="radio" name="motive" id="mot10" value="9"/><label for="mot10">その他</label>
		</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">保有チケット</th>
		<th><input type="text" name="ticket" id="ticket" />枚</th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">会員レベル</th>
		<th>
		<input type="radio" name="level" id="lev1" value="0" checked="checked" /><label for="lev1">レベル1</label>
		<input type="radio" name="level" id="lev2" value="1" /><label for="lev2">レベル2</label>
		<input type="radio" name="level" id="lev3" value="2" /><label for="lev3">レベル3</label>
		<input type="radio" name="level" id="lev4" value="3" /><label for="lev4">レベル4</label>
		<input type="radio" name="level" id="lev5" value="4" /><label for="lev5">レベル5</label></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">運営者用のメモ</th>
		<th><textarea name="memo" id="memo" rows="5" cols="70" ></textarea></th>
	</tr>
	<tr>
		<th class="a" style="width:16%;">退会処理</th>
		<th><input type="radio" name="member_check" id="member" value="0" checked="checked" /><label for="member">会員登録中</label>
			<input type="radio" name="member_check" id="non_member" value="1" /><label for="non_member">退会中</label></th>
	</tr>
</table>
</form>
<p>
<font color="red">(*)は必須項目です。</font>
<p>
<p style="text-align:center">
<input type="image" src="../css/image/contents/view.gif" onclick="return cus_input();"/>



</div>
<?php include("footer.php"); ?>
