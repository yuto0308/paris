<?php
	session_start();
	include("db_connect.php");

	$namestr=""; //data[0]
	$datastr=""; //data[1]
	$regdata=""; //data[2]

	$result_ary=array();

	$shop_name=$_POST['search_shop_id'];

	$year_s=$_POST['year_s'];
	$month_s=$_POST['month_s'];
	if($month_s<10){
		$month_s='0'.$month_s;
	}
	$day_s=$_POST['day_s'];
	if($day_s<10){
		$day_s='0'.$day_s;
	}
	$year_e=$_POST['year_e'];
	$month_e=$_POST['month_e'];
	if($month_e<10){
		$month_e='0'.$month_e;
	}
	$day_e=$_POST['day_e'];
	if($day_e<10){
		$day_e='0'.$day_e;
	}


//data[0]分
	$sql_members = ' SELECT * FROM members ';
	$sql_members.= ' WHERE shop_id='.$shop_name;
	$rs_members = mysqli_query($db,$sql_members);
	$members = mysqli_fetch_assoc($rs_members);


	$namestr .='<tr style="height:30px;" name="shopname">
				<th colspan="7" class="b">'.$members['name'].'の売上集計</th>
				</tr>';


//data[0]分

//data[1]分
	$basic = date('Y-m-d');
	$group = ' GROUP BY DATE_FORMAT(date,"%Y-%m-%d") ';

	$sql_shop = ' SELECT COUNT(count) as count_item, SUM(count) as sum_item, SUM(sum) as sum,SUM(ticket) as ticket, ';
	$sql_shop.= ' SUM(count_payback) as count_payback, SUM(payback) as sum_payback,SUM(card),SUM(cash) ';
	$sql_shop.= ' FROM shop_log_sub ';
	$sql_shop.= ' WHERE shop_id='.$shop_name.' AND ';
	$sql_shop.= ' DATE_FORMAT(date,"%Y-%m-%d")="'.$basic.'" '.$group;
	$rs_shop = mysqli_query($db, $sql_shop);
	$shopdata = mysqli_fetch_assoc($rs_shop);

	if($shopdata['count_item']==""){
	$count_item="0";
	}else{
	$count_item=$shopdata['count_item'];
	}

	if($shopdata['sum_item']==""){
	$sum_item="0";
	}else{
	$sum_item=$shopdata['sum_item'];
	}

	if($shopdata['sum']==""){
	$sum="0";
	$non_ticket="0";
	}else{

	//$non_ticket=$shopdata['SUM(card)']+$shopdata['SUM(cash)'];
	$non_ticket = $shopdata['sum'] - ($shopdata['ticket'] * 1080);
	$non_ticket=number_format($non_ticket);
	$sum=number_format($shopdata['sum']);
	}

	if($shopdata['count_payback']==""){
	$count_payback="0";
	}else{
	$count_payback=$shopdata['count_payback'];
	}

	if($shopdata['sum_payback']==""){
	$sum_payback="0";
	}else{
	$sum_payback=$shopdata['sum_payback'];
	$sum_payback=number_format($sum_payback);
	}

	$datastr.=	'<tr style="height:30px;" name="shopdata">
				<th class="a" style="text-align:center;">'.$members['name'].'</th>
				<th style="text-align:right;">'.$count_item.'件</th>
				<th style="text-align:right;">'.$sum_item.'点</th>
				<th style="text-align:right;">'.$sum.'円</th>
				<th style="text-align:right;">'.$count_payback.'点</th>
				<th style="text-align:right;">'.$sum_payback.'円</th>
				<th style="text-align:right;">'.$non_ticket.'円</th>
				</tr>';


//data[1]分

//data[2]分
	$sql_start = ' SELECT *,DATE_FORMAT(start_time,"%Y-%m-%d") as start_time ';
	$sql_start.= ' FROM pos_start WHERE shop_id='.$shop_name;
	$sql_start.= ' ORDER BY start_time DESC LIMIT 0,1 ';
	$rs_start = mysqli_query($db, $sql_start);
	$start_num=mysqli_num_rows($rs_start);
	$start = mysqli_fetch_assoc($rs_start);


	$sql_finish = ' SELECT *, DATE_FORMAT(datetime,"%Y-%m-%d") as datetime ';
	$sql_finish.= ' FROM pos_finish WHERE shop_id='.$shop_name;
	$sql_finish.= ' ORDER BY datetime DESC LIMIT 0,1 ';
	$rs_finish = mysqli_query($db, $sql_finish);
	$finish_num=mysqli_num_rows($rs_finish);
	$finish = mysqli_fetch_assoc($rs_finish);

	if($start_num==0 or $finish_num==0){
		$regdata.='<tr style="height:30px;" name="shopreg">
					<th class="a" style="text-align:center; width:20%;">'.$members['name'].'</th>
					<th style="text-align:center; width:40%;"><font color="red">レジの締め処理が行われていません。
					</font></th>
					<th style="text-align:center; width:40%;"><font color="red">レジの締め処理が行われていません。
					</font></th>
					</tr>';
					$result_ary[]=$namestr;
					$result_ary[]=$datastr;
					$result_ary[]=$regdata;
					echo json_encode($result_ary);
					return;
				}

		if($basic==$start['start_time']){
			$startstr = "設定は完了しています。";
		}
		else if($basic!=$start['start_time'] && $members['start']<=date('G')){
			$startstr = "<font color='red'>開始レジ金の設定が行われていません。</font>";
		}else if($basic!=$start['start_time'] && $members['start']>=date('G')){
			$startstr = "";
		}

		if($basic==$finish['datetime']){
			$finishstr = "設定は完了しています。";
		}
		else if($basic!=$finish['datetime'] && $members['end']<=date('G')){
			$finishstr = "<font color='red'>レジの締め処理が行われていません。</font>";
		}else if($basic!=$finish['datetime'] && $members['end']>=date('G')){
			$finishstr = "";
		}

	$regdata.='	<tr style="height:30px;" name="shopreg">
		<th class="a" style="text-align:center; width:20%;">'.$members['name'].'</th>
		<th style="text-align:center; width:40%;">'.$startstr.'</th>
		<th style="text-align:center; width:40%;">'.$finishstr.'</th>
		</tr>';

//data[2]分



//data[3]分
	$repeat1='<tr name="repeat1">
		<th class="a" style="text-align:center; width:20%;" rowspan="2">'.$members['name'].'</th>
		<th style="text-align:center;" class="a">レジ件数</th>
		<th style="text-align:center;" class="a">実質来店者数</th>
		<th style="text-align:center;" class="a">リピート顧客数</th>
		<th style="text-align:center;" class="a">リピート率</th>
		</tr>';

	$sql_regcount =' SELECT * FROM shop_log_sub WHERE date>="'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00 " ';
	$sql_regcount.=' AND date<="'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59 " ';
	$sql_regcount.=' AND shop_id='.$shop_name;
	$rs_regcount  =mysqli_query($db,$sql_regcount);
	$regcount =mysqli_num_rows($rs_regcount);

	$sql_bunshi =' SELECT * FROM shop_log_sub WHERE date>="'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00 " ';
	$sql_bunshi.=' AND date<="'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59 " ';
	$sql_bunshi.=' AND shop_id='.$shop_name.' group by guest_id having COUNT(guest_id) > 1 ';
	$rs_bunshi=mysqli_query($db,$sql_bunshi);
	$bunshi=mysqli_num_rows($rs_bunshi);

	$sql_bunbo =' SELECT COUNT(DISTINCT guest_id) as ninzu FROM shop_log_sub ';
	$sql_bunbo.=' WHERE date>="'.$year_s.'-'.$month_s.'-'.$day_s.' 00:00:00 " ';
	$sql_bunbo.=' AND date<="'.$year_e.'-'.$month_e.'-'.$day_e.' 23:59:59 " ';
	$sql_bunbo.=' AND shop_id='.$shop_name;
	$query_bunbo=mysqli_query($db,$sql_bunbo);
	$bunbo_rs=mysqli_fetch_assoc($query_bunbo);
	$bunbo=$bunbo_rs['ninzu'];
	if($bunbo==""){
		$bunbo=0;
	}
	if($bunbo==0 || $bunbo==""){
		$re='0';
	}else{
	$re=$bunshi/$bunbo*100;
	$re=round($re,2);
	}

	$repeat2='<tr name="repeat2">
		<th style="text-align:center;width:15%;">'.$regcount.'件</th>
		<th style="text-align:center;width:15%;">'.$bunbo.'人</th>
		<th style="text-align:center;width:15%;">'.$bunshi.'人</th>
		<th style="text-align:center;width:15%;">'.$re.'%</th>
		</tr>';
//data[3]分

$result_ary[]=$namestr;
$result_ary[]=$datastr;
$result_ary[]=$regdata;
$result_ary[]=$repeat1;
$result_ary[]=$repeat2;
echo json_encode($result_ary);
return;

?>