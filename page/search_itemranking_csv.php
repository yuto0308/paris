<?php
/*

作成者　：　油谷
itemreport の結果をCSV出力

*/
	session_start();
	include("db_connect.php");
	$str="";

	$shops = $_GET['shops'];
	if($shops == 0){
		$shops = '1';
	}else{
		$shops = ' shop_log.shop_id = '.$shops;
	}

	$cat_big=mysqli_real_escape_string($db,$_GET['cat_big']);//大カテゴリ
	if($cat_big == 0){
		$cat_big_str = "1";
	}else{
		$cat_big_str = "category_big = $cat_big ";
	}

	$sort = $_GET['sort'];
	if($sort == 0){
		$sort = ' ORDER BY price DESC ';
	}elseif($sort == 1){
		$sort = ' ORDER BY num DESC ';
	}

	$str .= "店舗番号,店舗名,商品コード,商品名,通常価格,点数,売上金額\n";		
	$year_s=mysqli_real_escape_string($db,$_GET['year_s']);
	$month_s=mysqli_real_escape_string($db,$_GET['month_s']);
	if($month_s < 10){
		$month_s = "0".$month_s;
	}
	$day_s_str = $year_s . "-" . $month_s . "-01 00:00:00";

	$year_e=mysqli_real_escape_string($db,$_GET['year_e']);
	$month_e=mysqli_real_escape_string($db,$_GET['month_e']);
	if($month_e < 10){
		$month_e = "0".$month_e;
	}
	$day_e_str = $year_e . "-" . $month_e . "-31 23:59:59";

	$sql_show = " SELECT ";
	$sql_show .= " goods_id, goods.name as goods_name, SUM(goods_num) AS num, ";
	$sql_show .= " SUM(shop_log.price) AS price, shop_log.shop_id AS s_id, goods.price AS g_price ";
	$sql_show .= " FROM shop_log,goods ";
	$sql_show .= " WHERE day >= '$day_s_str' AND ";
	$sql_show .= " day <= '$day_e_str' AND $shops ";
	$sql_show .= " AND shop_log.goods_id = goods.id ";
	$sql_show .= " AND  $cat_big_str ";
	$sql_show .= " GROUP BY goods_id $sort ";

	$query_show = mysqli_query($db,$sql_show);
	if(!$query_show){
		echo "対象データがありません。";
		return;
	}
	$row = mysqli_num_rows($query_show);
	if($row == 0){
		echo "対象データがありません";
		return;
	}
	while($arr_show = mysqli_fetch_assoc($query_show)){	
		$str .= $_SESSION['shop_id'].",".$_SESSION['name'].",".$arr_show['goods_id'].",".$arr_show['goods_name'].",".($arr_show['g_price']).",".$arr_show['num'].",".$arr_show['price']."\n";
	}


	header("Content-Type: application/octet-stream;charset=sjis-win");
	header("Content-Disposition: attachment; filename=item_monthly.csv");
	print(mb_convert_encoding($str,"SJIS","UTF-8"));
?>
