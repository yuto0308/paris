<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php");?>
<script>
	function nippou(){
		// var test=2020;
	var shopnum = $('#useshop').val();
	var result = '';
	var p = document.printdata.nippou;
	var pl = p.length;
	var tkd=0;
	if(pl){
	for(var i = 0; i < pl; i++ ){
		if(p[i].checked ){
			if(result==''){
			result = "'" + p[i].value + "'"; 
			}else{
			result =  result +","+"'" + p[i].value + "'";
			}
			tkd=tkd+1;
		}
	}
	}else if(p.checked){
		result ="'" + p.value +"'";
		tkd= tkd+1;
	}
	if(result){
		if(confirm("チェックされた対象を印刷してもよろしいですか。")==true){
		window.open('print_report.php?result='+result+'&tkd='+tkd+'&shopnum='+shopnum,"","width=650,height=500,resizable=yes");
		}else{
			return;
		}
	}else{
		alert('印刷対象を選択してください。');
	}
}

	function search(num){
		$("tr[name='add']").remove();
		$("tr[name='add2']").remove();
		$("input[name='add_button']").remove();
		var useshop = $('#useshop').val();
		var year_st = $('#year_s').val();
		var month_st = $('#month_s').val();
		var day_st = $('#day_s').val();
		var year_en = $('#year_e').val();
		var month_en = $('#month_e').val();
		var day_en = $('#day_e').val();
		var day_or_month = $("input:radio[name='dayormonth']:checked").val();
		//day_or_month : 日別表示のときは0,月別表示のときは1
		var sort = $("input:radio[name='day_or_sales']:checked").val();
		//day_or_sales : 日付順の時は0,売上順のときは1
		var staff_id = $('#staff').val();
	$.ajax({type:"POST",
 			url:"shop_log.php",
 				data:{	num:num,
 						useshop:useshop,
 						year_st:year_st,
 						month_st:month_st,
 						day_st:day_st,
 						year_en:year_en,
 						month_en:month_en,
 						day_en:day_en,
 						day_or_month:day_or_month,
 						sort:sort,
 						staff_id:staff_id
 						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#table').append(data[1]);
						$('#span').append(data[2]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown);
						}
						});
}
</script>
<script>
	$(document).ready(function(){   
    $("#staff").keyup(function (e) {
        var str = $("#staff").val();        
      	//strの中身をphpに飛ばす
      	$('#staffname').load('get_staff_name.php',{id:str});
    });
	
  });
</script>
<script>
function search_shopreport_csv(num){
		var useshop = $('#useshop').val();
		var year_st = $('#year_s').val();
		var month_st = $('#month_s').val();
		var day_st = $('#day_s').val();
		var year_en = $('#year_e').val();
		var month_en = $('#month_e').val();
		var day_en = $('#day_e').val();
		var day_or_month = $("input:radio[name='dayormonth']:checked").val();
		//day_or_month : 日別表示のときは0,月別表示のときは1
		var sort = $("input:radio[name='day_or_sales']:checked").val();
		//day_or_sales : 日付順の時は0,売上順のときは1
		var staff_id = $('#staff').val();
		window.open("./search_shopreport_csv.php?num="+num+"&year_st="+year_st+"&month_st="+month_st+"&day_st="+day_st+"&year_en="+year_en+"&month_en="+month_en+"&day_en="+day_en+"&day_or_month="+day_or_month+"&sort="+sort+"&staff_id="+staff_id+"&useshop="+useshop);


}
</script>
<script>
function search_shopreport_graph(num){
		var useshop = $('#useshop').val();
		var year_st = $('#year_s').val();
		var month_st = $('#month_s').val();
		var day_st = $('#day_s').val();
		var year_en = $('#year_e').val();
		var month_en = $('#month_e').val();
		var day_en = $('#day_e').val();
		var day_or_month = $("input:radio[name='dayormonth']:checked").val();
		//day_or_month : 日別表示のときは0,月別表示のときは1
		var sort = $("input:radio[name='day_or_sales']:checked").val();
		//day_or_sales : 日付順の時は0,売上順のときは1
		var staff_id = $('#staff').val();
		window.open("./make_graph.php?num="+num+"&year_st="+year_st+"&month_st="+month_st+"&day_st="+day_st+"&year_en="+year_en+"&month_en="+month_en+"&day_en="+day_en+"&day_or_month="+day_or_month+"&sort="+sort+"&staff_id="+staff_id+"&useshop="+useshop);


}
</script>

<div id="pagebodymain">
<h1>月日別の集計</h1>
<p>
	<table>
		<tr>
		<th colspan="4" class="b">検索条件</th>
		<tr>
			<th class="a">店舗</th>
			<th>
				<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?></th>
			<th class="a">集計対象</th>
			<th>
			<input type="radio" value="0" name="dayormonth" id="day" checked="checked"><label for="day">日別</label>
			<input type="radio" value="1" name="dayormonth" id="month"><label for="month">月別</label>
			</th>
		</tr>
		<tr>
			<th class="a">スタッフ番号</th>
			<th><input type="text" value="" id="staff"></th>
			<th class="a">担当者名</th>
			<th id="staffname"><!--担当者名表示--></th>
		</tr>
		<tr>
			<th class="a">表示順</th>
			<th colspan="3" align="left">
				<input type="radio" name="day_or_sales" value="0" checked="checked">日付順
				<input type="radio" name="day_or_sales" value="1">売り上げ順
			</th>
		<tr>
			<th class="a">
				集計期間
			</th>
			<th colspan="3" style="text-align:center;">
				<select id="year_s" >
					<?php
						$year_s = date("Y");
						for($i=2010;$i<=$year_s;$i++){
						if($i == $year_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>年
				<select id="month_s">
					<?php
						$month_s = date("n");
						for($i=1;$i<=12;$i++){
						if($i == $month_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>月
				<select id="day_s">
					<?php
						$day_s = date("j");
						for($i=1;$i<=31;$i++){
						if($i == 1){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>日  ～
					<select id="year_e" >
					<?php
						$year_e = date("Y");
						for($i=2010;$i<=$year_e;$i++){
						if($i == $year_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>年
				<select id="month_e">
					<?php
						$month_e = date("n");
						for($i=1;$i<=12;$i++){
						if($i == $month_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>月
				<select id="day_e">
					<?php
						$day_e = date("j");
						for($i=1;$i<=31;$i++){
						if($i == $day_e){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>日
			</th>
		</tr>
		<tr>
			<th colspan="4" style="text-align:center;">
				<input type="image" src="../css/image/contents/search_reset.gif" onclick="location.reload();" alt="条件をリセット">
				<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索" onclick="search(1);" /></th>
	</table>
<p><div style="text-align:left;float:left"><a href="#" onclick="search_shopreport_csv(1);"><img src="../css/image/contents/csv_btn.gif"></a></div>
<div style="text-align: right;"><a href="#" onclick="search_shopreport_graph(1);"><img src="../css/image/contents/graph(3).gif"></a></div>
</p>
<p>
	<form name="printdata"  action="#" method="post">
	<table id="table">
		<tr class="a" name="add2">
			<th style="text-align:center;">年月日</th>
			<th style="text-align:center;">点数</th>
			<th style="text-align:center;">現金</th>
			<th style="text-align:center;">ｸﾚｼﾞｯﾄ</th>
			<th style="text-align:center;">割引</th>
			<th style="text-align:center;">ﾁｹｯﾄ</th>
			<th style="text-align:center;">返金</th>
			<th style="text-align:center;">売上金額</th>
			<th style="text-align:center;">日報</th>
		</tr>
		<?php

	$year_st = date("Y");
	$month_st = date("n");
	$day_st = 1;
	$year_en = date("Y");
	$month_en = date("n");
	$day_en = 31;//正常にインデックスするようにするため
	$sum=0;
	$day="";
	$str="";
		
		if($series != ""){
		/*対象となるSQL文*/
		$sql = "SELECT shop_id,SUM(sum), SUM(count),DATE_FORMAT(date,'%Y-%m-%d') as times, DATE_FORMAT(date,'%Y年%m月%d日')as time , SUM(cash)as cash , SUM(card) as card,SUM(discount) as discount,SUM(ticket) as ticket, SUM(payback) as payback FROM shop_log_sub WHERE date >= '".$year_st."-".$month_st."-".$day_st."' and date <= '".$year_en."-".$month_en."-".$day_en."' group by time order by date DESC ";
		$sql_count=$sql;
		$rs_count=mysqli_query($db,$sql_count);
		$row=mysqli_num_rows($rs_count);
		$sql.=" LIMIT 0,10";
		$rs = mysqli_query($db,$sql);
		if(!$rs){
			die($sql/* 'クエリ失敗 連絡をおねがいします2' */);
			}
		while(($arr_item = mysqli_fetch_assoc($rs)))
			{//すべての行を処理
			 //日にちごとに合計を算出
			$day = $arr_item['time']; 
			$sum = $arr_item['cash']+$arr_item['card'];
			$count = $arr_item['SUM(count)'];
			$cash = $arr_item['cash'];
			$card = $arr_item['card'];
			$discount = $arr_item['discount'];
			$ticket = $arr_item['ticket'];
			$payback = $arr_item['payback'];
			$checktime=$arr_item['times'];

		$sql_fin=' SELECT * FROM pos_finish WHERE shop_id='.$arr_item['shop_id'].' AND DATE_FORMAT(datetime,"%Y-%m-%d")="'.$checktime.'"';
		$rs_fin=mysqli_query($db,$sql_fin);
		if(!$rs_fin){
			$checkbox='<input type="checkbox" name="nippou" disabled="disabled">';
		}
		$pos_check=mysqli_num_rows($rs_fin);
		if($pos_check>0){
			$checkbox='<input type="checkbox" value="'.$day.'" name="nippou">';
		}else{
			$checkbox='<input type="checkbox" name="nippou" disabled="disabled">';
		}
			$str .= '<tr name="add">
					<th style="text-align:center;">'.$day.'</th>
					<th style="text-align:center;">'.$count.'</th>
					<th style="text-align:center;">'.number_format($cash).'</th>
					<th style="text-align:center;">'.number_format($card).'</th>
					<th style="text-align:center;">'.number_format($discount).'</th>
					<th style="text-align:center;">'.number_format($ticket).'</th>
					<th style="text-align:center;">'.number_format($payback).'</th>
					<th style="text-align:center;">'.number_format($sum).'</th>
					<th style="text-align:center;">'.$checkbox.'</th>
					</tr>';
			/*挿入先テーブル情報*/
			/*<tr>
			<th>年月日</th>
			<th>点数</th>
			<th>現金</th>
			<th>ｸﾚｼﾞｯﾄ</th>
			<th>割引</th>
			<th>ﾁｹｯﾄ</th>
			<th>返金</th>
			<th>売上金額</th>
			<th>日報</th>
			</tr>*/
			}
			}
if($day==""){
	echo '<tr name="add"><th colspan="9" style="text-align:center;">対象データがありません</th></tr>';
}else{
echo $str;
}
?>
</table>
</form>
<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($row -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>

<p style="text-align: right;">
	<input type="image" src="../css/image/contents/btn_nippou.gif" onclick="nippou();" />
</div>
<?php include("footer.php"); ?>
