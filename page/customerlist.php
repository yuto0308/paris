<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php include("db_connect.php"); ?>
<script>
function search_guest(num){
	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順
	var sort_info=$("input:radio[name='display']:checked").val();
	
	//key_ctgr : 0 選択無し
	//		   : 1 会員名
	//         : 2 会員名（ふりがな）
	//         : 3 会員住所
	//         : 4 会員電話番号
	//         : 5 メールアドレス
	//         : 6 会員番号
	var key_ctgr=$("#category").val();
	
	//key_ctgr_word : キーワード
	var key_ctgr_word=$("#ctgr_word").val();

	//key_member_check : 0 指定無し
	//				   : 1 会員
	//                 : 2 退会
	var key_member_check=$("#member_check").val();
	
	//key_dm	: 0 指定無し
	//			: 1 配信する
	//			: 2 配信しない
	var key_dm=$("#dm").val();
	
	//key_year_s : 登録始点
	//key_month_s: 登録始点
	//key_day_s  : 登録始点
	var key_year_s=$("#year1").val();
	var key_month_s=$("#month1").val();
	var key_day_s=$("#day1").val();
	
	//key_year_e : 登録終点
	//同
	var key_year_e=$("#year2").val();
	var key_month_e=$("#month2").val();
	var key_day_e=$("#day2").val();

	//key_useshop : 利用店舗条件
				//: 0 初回
				//: 1 最終
	var key_useshop=$("input:radio[name='shop']:checked").val();
	//key_shop_name : 利用店舗
	var key_shop_name=$("#useshop").val();

	//use_day	:利用日条件
	//			:0 初回
	//			:1 最終
	var use_day=$("input:radio[name='day']:checked").val();
	//key_year_use_s(e):購入日
	//key_month_use_s(e):購入日
	//key_day_use_s(e):購入日
	var key_year_use_s=$("#year3").val();
	var key_month_use_s=$("#month3").val();
	var key_day_use_s=$("#day3").val();
	var key_year_use_e=$("#year4").val();
	var key_month_use_e=$("#month4").val();
	var key_day_use_e=$("#day4").val();
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({type:"POST",
 			url:"search_guest.php",
 				data:{	num:num,
 						sort_info:sort_info,
 					    key_ctgr:key_ctgr,
 						key_ctgr_word:key_ctgr_word,
 						key_member_check:key_member_check,
 						key_dm:key_dm,
 						key_year_s:key_year_s,
 						key_month_s:key_month_s,
 						key_day_s:key_day_s,
 						key_year_e:key_year_e,
 						key_month_e:key_month_e,
 						key_day_e:key_day_e,
 						key_useshop:key_useshop,
 						key_shop_name:key_shop_name,
 						use_day:use_day,
 						key_year_use_s:key_year_use_s,
 						key_month_use_s:key_month_use_s,
 						key_day_use_s:key_day_use_s,
 						key_year_use_e:key_year_use_e,
 						key_month_use_e:key_month_use_e,
 						key_day_use_e:key_day_use_e
 						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#num_guest').text(data[1]);
						$('#span').append(data[2]);
						$('#page_number').val(data[3]);
						},
				error:	function(XMLHttpRequest, textStatus, errorthrown){
						alert('error : ' + errorthrown + XMLHttpRequest.status + textStatus);
						}
						});
}
</script>

<script>
$(document).ready(function(){
	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順
	var sort_info=$("input:radio[name='display']:checked").val();
	
	//key_ctgr : 0 選択無し
	//		   : 1 会員名
	//         : 2 会員名（ふりがな）
	//         : 3 会員住所
	//         : 4 会員電話番号
	//         : 5 メールアドレス
	//         : 6 会員番号
	var key_ctgr=$("#category").val();
	
	//key_ctgr_word : キーワード
	var key_ctgr_word=$("#ctgr_word").val();

	//key_member_check : 0 指定無し
	//				   : 1 会員
	//                 : 2 退会
	var key_member_check=$("#member_check").val();
	
	//key_dm	: 0 指定無し
	//			: 1 配信する
	//			: 2 配信しない
	var key_dm=$("#dm").val();
	
	//key_year_s : 登録始点
	//key_month_s: 登録始点
	//key_day_s  : 登録始点
	var key_year_s=$("#year1").val();
	var key_month_s=$("#month1").val();
	var key_day_s=$("#day1").val();
	
	//key_year_e : 登録終点
	//同
	var key_year_e=$("#year2").val();
	var key_month_e=$("#month2").val();
	var key_day_e=$("#day2").val();

	//key_useshop : 利用店舗条件
				//: 0 初回
				//: 1 最終
	var key_useshop=$("input:radio[name='shop']:checked").val();
	//key_shop_name : 利用店舗
	var key_shop_name=$("#useshop").val();

	//use_day	:利用日条件
	//			:0 初回
	//			:1 最終
	var use_day=$("input:radio[name='day']:checked").val();
	//key_year_use_s(e):購入日
	//key_month_use_s(e):購入日
	//key_day_use_s(e):購入日
	var key_year_use_s=$("#year3").val();
	var key_month_use_s=$("#month3").val();
	var key_day_use_s=$("#day3").val();
	var key_year_use_e=$("#year4").val();
	var key_month_use_e=$("#month4").val();
	var key_day_use_e=$("#day4").val();
	
	var page_number = $('#page_number').val();
	
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({type:"POST",
 			url:"search_guest.php",
 				data:{	num:page_number,
 						sort_info:sort_info,
 					    key_ctgr:key_ctgr,
 						key_ctgr_word:key_ctgr_word,
 						key_member_check:key_member_check,
 						key_dm:key_dm,
 						key_year_s:key_year_s,
 						key_month_s:key_month_s,
 						key_day_s:key_day_s,
 						key_year_e:key_year_e,
 						key_month_e:key_month_e,
 						key_day_e:key_day_e,
 						key_useshop:key_useshop,
 						key_shop_name:key_shop_name,
 						use_day:use_day,
 						key_year_use_s:key_year_use_s,
 						key_month_use_s:key_month_use_s,
 						key_day_use_s:key_day_use_s,
 						key_year_use_e:key_year_use_e,
 						key_month_use_e:key_month_use_e,
 						key_day_use_e:key_day_use_e
 						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#num_guest').text(data[1]);
						$('#span').append(data[2]);
						$('#page_number').val(data[3]);
						},
				error:	function(XMLHttpRequest, textStatus, errorthrown){
						alert('error : ' + errorthrown + XMLHttpRequest.status + textStatus);
						}
						});
});
</script>

<script>
function search_customer_csv(num){
	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順
	var sort_info=$("input:radio[name='display']:checked").val();
	
	//key_ctgr : 0 選択無し
	//		   : 1 会員名
	//         : 2 会員名（ふりがな）
	//         : 3 会員住所
	//         : 4 会員電話番号
	//         : 5 メールアドレス
	//         : 6 会員番号
	var key_ctgr=$("#category").val();
	
	//key_ctgr_word : キーワード
	var key_ctgr_word=$("#ctgr_word").val();

	//key_member_check : 0 指定無し
	//				   : 1 会員
	//                 : 2 退会
	var key_member_check=$("#member_check").val();
	
	//key_dm	: 0 指定無し
	//			: 1 配信する
	//			: 2 配信しない
	var key_dm=$("#dm").val();
	
	//key_year_s : 登録始点
	//key_month_s: 登録始点
	//key_day_s  : 登録始点
	var key_year_s=$("#year1").val();
	var key_month_s=$("#month1").val();
	var key_day_s=$("#day1").val();
	
	//key_year_e : 登録終点
	//同
	var key_year_e=$("#year2").val();
	var key_month_e=$("#month2").val();
	var key_day_e=$("#day2").val();

	//key_useshop : 利用店舗条件
				//: 0 初回
				//: 1 最終
	var key_useshop=$("input:radio[name='shop']:checked").val();
	//key_shop_name : 利用店舗
	var key_shop_name=$("#useshop").val();

	//use_day	:利用日条件
	//			:0 初回
	//			:1 最終
	var use_day=$("input:radio[name='day']:checked").val();
	//key_year_use_s(e):購入日
	//key_month_use_s(e):購入日
	//key_day_use_s(e):購入日
	var key_year_use_s=$("#year3").val();
	var key_month_use_s=$("#month3").val();
	var key_day_use_s=$("#day3").val();
	var key_year_use_e=$("#year4").val();
	var key_month_use_e=$("#month4").val();
	var key_day_use_e=$("#day4").val();
window.open("./search_customer_csv.php?num="+num+"&sort_info="+sort_info+"&key_ctgr="+key_ctgr+"&key_member_check="+key_member_check+"&key_dm="+key_dm+"&key_year_s="+key_year_s+"&key_month_s="+key_month_s+"&key_day_s="+key_day_s+"&key_year_e="+key_year_e+"&key_month_e="+key_month_e+"&key_day_e="+key_day_e+"&key_year_use_s="+key_year_use_s+"&key_month_use_s="+key_month_use_s+"&key_day_use_s="+key_day_use_s+"&key_year_use_e="+key_year_use_e+"&key_month_use_e="+key_month_use_e+"&key_day_use_e="+key_day_use_e+"&key_ctgr_word="+key_ctgr_word+"&key_useshop="+key_useshop+"&key_shop_name="+key_shop_name+"&use_day="+use_day);	
 					
}
</script>
<div id="pagebodymain">
	<h1>会員データの一覧</h1>
	<?php if(isset($_GET['page_number'])){
		$page_number = $_GET['page_number'];
	}else{
		$page_number = 1;
	}
	echo '<input hidden id = "page_number" value="'.$page_number.'">';
?>

<table>
	<tr>
		<th class="b" colspan="4">検索条件</th>
	</tr>
	<tr>
		<th class="a">一覧の表示順</th>
		<th colspan="3">
			<input type="radio" name="display" id="new" value="0" checked="cheked"/><label for="new">登録の新しい順</label>
			<input type="radio" name="display" id="use" value="1" /><label for="use">利用回数の多い順</label>
			<input type="radio" name="display" id="buy" value="2" /><label for="buy">購入数量の多い順</label>
			<input type="radio" name="display" id="price" value="3" /><label for="price">購入金額の多い順</label>
		</th>
	</tr>
	<tr>
		<th class="a">検索カテゴリ</th>
		<th colspan="3">
			<select id="category"> 
			<option value="0">（選択してください）</option>
			<option value="1">会員名</option>
			<option value="2">会員名(フリガナ)</option>
			<!--<option value="3">会員住所</option>-->
			<option value="4">会員電話番号</option>
			<option value="5">メールアドレス</otion>
			<option value="6">会員番号</option>
			</select>
			キーワード
			<input type="text" id="ctgr_word" size="40"><br>
			※フルネームを入力される場合は、名字と名前の間に全角スペースを入れてください。
		</th>
	</tr>
	<tr>
		<th class="a">会員登録</th>
		<th>
			<select id="member_check">
			<option value="0">（指定しない）</option>
			<option value="1">会員登録済</option>
			<option value="2">退会中</option>
			</select>
		</th>
		<th class="a">DM登録</th>
		<th>
			<select id="dm">
			<option value="0">（指定しない）</option>
			<option value="1">配信する</option>
			<option value="2">配信しない</option>
		</select>
		</th>
	</tr>
	<tr>
		<th class="a">登録・更新日</th>
		<th colspan="3">
				<select id="year1">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month1">
				<?php 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day1">
				<?php 
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"><?php echo $d;?></option>
			<?php endfor?>
			</select>日 ～
			<select id="year2">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"<?php if($y==$now){echo 'selected="selected"';}?>><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month2">
				<?php 
				$month_now= date("m");
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"<?php if($m==$month_now){echo 'selected="selected"';}?>><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day2">
				<?php 
				$day_now=date("j");
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"<?php if($d==$day_now){echo 'selected="selected"';}?>><?php echo $d;?></option>
			<?php endfor?>
			</select>日
		</th>
	</tr>
	<tr>
		<th class="a">利用店舗</th>
		<th colspan="3">
				<input type="radio" name="shop" id="firstshop" value="0" checked="checked"/><label for="firstshop">初回</label>
				<input type="radio" name="shop" id="lastshop" value="1" /><label for="lastshop">最終</label>
					<select id="useshop">
					<option value="0" id="useshop" size="1">（選択しない）</option>
						<?php 
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option id='useshop' value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?>
		</th>
	</tr>
	<tr>
		<th class="a">利用日</th>
		<th colspan="3">
				<input type="radio" name="day" id="firstday" value="0" checked="checked" /><label for="firstday">初回</label>
				<input type="radio" name="day" id="lastday" value="1" /><label for="lastday">最終</label>
				<select id="year3">
				<option value="0" selected>----</option>
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month3">
				<option value="0" selected>--</option>
				<?php 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day3">
				<option value="0" selected>--</option>
				<?php 
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"><?php echo $d;?></option>
			<?php endfor?>
			</select>日 ～
			<select id="year4">
				<option value="0" selected>----</option>
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month4">
				<option value="0" selected>--</option>
				<?php 
				$month_now=date("m");
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day4">
				<option value="0" selected>--</option>
				<?php 
				$day_now=date("j");
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"><?php echo $d;?></option>
			<?php endfor?>
			</select>日
		</th>
	</tr>
	<tr>
		<th colspan="4" style="text-align:center;">
		<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
		<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search_guest(1);"></th>
	</tr>
</table>

<p>
	<div style="float:left; text-align:left;">
	<a href="customernew.php"><img src="../css/image/contents/btn1.gif"></a>
</div>
	<div style="text-align:right;">
<a href="#" onclick="search_customer_csv(1);"><img src="../css/image/contents/csv_btn.gif"></a>
	</div>
</p>

<p style="text-align:right;">
	会員数: <span id="num_guest"></span>人
</p>

<table id="table">
	<tr>
		<th class="a" style="text-align:center;">削除</th>
		<th class="a" style="text-align:center;">会員名</th>
		<th class="a" style="text-align:center;">都道府県</th>
		<th class="a" style="text-align:center;">DM</th>
		<th class="a" style="text-align:center;">チケット</th>
		<th class="a" style="text-align:center;">利用回数</th>
		<th class="a" style="text-align:center;">注文数</th>
		<th class="a" style="text-align:center;">注文金額</th>
		<th class="a" style="text-align:center;">退会</th>
	</tr>
</table>
<div id="span" align="center">
</div>
<p>
<p>*「会員名」をクリックすると会員情報を編集できます。

</div>
<?php include("footer.php"); ?>
