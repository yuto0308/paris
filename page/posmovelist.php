<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
function search_reg_move(num){
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	var shop = $('#useshop').val();
	var staffname = $('#staffname').val();
	var remarks = $('#remarks').val();
	var year_s = $('#year1').val();
	var month_s = $('#month1').val();
	var day_s = $('#day1').val();
	var year_e = $('#year2').val();
	var month_e = $('#month2').val();
	var day_e = $('#day2').val();
	$.ajax({type:"POST",
 			url:"search_reg_move.php",
 				data:{	num:num,
 						shop:shop,
 						staffname:staffname,
 						remarks:remarks,
 						year_s:year_s,
 						month_s:month_s,
 						day_s:day_s,
 						year_e:year_e,
 						month_e:month_e,
 						day_e:day_e
 						},
 						dataType:"json",
				success:function(data){
						$('#table').append(data[0]);
						$('#num_move').text(data[1]);
						$('#span').append(data[2]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown);
						}
						});
}
</script>
<div id="pagebodymain">
<h1>
	<div style="text-align:left; float:left;">レジ金移動の確認</div>
<div style="text-align:right"></h1>
<p>
	<table>
		<tr>
			<th class="b" colspan="4">検索条件</th>
		</tr>
		<tr>
			<th class="a">店舗</th>
			<th>
					<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
						include("db_connect.php");
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?></th>
			<th class="a">担当者名</th>
			<th>
				<input type="text" name="staffname" id="staffname" size="30" /></th>
					</tr>
		<tr>
			<th class="a">備考欄</th>
			<th colspan="3"><input type="text" name="remarks" id="remarks" size="50" /></th>
		<tr>
			<th class="a" >移動日</th>
			<th colspan="3">
				<select id="year1">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month1">
				<?php 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day1">
				<?php 
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"><?php echo $d;?></option>
			<?php endfor?>
			</select>日 ～
			<select id="year2">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"<?php if($y==$now){echo 'selected="selected"';}?>><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month2">
				<?php
				$now_month = date("m"); 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>" <?php if($m==$now_month){echo 'selected="selected"';}?>><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day2">
				<?php 
				$now_day = date("j");
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>" <?php if($d==$now_day){echo 'selected="selected"';}?>><?php echo $d;?></option>
			<?php endfor?>
			</select>日
		</th>
		<tr>
			<th colspan="4" style="text-align:center;">
		<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
		<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search_reg_move(1);"></th>
	</tr>
	</table>

<p>
<?php
$recordSet = mysqli_query($db, 'SELECT * FROM pos_move');
$count = mysqli_num_rows($recordSet);
?>

<p style="text-align:right;">移動数: <span id="num_move"><?php print("$count"); ?></span>

<P><table id="table">
	<tr class="a">
		<th>日時</th>
		<th>金額</th>
		<th>担当者</th>
		<th>店舗</th>
		<th>備考</th>
	</tr>
	<?php
	$str="";
	$recordset = mysqli_query($db, "SELECT DATE_FORMAT(datetime,'%Y年%m月%d日 %H:%i:%s') as datetime,price,name,shop_name,remarks FROM pos_move ORDER BY datetime DESC LIMIT 0,10");
	while ($table = mysqli_fetch_assoc($recordset)){
	$datetime=$table['datetime'];
	$price=number_format($table['price']);
	$name=$table['name'];
	$shop_name=$table['shop_name'];
	$remarks=$table['remarks'];
	$str.='<tr name="add">
			<th style="text-align:center;">'.$datetime.'</th>
			<th style="text-align:center;">'.$price.'</th>
			<th style="text-align:center;">'.$name.'</th>
			<th style="text-align:center;">'.$shop_name.'</th>
			<th style="text-align:center;"><textarea rows="2" cols="30" readonly>'.$remarks.'</textarea></th>
			</tr>';
	}
	if($str==""){
	$str.='<tr name="add">
			<th colspan="5" style="text-align:center;">対象データがありません</th>
			</tr>';
	}
	echo $str;
?>


	</table>
<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($count -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_reg_move('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_move('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_reg_move('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_move('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_move('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>

<p>
</div>
<?php include("footer.php"); ?>
