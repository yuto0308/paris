<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php
	include("db_connect.php");
    $post_id = $_GET['id'];
	$sql = sprintf("SELECT * FROM staff WHERE STFSEQ='$post_id'",mysqli_real_escape_string($db, $post_id));
	$recordSet = mysqli_query($db, $sql);
	$date = mysqli_fetch_assoc($recordSet);
?>
<script type="text/javascript">
/* ふりがなチェック */
function FuriganaCheck() {
   var str = document.staff_input.name_katakana.value;
   if( str.match( /[^ァ-ン　\s]+/ ) ) {
      alert("ふりがなは、全角カタカナのみで入力して下さい。");
      return 1;
   }
   return 0;
}

/* 半角数字チェック */
function NumberCheck() {
   var len = document.staff_input.id.value.length;
   if(4>len){
   	alert("スタッフ番号は4文字以上で入力してください。");
   	return 1;
   }

   return 0;
}

function staff_submit() {
	   var check = 0;
	   check += NumberCheck();
	   check += FuriganaCheck();
	   if( check > 0 ) {
      return false;
   }
      var flag = 0;
	if(document.staff_input.id.value == ""){ // スタッフ番号の入力をチェック以下同
		flag = 1;
	}
	else if(document.staff_input.name.value == ""){
		flag = 1;
	}
	else if(document.staff_input.name_katakana.value == ""){
		flag = 1;
	}
	else if(document.staff_input.abb_name.value == ""){
		flag = 1;
	}
	if(flag){
		window.alert('必須項目に未入力がありました');
		return false; // 送信を中止
}

    if(confirm("この内容で登録しますか") == true){
    var seq = $('#seq').val();
	var staff_id = $('#id').val();
	var staff_name = $('#name').val();
	var staff_name_kana = $('#name_katakana').val();
	var abb = $('#abb_name').val();
	var sex = $("input:radio[name='sex']:checked").val();
	var remarks = $('#remarks').val();
	var stop =$("#stop").val();
	$.post("staff_update_do.php?sid=<?php print($date['shop_id']);?>&id=<?php print($date['id']);?>",
		{seq:seq,id:staff_id,name:staff_name,name_katakana:staff_name_kana,abb_name:abb,sex:sex,remarks:remarks,stop:stop},function(data){
		if(data == 0){
			alert("IDの重複がないか確認してください。")
		}else{
			alert(data);
			document.location = "staff.php";}});
	//document.location = "staffnew.php";
}else{
	alert("キャンセルされました。");
}

}
       </script>

<div id="pagebodymain">
	<h1>
	<div style="text-align:left; float:left;">スタッフの編集</div>
	<div style="text-align:right;"><a href="staff.php" style="margin-right:10px;">スタッフの一覧へ</a></div>
</h1>
<!--コメント追加-->
<p>

	<form name="staff_input" action="#" method="post">
	<table>
		<tr>
			<th class="b" colspan="2">スタッフの情報</th>
		</tr>
		<tr>
			<th class="a">スタッフ番号</th>
			<th><input name="id" type="text" id="id" size="20" maxlength="20" value="<?php print(htmlspecialchars($date['id'], ENT_QUOTES)); ?>" onblur="NumberCheck();" />(半角4文字以上20文字まで)</th>
		<tr>
			<th class="a">スタッフ名</th>
			<th><input type="text" name="name" id="name" size="20" maxlength="20" value="<?php print(htmlspecialchars($date['name'], ENT_QUOTES)); ?>"  /></th>
		</tr>
		<tr>
			<th class="a">フリガナ</th>
			<th><input type="text" name="name_katakana" id="name_katakana" size="20" maxlength="20" value="<?php print(htmlspecialchars($date['name_katakana'], ENT_QUOTES)); ?>" onblur="FuriganaCheck();" /></th>
		</tr>
		<tr>
			<th class="a">略称</th>
			<th><input type="text" name="abb_name" id="abb_name" size="10" maxlength="10" value="<?php print(htmlspecialchars($date['abb_name'], ENT_QUOTES)); ?>"  /></th>
		</tr>
		<tr>
			<th class="a">性別</th>
			<th>
				<input type="radio" name="sex" id="sex_male" value="1" <?php if($date['sex'] == 1){print('checked="checked"');}?> /><label for="sex_male">男性</label>
				<input type="radio" name="sex" id="sex_female" value="0"  <?php if($date['sex'] == 0){print('checked="checked"');}?> /><label for="sex_female">女性</label>
			</th>
		</tr>
	</table>
	<P>
		<table>
			<tr>
				<th class="b" colspan="2">その他</th>
			</tr>
			<tr>
				<th class="a" width="135">備考欄</th>
				<th><textarea name="remarks" id="remarks" rows="5" cols="60" ><?php print(htmlspecialchars($date['remarks'], ENT_QUOTES)); ?></textarea></th>
			</tr>
			<tr>
				<th class="a">一時停止</th>
				<th><input type="checkbox" name="stop" id="stop" onclick="javascript:this.value = (this.checked)?'1':'0';" />一時的にスタッフを停止する</th>
				<input type="hidden" id="seq" value="<?php print(htmlspecialchars($date['STFSEQ'],ENT_QUOTES)); ?>" />
			</table>
		</form>
	<p style="text-align:center;">
<input type="image" src="../css/image/contents/view.gif" onclick="return staff_submit();"/>


</div>
<?php include("footer.php"); ?>