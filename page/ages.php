<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
function search(){
	var year = $('#year').val();
	var month = $('#month').val();
	$('*[name="add"]').remove();
	$.ajax({type:"POST",
		url:"search_ages.php",
			data:{
				year:year,
				month:month
			},
		dataType:"json",
		success:function(data){
//			$('#show1').append(data);
			$('#show1').append(data[0]);
			$('#show2').append(data[1]);
		},
		error:	function(XMLHttpRequest, textStatus, errorthrown){
			alert('error : ' + errorthrown + XMLHttpRequest.status + textStatus);
		}
	});
}

$(document).ready(search);
</script>
<div id="pagebodymain">
	<h1>年代別売上の集計</h1>
	<table>
		<tr>
			<th class="b" colspan="2">検索条件</th>
		</tr>
		<tr>
			<th class="a" style="text-align:center;">集計期間</th>
			<th colspan="3" style="text-align:center;">
				<select id="year" >
					<?php
						$year_s = date("Y");
						for($i=2010;$i<=$year_s;$i++){
						if($i == $year_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>年
				<select id="month">
					<?php
						$month_s = date("n");
						for($i=1;$i<=12;$i++){
						if($i == $month_s){
							echo '<option selected value="'.$i.'">'.$i;
						}else{
							echo '<option value="'.$i.'">'.$i;
						}	
						}
					?>
				</select>月
			</th>
		</tr>
		<tr>
			<th colspan="2" style="text-align:center;">
				<input type="image" src="../css/contents_img/search.gif" alt="この条件で検索" onclick="search();" />
			</th>
		</tr>
	</table>
	<p>
	<table id="show1">
		<tr>
			<th class="b" colspan="6">総合人数</th>
		</tr>
		<tr>
			<th class="a" style="text-align:center; width:16%;">20代</th>
			<th class="a" style="text-align:center; width:16%;">30代</th>
			<th class="a" style="text-align:center; width:16%;">40代</th>
			<th class="a" style="text-align:center; width:16%;">50代</th>
			<th class="a" style="text-align:center; width:16%;">60代</th>
			<th class="a" style="text-align:center;">70代以上</th>
		</tr>
	</table>
	<p>
	<table id="show2">
		<tr>
			<th class="b" colspan="5">年代別売上</th>
		</tr>
		<tr>
			<th class="a" style="text-align:center;">年代</th>
			<th class="a" style="text-align:center;">来店数</th>
			<th class="a" style="text-align:center;">購入金額(税抜)</th>
			<th class="a" style="text-align:center;">購入商品</th>
			<th class="a" style="text-align:center;">購入金額(税抜)</th>
		</tr>
	</table>
	<div style="height:100px;"></div>
</div>
<?php include("footer.php"); ?>
