<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<script>
$(function(){
	setInterval(function(){
			location.reload();
	},300000);//５分ごとにリロード
});
</script>
<script>
	function search_shop(){
		$("tr[name='shopname']").remove();
		$("tr[name='shopdata']").remove();
		$("tr[name='shopreg']").remove();
		$("tr[name='repeat1']").remove();
		$("tr[name='repeat2']").remove();
		var search_shop_id = $('#useshop').val();
		var year_s = $('#year_s').val();
		var month_s = $('#month_s').val();
		var day_s = $('#day_s').val();
		var year_e = $('#year_e').val();
		var month_e = $('#month_e').val();
		var day_e = $('#day_e').val();
	$.ajax({
		type:"POST",
		url:"search_index.php",
		data:{
			search_shop_id:search_shop_id,
			year_s:year_s,
			month_s:month_s,
			day_s:day_s,
			year_e:year_e,
			month_e:month_e,
			day_e:day_e
			},
		dataType:"json",
		success:function(data){
						$('#table').prepend(data[0]);
						$('#table').append(data[1]);
						$('#regdata').append(data[2]);
						$('#repeat').append(data[3]);
						$('#repeat').append(data[4]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}
</script>


<div id="pagebodymain">

<h1>POSレジ業務画面トップページ</h1></p>
<?php include("db_connect.php"); ?>
<p>
<table style="width:99%;">
	<tr style="height:30px;">
		<th colspan="7" class="b">全店舗の売上集計</th>
	</tr>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;"></th>
		<th class="a" style="text-align:center;">販売件数</th>
		<th class="a" style="text-align:center;">販売商品数</th>
		<th class="a" style="text-align:center;">販売金額</th>
		<th class="a" style="text-align:center;">返品商品数</th>
		<th class="a" style="text-align:center;">返品金額</th>
		<th class="a" style="text-align:center;">売上金額</th>
	</tr>
	<?php
	$basic = date('Y-m-d');
	$lastday = date('Y-m-d',strtotime('-1 day'));
	$this_m = date('Y-m');
	$last_m = date('Y-m',strtotime(date("Y-m-01") .'-1 month')); 
	$group = ' GROUP BY DATE_FORMAT(date,"%Y-%m-%d") ';
	$group_month = ' GROUP BY DATE_FORMAT(date,"%Y-%m") ';

	$sql = ' SELECT COUNT(count) as count_item, SUM(count) as sum_item, SUM(sum) as sum, ';
	$sql.= ' SUM(cash) as cash,SUM(card) as card, ';
	$sql.= ' SUM(count_payback) as count_payback, SUM(payback) as sum_payback, SUM(ticket) as ticket,date ';
	$sql_shop = $sql;
	$sql.= ' FROM shop_log_sub ';

	$sql_today = $sql;
	$sql_today.= ' WHERE DATE_FORMAT(date,"%Y-%m-%d")="'.$basic.'" '.$group;
	$rs_today = mysqli_query($db, $sql_today);
	$today = mysqli_fetch_assoc($rs_today);

	$sql_yesterday = $sql;
	$sql_yesterday.= ' WHERE DATE_FORMAT(date,"%Y-%m-%d")="'.$lastday.'" '.$group;
	$rs_yesterday = mysqli_query($db,$sql_yesterday);
	$yesterday = mysqli_fetch_assoc($rs_yesterday);

	$sql_month = $sql;
	$sql_month.= ' WHERE DATE_FORMAT(date,"%Y-%m")="'.$this_m.'" '.$group_month;
	$rs_month = mysqli_query($db,$sql_month);
	$month = mysqli_fetch_assoc($rs_month);
	
	$sql_lastm  = $sql;
	$sql_lastm .= ' WHERE DATE_FORMAT(date,"%Y-%m")="'.$last_m.'" '.$group_month;
	$rs_lastm = mysqli_query($db, $sql_lastm);
	$lastm = mysqli_fetch_assoc($rs_lastm);
	?>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;">本日</th>
		<?php
		if($today['count_item']==""){
		$count_item1="0";
		}else{
		$count_item1=number_format($today['count_item']);
		}

		if($today['sum_item']==""){
		$sum_item1="0";
		}else{
		$sum_item1=number_format($today['sum_item']);
		}

		if($today['sum']==""){
		$sum1="0";
		$non_ticket1="0";
		}else{
		// $sum1=$today['sum'];
		//$sum1=$today['cash']+$today['card'];
		$sum1 = $today['sum'];
		$non_ticket1=$sum1 - ($today['ticket'] * 1080);
		$sum1=number_format($sum1);
		$non_ticket1=number_format($non_ticket1);
		}

		if($today['count_payback']==""){
		$count_payback1="0";
		}else{
		$count_payback1=number_format($today['count_payback']);
		}

		if($today['sum_payback']==""){
		$sum_payback1="0";
		}else{
		$sum_payback1=$today['sum_payback'];
		$sum_payback1=number_format($sum_payback1);
		}
		?>
		<th style="text-align:right;"><?php echo $count_item1 ;?>件</th>
		<th style="text-align:right;"><?php echo $sum_item1 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum1 ;?>円</th>
		<th style="text-align:right;"><?php echo $count_payback1 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum_payback1 ;?>円</th>
		<th style="text-align:right;"><?php echo $non_ticket1 ;?>円</th>

	</tr>
	<tr style="height:30px;">
		<?php
		if($yesterday['count_item']==""){
		$count_item2="0";
		}else{
		$count_item2=number_format($yesterday['count_item']);
		}

		if($yesterday['sum_item']==""){
		$sum_item2="0";
		}else{
		$sum_item2=number_format($yesterday['sum_item']);
		}

		if($yesterday['sum']==""){
		$sum2="0";
		$non_ticket2="0";
		}else{
		//$sum2=$yesterday['cash']+$yesterday['card'];
		//$non_ticket2=$yesterday['cash']+$yesterday['card'];
		$sum2 = $yesterday['sum'];
		$non_ticket2 = $yesterday['sum'] - ($yesterday['ticket'] * 1080);
		$sum2=number_format($sum2);
		$non_ticket2=number_format($non_ticket2);
		}

		if($yesterday['count_payback']==""){
		$count_payback2="0";
		}else{
		$count_payback2=number_format($yesterday['count_payback']);
		}

		if($yesterday['sum_payback']==""){
		$sum_payback2="0";
		}else{
		$sum_payback2=$yesterday['sum_payback'];
		$sum_payback2=number_format($sum_payback2);
		}
		?>
		<th class="a" style="text-align:center;">昨日</th>
		<th style="text-align:right;"><?php echo $count_item2 ;?>件</th>
		<th style="text-align:right;"><?php echo $sum_item2 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum2 ;?>円</th>
		<th style="text-align:right;"><?php echo $count_payback2 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum_payback2 ;?>円</th>
		<th style="text-align:right;"><?php echo $non_ticket2 ;?>円</th>
	</tr>
		<tr style="height:30px;">
		<th class="a" style="text-align:center;">今月</th>
		<?php
		if($month['count_item']==""){
		$count_item3="0";
		}else{
		$count_item3=number_format($month['count_item']);
		}

		if($month['sum_item']==""){
		$sum_item3="0";
		}else{
		$sum_item3=number_format($month['sum_item']);
		}

		if($month['sum']==""){
		$sum3="0";
		$non_ticket3="0";
		}else{
		//$sum3=$month['cash']+$month['card'];
		//$non_ticket3=$month['card']+$month['cash'];
		$sum3= $month['sum'];
		$non_ticket3 = $month['sum'] - ($month['ticket'] * 1080);
		$sum3=number_format($sum3);
		$non_ticket3=number_format($non_ticket3);
		}

		if($month['count_payback']==""){
		$count_payback3="0";
		}else{
		$count_payback3=number_format($month['count_payback']);
		}

		if($month['sum_payback']==""){
		$sum_payback3="0";
		}else{
		$sum_payback3=$month['sum_payback'];
		$sum_payback3=number_format($sum_payback3);
		}
		?>
		<th style="text-align:right;"><?php echo $count_item3 ;?>件</th>
		<th style="text-align:right;"><?php echo $sum_item3 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum3 ;?>円</th>
		<th style="text-align:right;"><?php echo $count_payback3 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum_payback3 ;?>円</th>
		<th style="text-align:right;"><?php echo $non_ticket3 ;?>円</th>
	</tr>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;">先月</th>
		<?php
		if($lastm['count_item']==""){
		$count_item4="0";
		}else{
		$count_item4=number_format($lastm['count_item']);
		}

		if($lastm['sum_item']==""){
		$sum_item4="0";
		}else{
		$sum_item4=number_format($lastm['sum_item']);
		}

		if($lastm['sum']==""){
		$sum4="0";
		$non_ticket4="0";
		}else{
		//$sum4=$lastm['cash']+$lastm['card'];
		//$non_ticket4=$lastm['card']+$lastm['cash'];
		$sum4 = $lastm['sum'];
		$non_ticket4 = $lastm['sum'] - ($lastm['ticket'] * 1080);
		$sum4=number_format($sum4);
		$non_ticket4=number_format($non_ticket4);
		}

		if($lastm['count_payback']==""){
		$count_payback4="0";
		}else{
		$count_payback4=number_format($lastm['count_payback']);
		}

		if($lastm['sum_payback']==""){
		$sum_payback4="0";
		}else{
		$sum_payback4=$lastm['sum_payback'];
		$sum_payback4=number_format($sum_payback4);
		}
		?>
		<th style="text-align:right;"><?php echo $count_item4 ;?>件</th>
		<th style="text-align:right;"><?php echo $sum_item4 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum4 ;?>円</th>
		<th style="text-align:right;"><?php echo $count_payback4 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum_payback4 ;?>円</th>
		<th style="text-align:right;"><?php echo $non_ticket4 ;?>円</th>
	</tr>
</table>
<p>
<table style="width:99%;">
	<tr style="height:30px;">
		<th class="b" style="width:20%; text-align:center;">集計店舗</th>
		<th>
			<select id="useshop" onchange="search_shop();">
				<?php 
				include("db_connect.php");
				$sql_shopsel = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
					$recordSet = mysqli_query($db, $sql_shopsel);
					while($arr_item = mysqli_fetch_assoc($recordSet))
					{
					echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
					}
					$min_id = $arr_item['min_id'];
				?></th>
	</tr>
	<?php
	$sql_min = ' SELECT MIN(shop_id) as min_id FROM members WHERE shop_id>0';
	$rs_min = mysqli_query($db, $sql_min);
	$min_result = mysqli_fetch_assoc($rs_min);
	$min_id = $min_result['min_id'];

	$sql_shop.= ' FROM shop_log_sub ';
	$sql_shop.= ' WHERE ';
	$sql_shop.= ' shop_id='.$min_id.' AND ';
	$sql_shop.= ' DATE_FORMAT(date,"%Y-%m-%d")="'.$basic.'" '.$group;
	$rs_shop = mysqli_query($db, $sql_shop);
	$shopdata = mysqli_fetch_assoc($rs_shop);

		$sql_nm=' SELECT name FROM members WHERE shop_id='.$min_id;
		$rs_nm=mysqli_query($db,$sql_nm);
		$shpNM=mysqli_fetch_assoc($rs_nm);
		$SHPNM=$shpNM['name'];
	if($shopdata['count_item']==""){
	$count_item5="0";
	}else{
	$count_item5=number_format($shopdata['count_item']);
	}

	if($shopdata['sum_item']==""){
	$sum_item5="0";
	}else{
	$sum_item5=number_format($shopdata['sum_item']);
	}

	if($shopdata['sum']==""){
	$sum5="0";
	$non_ticket5="0";
	}else{
	//$sum5=$shopdata['card']+$shopdata['cash'];
	//$non_ticket5=$shopdata['card']+$shopdata['cash'];
	$sum5 = $shopdata['sum'];
	$non_ticket5 = $shopdata['sum'] - ($shopdata['ticket'] * 1080);
	$sum5=number_format($sum5);
	$non_ticket5=number_format($non_ticket5);
	}

	if($shopdata['count_payback']==""){
	$count_payback5="0";
	}else{
	$count_payback5=number_format($shopdata['count_payback']);
	}

	if($shopdata['sum_payback']==""){
	$sum_payback5="0";
	}else{
	$sum_payback5=$shopdata['sum_payback'];
	$sum_payback5=number_format($sum_payback5);
	}
	?>
</table>
<p>
<table style="width:99%;" id="table">
	<tr style="height:30px;" name="shopname">
		<th colspan="7" class="b"><?php echo $SHPNM; ?>の売上集計[本日]</th>
	</tr>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;"></th>
		<th class="a" style="text-align:center;">販売件数</th>
		<th class="a" style="text-align:center;">販売商品数</th>
		<th class="a" style="text-align:center;">販売金額</th>
		<th class="a" style="text-align:center;">返品商品数</th>
		<th class="a" style="text-align:center;">返品金額</th>
		<th class="a" style="text-align:center;">売上金額</th>
	</tr>
	<tr style="height:30px;" name="shopdata">
		<th class="a" style="text-align:center;"><?php echo $SHPNM; ?></th>
		<th style="text-align:right;"><?php echo $count_item5 ;?>件</th>
		<th style="text-align:right;"><?php echo $sum_item5 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum5 ;?>円</th>
		<th style="text-align:right;"><?php echo $count_payback5 ;?>点</th>
		<th style="text-align:right;"><?php echo $sum_payback5 ;?>円</th>
		<th style="text-align:right;"><?php echo $non_ticket5 ;?>円</th>
	</tr>
</table>
<p>
<table style="width:99%;" id="regdata">
	<?php
	$sql_start = ' SELECT *,DATE_FORMAT(start_time,"%Y-%m-%d") as start_time ';
	$sql_start.= ' FROM pos_start WHERE shop_id='.$min_id;
	$sql_start.= ' ORDER BY start_time DESC LIMIT 1 ';
	$rs_start = mysqli_query($db, $sql_start);
	$start = mysqli_fetch_assoc($rs_start);

	$sql_finish = ' SELECT *, DATE_FORMAT(datetime,"%Y-%m-%d") as datetime ';
	$sql_finish.= ' FROM pos_finish WHERE shop_id='.$min_id;
	$sql_finish.= ' ORDER BY datetime DESC LIMIT 1 ';
	$rs_finish = mysqli_query($db, $sql_finish);
	$finish = mysqli_fetch_assoc($rs_finish);

	$sql_members = ' SELECT * FROM members WHERE shop_id='.$min_id;
	$rs_members = mysqli_query($db, $sql_members);
	$members = mysqli_fetch_assoc($rs_members);
	?>
	<tr style="height:30px;">
		<th colspan="3" class="b">レジの状況</th>
	</tr>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;">店舗名</th>
		<th class="a" style="text-align:center;">開始レジ金の設定</th>
		<th class="a" style="text-align:center;">レジの締め処理</th>
	</tr>
	<tr style="height:30px;" name="shopreg">
		<th class="a" style="text-align:center; width:20%;"><?php echo $members['name'];?></th>
		<th style="text-align:center; width:40%;">
		<?php
		if($basic==$start['start_time']){
			echo "設定は完了しています。";
		}
		else if($basic!=$start['start_time'] && $members['start']<=date('G')){
			echo "<font color='red'>開始レジ金の設定が行われていません。</font>";
		}else if($basic!=$start['start_time'] && $members['start']>=date('G')){
			echo "";
		}
		?>
		</th>
		<th style="text-align:center; width:40%;">
		<?php
		if($basic==$finish['datetime']){
			echo "設定は完了しています。";
		}
		else if($basic!=$finish['datetime'] && $members['end']<=date('G')){
			echo "<font color='red'>レジの締め処理が行われていません。</font>";
		}else if($basic!=$finish['datetime'] && $members['end']<=date('G')){
			echo "";
		}
		?>
		</th>
	</table>
	<p>
	<table style="width:99%;" id="repeat">
	<tr style="height:30px;">
		<th colspan="5" class="b">顧客リピート率</th>
	</tr>
	<tr style="height:30px;">
		<th class="a" style="text-align:center;" rowspan="2">集計期間</th>
			<th style="text-align:center;" colspan="4">
			<select id="year_s">
			<?php
				$year_s=date("Y");
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_s">
			<?php
				$month_s=date("m");
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月
			<select id="day_s">
				<?php
				$day_s=date("d");
				for($i=1;$i<=31;$i++){
				if($i==1){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
				?>
			</select>
			日　〜　
			<select id="year_e">
			<?php
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_e">
			<?php
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月
			<select id="day_e">
				<?php
				$day_s=date("d");
				for($i=1;$i<=31;$i++){
				if($i==$day_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
				?>
			</select>
			日
			</th>
	</tr>
	<tr>
			<th style="text-align:center;" colspan="4">
				<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search_shop();">
			</th>
		</tr>
	<?php
	$start=date("Y").'-'.date("m").'-01 00:00:00';
	$end=date("Y").'-'.date("m").'-'.date("d").' 23:59:59';
	$sql_regcount=' SELECT * FROM shop_log_sub WHERE date>="'.$start.'" AND date<="'.$end.'" AND shop_id='.$min_id;
	$count_query=mysqli_query($db,$sql_regcount);
	$regcount=mysqli_num_rows($count_query);
	$sql3=' SELECT * FROM shop_log_sub WHERE date>="'.$start.'" AND date<="'.$end.'" AND shop_id='.$min_id.' group by guest_id having COUNT(guest_id) > 1 ';
	$rs=mysqli_query($db,$sql3);
	$data3=mysqli_num_rows($rs);
	$sql2=' SELECT COUNT(DISTINCT guest_id) as ninzu FROM shop_log_sub WHERE date>="'.$start.'" AND date<="'.$end.'" AND shop_id='.$min_id;
	$query2=mysqli_query($db,$sql2);
	$data2=mysqli_fetch_assoc($query2);


	$bunbo=$data2['ninzu'];
	if($bunbo==""){
		$bunbo=0;
	}
	if($bunbo==0 || $bunbo==""){
		$re='0';
	}else{
	$re=$bunshi/$bunbo*100;
	$re=round($re,2);
	}
	?>
	<tr name="repeat1">
		<th class="a" style="text-align:center; width:20%;" rowspan="2"><?php echo $members['name'];?></th>
		<th style="text-align:center;" class="a">レジ件数</th>
		<th style="text-align:center;" class="a">実質来店者数</th>
		<th style="text-align:center;" class="a">リピート顧客数</th>
		<th style="text-align:center;" class="a">リピート率</th>
	</tr>
	<tr name="repeat2">
		<th style="text-align:center;width:15%;"><?php echo $regcount.'件'; ?></th>
		<th style="text-align:center;width:15%;"><?php echo $data2['ninzu'].'人'; ?></th>
		<th style="text-align:center;width:15%;"><?php echo $data3.'人'; ?></th>
		<th style="text-align:center;width:15%;"><?php echo $re.'%'; ?></th>
	</tr>
	</table>
	<p>
</div>
<?php include("footer.php"); ?>
