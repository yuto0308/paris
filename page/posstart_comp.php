<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<div id="pagebodymain">
<h1>開始レジ金の設定</h1>
	<?php
		include("db_connect.php");
		$sql = "SELECT DATE_FORMAT(start_time,'%Y-%m-%d') as stime FROM pos_start WHERE shop_id='".$shop_id."' ORDER BY stime DESC";
		$recordset = mysqli_query($db, $sql);
		$time = mysqli_fetch_assoc($recordset);
		if($time['stime'] != date("Y-m-d")){
			header('location: posstart.php');
		}
	?>
<p>
<font color="red">「開始レジ金の設定」は完了しています。</font>
		<?php
		$sd = "SELECT * ,DATE_FORMAT(start_time,'%Y年%m月%d日 %H:%i:%s') as start_time FROM pos_start WHERE shop_id='".$shop_id."' ORDER BY start_time DESC LIMIT 1";
		$recordSet = mysqli_query($db, $sd);
		$data = mysqli_fetch_assoc($recordSet);
	?>
<table>
	<tr>
		<th class="b" colspan="4">基本情報</th>
	</tr>
	<tr>
		<th class="a">店舗</th>
		<th><?php echo $_SESSION['name']; ?></th>
		<th class="a">設定日時</th>
		<th><?php echo $data['start_time']; ?></th>
	</tr>
	<tr>
		<th class="a">スタッフ番号</th>
		<th><?php echo $data['id']; ?>
		</th>
		<th class="a">担当者名</th>
		<th><?php echo $data['name']; ?></th>
	</table>
	<p>
	<table id="posm_row">
	<tr>
		<th class="b" colspan="8">開始レジ金</th>
	</tr>
	<tr>
		<th class="a">一万円</th>
		<th><?php echo $data['ten_th']; ?>枚</th>
		<th class="a">五千円</th>
		<th><?php echo $data['five_th']; ?>枚</th>
		<th class="a" >二千円</th>
		<th><?php echo $data['two_th']; ?>枚</th>
		<th class="a">千円</th>
		<th><?php echo $data['one_th']; ?>枚</th>
	</tr>
	<tr>
		<th class="a">五百円</th>
		<th><?php echo $data['five_hun']; ?>枚</th>
		<th class="a">百円</th>
		<th><?php echo $data['one_hun']; ?>枚</th>
		<th class="a">五十円</th>
		<th><?php echo $data['fifty']; ?>枚</th>
		<th class="a">十円</th>
		<th><?php echo $data['ten']; ?>枚</th>
	</tr>
	<tr>
		<th class="a">五円</th>
		<th><?php echo $data['five']; ?>枚</th>
		<th class="a">一円</th>
		<th><?php echo $data['one']; ?>枚</th>
		<th colspan="4"><div style="text-align:left; float:left; margin-top:5px;">合計金額: </div><div style="text-align:right;"><span id="sum" style="font-size:18px; font-weight:bold;"><?php echo $data['sum']; ?></span> 円</div></th>
	</table>
<p>
</div>
<?php include("footer.php"); ?>
