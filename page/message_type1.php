<script>
	$(function(){
		$('#send').click(function(){
			if(confirm("この内容で登録しますか。") == true){
				var contents = $('#rep').val();
				var seq = '<?php echo $seq; ?>';
				var num = $('#num').val();
				if(contents == ''){
					alert('本文がが空です。');
					return;
				}
				var fd = new FormData($('#info').get(0));
				$.ajax({
					url: "upload_file.php",
					type: "POST",
					data: fd,
					processData: false,
					contentType: false
				})
				.done(function( data ) {
					if(data == 'error'){
						alert('ファイル登録失敗');
					}else{
						var img_seq = data;
						if(img_seq == 0){
							img_seq = "";
						}
						$.ajax({
							url : 'message_rep0.php',
							type : 'POST',
							data : {contents:contents,
									seq:seq,
									num:num,
									img_seq:img_seq}
						})
						.done(function(data){
						if(data == 0){
							alert("エラー もう一度登録してください。");
							return;
						}else{
							alert(data);
							document.location = "message_list.php";
						}
						});
					}
				});
				return false;
			}else{
				alert("キャンセルされました。");
			}
		});
	});

</script>

<?php
$sql = ' SELECT * FROM info WHERE SEQ ='.$seq;
$que = mysqli_query($db_info,$sql);
$rs = mysqli_fetch_assoc($que);

$img = $rs['img_seq'];
$img = explode(",",$img);
$img = count($img) - 1;
$img = $img.'件';
if($img == '0件'){
	$img_text = '添付ファイルはありません。';
}else{
	$img_text = '<a href="file_get.php?img_seq='.$rs['img_seq'].'">'.$img.'の添付ファイルがあります。</a>';
}
?>
<table>
	<tr>
		<th class="b" colspan="2">通知内容</th>
	</tr>
	<tr>
		<th class="a" style="width:40px;">タイトル</th>
		<th><?php echo '<input type="text" id="title" size="79" value="'.$rs['title'].'" readonly>'?></th>
	</tr>
	<tr>
		<th class="a" style="height:20px;">添付</th>
		<th>
			<?php
				echo $img_text;
			?>
		</th>
	</tr>
	<tr>
		<th class="a">本文</th>
		<th>
			<?php
				echo '<textarea id="data" name="data" rows="20" cols="80" readonly >'.$rs['data'].'</textarea>';
			?>
		</th>
	</tr>
</table>
<p>
	<?php
	if($reply == 0){
		$text =  '
			<tr>
				<form id="info" enctype="multipart/form-data">
				<th class="a" style="width:40px;">添付</th>
				<th>
					<input type="file" id="file" name="image_file[]" multiple="multiple">
					※Ctrlを押しながらで複数ファイルを選択できます。
				</th>
			</tr>
			<tr>
				<th class="a">本文</th>
				<th>
					<textarea id="rep" rows="20" cols="80"></textarea>
				</th>
			</tr>
			</table>
			<p style="text-align:center;">
			<input type="hidden" id="num" value="0">
			<input type="image" id="send" src="../css/contents_img/view.gif" onclick="post_shop();" alt="上記の内容で登録"/>
			<form id="info" enctype="multipart/form-data">';
	}else{
		$rep_sql = ' SELECT * FROM reply WHERE seq ='.$seq.' AND id="'.$_SESSION['id'].'" ';
		$rep_que = mysqli_query($db_info,$rep_sql);
		$rep_rs = mysqli_fetch_assoc($rep_que);
		$img = $rep_rs['img_seq'];
		if($img == ''){
			$img_text = '<th>
							<input type="file" id="file" name="image_file[]" multiple="multiple">
							※Ctrlを押しながらで複数ファイルを選択できます。
						</th>';
		}else{
			$img = explode(",", $img);
			$img = count($img) - 1;

			$img_text = '<th>
							<input type="file" id="file" name="image_file[]" multiple="multiple">
							※Ctrlを押しながらで複数ファイルを選択できます。
							(現在:'.$img.'件)
						</th>';
		}
		$text = '
			<tr>
				<form id="info" enctype="multipart/form-data">
				<th class="a" style="width:40px;">添付</th>
				'.$img_text.'
			</tr>
			<tr>
				<th class="a">本文</th>
				<th style="text-align:center;">
					<textarea  id="rep" rows="20" cols="80">'.$rep_rs['contents'].'</textarea>
				</th>
			</tr>
			</table>
			<p style="text-align:center;">
			<input type="hidden" id="num" value="1">
			<input type="image" id="send" src="../css/contents_img/view.gif" onclick="post_shop();" alt="上記の内容で登録"/>
			</table>';
	}
	?>
<table>
	<tr>
		<th class="b" colspan="2">返信</th>
	</tr>
	<?php echo $text; ?>
<div style="height:50px;"></div>