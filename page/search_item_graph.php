<!DOCTYPE html>
<html lang="ja">
<meta name="robots" content="noindex">
<head>
<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<title>売上内訳グラフ</title> 
<script language="javascript" type="text/javascript" src="jquery-2.1.1.js"></script>
<!--[if lt IE 9]>
    <script language="javascript" type="text/javascript" src="excanvas.min.js"></script>
<![endif]-->
<script language="javascript" type="text/javascript" src="jquery.jqplot.min.js"></script>
<script language="javascript" type="text/javascript" src="plugins/jqplot.pieRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.jqplot.min.css" />

<script>
jQuery( function() {
    jQuery . jqplot(
        'jqPlot-sample',
        [
         [ 
         <?php
         $nothing = 0;
		session_start();
		include("db_connect.php");
		$str="";
		

		
		$num=$_GET['num'];
		$useshop = $shop_id;

		$goods_id=$_GET['goods_id'];
		if($goods_id==""){
			$goods_id_str="1";
		}else{
			$goods_id_str=" goods_id = $goods_id ";
		}
		$goods_name=$_GET['goods_name'];
		if($goods_name==""){
			$goods_name_str="1";
		}else{
			$goods_name_str=" goods_name LIKE '%$goods_name%' ";
		}
		
		$cat_big=$_GET['cat_big'];//大カテゴリ
		if($cat_big == 0){
			$cat_big_str = "1";
			$group_by_str = "goods.category_big_str";
			$order_by_str = "goods.category_big";
		}else{
			$cat_big_str = "category_big = $cat_big ";
			$group_by_str = "goods.category_mid_str";
			$order_by_str = "goods.category_mid";
		}
		$cat_mid=$_GET['cat_mid'];//中カテゴリ
		if($cat_mid == 0){
			$cat_mid_str = "1";
		}else{
			$cat_mid_str = "category_mid = $cat_mid ";
			$group_by_str = "goods.name";
			$order_by_str = "goods.id";
		}
		$cat_sma=$_GET['cat_sma'];//小カテゴリ
		if($cat_sma == 0){
			$cat_sma_str = "1";
		}else{
			$cat_sma_str = "category_sma = $cat_sma ";
		}
		
		$day_or_mon=$_GET['day_or_mon'];//日別は０、月別は１

		
		if($day_or_mon == 1){//月別
			
		$year_s=$_GET['year_s'];
		$month_s=$_GET['month_s'];
		if($month_s < 10){
			$month_s = "0".$month_s;
		}
		
		$day_s_str = $year_s . "-" . $month_s . "-01 00:00:00";
		
		$year_e=$_GET['year_e'];
		$month_e=$_GET['month_e'];
		if($month_e < 10){
			$month_e = "0".$month_e;
		}
		$day_e_str = $year_e . "-" . $month_e . "-31 23:59:59";
		
		$sql_search = " SELECT  SUM(shop_log.price) as sum, ".$group_by_str." as group_name ";
		$sql_search .= " FROM shop_log,goods,members WHERE shop_log.shop_id=members.shop_id ";
		$sql_search .= " AND shop_log.goods_id = goods.id ";
		$sql_search .= " AND $useshop ";
		$sql_search .= " AND $goods_id_str AND $goods_name_str ";
		$sql_search .= " AND day >= '$day_s_str' AND day <= '$day_e_str' ";
		$sql_search .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
		$sql_search .= " GROUP BY $group_by_str ORDER BY $group_by_str  DESC";

		$query_search = mysqli_query($db,$sql_search);
		if(!$query_search){
			$nothing = 1;
			goto nothing;
						}
		if(mysqli_num_rows($query_search) == 0){
			$nothing = 1;
			goto nothing;
		}else{
		$count_loop = 0;
		while($arr_search = mysqli_fetch_assoc($query_search)){
			if($count_loop == 0){
			$str .= "['".$arr_search['group_name']."', ".$arr_search['sum']."]";
			$count_loop++;
		}else{
			$str .= ",['".$arr_search['group_name']."', ".$arr_search['sum']."]";
			$count_loop++;
		}
		}
		
		
}


		
		echo $str;
	
	
	
	
	
	
	
	
		}else{//日別
		
		
		
		
		
			
		$year_s=$_GET['year_s'];
		$month_s=$_GET['month_s'];
		if($month_s < 10){
			$month_s = "0".$month_s;
		}
		$day_s=$_GET['day_s'];
		if($day_s < 10){
			$day_s = "0".$day_s;
		}
		
		$day_s_str = $year_s . "-" . $month_s . "-" . $day_s;
		
		$year_e=$_GET['year_e'];
		$month_e=$_GET['month_e'];
		if($month_e < 10){
			$month_e = "0".$month_e;
		}
		$day_e=$_GET['day_e'];
		$day_e=$day_e + 1;
		if($day_e < 10){
			$day_e = "0".$day_e;
		}

		$day_e_str = $year_e . "-" . $month_e . "-" . $day_e;
		
		$sql_search = " SELECT SUM(shop_log.price) as sum, ".$group_by_str." as group_name ";
		$sql_search .= " FROM shop_log,goods,members WHERE shop_log.shop_id = members.shop_id ";
		$sql_search .= " AND $goods_id_str AND $goods_name_str ";
		$sql_search .= " AND $useshop ";
		$sql_search .= " AND shop_log.goods_id = goods.id ";
		$sql_search .= " AND day >= '$day_s_str' AND day <= '$day_e_str' ";
		$sql_search .= " AND  $cat_big_str AND $cat_mid_str AND $cat_sma_str ";
		$sql_search .= " GROUP BY $group_by_str ORDER BY $group_by_str DESC";
		
		$query_search = mysqli_query($db,$sql_search);
		if(!$query_search){
			$nothing = 1;
			goto nothing;
						}
		if(mysqli_num_rows($query_search) == 0){
			$nothing = 1;
			goto nothing;
		}else{
		$count_loop = 0;
		while($arr_search = mysqli_fetch_assoc($query_search)){
			if($count_loop == 0){
			$str .= "['".$arr_search['group_name']."', ".$arr_search['sum']."]";
			$count_loop++;
		}else{
			$str .= ",['".$arr_search['group_name']."', ".$arr_search['sum']."]";
			$count_loop++;
		}
		}
		

}

		echo $str;
	
	
		}
		nothing:
	?>

         ]


        ],
        {
            seriesDefaults: {
                renderer: jQuery . jqplot . PieRenderer,
                rendererOptions: {
                    padding: 15,
                    fill:true,
                    lineWidth:1,
                    dataLabels: 'percent',
                    showDataLabels: true,
                    startAngle: -90
                }        
            },
            legend: {
                show: true,
                location: 's',
                rendererOptions: {
                    numberRows: 1
                },
            }
           }
    );
} );
</script>
</head>
<body>
 <div style="text-align:center;">
 <?php
 	if($nothing == 1){
	 	echo "対象データがありません";
	 	return;
 	}
 
 ?>
 </div>
<div style="text-align:center;"><?php
if($_GET['day_or_mon'] == 0){
	echo $_GET['year_s']."年".$_GET['month_s']."月".$_GET['day_s']."日 ~ ".$_GET['year_e']."年".$_GET['month_e']."月".$_GET['day_e']."日までのカテゴリ別売上グラフ";
}else{
	echo $_GET['year_s']."年".$_GET['month_s']."月 ~ ".$_GET['year_e']."年".$_GET['month_e']."月までのカテゴリ別売上グラフ";

}
 ?></div>
 <div style="text-align:center;">
 <?php //店舗
 echo "<table border='1' align='center'><tr>";
echo "<th>集計店舗</th>";
	$sql = " SELECT name FROM members WHERE shop_id = ".$shop_id;
	$res = mysqli_query($db,$sql);
	$data_arr = mysqli_fetch_assoc($res);
	echo "<th>".$data_arr['name']."</th>";

	 echo "</tr>";
 ?>
 
 <?php //集計対象
 echo "<tr>";
	 echo "<th>集計対象</th>";
	 if($day_or_mon == 0){
		 echo "<th>日別</th>";
	 }else{
		 echo "<th>月別</th>";
	 }
	 echo "</tr>";
 ?>
 
 <?php //商品コード
 echo "<tr>";
	 echo "<th>商品コード</th>";
	 if($goods_id==""){
			echo "<th>指定無し</th>";
		}else{
			echo "<th>$goods_id</th>";
		}
		echo "</tr>";
	 
 ?>
 
 <?php //商品名
 	echo "<tr>";
	 echo "<th>商品名</th>";
	 if($goods_name == ""){
		 echo "<th>指定無し</th>";
	 }else{
		 echo "<th>$goods_name</th>";
	 }
	 	echo "</tr>";
 ?>
 
 <?php //大カテゴリー
 echo "<tr>";
	 echo "<th>大カテゴリ</th>";
	 if($cat_big == 0){
		 echo "<th>指定無し</th>";
	 }else{
		 $sql = " SELECT category_big_str FROM goods WHERE category_big = $cat_big ";
		 $res = mysqli_query($db,$sql);
	$data_arr = mysqli_fetch_assoc($res);
	echo "<th>".$data_arr['category_big_str']."</th>";

	 }
	 echo "</tr>";
 ?>
 
 <?php //中カテゴリー
 echo "<tr>";
	  echo "<th>中カテゴリ</th>";
	 if($cat_mid == 0){
		 echo "<th>指定無し</th>";
	 }else{
		 $sql = " SELECT category_mid_str FROM goods WHERE category_mid = $cat_mid ";
		 $res = mysqli_query($db,$sql);
	$data_arr = mysqli_fetch_assoc($res);
	echo "<th>".$data_arr['category_mid_str']."</th>";

	 }
	 echo "</tr>";
	 echo "</table>";
 ?>
 </div>
<div id="jqPlot-sample" style="height: 100%; width: 100%;"><?php return; ?></div>
<div id ="test"></div>
</body>
</html>