<?php 
	/*
		作成者	：油谷
		昨日	：ゲストテーブルを参照し、CSV出力	
	*/
	session_start();
	include("db_connect.php"); 
	$str="コード,姓,名,セイ,メイ,郵便番号前,郵便番号後,都道府県,市区町村,番地等,建物名,電話番号1,電話番号2,メールアドレス,誕生年,誕生月,誕生日,メール,DM\n";
	//result_ary : １つ目に行表示HTML
	//			 : ２つめに該当会員数
	//			 : ３つめにインデックスボタン
	
 	
 	//sort_info : 0 登録が新しい順
	//			: 1 利用回数が多い順
	//			: 2 購入数の多い順
	//			: 3 購入金額の多い順

 	$sort_info=mysqli_real_escape_string($db,$_GET['sort_info']);
 	if($sort_info=="0"){
	 	$sort_str="created DESC";
 	}else if($sort_info=="1"){
	 	$sort_str="use_times DESC";
 	}else if($sort_info=="2"){
 		$sort_str="quantity DESC";
 	}else if($sort_info=="3"){
	 	$sort_str="price DESC";
 	}

 	//key_ctgr : 0 選択無し
	//		   : 1 会員名
	//         : 2 会員名（ふりがな）
	//         : 3 会員住所//今のところ保留
	//         : 4 会員電話番号
	//         : 5 メールアドレス
	//         : 6 会員番号
 	$key_ctgr=mysqli_real_escape_string($db,$_GET['key_ctgr']);
 	if($key_ctgr == "0"){
 		$ctgr = "1";
 	}else if($key_ctgr == "1"){
	 	$ctgr = "fullname_kanji like";
 	}else if($key_ctgr == "2"){
	 	$ctgr = "fullname_kana like";
 	}else if($key_ctgr == "3"){
	 	$ctgr = "1";
 	}else if($key_ctgr == "4"){
	 	$ctgr = "tel1 like ";
 	}else if($key_ctgr == "5"){
 		$ctgr = "mailaddress like ";
 	}else if($key_ctgr == "6"){
 		$ctgr = " concat(sub_id,'-',id) like ";
 	}
 	$key_ctgr_word='"%';
 	$key_ctgr_word.=mysqli_real_escape_string($db,$_GET['key_ctgr_word']);
 	$key_ctgr_word.='%"';
 	if($key_ctgr == "0" || $key_ctgr == "3"){
	 	$ctgr_result = $ctgr;
 	}else{
 		$ctgr_result = $ctgr . $key_ctgr_word;	
 	}
 	//key_member_check : 0 指定無し
	//				   : 1 会員
	//                 : 2 退会
 	$key_member_check=mysqli_real_escape_string($db,$_GET['key_member_check']);
 	if($key_member_check == "0"){
	 	$key_member_check_str="1";
 	}else if($key_member_check == "1"){
	 	$key_member_check_str='member_check = "0"';
 	}else if($key_member_check == "2"){
	 	$key_member_check_str='member_check = "1"';
 	}
 	//key_dm	: 0 指定無し
	//			: 1 配信する
	//			: 2 配信しない
 	$key_dm=mysqli_real_escape_string($db,$_GET['key_dm']);
 	if($key_dm =="0"){
 		$key_dm_str="1";
 	}else if($key_dm =="1"){
	 	$key_dm_str='dm = "1"';
 	}else if($key_dm =="2"){
	 	$key_dm_str='dm = "0"';
 	}

 	if(mysqli_real_escape_string($db,$_GET['key_shop_name']) == 0){
 		$use_shop = 1;
 	}else if(mysqli_real_escape_string($db,$_GET['key_useshop'])==0){
 		$use_shop ="shop_id=".mysqli_real_escape_string($db,$_GET['key_shop_name']);
 	}else if(mysqli_real_escape_string($db,$_GET['key_useshop'])==1){
 		$use_shop ="last_shop=".mysqli_real_escape_string($db,$_GET['key_shop_name']);
 	}

 	if(mysqli_real_escape_string($db,$_GET['use_day']) == 0){
 		$use_day = "created";
 	}else{
 		$use_day ="last";
 	}

 	$key_year_s=mysqli_real_escape_string($db,$_GET['key_year_s']);
 	$key_month_s=mysqli_real_escape_string($db,$_GET['key_month_s']);
 	$key_day_s=mysqli_real_escape_string($db,$_GET['key_day_s']);
 	$key_year_e=mysqli_real_escape_string($db,$_GET['key_year_e']);
 	$key_month_e=mysqli_real_escape_string($db,$_GET['key_month_e']);
 	$key_day_e=mysqli_real_escape_string($db,$_GET['key_day_e']);
 	$key_day_e=$key_day_e + 1;
 	$key_year_use_s=mysqli_real_escape_string($db,$_GET['key_year_use_s']);
 	$key_month_use_s=mysqli_real_escape_string($db,$_GET['key_month_use_s']);
 	$key_day_use_s=mysqli_real_escape_string($db,$_GET['key_day_use_s']);
 	$key_year_use_e=mysqli_real_escape_string($db,$_GET['key_year_use_e']);

 	if($key_year_use_e == 0){
	 	$key_year_use_e = date("Y");
 	}

 	$key_month_use_e=mysqli_real_escape_string($db,$_GET['key_month_use_e']);
 	if($key_month_use_e == 0){
	 	$key_month_use_e = date("m");
 	}

 	$key_day_use_e=mysqli_real_escape_string($db,$_GET['key_day_use_e']);
 	if($key_day_use_e == 0){
	 	$key_day_use_e = date("d");
 	}

 	$key_day_use_e = $key_day_use_e  + 1;
	
	
	$sql = "SELECT * ";
	$sql .= "FROM guest ";
	$sql .= 'WHERE '.$use_shop .' and ' .$ctgr_result.' and '.$key_member_check_str.' and '.$key_dm_str ;
	$sql .= ' and updating >= "'.$key_year_s.'-'.$key_month_s.'-'.$key_day_s.'" and updating <= "'.$key_year_e.'-'.$key_month_e.'-'.$key_day_e.'" and '.$use_day.' >=  "'.$key_year_use_s.'-'.$key_month_use_s.'-'.$key_day_use_s.'" and '.$use_day.' <= "'.$key_year_use_e.'-'.$key_month_use_e.'-'.$key_day_use_e.'"';
	$sql .= " order by $sort_str ";

	$rs = mysqli_query($db,$sql) or exit($sql);
	if(mysqli_num_rows($rs) == 0){
		echo "対象データがありません";
		return;
	}

		while($arr_item = mysqli_fetch_assoc($rs)){
			$id=$arr_item['id'];
			if($arr_item['sub_id'] != ""){
				$id = $arr_item['sub_id']."-".$id;
			}
			$name1 = $arr_item['GuestName_kanji'];
			$name2 = $arr_item['guestname_kanji_first'];
			$name = $name1 . $name2; 
			$name_katakana = $arr_item['GuestName_Katakana'] . $arr_item['guestname_katakana_first'];
			$pref = $arr_item['prefecture'];
			$pos1 = $arr_item['postal_num1'];
			$pos2 = $arr_item['postal_num2'];
			$city = $arr_item['city'];
			$address = $arr_item['address'];
			$building = $arr_item['building'];
			$tel1 = $arr_item['tel1'];
			$tel2 = $arr_item['tel2'];
			$mailaddress = $arr_item['mailaddress'];
			$birth_y = $arr_item['birth_y'];
			$birth_m = $arr_item['birth_m'];
			$birth_d = $arr_item['birth_d'];
			$mail_mag = $arr_item['mail_mag'];
			$dm = $arr_item['dm'];
			
			
						
		$str .= "'$id',$name1,$name2,".$arr_item['GuestName_Katakana'].",".$arr_item['guestname_katakana_first'].",'$pos1','$pos2',$pref,$city,$address,$building,'$tel1','$tel2','$mailaddress','$birth_y','$birth_m','$birth_d',$mail_mag,$dm\n";
			}
			
		header("Content-Type: application/octet-stream;charset=sjis-win");
		header("Content-Disposition: attachment; filename=customer.csv");
		print(mb_convert_encoding($str,"SJIS-win","UTF-8"));
		return;

/*コード	姓	名	セイ	メイ	郵便番号前	郵便番号後	都道府県	市区町村	番地等	建物名	電話番号1	電話番号2	メールアドレス	誕生年	誕生月	誕生日	メール	DM*/
?>