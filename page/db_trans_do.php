<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text"; charset="UTF-8">
<title>顧客情報入力</title> 
<link rel="stylesheet" href="../css/style.css">
<script src="jquery-2.1.1.js"></script>
<script src="./ajaxzip3.js" charset="UTF-8"></script>
<link type="text/css" rel="stylesheet"
  href="http://code.jquery.com/ui/1.10.3/themes/cupertino/jquery-ui.min.css" />
<script type="text/javascript"
  src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript">
$(function() {
  $('#acc').accordion({
    collapsible: true ,
    active: 100 ,
    heightStyle: 'content'
  });
});

</script>
</head>
<body>
<?php session_start(); ?>
顧客情報DB移植が完了しました<br>
<a href="./db_trans.php" >戻る</a><br>
<?php
/*
$shop_id = $_SESSION['shop_id'];
$db_name = $_SESSION['db_name'];
$db_id=$_SESSION['db_id'];
$db_pass = $_SESSION['db_pass'];
$series = $_SESSION['series'];
$db = mysqli_connect($db_name,$db_id,$db_pass,$series) or die(mysqli_connect_error());
mysqli_set_charset($db, 'utf8');
*/

$db_name = "localhost";
$db_id = "admin";
$db_pass = "";
$series = "test_error2";
$db = mysqli_connect($db_name,$db_id,$db_pass,$series) or die(mysqli_connect_error());
mysqli_set_charset($db,'utf8');
//現状owner1に繋いでいるが、本番はSHPSEQでオーナーごとにふるい分ける
//ownerとSHPSEQの相関関係を調べる必要あり
$sql = " SELECT * FROM mstCUSTOMER WHERE 1 ";
$rs = mysqli_query($db,$sql) or die($sql);
while($arr = mysqli_fetch_assoc($rs)){
	//GuestName_kanji
	$guestname_kanji = mb_convert_encoding($arr['CSTNM1'],"UTF-8","auto");
	if($guestname_kanji == ""){
		$guestname_kanji = "姓不明";
	}
	//guestname_kanji_first
	$guestname_kanji_first = mb_convert_encoding($arr['CSTNM2'],"UTF-8","auto");
	if($guestname_kanji_first == ""){
		$guestname_kanji_first == "名不明";
	}
	//GuestName_Katakana
	$guestname_katakana = mb_convert_encoding($arr['CSTKN1'],"UTF-8","auto");
	if($guestname_katakana == ""){
		$guestname_katakana = "セイフメイ";
	}
	//guestname_katakana_first
	$guestname_katakana_first = mb_convert_encoding($arr['CSTKN2'],"UTF-8","auto");
	//id
	//sub_id
	//CSTCODE無い場合あり
	//CSTCOEが数字と文字が入り交じっている可能性あり
	//CSTCODEがハイフン入力の可能性あり
	$id="";
	$sub_id="";
	$haihun_flag = 0;
	$temp = mb_convert_encoding($arr['CSTCODE'],"UTF-8","auto");
	if($temp == ""){//空なら
		$id=$arr['CSTSEQ'];
		$sub_id = "";
	}else{//空でなければ
		$temp_str = mb_convert_kana($temp, 'a');//全角のものを全て半角に変換
		for($i=0;$i < strlen($temp_str);$i++){
			if($temp_str[$i] == "-"){
				$haihun_flag = 1;
			}
		}
		if($haihun_flag == 1){//途中にハイフンが入っている場合
			$str_arr = split("-",$temp_str);
			if(count($str_arr) >= 3){
				$id=$arr['CSTSEQ'];
				$sub_id = "";
			}else{
				if(is_Numeric($str_arr[0]) && is_Numeric($str_arr[1])){
					$id = $str_arr[1];
					$sub_id = $str_arr[0];
				}else if(is_Numeric($str_arr[0]) && !is_numeric($str_arr[1])){
					$id = $str_arr[0];
					$sub_id = $str_arr[1];
				}else if(is_Numeric($str_arr[1]) && !is_numeric($str_arr[0])){
					$id = $str_arr[1];
					$sub_id = $str_arr[0];
				}else{
					$id=$arr['CSTSEQ'];
					$sub_id = "";
				}
			}
		}else{//ハイフンが入っていない場合
		for($i=0;$i < strlen($temp_str);$i++){
			if(is_Numeric($temp_str[$i])){//数字ならidにいれる
				$id .= $temp_str[$i];
			}else if($temp_str[$i] != "-" && $temp_str[$i] != "."){//数字以外はサブに入れる
				$sub_id .= $temp[$i];
			}
		}
		}
	}
	
	//postal_num1
	if(!is_numeric(mb_convert_encoding($arr['CSTZIP1'],"UTF-8","auto"))){
		$postal_num1 = 0;
	}else{
		$postal_num1 = mb_convert_encoding($arr['CSTZIP1'],"UTF-8","auto");
		}
	//postalnum2
	if(!is_numeric(mb_convert_encoding($arr['CSTZIP2'],"UTF-8","auto"))){
		$postal_num2 = 0;
	}else{
		$postal_num2 = mb_convert_encoding($arr['CSTZIP2'],"UTF-8","auto");
	}
	//prefecture
	$prefecture = mb_convert_encoding($arr['CSTPREF'],"UTF-8","auto");
	//city
	$city = mb_convert_encoding($arr['CSTCITY'],"UTF-8","auto");
	//address
	$address = mb_convert_encoding($arr['CSTTOWN'],"UTF-8","auto");
	//building
	$building = mb_convert_encoding($arr['CSTBUIL'],"UTF-8","auto");
	//電話番号はハイフンを省いて入力する
	//tel1
	$temp = "";
	$tel1 = "";
	$temp = mb_convert_encoding($arr['CSTTEL1'],"UTF-8","auto");
	if($temp != ""){
		for($i=0;$i < strlen($temp);$i++){
				if(is_Numeric($temp[$i])){
					$tel1 .= $temp[$i];
					}
			}
			}
	//tel2
	$temp = "";
	$tel2 = "";
	$temp = mb_convert_encoding($arr['CSTTEL2'],"UTF-8","auto");
	if($temp != ""){
		for($i=0;$i < strlen($temp);$i++){
				if(is_Numeric($temp[$i])){
					$tel2 .= $temp[$i];
				}
			}
			}

	//fax
	$temp = "";
	$fax = "";
	$temp = mb_convert_encoding($arr['CSTFAX'],"UTF-8","auto");
	if($temp != ""){
		for($i=0;$i < strlen($temp);$i++){
				if(is_Numeric($temp[$i])){
					$fax .= $temp[$i];
					}
			}
			}
	//mailaddress
	$mailaddress = mb_convert_encoding($arr['CSTMAIL'],"UTF-8","auto");
	//sex
	//sexはバイナリで入っているので、バイナリをtextに変換したものを用意する
	$sex = 2;
	//birth_y
	$birth_y_temp = $arr['CSTBYEAR'];
	if($birth_y_temp == 0){
		$birth_y = 1900;
	}else{
		$birth_y = $birth_y_temp; 
	}
	//birth_m
	$birth_m_temp = $arr['CSTBMONTH'];
	if($birth_m_temp == 0){
		$birth_m = 1;
	}else{
		$birth_m = $birth_m_temp; 
	}
	//birth_d
	$birth_d_temp = $arr['CSTBDAY'];
	if($birth_d_temp == 0){
		$birth_d = 1;
	}else{
		$birth_d = $birth_d_temp; 
	}
	//mail_mag
	$mail_mag = $arr['F_CSTMAIL'];
	//dm
	$dm = $arr['F_CSTDM'];
	//motive
	$motive = 0;
	$motive_temp="";
	$motive_temp = mb_convert_encoding($arr['CSTTOUCH'],"UTF-8","auto");
	
	if($motive_temp == ""){
		//不明
		$motive = 0;
	}else if($motive_temp == "TC"){
		//立寄
		$motive = 1;
	}else if($motive_temp == "IV"){
		//紹介
		$motive = 2;
	}else if($motive_temp == "DM"){
		//DM
		$motive = 3;
	}else if($motive_temp == "HP"){
		//HP,blog
		$motive = 4;
	}else if($motive_temp == "AD"){
		//広告
		$motive = 5;
	}else if($motive_temp == "FP"){
		//Fペーパー
		$motive = 6;
	}else if($motive_temp == "CR"){
		//ちらし
		$motive = 7;
	}else if($motive_temp == "CH"){
		//キャッチ
		$motive = 8;
	}else if($motive_temp == "OT"){
		//その他
		$motive = 9;
	}
		
	//Tickets
	$tickets = $arr['CSTPOINT'];
	//level
	$level = $arr['CSTLEVEL'];
	if($level == 0){
		$level = 0;
	}else if($level == 1){
		$level = 0;
	}else if($level == 2){
		$level = 1;
	}else if($level == 3){
		$level = 2;
	}else if($level == 4){
		$level = 3;
	}else if($level == 5){
		$level = 4;
	}
	//shop_id
	$shop_id = $arr['SHPSEQ'];
	//remarks
	//$bikou = $arr['blob_text'];
	$bikou = mb_convert_encoding($arr['CSTNOTE'],"UTF-8","auto");
	//member_check
	$member_check = $arr['F_CSTDEL'];
	//member_check_time
	$member_check_time = $arr['CSTDELTDATE'];
	//last
	$last = $arr['CSTLASTDATE'];
	//last_shop
	$last_shop = $arr['SHPSEQ'];
	//created
	$created = $arr['CSTPOSTDATE'];
	//updating
	$updating = $arr['CSTEDITDATE'];
	if($updating == '0000-00-00 00:00:00'){
		$updating = $arr['CSTPOSTDATE'];
	}
	//use_times
	$use_times = $arr['CSTTIMES'];
	//quantity
	$quantity = $arr['CSTQTTY'];
	//price
	$price = $arr['CSTPRICE'];
	//use_ticket
	$use_ticket = $arr['CSTUSEPOINT'];
	//fullname_kanji
	$fullname_kanji = mb_convert_encoding($arr['CSTNM1'],"UTF-8","auto")."　".mb_convert_encoding($arr['CSTNM2'],"UTF-8","auto");
	//fullname_kana
	$fullname_kana = mb_convert_encoding($arr['CSTKN1'],"UTF-8","auto")."　".mb_convert_encoding($arr['CSTKN2'],"UTF-8","auto");
	
	$db = mysqli_connect($db_name,$db_id,$db_pass,'owner1') or die(mysqli_connet_error());
	mysqli_set_charset($db,'utf8');
	$sql_sub = "INSERT INTO guest(GuestName_kanji,guestname_kanji_first,GuestName_Katakana,guestname_katakana_first,
				id,sub_id,postal_num1,postal_num2,prefecture,city,address,building,tel1,tel2,fax,mailaddress,sex,birth_y,
				birth_m,birth_d,mail_mag,dm,motive,Tickets,level,shop_id,remarks,member_check,member_check_time,
				last,last_shop,created,updating,use_times,quantity,price,use_ticket,fullname_kanji,fullname_kana) 
				VALUE ('$guestname_kanji','$guestname_kanji_first','$guestname_katakana','$guestname_katakana_first','$id','$sub_id','$postal_num1','$postal_num2','$prefecture','$city','$address','$building','$tel1','$tel2','$fax','$mailaddress','$sex','$birth_y',
				'$birth_m','$birth_d','$mail_mag','$dm','$motive','$tickets','$level','$shop_id','$bikou','$member_check','$member_check_time',
				'$last','$last_shop','$created','$updating','$use_times','$quantity','$price','$use_ticket','$fullname_kanji','$fullname_kana') ";
	$rs_sub = mysqli_query($db,$sql_sub) or die($sql_sub."クエリ失敗");
	}
?>

</body>
</html>