<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
<?php
	include("db_connect.php");
?>
<script>
function goods_search(num){
	//day_or_mon
	//0 日別
	//1 月別
	var day_or_mon = $('*[name="day_or_mon"]:checked').val();
	//goods_id
	//商品コード
	var goods_id = $('#goods_id').val();
	//goods_name
	//商品名
	var goods_name = $('#goods_name').val();
	//cat_*
	//カテゴリ
	var cat_big = $("#cat_big").val();
	var cat_mid = $("#cat_mid").val();
	var cat_sma = $("#cat_sma").val();
	//店舗
	var useshop = $('#useshop').val();
	//月日	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var day_s = $('#day_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	var day_e = $('#day_e').val();
	
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({
		type:"POST",
		url:"goods_reg_search.php",
		data:{
			num:num,
			day_or_mon:day_or_mon,
			goods_id:goods_id,
			goods_name:goods_name,
			cat_big:cat_big,
			cat_mid:cat_mid,
			cat_sma:cat_sma,
			useshop:useshop,
			year_s:year_s,
			month_s:month_s,
			day_s:day_s,
			year_e:year_e,
			month_e:month_e,
			day_e:day_e			
		},
		dataType:"json",
		success:function(data){
						$('#table').append(data[0]);
						$('#span').append(data[1]);
						},
				error:	function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}

function change_select_big(){
	var select_big_val = $("#cat_big").val();
	$.ajax({
		type:"POST",
		url:"search_cat_big.php",
		data:{
			select_big_val:select_big_val
		},
		success:function(data){
				$("#cat_mid").html(data);
				},
		error:function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}

function change_select_mid(){
	var select_big_val = $("#cat_big").val();
	var select_mid_val = $("#cat_mid").val();
	$.ajax({
		type:"POST",
		url:"search_cat_mid.php",
		data:{
			select_big_val:select_big_val,
			select_mid_val:select_mid_val
		},
		success:function(data){
				$("#cat_sma").html(data);
				},
		error:function(XMLHttpRequest, textStatus, errorThrown){
						alert('error : ' + errorThrown + textStatus + XMLHttpRequest);
						}
	});
	
}

</script>
<script>
function search_itemreport_csv(num){
	//day_or_mon
	//0 日別
	//1 月別
	var day_or_mon = $('*[name="day_or_mon"]:checked').val();
	//goods_id
	//商品コード
	var goods_id = $('#goods_id').val();
	//goods_name
	//商品名
	var goods_name = $('#goods_name').val();
	//cat_*
	//カテゴリ
	var cat_big = $("#cat_big").val();
	var cat_mid = $("#cat_mid").val();
	var cat_sma = $("#cat_sma").val();
	//店舗
	var useshop = $('#useshop').val();
	//月日	
	var year_s = $('#year_s').val();
	var month_s = $('#month_s').val();
	var day_s = $('#day_s').val();
	var year_e = $('#year_e').val();
	var month_e = $('#month_e').val();
	var day_e = $('#day_e').val();
	
window.open("./search_itemreport_csv.php?num="+num+"&day_or_mon="+day_or_mon+"&goods_id="+goods_id+"&goods_name="+goods_name+"&cat_big="+cat_big+"&cat_mid="+cat_mid+"&cat_sma="+cat_sma+"&year_s="+year_s+"&month_s="+month_s+"&day_s="+day_s+"&year_e="+year_e+"&month_e="+month_e+"&day_e="+day_e+"&useshop="+useshop);	
}
</script>
<div id="pagebodymain">
<h1>商品別の集計</h1>
<p>
	<table>
		<tr>
			<th colspan="4" class="b">検索条件</th>
		<tr>
			<th class="a">店舗</th>
			<th>
					<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
						include("db_connect.php");
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?></th>
			<th class="a">集計対象</th>
			<th style="text-align:center;">
			<input type="radio" name="day_or_mon" value="1">
			月別
			</input>
			<input type="radio" name="day_or_mon" checked value="0">
			日別
			</input>
			</th>
		<tr>
			<th class="a">商品コード</th>
			<th style="text-align:center;"><input type="text" id="goods_id"></th>
			<th class="a">商品名</th>
			<th style="text-align:center;"><input type="text" id="goods_name"></th>
		<tr>
			<th class="a" rowspan="3">カテゴリー</th>
			<th colspan="3">大カテゴリー
			<select id="cat_big" onchange="change_select_big();">
					<option value="0">大カテゴリを選択してください</option>
				<?php
					$goods_shopid="";
					$sql_shopid =' SELECT shop_id FROM members WHERE shop_id>0 ';
					$rs_shopid=mysqli_query($db,$sql_shopid);
						while($mem=mysqli_fetch_assoc($rs_shopid)){
								$goods_shopid.=' shop_id = '.$mem['shop_id'].' OR ';
								}
					$sql_make_big_cat = " SELECT category_big_str , category_big FROM goods ";
					$sql_make_big_cat .= " WHERE ($goods_shopid shop_id=0 ) ";
					$sql_make_big_cat .= " GROUP BY category_big_str ORDER BY category_big ";
					$rs_make_big_cat = mysqli_query($db,$sql_make_big_cat) or exit("クエリ失敗");
					while($arr_make_big_cat = mysqli_fetch_assoc($rs_make_big_cat)){
						echo "<option value='".$arr_make_big_cat['category_big']."'>".$arr_make_big_cat['category_big_str']."</option>";
					}
				?>
			</select>
			</th>
		<tr>
			<th colspan="3">中カテゴリー
				<select id="cat_mid" onchange="change_select_mid();">
					<option value="0">中カテゴリを選択してください</option>
				</select>
			</th>
		<tr>
			<th colspan="3">小カテゴリー
				<select id="cat_sma">
				<option value="0">小カテゴリを選択してください</option>
				</select>
			</th>
		<tr>
			<th class="a">集計期間</th>
			<th colspan="3" style="text-align:center;">
			<select id="year_s">
			<?php
				$year_s=date("Y");
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_s">
			<?php
				$month_s=date("m");
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月
			<select id="day_s">
				<?php
				$day_s=date("d");
				for($i=1;$i<=31;$i++){
				if($i==1){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
				?>
			</select>
			日　〜　
			<select id="year_e">
			<?php
				for($i=2000;$i<=$year_s;$i++){
				if($i==$year_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>年
			<select id="month_e">
			<?php
				for($i=1;$i<=12;$i++){
				if($i==$month_s){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
			?>
			</select>
			月
			<select id="day_e">
				<?php
				$day_s=date("d");
				for($i=1;$i<=31;$i++){
				if($i==31){
					echo "<option value='$i' selected>$i</option>";	
				}else{
					echo "<option value='$i'>$i</option>";
				}
				}
				?>
			</select>
			日

			</th>
		<tr>
			<th colspan="4" style="text-align:center;">
				<input type="image" src="../css/image/contents/search_reset.gif" onclick="location.reload();" alt="条件をリセット">
				<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索" onclick=<?php
				echo '"goods_search(1);"';
				?>>
			</th>
	</table>
	
<!-- <p style="text-align: right;">検索条件を閉じる</p> -->
<p>
<a href="#" onclick="search_itemreport_csv(1);"><img src="../css/image/contents/csv_btn.gif"></a></br>

<small>集計結果の一部を表示します。全ての集計結果を確認する場合は「CSVデータ」をダウンロードして下さい。</small>
<p></p>

<table id="table">
		<?php
		
		echo "
				<tr class='a' name='add'>
				<th style='text-align:center; width:17%;'>年月日</th>
				<th style='text-align:center; width:8%;'>曜日</th>
				<th style='text-align:center; width:12%;'>商品ｺｰﾄﾞ</th>
				<th style='text-align:center;'>商品名</th>
				<th style='text-align:center; width:8%;'>点数</th>
				<th style='text-align:center; width:12%;'>売上金額</th>
				</tr>";
		
		$sql_search = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date, ";
		$sql_search .= " DATE_FORMAT(day,'%Y年%m月%d日') AS date_j, ";
		$sql_search .= " DATE_FORMAT(day,'%a') AS week ";
		$sql_search .= " FROM shop_log WHERE ";
		$sql_search .= " day >= '".date("Y")."-".date("m")."-01' ";
		$sql_search .= " AND day <= '".date("Y")."-".date("m")."-31' "; 
		$sql_search .= " GROUP BY date ";
		$sql_search .= " ORDER BY date DESC LIMIT 0,5 ";
		$query_search = mysqli_query($db,$sql_search);
		if(mysqli_num_rows($query_search) == 0){
			echo "<tr name='add'>
					<th colspan = '6' style='text-align:center;'>対象データがありません</th>
				  </tr>";
		}else{
		
		$odd = 0;
		while($arr_search = mysqli_fetch_assoc($query_search)){
			$date = $arr_search['date'];
			$date_j = $arr_search['date_j'];
			$week = $arr_search['week'];
			switch($week){
				case 'Mon':
					$week_str = "月";
					break;
				case 'Tue':
					$week_str = "火";
					break;
				case 'Wed':
					$week_str = "水";
					break;
				case 'Thu':
					$week_str = "木";
					break;
				case 'Fri':
					$week_str = "金";
					break;
				case 'Sat':
					$week_str = "土";
					break;
				case 'Sun':
					$week_str ="日";
					break;
			}
			$sql_show = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date, ";
			$sql_show .= " goods_id, goods_name, SUM(goods_num) AS num, ";
			$sql_show .= " SUM(price) AS price ";
			$sql_show .= " FROM shop_log ";
			$sql_show .= " WHERE day >= '$date 00:00:00' AND ";
			$sql_show .= " day <= '$date 23:59:59' ";
			$sql_show .= " GROUP BY goods_name ORDER BY goods_id ASC ";
			$query_show = mysqli_query($db,$sql_show) or exit("クエリ失敗");
			$row = mysqli_num_rows($query_show);
			if($row == 0){
			echo "<tr name='add'>
					<th colspan = '6'>対象データがありません</th>
				  </tr>";
		    }else{
		    $count = 0;
			while($arr_show = mysqli_fetch_assoc($query_show)){	
			if($count==0){
			if($odd % 2 == 0){

				echo "<tr name='add' class='table-even'>
				<th rowspan='$row'>$date_j</th>
				<th rowspan='$row'>$week_str</th>
				<th>".$arr_show['goods_id']."</th>
				<th>".$arr_show['goods_name']."</th>
				<th>".$arr_show['num']."</th>
				<th>".number_format($arr_show['price'])."</th>
			  </tr>";
			  }else{
				 echo "<tr name='add' class='table-odd'>
				<th rowspan='$row'>$date_j</th>
				<th rowspan='$row'>$week_str</th>
				<th>".$arr_show['goods_id']."</th>
				<th>".$arr_show['goods_name']."</th>
				<th>".$arr_show['num']."</th>
				<th>".number_format($arr_show['price'])."</th>
			  </tr>"; 
			  }
			  }else{
			  if($odd % 2 == 0){
			  echo "<tr name='add' class = 'table-even'>
			    <th >".$arr_show['goods_id']."</th>
				<th>".$arr_show['goods_name']."</th>
				<th>".$arr_show['num']."</th>
				<th>".number_format($arr_show['price'])."</th>
			  </tr>";		
			  }else{
				  echo "<tr name='add' class = 'table-odd'>
			    <th >".$arr_show['goods_id']."</th>
				<th>".$arr_show['goods_name']."</th>
				<th>".$arr_show['num']."</th>
				<th>".number_format($arr_show['price'])."</th>
			  </tr>";
			  }
			  }
			  $count++;
		}
		}
		$odd++;
		}	
	}
	?>
</table>
<div style="height:30px;"></div>
<?php
		$sql_search = " SELECT DATE_FORMAT(day,'%Y-%m-%d') AS date ";
		$sql_search .= " FROM shop_log WHERE ";
		$sql_search .= " day >= '".date("Y")."-".date("m")."-01' ";
		$sql_search .= " AND day <= '".date("Y")."-".date("m")."-31' "; 
		$sql_search .= " GROUP BY date ";
		$query_search = mysqli_query($db,$sql_search) or exit("クエリ失敗");
		$num_rows = mysqli_num_rows($query_search);
?>
<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($num_rows -1 )/ 5) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="goods_search('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>
</div>
<?php include("footer.php"); ?>
