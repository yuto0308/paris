<?php
session_start();

//セッション情報を削除
$_SESSION = array();
if(ini_get("session.use_cookies")){
	$params = session_get_cookie_params();
	setcookie(session_name(), '', time() - 42000,
		$params['path'], $params['domain'], $params['secure'], $params['httponly']);
}

session_destroy();

//cookie情報を削除
setcookie('id', '', time()-3600);
setcookie('pass', '', time()-3600);
setcookie('name', '', time()-3600);
setcookie('ownercheck', '', time()-3600);
setcookie('db_name', '', time()-3600);
setcookie('db_id', '', time()-3600);
setcookie('db_pass', '', time()-3600);
setcookie('series', '', time()-3600);
setcookie('shop_id', '', time()-3600);


header('location: login.php');
exit();
?>