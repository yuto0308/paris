<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
	<?php
		include("db_connect.php");
		$sql = 'SELECT *,DATE_FORMAT(pos_finish.start,"%Y年 %m月 %d日<br>%H:%i:%s") as starttime,DATE_FORMAT(datetime,"%Y-%m-%d") as datet,members.name as shop_name,DATE_FORMAT(datetime,"%Y年 %m月 %d日<br>%H:%i:%s") as finishtime FROM pos_finish,members WHERE pos_finish.shop_id=members.shop_id AND members.shop_id>0 ORDER BY datet DESC';
		$sql_count = $sql;
		$rs_count = mysqli_query($db, $sql_count);
		$count = mysqli_num_rows($rs_count);
		$sql.=' LIMIT 0,10 ';
		$recordsets=mysqli_query($db,$sql);
	?>

<script>
function search_reg_finish(num){
	var shop = $("#useshop").val();
	var staffname =$("#staffname").val();
	var year_s =$("#year1").val();
	var month_s =$("#month1").val();
	var day_s =$("#day1").val();
	var year_e =$("#year2").val();
	var month_e =$("#month2").val();
	var day_e =$("#day2").val();
	$("tr[name='add']").remove();
	$("input[name='add_button']").remove();
	$.ajax({type:"POST",
 			url:"search_posfinish.php",
 				data:{	num:num,
 						shop:shop,
 						staffname:staffname,
 						year_s:year_s,
 						month_s:month_s,
 						day_s:day_s,
 						year_e:year_e,
 						month_e:month_e,
 						day_e:day_e
 					},
 					dataType:"json",
					success:function(data){
						 $('#table').append(data[0]);
						 $('#num_finish').text(data[1]);
						 $('#span').append(data[2]);
					},
					error:function(XMLHttpRequest, textStatus, errorthrown){
						alert('error : ' + errorthrown + XMLHttpRequest.status + textStatus);
						}
						});
}

</script>

<div id="pagebodymain">
<p>
<h1>締め処理の確認</h1>
<p>
	<table>
		<tr>
			<th class="b" colspan="4">検索条件</th>
		</tr>
		<tr>
			<th class="a">店舗</th>
			<th>
					<select id="useshop">
					<option value="0" name="shop" size="1">全店舗</option>
						<?php 
						include("db_connect.php");
						$sql = sprintf("SELECT name,shop_id FROM members WHERE shop_id>0 ORDER BY shop_id");
						$recordSet = mysqli_query($db, $sql);
						while($arr_item = mysqli_fetch_assoc($recordSet))
						{
						echo "<option value='".$arr_item['shop_id']."'> ".$arr_item['name'] ."</option><br>";
						}
						?></th>
			<th class="a">担当者名</th>
			<th>
				<input type="text" name="staffname" id="staffname" size="30" /></th>
					</tr>
		<tr>
			<th class="a" >設定日</th>
			<th colspan="3">
				<select id="year1">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month1">
				<?php 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>"><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day1">
				<?php 
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>"><?php echo $d;?></option>
			<?php endfor?>
			</select>日 ～
			<select id="year2">
				<?php 
				$now = date("Y");
				for($y = 2000; $y <= $now; $y++):?>
				<option value="<?php echo $y;?>"<?php if($y==$now){echo 'selected="selected"';}?>><?php echo $y;?></option>
			<?php endfor ?>
			</select>年
			<select id="month2">
				<?php
				$now_month = date("m"); 
				for($m = 1; $m <= 12; $m++):?>
				<option value="<?php echo $m;?>" <?php if($m==$now_month){echo 'selected="selected"';}?>><?php echo $m;?></option>
			<?php endfor ?>
			</select>月
			<select id="day2">
				<?php 
				$now_day = date("j");
				for($d = 1; $d <= 31; $d++):?>
				<option value="<?php echo $d;?>" <?php if($d==$now_day){echo 'selected="selected"';}?>><?php echo $d;?></option>
			<?php endfor?>
			</select>日
		</th>
	</tr>
		<tr>
			<th colspan="4" style="text-align:center;">
		<input type="image" src="../css/image/contents/search_reset.gif" alt="条件をリセットする" onclick="location.reload();">
		<input type="image" src="../css/image/contents/search.gif" alt="この条件で検索する" onclick="search_reg_finish(1);"></th>
	</tr>
	</table>
</p>
<p style="text-align:right;">
	設定数： <span id="num_finish"><?php echo $count; ?></span>件</p>
<p>
	<table id="table">
		<tr>
			<th class="a" style="text-align:center; width:18%;">対象日</th>
			<th class="a" style="text-align:center; width:18%;">処理日</th>
			<th class="a" style="text-align:center; width:18%;">店舗</th>
			<th class="a" style="text-align:center; width:12%;">売上金額</th>
			<th class="a" style="text-align:center; width:12%;">理論レジ金</th>
			<th class="a" style="text-align:center; width:12%;">実レジ金</th>
			<th class="a" style="text-align:center; width:10%;">差額</th>
		</tr>
			<?php
			if(mysqli_num_rows($recordsets)==0){
				echo '<tr name="add">
					<th colspan="7" style="text-align:center;">対象データがありません';
			}
			while ($table = mysqli_fetch_assoc($recordsets)){
			?>
		<tr name="add">
		<th style="text-align:center;"><a href="posfinish_comp.php?end=<?php echo $table['datetime']; ?>"><?php echo $table['starttime'] ; ?></a></th>
		<th style="text-align:center;"><?php echo $table['finishtime']; ?></th>
		<th style="text-align:center;"><?php print(htmlspecialchars($table['abb_name'])); ?></th>
		<th style="text-align:right;"><?php print(htmlspecialchars(number_format($table['cash_reg']))); ?> 円</th>
		<th style="text-align:right;"><?php print(htmlspecialchars(number_format($table['theory_reg']))); ?> 円</th>
		<th style="text-align:right;"><?php print(htmlspecialchars(number_format($table['actually']))); ?> 円</th>
		<th style="text-align:right;"><?php print(htmlspecialchars(number_format($table['difference']))); ?> 円</th>
		</tr>
		<?php
		}
		?>
	</table>
	<div id="span" align="center">
<?php
$str_button="";
$button_num = floor(($count -1 )/ 10) + 1;
if($button_num <= 12){
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
				$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_reg_finish('.$i.')">';
			}else{
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_finish('.$i.')">';
			}
			}
			echo $str_button;
}else{
	$dot_count = 0;
	for($i = 1;$i<=$button_num;$i++){
			if($i == 1){
			$str_button .= '<input type="button" style="color:blue;font-size:large;" name="add_button" value="'.$i.'" onclick="search_reg_finish('.$i.')">';
			}else if($i < 12){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_finish('.$i.')">';
			}else if($i >= 12 && $i != $button_num && $dot_count == 0){
				$str_button .= '<input type="button" name="add_button" disabled="disabled" value="…">';
				$dot_count = 1;
			}else if($i == $button_num){
				$str_button .= '<input type="button" name="add_button" value="'.$i.'" onclick="search_reg_finish('.$i.')">';
			}
			}
			echo $str_button;
}
			?>
</div>
	<p>※「対象日」をクリックすると詳細を確認できます。
</div>
<?php include("footer.php"); ?>
